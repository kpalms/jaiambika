﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;

namespace Restaurant.PL
{
    public partial class ChangePrice : Form
    {
        BL_Field bl_field = new BL_Field();
        BL_Sale bl_sale = new BL_Sale();
        main _mainForm;
        //decimal quty = 0;
        public ChangePrice(string price, string saleid, main mainfrm)
        {
            _mainForm = mainfrm;
            InitializeComponent();
            txtprice.Text = price;
            bl_field.SaleId = saleid;




            this.KeyPreview = true;


        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ChgPrice();
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }


        }
        private void ChgPrice()
        {
            if (txtprice.Text == "" || txtprice.Text=="0")
            {
                MessageBox.Show("Please enter valid Price?");
                txtprice.Focus();
            }
            else{
            bl_field.PriceS = txtprice.Text;
            bl_sale.ChangePrice(bl_field);
            this.Hide();
            _mainForm.PageRefreshEvent();
            }
        }
        private void txtprice_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            //{
            //    e.Handled = true;

            //}
            Common common = new Common();
            common.OneDecimalPoint(txtprice, e);
        }

        private void ChangePrice_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ChangePrice_Load(object sender, EventArgs e)
        {

        }

        private void txtprice_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ChgPrice();
            }
        }

        private void ChangePrice_Load_1(object sender, EventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using Restaurant.PL.Reports;
using System.Collections;

namespace Restaurant.PL
{
    public partial class Shifting : Form
    {
        BL_Shifting bl_shifting = new BL_Shifting();
        BL_Field bl_field = new BL_Field();
        BL_Worker bl_worker = new BL_Worker();
        BL_Merge bl_merge = new BL_Merge();
        main main;
        public Shifting(main _main)
        {
            InitializeComponent();
            main = _main;
        }

        private void Shifting_Load(object sender, EventArgs e)
        {
            SelectTable();
            BindTABLE();
        }

        private void SelectTable()
        {
            DataTable dt = bl_merge.SelectTableName();
            DataRow row = dt.NewRow();
            row["TableName"] = "Select";
            dt.Rows.InsertAt(row, 0);
            cbofrom.DataSource = dt;
            cbofrom.DisplayMember = "TableName";
        }

        private void BindTABLE()
        {
            try
            {
                DataTable dt = bl_worker.SelectTable();
                cboto.DataSource = dt;
                cboto.DisplayMember = "TableNo";
                cboto.ValueMember = "TblId";

            }
            catch (Exception ex)
            {
                MessageBox.Show("erro" + ex);
            }


        }

        private void BindTableFrom()
        {
            try
            {
                DataTable dt = bl_worker.SelectTable();
                cbofrom.DataSource = dt;
                cbofrom.DisplayMember = "TableNo";
                cbofrom.ValueMember = "TblId";


            }
            catch (Exception ex)
            {
                MessageBox.Show("erro" + ex);
            }


        }

        private void btnok_Click(object sender, EventArgs e)
        {
            if (cbofrom.Text == "Select")
            {
                MessageBox.Show("Please select From Table");
            }
            else if (cboto.Text == "Select")
            {
                MessageBox.Show("Please select To Table");
            }
            else
            {
                SetFill();
                bl_shifting.UpdateShifting(bl_merge);
                main.BindTable();
                main.BindItenName();
                MessageBox.Show("Shift Successfully");
                this.Close();

            }
        }
        private void SetFill()
        {
            string s = cbofrom.Text;
            string[] words = s.Split(' ');
            bl_merge.TableNo = Convert.ToInt32(words[0]);
            bl_merge.SeatFrom = words[1];

            bl_merge.TableNoNew = Convert.ToInt32(cboto.Text);
            if (cboseat.Text == "")
            {
                bl_merge.Seat = "A";
            }
            else
            {
                bl_merge.Seat = cboseat.Text;
            }


        }
        private void btncancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void Shifting_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void cboseat_KeyPress(object sender, KeyPressEventArgs e)
        {
            Common common = new Common();
            common.ComboBoxCheckValue(cboseat, e);


        }

        private void cboseat_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnok.Focus();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;


namespace Restaurant.PL
{
    public partial class ItemAccess : Form
    {

        BL_TreeView bl_treeview = new BL_TreeView();
        BL_Field bl_field = new BL_Field();
        Common common = new Common();


        public ItemAccess()
        {
            InitializeComponent();
        }

        private void ItemAccess_Load(object sender, EventArgs e)
        {
            BindCatagory();
            btndelete.Enabled = false;
            btnUpdate.Enabled = false;
            
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            try
            {
                
               if(ddlcatagory.Text=="SELECT")
                {
                    MessageBox.Show("Please Select Category");
                    ddlcatagory.Focus();
                }
                else if (txtitem.Text == "")
                {
                    MessageBox.Show("Please enter Name?");
                    txtitem.Focus();
                }
                
                else
                {
                    Insert();
                }

            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration");
            }
 

        }


        private void Insert()
        {
   
                SetField();
                bl_treeview.InsertTreeViewItem(bl_field);

                MessageBox.Show("Record inserted Successfully");
                string catid = ddlcatagory.SelectedValue.ToString();

                BindGrid(catid);
                txtitem.Focus();
                clear();
       
       
        }

        private void SetField()
        {
            bl_field.MenuId = Convert.ToInt32(ddlcatagory.SelectedValue.ToString());
            bl_field.ItemName = txtitem.Text;
        }
        private void BindCatagory()
        {
            DataTable dt = bl_treeview.SelectTreeViewMenu(bl_field);
            DataRow row = dt.NewRow();
            row["MenuName"] = "SELECT";
            dt.Rows.InsertAt(row, 0);
            ddlcatagory.DataSource = dt;
            ddlcatagory.ValueMember = "MenuId";
            ddlcatagory.DisplayMember = "MenuName";
        }
        private void BindGrid(string catid)
        {
            DataTable dt = bl_treeview.SelectTreeViewItem(catid);
            dataGridView1.DataSource = dt;
            dataGridView1.Columns["ItemId"].Visible = false;
            dataGridView1.Columns["MenuId"].Visible = false;
            dataGridView1.Columns["Status"].Visible = false;

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            bl_field.ItemId = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            txtitem.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
            btnUpdate.Enabled = true;
            btndelete.Enabled = true;
            btnadd.Enabled = false;
        }

        private void clear()
        {
            txtitem.Text = "";
            txtitem.Focus();
            btnUpdate.Enabled = false;
            btndelete.Enabled = false;

        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure to delete selected Record?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_field.ItemId = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                    bl_treeview.DeleteTreeViewItem(bl_field);
                    txtitem.Text = "";
                    MessageBox.Show("Record Deleteed Successfully");
                    string catid = ddlcatagory.SelectedValue.ToString();

                    BindGrid(catid);
                    clear();
                    txtitem.Focus();
                    btnUpdate.Enabled = false;
                    btndelete.Enabled = false;
                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
        }

        private void ddlcatagory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlcatagory.Text != "System.Data.DataRowView")
            {
                if (ddlcatagory.ValueMember == "MenuId")
                {
                    string catid = ddlcatagory.SelectedValue.ToString();
                    BindGrid(catid);
                }
            }
        }

        private void ItemAccess_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
           
            BindCatagory();
            txtitem.Text = "";
            btndelete.Enabled = false;
            btnadd.Enabled = true;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
           
                if (ddlcatagory.Text == "SELECT")
                {
                    MessageBox.Show("Please Select Record");
                    ddlcatagory.Focus();
                }
                SetField();
                bl_treeview.UpdateTreeViewItem(bl_field);
                clear();
                MessageBox.Show("Record Updated Successfully");
                string catid = ddlcatagory.SelectedValue.ToString();

                BindGrid(catid);
                clear();
                btnUpdate.Enabled = false;
                btndelete.Enabled = false;

        }





    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using Restaurant.PL.Reports;


namespace Restaurant.PL
{
    public partial class Report2DateSelect : Form
    {
        public string frm = "";
        public static String dt1 = "", dt2 = "";
        BL_Sale bl_sale = new BL_Sale();
        BL_Field bl_field = new BL_Field();
        BL_Worker bl_worker = new BL_Worker();
        public Report2DateSelect(string fm)
        {
            InitializeComponent();

            frm = fm;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            DateTime dd1 = Convert.ToDateTime(dtpFBill.SelectionStart.ToLongDateString());
            DateTime dd2 = Convert.ToDateTime(dtpTBill.SelectionStart.ToLongDateString());

            bl_field.FromDate = dd1.ToString("dd/MM/yyyy");
            bl_field.Todate = dd2.ToString("dd/MM/yyyy");


            if (frm == "PeriodWise")
            {
                this.Close();
                ReportsViewer reportviewer = new ReportsViewer("PeriodWise");
                reportviewer.Show();
            }
            if (frm == "PeriodWiseBar")
            {
                this.Close();
                ReportsViewer reportviewer = new ReportsViewer("PeriodWiseBar");
                reportviewer.Show();
            }

            if (frm == "ParsalReports")
            {
                this.Close();
                ReportsViewer reportviewer = new ReportsViewer("ParsalReports");
                reportviewer.Show();
                
            }

            if (frm == "salebyTable")
            {
                this.Close();
                ReportsViewer reportviewer = new ReportsViewer("salebyTable");
                reportviewer.Show();
            }
            if (frm == "salebyBillwiseViewItem")
            {
                this.Close();
                ReportsViewer reportviewer = new ReportsViewer("salebyBillwiseViewItem");
                reportviewer.Show();
            }
            if (frm == "CancelItem")
            {
                this.Close();
                ReportsViewer reportviewer = new ReportsViewer("CancelItem");
                reportviewer.Show();
            }
            if (frm == "ItemPriceList")
            {
                this.Close();
                ReportsViewer reportviewer = new ReportsViewer("ItemPriceList");
                reportviewer.Show();
            }

            if (frm == "salebyitem")
            {
                this.Close();
                ReportsViewer reportviewer = new ReportsViewer("salebyitem");
                reportviewer.Show();
            }
            if (frm == "salebyitemBar")
            {
                this.Close();
                ReportsViewer reportviewer = new ReportsViewer("salebyitemBar");
                reportviewer.Show();
            }

            if (frm == "PurchasingExp")
            {
                this.Close();
                ReportsViewer reportviewer = new ReportsViewer("PurchasingExp");
                reportviewer.Show();
            }
            if (frm == "DisCountReports")
            {
                this.Close();
                ReportsViewer reportviewer = new ReportsViewer("DisCountReports");
                reportviewer.Show();
            }

            if (frm == "CashierWise")
            {
                this.Close();
                bl_field.CashierNameStatic = lstcashier.SelectedValue.ToString();
                ReportsViewer reportviewer = new ReportsViewer("CashierWise");
                reportviewer.Show();
            }

            if (frm == "KotWise")
            {
                this.Close();
                ReportsViewer reportviewer = new ReportsViewer("KotWise");
                reportviewer.Show();
            }
            if (frm == "KotWiseItem")
            {
                this.Close();
                ReportsViewer reportviewer = new ReportsViewer("KotWiseItem");
                reportviewer.Show();
            }

            if (frm == "BillSummary")
            {
                this.Close();
                ReportsViewer reportviewer = new ReportsViewer("BillSummary");
                reportviewer.ShowDialog();
            }
            if (frm == "TotalSale")
            {
                this.Close();
                ReportsViewer reportviewer = new ReportsViewer("TotalSale");
                reportviewer.ShowDialog();
            }
            if (frm == "PurchasingReport")
            {
                this.Close();
                ReportsViewer reportviewer = new ReportsViewer("PurchasingReport");
                reportviewer.ShowDialog();
            }
            if (frm == "WastageReport")
            {
                this.Close();
                ReportsViewer reportviewer = new ReportsViewer("WastageReport");
                reportviewer.ShowDialog();
            }
            if (frm == "BillDetailsReport")
            {
                this.Close();
                ReportsViewer reportviewer = new ReportsViewer("BillDetailsReport");
                reportviewer.ShowDialog();
            }
            if (frm == "CounterReport")
            {
                this.Close();
                ReportsViewer reportviewer = new ReportsViewer("CounterReport");
                reportviewer.ShowDialog();
            }
            if (frm == "CancelBill")
            {
                this.Close();
                ReportsViewer reportviewer = new ReportsViewer("CancelBill");
                reportviewer.ShowDialog();
            }
            if (frm == "CustomerHistory")
            {
                this.Close();
                ReportsViewer reportviewer = new ReportsViewer("CustomerHistory");
                reportviewer.ShowDialog();
            }
            
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Report2DateSelect_Load(object sender, EventArgs e)
        {
            lstcashier.Visible = false;
            if (frm == "PeriodWise")
            {
                lblheader.Text = "Sale Period Wise";
            }
            else if (frm == "ParsalReports")
            {
                lblheader.Text = "ParsalReports";
            }
            else if (frm == "today")
            {
                lblheader.Text = "today";
            }
            else if (frm == "yesturday")
            {
                lblheader.Text = "yesturday";
            }
            else if (frm == "salebyTable")
            {
                lblheader.Text = "salebyTable";
            }
            else if (frm == "salebyBillwiseViewItem")
            {
                lblheader.Text = "salebyBillwiseViewItem";
            }
            else if (frm == "ItemPriceList")
            {
                lblheader.Text = "ItemPriceList";
            }

            else if (frm == "salebyitem")
            {
                lblheader.Text = "salebyitem";
            }
            else if (frm == "salebyitemBar")
            {
                lblheader.Text = "salebyitemBar";
            }
            else if (frm == "PurchasingExp")
            {
                lblheader.Text = "Purchasing/Expencess Reports";
            }
            else if (frm == "DisCountReports")
            {
                lblheader.Text = "DisCountReports";
            }

            else if (frm == "CashierWise")
            {
                lstcashier.Visible = true;
                BindCashierName();
                lblheader.Text = "CashierWise Reports";
            }
            else if (frm == "KotWise")
            {
               lblheader.Text = "KotWise Reports";
            }
            else if (frm == "KotWiseItem")
            {
                lblheader.Text = "KotWise Item Reports";
            }


            else if (frm == "BillSummary")
            {
                lblheader.Text = "Restaurant Bill Summary Reports";
            }
            else if (frm == "TotalSale")
            {
                lblheader.Text = "TotalSale Reports";
            }
            else if (frm == "StockEntryReport")
            {
                lblheader.Text = "StockEntry Reports";
            }
            else if (frm == "WastageReport")
            {
                lblheader.Text = "Wastage Report";
            }
            else if (frm == "BillDetailsReport")
            {
                lblheader.Text = "BillDetails Report";
            }
            else if (frm == "CounterReport")
            {
                lblheader.Text = "Counter Report ";
            }
            else if (frm == "CancelBill")
            {
                lblheader.Text = "CancelBill Report";
            }
            else if (frm == "CustomerHistory")
            {
                lblheader.Text = "CustomerHistory Report";
            }
        }

        private void BindCashierName()
        {
            DataTable dt = bl_worker.SelectCashier();
            if (dt.Rows.Count > 0)
            {
                lstcashier.DataSource = dt;
                lstcashier.ValueMember = "Username";
                lstcashier.DisplayMember = "Username";
            }
            else { 
            
            }
        }

    }
}

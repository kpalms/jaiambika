﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Restaurant.BL;
//using System.Data;
using System.Data.SqlClient;
using Restaurant.DL;
namespace Restaurant.PL
{
    public partial class SalaryCalculator : Form
    {
        public SalaryCalculator()
        {
            InitializeComponent();
        }
        BL_Worker bl_worker = new BL_Worker();
        int id;
     //   string paid;
     //public  decimal ondaysal, halfdaysal;



        private void SalaryCalculator_Load(object sender, EventArgs e)
        {
            BindWorkers();

            //string date = bl_changeofday.SelectDate();
            //DTP.Text = date;
            
           
        }
        private void BindWorkers()
        {
            DataTable dt = bl_worker.SelectWorker();
            DataRow row = dt.NewRow();
            row["Name"] = "Select";
            dt.Rows.InsertAt(row, 0);
            comboworkars.DataSource = dt;
            comboworkars.DisplayMember = "Name";
            comboworkars.ValueMember = "WorkerId";

        }
        private void Setfill()
        {
            bl_worker.Date = DTP.Text;
            bl_worker.WorkerId = Convert.ToInt32(comboworkars.SelectedValue.ToString());
            bl_worker.PresentDays = Convert.ToInt32(txtpresentdays.Text);
            bl_worker.AbsentDays = Convert.ToInt32(txtabsentdays.Text);
            bl_worker.Halfdays =Convert.ToInt32(txthalfdays.Text);
            bl_worker.Extra = Convert.ToDecimal(txtovertime.Text);
            bl_worker.Deduction = Convert.ToDecimal(txtdeduction.Text);
            bl_worker.Debit = Convert.ToDecimal(txtpaid.Text);
            bl_worker.Remark = Rtxtremarks.Text;
        }

        private void Clear()
        {
            
            //string date = bl_changeofday.SelectDate();
            //DTP.Text = date;

            txtdeduction.Text = "";
            Rtxtremarks.Text = "";
            txtovertime.Text = "";
            txthalfdays.Text = "";
            txtabsentdays.Text = "";
            txtpresentdays.Text = "";
           
            txtpaid.Text = "";
            comboworkars.Text = "Select";
        
           
            //txtsalary.Text = "";

            lblapsentdays.Text = "Apsent Day";
            lblhalfdays.Text = "Half Days";
            lblpresentdays.Text = "Present Days";
            
           
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                if (comboworkars.Text == "SELECT")
                {
                    MessageBox.Show("Please Select Worker Name");
                    comboworkars.Focus();
                }
                else
                {
                    Setfill();
                    bl_worker.InsertSalaryCalculator(bl_worker);
                    Clear();
                   // BindSalaryCalculater();
                }
              }
              catch
              {

                  MessageBox.Show("error occured!Please contact Administration?");
              }
        }

        private void comboworkars_SelectedIndexChanged(object sender, EventArgs e)
        {

             try
            {
            if (comboworkars.Text != "System.Data.DataRowView")
            {
                if (comboworkars.ValueMember == "WorkerId")
                {

                    if (comboworkars.Text != "Select")
                    {
                        bl_worker.WorkerId = Convert.ToInt32(comboworkars.SelectedValue.ToString());


                        DataTable dtgrid = bl_worker.SelectSalaryCalculater(bl_worker);
                        dgvsalarycalculater.DataSource = dtgrid;
                        dgvsalarycalculater.Columns["WorkerId"].Visible = false;
                        dgvsalarycalculater.Columns["SalaryCalsId"].Visible = false;


                        DataTable dt = bl_worker.Selectworkerdetails(bl_worker);
                        if (dt.Rows.Count > 0)
                        {
                            txtsalary.Text = dt.Rows[0]["Salary"].ToString();
                            lblpresentdays.Text = dt.Rows[0]["PresentDay"].ToString();
                            lblapsentdays.Text = dt.Rows[0]["AbsentDay"].ToString();
                            lblhalfdays.Text = dt.Rows[0]["HalfDay"].ToString();
                        }

                    }

                }
            }
            }
             catch
             {

                 MessageBox.Show("error occured!Please contact Administration?");
             }

        }

        private void txtpresentdays_TextChanged(object sender, EventArgs e)
        {
         
            CalculateSalary();
        }


        private void CalculateSalary()
        {
            if (txtpresentdays.Text != "")
            {

                decimal d = Convert.ToDecimal(txtsalary.Text) / Convert.ToDecimal(30) * Convert.ToDecimal(txtpresentdays.Text);
                txtpaid.Text = Convert.ToString(decimal.Round(d, 2));
            }

            if (txtabsentdays.Text != "")
            {

                decimal oneday = Convert.ToDecimal(txtsalary.Text) / Convert.ToDecimal(30) ;
                decimal totalday = oneday * Convert.ToDecimal(txtabsentdays.Text);
                decimal amount = Convert.ToDecimal(txtpaid.Text) - totalday;
                txtpaid.Text = Convert.ToString(decimal.Round(amount, 2));
            }

            if (txthalfdays.Text != "")
            {

                decimal oneday = Convert.ToDecimal(txtsalary.Text) / Convert.ToDecimal(30);
                decimal halpday = oneday /2;
                decimal totalhalpday = halpday * Convert.ToDecimal(txthalfdays.Text);
                decimal halpamount = Convert.ToDecimal(txtpaid.Text) - totalhalpday;
                txtpaid.Text = Convert.ToString(decimal.Round(halpamount, 2));
            }
            if (txtovertime.Text != "")
            {
                txtpaid.Text = Convert.ToString(Convert.ToDecimal(txtpaid.Text) + Convert.ToDecimal(txtovertime.Text));
            }
            if (txtdeduction.Text != "")
            {
                txtpaid.Text = Convert.ToString(Convert.ToDecimal(txtpaid.Text) - Convert.ToDecimal(txtdeduction.Text));
            }

           
        }

       
        private void txthalfdays_TextChanged(object sender, EventArgs e)
        {
           // CalculateSalary2();
            CalculateSalary();
        }

        private void txtovertime_TextChanged(object sender, EventArgs e)
        {
           // CalculateSalary3();
            CalculateSalary();

           
        }

        private void txtdeduction_TextChanged(object sender, EventArgs e)
        {
           // CalculateSalary4();
            CalculateSalary();
        }

        private void DTP_ValueChanged(object sender, EventArgs e)
        {

        }


        private void dgvsalarycalculater_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvsalarycalculater.Rows.Count > 0)
            {
                id =Convert.ToInt32(dgvsalarycalculater.SelectedRows[0].Cells[0].Value.ToString());
                DTP.Text = dgvsalarycalculater.SelectedRows[0].Cells[1].Value.ToString();
                txtpresentdays.Text = dgvsalarycalculater.SelectedRows[0].Cells[3].Value.ToString();
                txtabsentdays.Text = dgvsalarycalculater.SelectedRows[0].Cells[4].Value.ToString();
                txthalfdays.Text = dgvsalarycalculater.SelectedRows[0].Cells[5].Value.ToString();
                txtovertime.Text = dgvsalarycalculater.SelectedRows[0].Cells[6].Value.ToString();
                txtdeduction.Text = dgvsalarycalculater.SelectedRows[0].Cells[7].Value.ToString();
                txtpaid.Text = dgvsalarycalculater.SelectedRows[0].Cells[8].Value.ToString();
                Rtxtremarks.Text = dgvsalarycalculater.SelectedRows[0].Cells[10].Value.ToString();

            }
        }

        private void btnupdate_Click(object sender, EventArgs e)
        {
            try
            {
                Setfill();
                bl_worker.SalaryCalsId = id;
                bl_worker.UpdateSalaryCalculator(bl_worker);
                Clear();
               // BindSalaryCalculater();
            }
            catch
            {
                MessageBox.Show("error occured!Please contact Administration?");

            }
        }

        private void txtsalary_TextChanged(object sender, EventArgs e)
        {
            txtpaid.Text = txtsalary.Text;
        }

        private void txtsalary_KeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = true;
        }

        private void txtsalary_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void txtpresentdays_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            {
                e.Handled = true;
                MessageBox.Show("Please enter number only");
            }
        }

        private void txtabsentdays_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            {
                e.Handled = true;
                MessageBox.Show("Please enter number only");
            }
        }

        private void txthalfdays_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            {
                e.Handled = true;
                MessageBox.Show("Please enter number only");
            }
        }

        private void txtovertime_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            //{
            //    e.Handled = true;
            //    MessageBox.Show("Please enter number only");
            //}
            Common common = new Common();
            common.OneDecimalPoint(txtovertime, e);
        }

        private void txtdeduction_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            //{
            //    e.Handled = true;
            //    MessageBox.Show("Please enter number only");
            //}
            Common common = new Common();
            common.OneDecimalPoint(txtdeduction, e);
        }

        private void txtpaid_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            //{
            //    e.Handled = true;
            //    MessageBox.Show("Please enter number only");
            //}
            Common common = new Common();
            common.OneDecimalPoint(txtpaid, e);
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {

                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Catagory?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_worker.AttendanceId = id;
                    bl_worker.DeleteSalaryCalculator(bl_worker);
                    Clear();
                  //   BindSalaryCalculater();
                   
                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
        }

        private void txtabsentdays_TextChanged(object sender, EventArgs e)
        {
            CalculateSalary();
        }


    }
}

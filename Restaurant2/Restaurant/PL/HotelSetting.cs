﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using Restaurant.BL;

namespace Restaurant.PL
{
    public partial class HotelSetting : Form
    {

        BL_Field bl_field = new BL_Field();
        BL_HotelSetting bl_hotelSetting = new BL_HotelSetting();

        public HotelSetting()
        {
            InitializeComponent();
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                if (btnsave.Text == "Save")
                {
                    if (txtname.Text != "")
                    {
                        SetField();
                        bl_hotelSetting.InsertHotelSetting(bl_field);
                        txtname.Text = "";
                        MessageBox.Show("Record inserted Successfully");
                        BindHotelSetting();

                    }
                    else
                    {
                        MessageBox.Show("Please enter Name?");
                    }
                }
                if (btnsave.Text == "Update")
                {
                    SetField();
                    bl_hotelSetting.UpdateHotelSetting(bl_field);

                    MessageBox.Show("Record Updated Successfully");
                    BindHotelSetting();
                }
            }
            catch
            {
                MessageBox.Show("error occured!Please contact Administration?");

            }
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure to delete selected Record ?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_hotelSetting.DeleteHotelSetting(bl_field);
                    txtname.Text = "";
                    MessageBox.Show("Record Delete Successfully");
                    BindHotelSetting();

                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
        }

        private void btnclose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SetField()
        {
            bl_field.Name = txtname.Text;
            bl_field.Address = txtaddress.Text;
            bl_field.No = txtno.Text;

        }
        private void Clear()
        {
            txtname.Text = "";
            txtaddress.Text = "";
            txtno.Text = "";
        }

        private void BindHotelSetting()
        {
            DataTable dt = bl_hotelSetting.SelectHotelSetting();
            if (dt.Rows.Count > 0)
            {
                bl_field.HotelId = dt.Rows[0][0].ToString();
                txtname.Text = dt.Rows[0][1].ToString();
                txtaddress.Text = dt.Rows[0][2].ToString();
                txtno.Text = dt.Rows[0][3].ToString();
                btnsave.Text = "Update";
            }
            else
            {
                btnsave.Text = "Save";
                Clear();
            }

        }

        private void txtno_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            {
                e.Handled = true;

            }
        }

        private void HotelSetting_Load(object sender, EventArgs e)
        {
            BindHotelSetting();
        }

        private void HotelSetting_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Restaurant.BL;
using System.Data.SqlClient;

using System.Collections;



namespace Restaurant.PL
{
    public partial class Security : Form
    {
        BL_Security bl_security = new BL_Security();
        BL_Sale bl_sale = new BL_Sale();
        BL_Field bl_field = new BL_Field();
        BL_Worker bl_worker = new BL_Worker();
        //main _main;

        CheckBox chkbox = new CheckBox();

        public Security()
        {
            InitializeComponent();
           // _main = man;
        }


        private void Security_Load(object sender, EventArgs e)
        {
            BindSale();
        }
        private void rdooff_CheckedChanged(object sender, EventArgs e)
        {
            BindSale();
        }

        private void lnkdeletePur_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                  DialogResult msgg = MessageBox.Show("Are you sure you want to delete All Records?", "Delete", MessageBoxButtons.YesNo);
                  if (msgg == DialogResult.Yes)
                  {
                      bl_security.TruncateData();
                     // _main.PageRefreshEvent();
                      MessageBox.Show("Records Deleted Successfully");
                  }
            }
            catch 
            {

                MessageBox.Show("error occured!Please contact Administration?");             
 
            }
           
        }

        private void lnkdeletesale_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to delete All Records?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_security.TruncateDataSale();
                    MessageBox.Show("Records Deleted Successfully");
                    BindSale();

                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");

            }
        }

        private void BindSale()
        {
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.Columns.Clear();
            DataSet ds = bl_sale.GetAllSale(bl_field);
            dataGridView1.DataSource = ds.Tables[0];
            decimal tot = 0;
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                tot += Convert.ToDecimal(dr[3].ToString());
            }
            lbltotal.Text = Convert.ToString(tot);
            dataGridView1.Columns["OrderId"].Visible = false;
    
        }

        private void chkBoxChange(object sender, EventArgs e)
        {
            for (int k = 0; k <= dataGridView1.RowCount - 1; k++)
            {
                this.dataGridView1[0, k].Value = this.chkbox.Checked;
            }
            this.dataGridView1.EndEdit();
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count > 0)
            {
                try
                {


                    //List<int> ChkedRow = new List<int>();
                    //List<int> receiptno = new List<int>();

                   // DataRow dr;
                    DialogResult msgg = MessageBox.Show("Are you sure you want to Delete selected Item?", "Delete", MessageBoxButtons.YesNo);
                    if (msgg == DialogResult.Yes)
                    {
                        bl_field.OrderId = dataGridView1.SelectedRows[0].Cells[5].Value.ToString();
                        bl_sale.DeleteSaleLast(bl_field);
                        BindSale();
                       // _main.PageRefreshEvent();

                    }
                }
                catch
                {

                    MessageBox.Show("error occured!Please contact Administration?");
                }
            }
            else
            {
                MessageBox.Show("Please select Item first?");
            }
        }

        private void btnclose_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        //private void btnok_Click(object sender, EventArgs e)
        //{
        //    bl_field.Password = txtpassword.Text;
        //    SqlDataReader dr = bl_worker.CheckSecurity(bl_field);
        //    if (dr.Read())
        //    {
        //        string role = dr[0].ToString();
        //        if (role == "Admin")
        //        {
        //           // lnkalert.Visible = true;
        //            pnlpassword.Visible = false;
        //        }
        //        else {
                   
        //            //lnkalert.Visible = false;
        //            pnlpassword.Visible = true;
        //            MessageBox.Show("Please check password?");
        //        }
        //    }
        //    else
        //    {
        //        MessageBox.Show("Please check password?");
        //    }
        //}

        private void rdoon_CheckedChanged(object sender, EventArgs e)
        {
           // pnlpassword.Visible = true;
        }

        private void rdoresta_CheckedChanged(object sender, EventArgs e)
        {
            BindSale();
        }

        private void rdobar_CheckedChanged(object sender, EventArgs e)
        {
            BindSale();
        }

        private void Security_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Restaurant.BL;
using Restaurant.DL;

namespace Restaurant.PL
{
    public partial class AddTable : Form
    {
        BL_Field bl_field = new BL_Field();
        BL_Table bl_Table = new BL_Table();
        Common common = new Common();

        public AddTable()
        {
            InitializeComponent();
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                if (btnsave.Text == "Save")
                {
                    if (txttableno.Text != "")
                    {
                        bl_field.TableNo = txttableno.Text;
                            SetField();
                            bl_Table.InsertTable(bl_field);
                            MessageBox.Show("Record inserted Successfully");
                            BindGrid();
                            Clear();
                            txttableno.Focus();
                       }
                    else
                    {
                        MessageBox.Show("Please enter Name?");
                    }
                }
                
            }
            catch
            {

            }
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            txttableno.Text = "";
            txtlocation.Text = "";
            btnUpdate.Enabled = false;
            btndelete.Enabled = false;
            btnsave.Enabled = true;
           
        }
        private void SetField()
        {
           bl_field.TableNo= txttableno.Text;
           bl_field.Location= txtlocation.Text;
        }
        private void Clear()
        {
            txttableno.Text = "";
            txtlocation.Text = "";
            
        }
        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Table?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_field.TblId = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                    bl_Table.RemoveTable(bl_field);
                    MessageBox.Show("Record Deleted Successfully");
                    Clear();
                    BindGrid();
                    btndelete.Enabled = false;
                    btnUpdate.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void BindGrid()
        {
            DataTable dt = bl_Table.SelectTable();
            dataGridView1.DataSource = dt;
            dataGridView1.Columns["TblId"].Visible = false;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                bl_field.TblId = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                txttableno.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
                txtlocation.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
                btndelete.Enabled = true;
                btnUpdate.Enabled = true;
                btnsave.Enabled = false;
            }
            catch 
            {
                
               
            }

        }

        private void AddTable_Load(object sender, EventArgs e)
        {
            BindGrid();
            btndelete.Enabled = false;
            btnUpdate.Enabled = false;
        }

        private void dataGridView1_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                bl_field.TblId = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                txttableno.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
                txtlocation.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
                btnUpdate.Enabled = true;
                btndelete.Enabled = true;
            }
            catch
            {


            }

        }

        private void AddTable_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
                SetField();
                bl_Table.UpdateTable(bl_field);
                MessageBox.Show("Record Updated Successfully");
                Clear();
                BindGrid();
                txttableno.Focus();
                btndelete.Enabled = false;
                btnUpdate.Enabled = false;
        }
    }
}

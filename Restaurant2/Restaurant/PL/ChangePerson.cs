﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;


namespace Restaurant.PL
{
    public partial class ChangePerson : Form
    {
        BL_Sale bl_sale = new BL_Sale();
        BL_Field bl_field = new BL_Field();
        main _main;
        public ChangePerson(string tableno,string person,main main)
        {
            _main = main;
            InitializeComponent();
            bl_field.TableNoS = tableno;
            bl_field.Person = person;
            txtchagep.Text = person;
            this.KeyPreview = true;
        }

        private void btnchage_Click(object sender, EventArgs e)
        {
            Change();

        }
        private void Change()
        {
            try
            {
                if (txtchagep.Text == "" || txtchagep.Text == "0")
                {
                    MessageBox.Show("Please Enter Valid No. of Person?");
                    txtchagep.Focus();
                }
                else
                {
                    bl_field.Person = txtchagep.Text;
                    bl_sale.UpdatePerson(bl_field);
                    this.Hide();
                    _main.PageRefreshEvent();
                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
        }
        private void txtchagep_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            {
                e.Handled = true;
            }
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void ChangePerson_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
            if (e.KeyCode == Keys.Enter)
            {
                Change();
            }
        }

        private void ChangePerson_Load(object sender, EventArgs e)
        {

        }
    }
}

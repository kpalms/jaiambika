﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;



using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;

namespace Restaurant.PL
{
    public partial class CreateUser : Form
    {
        BL_Worker bl_worker = new BL_Worker();
        BL_Field bl_field = new BL_Field();

        public CreateUser()
        {
            InitializeComponent();
        }

        private void btncreate_Click(object sender, EventArgs e)
        {
            if (txtusername.Text == "")
            {
                MessageBox.Show("Please enter Username");
            }
            else if (txtpassword.Text == "")
            {
                MessageBox.Show("Please enter Password");
            }
            else
            {
                bl_field.Username = txtusername.Text;
                DataTable dt = bl_worker.CheckUsername(bl_field );
                if (dt.Rows.Count > 0)
                {
                    MessageBox.Show("Username already exits?please try again?");
                }
                else
                {
                   
                    bl_field.Username = txtusername.Text;
                    bl_field.Password = txtpassword.Text;
                    bl_field.Name = txtusername.Text;
                    bl_field.Role = "User";
                    bl_worker.InsertWorker(bl_field);
                    MessageBox.Show("Records Added successfully.Please login");
                    txtusername.Text = "";
                    txtpassword.Text = "";
                }
            }
        }

        private void CreateUser_Load(object sender, EventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Restaurant.BL;
using System.Data.SqlClient;
using Restaurant.PL;

namespace Restaurant.PL
{
    public partial class PasswordChangePrice : Form
    {
        BL_Security bl_security = new BL_Security();
        BL_Sale bl_sale = new BL_Sale();
        BL_Field bl_field = new BL_Field();
        BL_Worker bl_worker = new BL_Worker();
        //string formname;
        main _mainForm;
        string prices = "";
        string salesid = "";
        public PasswordChangePrice(string price, string saleid, main mainfrm)
        {
            InitializeComponent();
            _mainForm = mainfrm;
            prices = price;
            salesid = saleid;


        }

        private void PasswordChangePrice_Load(object sender, EventArgs e)
        {
 
        }


        private void CheckPassword()
        {
            bl_field.Password = txtpassword.Text;
            SqlDataReader dr = bl_worker.CheckSecurity(bl_field);
            if (dr.Read())
            {
                string role = dr[0].ToString();
                if (role == "Admin")
                {
                    this.Close();
                    ChangePrice al = new ChangePrice(prices, salesid, _mainForm);
                    al.ShowDialog();
                  


                }
                else
                {
                    MessageBox.Show("Please check password?");
                }
            }
            else
            {
                MessageBox.Show("Please check password?");
            }
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            CheckPassword();
        }

        private void txtpassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (txtpassword.Text != "")
            {
                if (e.KeyCode == Keys.Enter)
                {
                    CheckPassword();
                }
            }
        }
    }
}

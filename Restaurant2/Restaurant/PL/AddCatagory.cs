﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using Restaurant.BL;
using Restaurant.DL;
namespace Restaurant.PL
{
    public partial class AddCatagory : Form
    {
        BL_Field bl_field = new BL_Field();
        BL_Catagory bl_catagory = new BL_Catagory();
        //main _main;
        public AddCatagory()
        {
           // _main = main;
            InitializeComponent();
            this.KeyPreview = true;
        }

       

        private void btnadd_Click(object sender, EventArgs e)
        {
            
            

                try
                {
                        
                         if (txtname.Text == "")
                        {
                            MessageBox.Show("Please enter Name?");
                        }
                         else if (ddltype.Text == "SELECT")
                        {
                            MessageBox.Show("Please Slect Type");
                        }
                        else if (ddltype.SelectedIndex < 0)
                        {
                            MessageBox.Show("Please select type");
                        }
                        else
                        {

                            SetField();
                            bl_catagory.InsertCatagory(bl_field);
                            Clear();
                            MessageBox.Show("Record inserted Successfully");
                            BindCatagory();
                            //_main.BindCatagory();
                            txtname.Focus();
                        }
                    

                }
                catch
                {
                    MessageBox.Show("error occured!Please contact Administration?");

                }
            
           
        }

        private void SetField()
        {
            bl_field.CatagoryName = txtname.Text;
            if (ddltype.Text == "Restaurant")
            {
                bl_field.Type = 0;
            }
            else
            {
                bl_field.Type = 1;
            }
            bl_field.Sequence = txtsequence.Text;
           // bl_field.Type = txtname.Text;
        }
        private void Clear()
        {
            txtname.Text = "";
            btndelete.Enabled = false;
            btnUpdate.Enabled = false;
            btnadd.Enabled = true;

        }
        private void AddCatagory_Load(object sender, EventArgs e)
        {
            BindCatagory();
            btndelete.Enabled = false;
            btnUpdate.Enabled = false;
           
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            bl_field.CatId = dataGridView1.SelectedRows[0].Cells["CatId"].Value.ToString();
            txtname.Text = dataGridView1.SelectedRows[0].Cells["CatagoryName"].Value.ToString();
            ddltype.Text = dataGridView1.SelectedRows[0].Cells["Type"].Value.ToString();
            txtsequence.Text = dataGridView1.SelectedRows[0].Cells["Sequence"].Value.ToString();
            btndelete.Enabled = true;
            btnUpdate.Enabled = true;
            btnadd.Enabled = false;
        }

        private void BindCatagory()
        {
            DataTable dt = bl_catagory.Catagory(bl_field);
            dataGridView1.DataSource = dt;
            dataGridView1.Columns["CatId"].Visible = false;
            //DataRow row = dt.NewRow();
            //row["Type"] = "Select";
            //dt.Rows.InsertAt(row, 0);
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            txtname.Text = "";
            txtsequence.Text = "";
            ddltype.Text = "SELECT";
            txtname.Focus();

        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Catagory?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_catagory.DeleteCatagory(bl_field);
                    MessageBox.Show("Record Delete Successfully");
                    BindCatagory();
                   // _main.BindCatagory();
                    Clear();
                    txtname.Focus();
                }
            }
            catch 
            {

                MessageBox.Show("error occured!Please contact Administration?");             
            }
     
        }

        private void AddCatagory_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void txtname_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = Char.ToUpper(e.KeyChar);
        }

        private void txtsequence_KeyPress(object sender, KeyPressEventArgs e)
        {
            Common common = new Common();
            common.NumberOnly(e);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            
            
                if (txtname.Text == "")
                {
                    MessageBox.Show("Please enter Name?");
                }
                else if (ddltype.SelectedIndex < 0)
                {
                    MessageBox.Show("Please select type");
                }
                else
                {
                    SetField();
                    bl_catagory.UpdateCatagory(bl_field);
                    Clear();
                    MessageBox.Show("Record Updated Successfully");
                    BindCatagory();
                   //btnadd.Text = "Add";
                    txtname.Focus();

                }

            
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void ddltype_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtsequence_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtname_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

﻿namespace Restaurant.PL
{
    partial class Feedback
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Feedback));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblFeedback = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.frdoExcellent = new System.Windows.Forms.RadioButton();
            this.btnadd = new System.Windows.Forms.Button();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.txtComment = new System.Windows.Forms.TextBox();
            this.cboCustomer = new System.Windows.Forms.ComboBox();
            this.cboServer = new System.Windows.Forms.ComboBox();
            this.frdoGood = new System.Windows.Forms.RadioButton();
            this.frdoAverage = new System.Windows.Forms.RadioButton();
            this.frdoPoor = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.srdoGood = new System.Windows.Forms.RadioButton();
            this.srdoExcellent = new System.Windows.Forms.RadioButton();
            this.srdoAverage = new System.Windows.Forms.RadioButton();
            this.srdoPoor = new System.Windows.Forms.RadioButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.ardoGood = new System.Windows.Forms.RadioButton();
            this.ardoExcellent = new System.Windows.Forms.RadioButton();
            this.ardoAverage = new System.Windows.Forms.RadioButton();
            this.ardoPoor = new System.Windows.Forms.RadioButton();
            this.btndelete = new System.Windows.Forms.Button();
            this.btnCustomerAdd = new System.Windows.Forms.Button();
            this.grdFeedback = new System.Windows.Forms.DataGridView();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdFeedback)).BeginInit();
            this.SuspendLayout();
            // 
            // lblFeedback
            // 
            this.lblFeedback.AutoSize = true;
            this.lblFeedback.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFeedback.Location = new System.Drawing.Point(396, 9);
            this.lblFeedback.Name = "lblFeedback";
            this.lblFeedback.Size = new System.Drawing.Size(134, 31);
            this.lblFeedback.TabIndex = 0;
            this.lblFeedback.Text = "Feedback\r\n";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate.Location = new System.Drawing.Point(44, 59);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(48, 24);
            this.lblDate.TabIndex = 1;
            this.lblDate.Text = "Date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(44, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(147, 24);
            this.label2.TabIndex = 2;
            this.label2.Text = "Customer Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(44, 141);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 24);
            this.label3.TabIndex = 3;
            this.label3.Text = "Server\'s Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(44, 182);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(141, 24);
            this.label4.TabIndex = 4;
            this.label4.Text = "Quality Of Food";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(45, 226);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 24);
            this.label5.TabIndex = 5;
            this.label5.Text = "Service";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(44, 272);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 24);
            this.label6.TabIndex = 6;
            this.label6.Text = "Ambience";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(44, 321);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 24);
            this.label7.TabIndex = 7;
            this.label7.Text = "Comments";
            // 
            // frdoExcellent
            // 
            this.frdoExcellent.AutoSize = true;
            this.frdoExcellent.Location = new System.Drawing.Point(18, 14);
            this.frdoExcellent.Name = "frdoExcellent";
            this.frdoExcellent.Size = new System.Drawing.Size(68, 17);
            this.frdoExcellent.TabIndex = 12;
            this.frdoExcellent.TabStop = true;
            this.frdoExcellent.Text = "Excellent";
            this.frdoExcellent.UseVisualStyleBackColor = true;
            // 
            // btnadd
            // 
            this.btnadd.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnadd.BackgroundImage")));
            this.btnadd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnadd.Location = new System.Drawing.Point(236, 415);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(77, 30);
            this.btnadd.TabIndex = 16;
            this.btnadd.Text = "Save";
            this.btnadd.UseVisualStyleBackColor = true;
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click);
            // 
            // dtpDate
            // 
            this.dtpDate.AccessibleRole = System.Windows.Forms.AccessibleRole.Document;
            this.dtpDate.AllowDrop = true;
            this.dtpDate.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(235, 33);
            this.dtpDate.MaxDate = new System.DateTime(2210, 12, 31, 0, 0, 0, 0);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(161, 38);
            this.dtpDate.TabIndex = 53;
            // 
            // txtComment
            // 
            this.txtComment.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComment.Location = new System.Drawing.Point(235, 321);
            this.txtComment.Multiline = true;
            this.txtComment.Name = "txtComment";
            this.txtComment.Size = new System.Drawing.Size(432, 79);
            this.txtComment.TabIndex = 54;
            // 
            // cboCustomer
            // 
            this.cboCustomer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCustomer.FormattingEnabled = true;
            this.cboCustomer.Location = new System.Drawing.Point(235, 77);
            this.cboCustomer.Name = "cboCustomer";
            this.cboCustomer.Size = new System.Drawing.Size(161, 39);
            this.cboCustomer.TabIndex = 55;
            this.cboCustomer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCustomer_KeyPress);
            this.cboCustomer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboCustomer_KeyDown);
            // 
            // cboServer
            // 
            this.cboServer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboServer.FormattingEnabled = true;
            this.cboServer.Location = new System.Drawing.Point(235, 122);
            this.cboServer.Name = "cboServer";
            this.cboServer.Size = new System.Drawing.Size(161, 39);
            this.cboServer.TabIndex = 56;
            this.cboServer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboServer_KeyPress);
            this.cboServer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboServer_KeyDown);
            // 
            // frdoGood
            // 
            this.frdoGood.AutoSize = true;
            this.frdoGood.Location = new System.Drawing.Point(109, 14);
            this.frdoGood.Name = "frdoGood";
            this.frdoGood.Size = new System.Drawing.Size(51, 17);
            this.frdoGood.TabIndex = 57;
            this.frdoGood.TabStop = true;
            this.frdoGood.Text = "Good";
            this.frdoGood.UseVisualStyleBackColor = true;
            // 
            // frdoAverage
            // 
            this.frdoAverage.AutoSize = true;
            this.frdoAverage.Location = new System.Drawing.Point(215, 14);
            this.frdoAverage.Name = "frdoAverage";
            this.frdoAverage.Size = new System.Drawing.Size(65, 17);
            this.frdoAverage.TabIndex = 60;
            this.frdoAverage.TabStop = true;
            this.frdoAverage.Text = "Average";
            this.frdoAverage.UseVisualStyleBackColor = true;
            // 
            // frdoPoor
            // 
            this.frdoPoor.AutoSize = true;
            this.frdoPoor.Location = new System.Drawing.Point(306, 14);
            this.frdoPoor.Name = "frdoPoor";
            this.frdoPoor.Size = new System.Drawing.Size(47, 17);
            this.frdoPoor.TabIndex = 63;
            this.frdoPoor.TabStop = true;
            this.frdoPoor.Text = "Poor";
            this.frdoPoor.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.frdoGood);
            this.panel1.Controls.Add(this.frdoExcellent);
            this.panel1.Controls.Add(this.frdoAverage);
            this.panel1.Controls.Add(this.frdoPoor);
            this.panel1.Location = new System.Drawing.Point(235, 170);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(432, 45);
            this.panel1.TabIndex = 66;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.srdoGood);
            this.panel2.Controls.Add(this.srdoExcellent);
            this.panel2.Controls.Add(this.srdoAverage);
            this.panel2.Controls.Add(this.srdoPoor);
            this.panel2.Location = new System.Drawing.Point(235, 219);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(432, 45);
            this.panel2.TabIndex = 67;
            // 
            // srdoGood
            // 
            this.srdoGood.AutoSize = true;
            this.srdoGood.Location = new System.Drawing.Point(109, 14);
            this.srdoGood.Name = "srdoGood";
            this.srdoGood.Size = new System.Drawing.Size(51, 17);
            this.srdoGood.TabIndex = 57;
            this.srdoGood.TabStop = true;
            this.srdoGood.Text = "Good";
            this.srdoGood.UseVisualStyleBackColor = true;
            // 
            // srdoExcellent
            // 
            this.srdoExcellent.AutoSize = true;
            this.srdoExcellent.Location = new System.Drawing.Point(18, 14);
            this.srdoExcellent.Name = "srdoExcellent";
            this.srdoExcellent.Size = new System.Drawing.Size(68, 17);
            this.srdoExcellent.TabIndex = 12;
            this.srdoExcellent.TabStop = true;
            this.srdoExcellent.Text = "Excellent";
            this.srdoExcellent.UseVisualStyleBackColor = true;
            // 
            // srdoAverage
            // 
            this.srdoAverage.AutoSize = true;
            this.srdoAverage.Location = new System.Drawing.Point(215, 14);
            this.srdoAverage.Name = "srdoAverage";
            this.srdoAverage.Size = new System.Drawing.Size(65, 17);
            this.srdoAverage.TabIndex = 60;
            this.srdoAverage.TabStop = true;
            this.srdoAverage.Text = "Average";
            this.srdoAverage.UseVisualStyleBackColor = true;
            // 
            // srdoPoor
            // 
            this.srdoPoor.AutoSize = true;
            this.srdoPoor.Location = new System.Drawing.Point(306, 14);
            this.srdoPoor.Name = "srdoPoor";
            this.srdoPoor.Size = new System.Drawing.Size(47, 17);
            this.srdoPoor.TabIndex = 63;
            this.srdoPoor.TabStop = true;
            this.srdoPoor.Text = "Poor";
            this.srdoPoor.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.ardoGood);
            this.panel3.Controls.Add(this.ardoExcellent);
            this.panel3.Controls.Add(this.ardoAverage);
            this.panel3.Controls.Add(this.ardoPoor);
            this.panel3.Location = new System.Drawing.Point(235, 270);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(432, 45);
            this.panel3.TabIndex = 68;
            // 
            // ardoGood
            // 
            this.ardoGood.AutoSize = true;
            this.ardoGood.Location = new System.Drawing.Point(109, 14);
            this.ardoGood.Name = "ardoGood";
            this.ardoGood.Size = new System.Drawing.Size(51, 17);
            this.ardoGood.TabIndex = 57;
            this.ardoGood.TabStop = true;
            this.ardoGood.Text = "Good";
            this.ardoGood.UseVisualStyleBackColor = true;
            // 
            // ardoExcellent
            // 
            this.ardoExcellent.AutoSize = true;
            this.ardoExcellent.Location = new System.Drawing.Point(18, 14);
            this.ardoExcellent.Name = "ardoExcellent";
            this.ardoExcellent.Size = new System.Drawing.Size(68, 17);
            this.ardoExcellent.TabIndex = 12;
            this.ardoExcellent.TabStop = true;
            this.ardoExcellent.Text = "Excellent";
            this.ardoExcellent.UseVisualStyleBackColor = true;
            // 
            // ardoAverage
            // 
            this.ardoAverage.AutoSize = true;
            this.ardoAverage.Location = new System.Drawing.Point(215, 14);
            this.ardoAverage.Name = "ardoAverage";
            this.ardoAverage.Size = new System.Drawing.Size(65, 17);
            this.ardoAverage.TabIndex = 60;
            this.ardoAverage.TabStop = true;
            this.ardoAverage.Text = "Average";
            this.ardoAverage.UseVisualStyleBackColor = true;
            // 
            // ardoPoor
            // 
            this.ardoPoor.AutoSize = true;
            this.ardoPoor.Location = new System.Drawing.Point(306, 14);
            this.ardoPoor.Name = "ardoPoor";
            this.ardoPoor.Size = new System.Drawing.Size(47, 17);
            this.ardoPoor.TabIndex = 63;
            this.ardoPoor.TabStop = true;
            this.ardoPoor.Text = "Poor";
            this.ardoPoor.UseVisualStyleBackColor = true;
            // 
            // btndelete
            // 
            this.btndelete.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btndelete.BackgroundImage")));
            this.btndelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btndelete.Location = new System.Drawing.Point(428, 415);
            this.btndelete.Name = "btndelete";
            this.btndelete.Size = new System.Drawing.Size(77, 30);
            this.btndelete.TabIndex = 70;
            this.btndelete.Text = "Delete";
            this.btndelete.UseVisualStyleBackColor = true;
            this.btndelete.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnCustomerAdd
            // 
            this.btnCustomerAdd.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCustomerAdd.BackgroundImage")));
            this.btnCustomerAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCustomerAdd.Location = new System.Drawing.Point(414, 77);
            this.btnCustomerAdd.Name = "btnCustomerAdd";
            this.btnCustomerAdd.Size = new System.Drawing.Size(28, 30);
            this.btnCustomerAdd.TabIndex = 72;
            this.btnCustomerAdd.Text = "+";
            this.btnCustomerAdd.UseVisualStyleBackColor = true;
            this.btnCustomerAdd.Click += new System.EventHandler(this.btnCustomerAdd_Click);
            // 
            // grdFeedback
            // 
            this.grdFeedback.AllowUserToAddRows = false;
            this.grdFeedback.AllowUserToDeleteRows = false;
            this.grdFeedback.AllowUserToResizeColumns = false;
            this.grdFeedback.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.grdFeedback.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.grdFeedback.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdFeedback.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.AntiqueWhite;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Maroon;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.PeachPuff;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdFeedback.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.grdFeedback.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Maroon;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Maroon;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdFeedback.DefaultCellStyle = dataGridViewCellStyle3;
            this.grdFeedback.EnableHeadersVisualStyles = false;
            this.grdFeedback.Location = new System.Drawing.Point(49, 451);
            this.grdFeedback.MultiSelect = false;
            this.grdFeedback.Name = "grdFeedback";
            this.grdFeedback.ReadOnly = true;
            this.grdFeedback.RowHeadersVisible = false;
            this.grdFeedback.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdFeedback.Size = new System.Drawing.Size(672, 178);
            this.grdFeedback.TabIndex = 92;
            this.grdFeedback.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdFeedback_CellClick);
            // 
            // btnClear
            // 
            this.btnClear.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnClear.BackgroundImage")));
            this.btnClear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClear.Location = new System.Drawing.Point(512, 415);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(77, 30);
            this.btnClear.TabIndex = 93;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnUpdate.BackgroundImage")));
            this.btnUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdate.Location = new System.Drawing.Point(332, 415);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(77, 30);
            this.btnUpdate.TabIndex = 94;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // Feedback
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(253)))), ((int)(((byte)(244)))));
            this.ClientSize = new System.Drawing.Size(742, 662);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.grdFeedback);
            this.Controls.Add(this.btnCustomerAdd);
            this.Controls.Add(this.btndelete);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.cboServer);
            this.Controls.Add(this.cboCustomer);
            this.Controls.Add(this.txtComment);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.btnadd);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.lblFeedback);
            this.Name = "Feedback";
            this.Text = "Feedback";
            this.Load += new System.EventHandler(this.Feedback_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdFeedback)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFeedback;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton frdoExcellent;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.TextBox txtComment;
        private System.Windows.Forms.ComboBox cboCustomer;
        private System.Windows.Forms.ComboBox cboServer;
        private System.Windows.Forms.RadioButton frdoGood;
        private System.Windows.Forms.RadioButton frdoAverage;
        private System.Windows.Forms.RadioButton frdoPoor;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton srdoGood;
        private System.Windows.Forms.RadioButton srdoExcellent;
        private System.Windows.Forms.RadioButton srdoAverage;
        private System.Windows.Forms.RadioButton srdoPoor;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton ardoGood;
        private System.Windows.Forms.RadioButton ardoExcellent;
        private System.Windows.Forms.RadioButton ardoAverage;
        private System.Windows.Forms.RadioButton ardoPoor;
        private System.Windows.Forms.Button btndelete;
        private System.Windows.Forms.Button btnCustomerAdd;
        private System.Windows.Forms.DataGridView grdFeedback;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnUpdate;
    }
}
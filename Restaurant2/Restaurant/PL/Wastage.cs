﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using Restaurant.PL.Reports;

namespace Restaurant.PL
{
    public partial class Wastage : Form
    {

        BL_Field bl_field = new BL_Field();
        BL_Wastage bl_wastage= new BL_Wastage();
        BL_Vendor bl_vendor = new BL_Vendor();
        BL_NewItem bl_newitem = new BL_NewItem();
        Common commmon = new Common();

        public Wastage()
        {
            InitializeComponent();
        }

        private void txtitemname_TextChanged(object sender, EventArgs e)
        {
            
            
            
            //if (txtitemname.Text != "")
            //{
            //    bl_field.TextSearch = txtitemname.Text;
            //    DataTable dt = bl_newitem.SearchNewItemBySearch(bl_field);
            //    if (dt.Rows.Count > 0)
            //    {
            //        grditemName.Visible = true;
            //        grditemName.DataSource = dt;
            //        grditemName.Columns["ItemId"].Visible = false;
            //        grditemName.Columns["Rate"].Visible = false;
            //        grditemName.Columns["Inventory"].Visible = false;
            //        grditemName.Columns["PrimeryId"].Visible = false;
            //        grditemName.Columns["SeconderyId"].Visible = false;
            //        grditemName.Columns["ConversionUnit"].Visible = false;

            //    }
            //}
            //else 
            //{
            //    DataTable dt = bl_newitem.SearchNewItemAll();
            //    if (dt.Rows.Count > 0)
            //    {
            //        grditemName.Visible = true;
            //        grditemName.DataSource = dt;
            //        grditemName.Columns["ItemId"].Visible = false;
            //        grditemName.Columns["Rate"].Visible = false;
            //        grditemName.Columns["Inventory"].Visible = false;
            //        grditemName.Columns["PrimeryId"].Visible = false;
            //        grditemName.Columns["SeconderyId"].Visible = false;
            //        grditemName.Columns["ConversionUnit"].Visible = false;

            //    }
            //}
        }

        private void txtitemname_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtqty.Focus();
            }

            //if (e.KeyCode == Keys.Enter)
            //{

            //    if (grditemName.Rows.Count > 0)
            //    {

            //        txtitemname.Text = grditemName.SelectedRows[0].Cells[1].Value.ToString();
            //        bl_field.ItemId = grditemName.SelectedRows[0].Cells["ItemId"].Value.ToString();

            //        DataTable dt = bl_newitem.SelectNewItemById(bl_field);
            //        if (dt.Rows.Count > 0)
            //        {
            //            string primeryid = dt.Rows[0]["PrimeryId"].ToString();
            //            string seconderyid = dt.Rows[0]["SeconderyId"].ToString();
            //            commmon.BindUnitName(ddlunit, primeryid, seconderyid);
            //        }

            //         grditemName.Hide();
            //        txtqty.Focus();


            //    }
            //    else
            //    {

            //        grditemName.Visible = false;
            //        txtitemname.Focus();
            //    }

            //}

            //if (e.KeyCode == Keys.Up)
            //{
            //    DataTable dtTemp = grditemName.DataSource as DataTable;


            //    object[] arr = dtTemp.Rows[dtTemp.Rows.Count - 1].ItemArray;
            //    for (int i = dtTemp.Rows.Count - 2; i >= 0; i--)
            //    {
            //        dtTemp.Rows[i + 1].ItemArray = dtTemp.Rows[i].ItemArray;
            //    }
            //    dtTemp.Rows[0].ItemArray = arr;



            //}

            //if (e.KeyCode == Keys.Down)
            //{

            //    DataTable dtTemp = grditemName.DataSource as DataTable;

            //    object[] arr = dtTemp.Rows[0].ItemArray;
            //    for (int i = 1; i < dtTemp.Rows.Count; i++)
            //    {
            //        dtTemp.Rows[i - 1].ItemArray = dtTemp.Rows[i].ItemArray;
            //    }
            //    dtTemp.Rows[dtTemp.Rows.Count - 1].ItemArray = arr;



            //}
            

        }

        private void Wastage_Load(object sender, EventArgs e)
        {
            ddldate.CustomFormat = "dd/MM/yyyy";
            //string date = bl_changeofday.SelectDate();
            //ddldate.Text = date;
            BindWastage();
            commmon.BindUnitName(ddlunit);
            btndelete.Enabled = false;
            btnUpdate.Enabled = false;
           // grditemName.Visible = false;
        }



        private void SetField()
        {
            bl_field.Date = ddldate.Text;
            //bl_field.ItemId = grditemName.SelectedRows[0].Cells["ItemId"].Value.ToString(); 
            bl_field.ItemName=txtitemname.Text;
            bl_field.Qty = txtqty.Text;
            bl_field.UnitId = ddlunit.SelectedValue.ToString();
            bl_field.Notes = txtnotes.Text;
   

        }
        private void Clear()
        {
            ddldate.Text="";
            txtitemname.Text = "";
           // grditemName.Visible = false;
            txtqty.Text = "";
            //string date = bl_changeofday.SelectDate();
            //ddldate.Text = date;
            commmon.BindUnitName(ddlunit);
            txtnotes.Text = "";
            //grditemName.Visible = false;
           
        }

        private void btnadd_Click(object sender, EventArgs e)
        {                
            if (txtitemname.Text== "")
            {
                MessageBox.Show("Please Select Item Name");
                txtitemname.Focus();
            }
            else if (txtqty.Text == "" || Convert.ToInt32(txtqty.Text) <= 0)
            {
                MessageBox.Show("Please Enter Qty");
                txtqty.Focus();

            }
            else if (ddlunit.Text == "SELECT")
            {
                MessageBox.Show("Please Select Unit");
                ddlunit.Focus();

            }
            else if (txtnotes.Text == "")
            {
                MessageBox.Show("Please Enter Reason");
                txtnotes.Focus();

            }
            else
            {
                SetField();
                bl_wastage.InsertWastage(bl_field);
                BindWastage();
                MessageBox.Show("Recods Inserted Successfully");
                Clear();
            } 
        }


        private void BindWastage()
        {
            bl_field.Date = ddldate.Text;
            DataTable dt = bl_wastage.SelectWastage(bl_field);
            grdpurchase.DataSource = dt;
            grdpurchase.Columns["WastageId"].Visible = false;
            grdpurchase.Columns["UnitId"].Visible = false;
        }

        private void grdpurchase_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                bl_field.WastageId = Convert.ToInt32(grdpurchase.SelectedRows[0].Cells["WastageId"].Value.ToString());

                ddldate.Text = grdpurchase.SelectedRows[0].Cells["Date"].Value.ToString();

                string d = grdpurchase.SelectedRows[0].Cells["Date"].Value.ToString();      

                txtitemname.Text = grdpurchase.SelectedRows[0].Cells["ItemName"].Value.ToString();
               // grditemName.Visible = false;
                ddlunit.Text = grdpurchase.SelectedRows[0].Cells["UnitName"].Value.ToString();
                txtqty.Text = grdpurchase.SelectedRows[0].Cells["Qty"].Value.ToString();              
                txtnotes.Text = grdpurchase.SelectedRows[0].Cells["Resion"].Value.ToString();
                ddldate.Text = d;
                btndelete.Enabled = true;
                btnUpdate.Enabled = true;
                btnadd.Enabled = false;
               
            }
            catch
            {
                MessageBox.Show("error occured ?please contact administrator");

            }
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (grdpurchase.Rows.Count > 0)
                {
                    DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Item?", "Delete", MessageBoxButtons.YesNo);
                    if (msgg == DialogResult.Yes)
                    {
                        bl_wastage.DeleteWastage(bl_field);
                        BindWastage();
                        Clear();
                        btndelete.Enabled = false;
                        btnUpdate.Enabled = false;
                        //=======hi======
                    }
                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
        }

        private void Wastage_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            Clear();
            btndelete.Enabled = false;
            btnUpdate.Enabled = false;
            btnadd.Enabled = true;
        }

        private void btnnewItem_Click(object sender, EventArgs e)
        {
            CreateItem ci = new CreateItem();
            ci.ShowDialog();
        }

        private void txtqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            Common common = new Common();
            common.OneDecimalPoint(txtqty, e);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Unit u = new Unit();
            u.ShowDialog();
        }

        private void txtqty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ddlunit.Focus();
            }
        }

        private void ddlunit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtnotes.Focus();
            }
        }

        private void ddlunit_KeyPress(object sender, KeyPressEventArgs e)
        {
            commmon.ComboBoxCheckValue(ddlunit, e);
        }

        private void grdpurchase_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
                     
            {
                if (txtitemname.Text != "" && txtqty.Text != "")
                {
                    SetField();
                    
                    bl_wastage.UpdateWastage(bl_field);
                    Clear();
                    BindWastage();
                    btndelete.Enabled = false;
                    btnUpdate.Enabled = false;
                }
                else
                {
                    MessageBox.Show("Please Select Specific Record");
                }
            }
        }

        private void ddldate_ValueChanged(object sender, EventArgs e)
        {
            BindWastage();
        }

       


    }
}

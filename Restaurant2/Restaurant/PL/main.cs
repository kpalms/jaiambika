﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using Restaurant.PL.Reports;
using System.Collections;

namespace Restaurant.PL
{
    public partial class main : Form
    {
        BL_Catagory bl_catagory = new BL_Catagory();
        BL_Table bl_table = new BL_Table();
        BL_Field bl_feild = new BL_Field();
        BL_Sale bl_sale = new BL_Sale();
        Common common = new Common();
        BL_Customer bl_customer = new BL_Customer();
        BL_Worker bl_worker = new BL_Worker();
        BL_NewItem bl_newitem = new BL_NewItem();
        CheckBox chkbox = new CheckBox();
        public static string LoginId = "";
        //ArrayList SubCatidToPrint = new ArrayList();
        Dictionary<int, int> SubCatidToPrint = new Dictionary<int, int>();

        string seat;
     

       public main()
        {
            InitializeComponent();
            this.KeyPreview = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
           KotDetails kd = new KotDetails();
            kd.ShowDialog();

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            bl_feild.Time = DateTime.Now.ToString("hh:mm");
            bl_worker.UpdateLogOutDetails(bl_feild);
            this.Hide();
            Login log = new Login();
            log.Show();
        }

        private void btnChangePrice_Click(object sender, EventArgs e)
        {


            if (grdOrder.Rows.Count > 0)
            {
                try
                {
                    if (grdOrder.Rows.Count > 0)
                    {
                        string price = grdOrder.SelectedRows[0].Cells["Price"].Value.ToString();
                        string qty = grdOrder.SelectedRows[0].Cells["Qty"].Value.ToString();
                        string saleid = grdOrder.SelectedRows[0].Cells["OrderDetailId"].Value.ToString();

                        PasswordChangePrice changep = new PasswordChangePrice(price, saleid, this);
                        changep.ShowDialog();
                    }
                }
                catch
                {

                }
            }
            else
            {
                MessageBox.Show("Please select order first?");
            }

        }
        public void PageRefreshEvent()
        {
            BindItenName();

        }
        public void GetTableId(string tblid)
        {
            string s = tblid;
            string[] words = s.Split(' ');
            cbotable.Text = words[0];
            comboseat.Text = words[1];

            //cbotable.Text = tblid;
        }
        string[] arg = new string[10];

        private void main_Load(object sender, EventArgs e)
        {
            
            lblopendate.Text = common.ReturnOneValue("select Date from Date where  Status=1");


            pnlcustomername.Visible = false;
              
            //lsttableno.Visible = false;
            //lstwaiter.Visible = false;
            //cbotable.Focus();
            lblcashierName.Text = bl_feild.LoginUser;
            //grdkotremark.Visible = false;
            BindCatagory();
            BindItenName();
            BindTable();
            BindTableNo();
            BindDepartment();
            BindWaitorName();
            BindKotRemark();

            //cbotable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            //cbotable.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            //cbotable.AutoCompleteSource = AutoCompleteSource.ListItems;


        }



        private void BindDepartment()
        {
            DataTable dt = bl_worker.SelectDepartment();
            //DataRow row = dt.NewRow();
            //row["Department"] = "SELECT";
            //dt.Rows.InsertAt(row, 0);
            combodepartment.DataSource = dt;
            combodepartment.DisplayMember = "Department";
            combodepartment.ValueMember = "DpId";


        }


        private void BindTableNo()
        {
            //if (txttableno.Focused)
            //{
            //    if (txttableno.Text != "")
            //    {
            //        bl_feild.TableNo = txttableno.Text;
            //        DataTable dt = bl_table.SearchTable(bl_feild);
            //        if (dt.Rows.Count > 0)
            //        {
            //            lsttableno.Visible = true;
            //            lsttableno.DataSource = dt;
            //            lsttableno.Columns["TblId"].Visible = false;
            //        }
                    
            //    }

            //    else
            //    {
            //        DataTable dt = bl_table.SearchTableAll(bl_feild);
            //        if (dt.Rows.Count > 0)
            //        {
            //            lsttableno.Visible = true;
            //            lsttableno.DataSource = dt;
            //            lsttableno.Columns["TblId"].Visible = false;
            //        }

            //    }
            //}

            DataTable dt = bl_table.SearchTableAll(bl_feild);
            cbotable.DataSource = dt;
            cbotable.DisplayMember = "TableNo";
            cbotable.ValueMember = "TableNo";

        }
        public  void BindCatagory()
        {
            flowLayoutPanel1.Controls.Clear();
            DataTable dt = bl_catagory.SelectCatagory(bl_feild);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt .Rows .Count ; i++)
                {
                    Button bb = new Button();
                    bb.Name = dt.Rows[i][0].ToString();
                    bb.Size = new Size(64, 40);
                    bb.Text = dt.Rows[i][1].ToString();
                    bb.ForeColor = Color.Black;
                    bb.Font = new Font("Arial", 7, FontStyle.Bold);

                    bb.Cursor = Cursors.Hand;
                    this.flowLayoutPanel1.Controls.Add(bb);
                    bb.Click += new EventHandler(bb_Click);
                    bb.MouseEnter += new EventHandler(bb_MouseEnter);
                    bb.MouseLeave += new EventHandler(bb_MouseLeave);
                    bb.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
                    bb.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
                    bb.FlatAppearance.BorderColor = System.Drawing.Color.Green;
                    bb.FlatAppearance.BorderSize = 1;

                    bb.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
       
              

                }
            
            }
        }
        void bb_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
           string catid=  btn.Name;
            bl_feild.SaticCatId = catid;
            bl_feild.DpId = combodepartment.SelectedValue.ToString();
            BindItemNamedp( bl_feild);

       }
        private void bb_MouseEnter(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            btn.FlatAppearance.BorderSize = 1;
            btn.BackColor = Color.Red;
            btn.ForeColor = Color.White;
           // btn.Font = new Font("Arial", 9, FontStyle.Bold);

        }
        private void bb_MouseLeave(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            btn.FlatAppearance.BorderSize = 1;
            btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            btn.ForeColor = Color.Black;
            //btn.Font = new Font("Arial", 7, FontStyle.Bold);
        }
        private void BindItemName(string catid)
        {
            DataTable dt = bl_catagory.SelectSubCatagory(catid);
            grditem.DataSource = dt;
            grditem.Columns["SubCatId"].Visible = false;
            grditem.Columns["CatId"].Visible = false;
            grditem.Columns["ItemName"].Width = 110;
            grditem.Columns["Price"].Width = 50;
            grditem.Columns["ShortName"].Width = 50;
            grditem.Columns["Type"].Visible = false;
        }

        private void BindItemNamedp(BL_Field bl_field)
        {
            DataTable dt = bl_catagory.SelectSubCatagorydp(bl_field);
            grditem.DataSource = dt;
            grditem.Columns["SubCatId"].Visible = false;
            grditem.Columns["CatId"].Visible = false;
            grditem.Columns["ItemName"].Width = 150;
            grditem.Columns["Price"].Width = 50;
            grditem.Columns["ShortName"].Width = 50;
            grditem.Columns["Type"].Visible = false;
        }
       void order_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string catid = btn.Text;
        }
        private void grditem_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void btnAddOrder_Click(object sender, EventArgs e)
        {
            if (cbotable.Text != "")
            {
                bl_feild.TableNo = cbotable.Text;
                if (comboseat.Text != "")
                {
                    bl_feild.Seat = comboseat.Text;
                }
                else
                {
                    bl_feild.Seat = "A";
                }
                DataTable dt = bl_sale.CheckPendingStatus(bl_feild);

                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["PendingStatus"].ToString() == "0")
                    {
                        string billno = dt.Rows[0]["BillNo"].ToString();
                        string Total = dt.Rows[0]["Total"].ToString();

                        AmountPay am = new AmountPay(Total, billno, cbotable.Text, comboseat.Text, this);
                        am.ShowDialog();
                    }
                }
                else
                {

                    AddToOrder();
                }
            }
            else
            {
                MessageBox.Show("please select tableno");
            }
       }
        public void AddToOrder()
        {
            try
            {

                if (cbotable.Text != "" )
                {
                    if (grditem.Rows.Count > 0)
                    {
                        decimal qty = 0;
                        if (cboqty.Text == "" || Convert.ToInt32(cboqty.Text) <= 0)
                        {
                            qty = 1;
                        }
                        else
                        {
                            qty = Convert.ToInt32(cboqty.Text);
                        }

                        if (comboseat.Text == "")
                        {
                           bl_feild.Seat= "A";
                           
                        }
                        else
                        {
                             bl_feild.Seat=  comboseat.Text;
                        }


                        bl_feild.TableNoS = cbotable.Text;

                        if (txtperson.Text != "")
                        {
                            bl_feild.Person = txtperson.Text;
                        }
                        else
                        {
                            bl_feild.Person = "1";
                        }
                        bl_feild.DpId = combodepartment.SelectedValue.ToString();
                        bl_feild.SubcatId = grditem.SelectedRows[0].Cells["SubCatId"].Value.ToString();
                        bl_feild.ItemName = grditem.SelectedRows[0].Cells["ItemName"].Value.ToString();
                        bl_feild.PriceS = grditem.SelectedRows[0].Cells["Price"].Value.ToString();
                        bl_feild.QtyS = Convert.ToInt32(qty);
                        bl_feild.KotRemark = cbokotremark.Text;
                        bl_feild.Name = cbowaitor.Text;

                        bl_feild.Date = bl_feild.GlobalDate;

                        bl_feild.ParsalNo = 0;
                        bl_feild.Status = "Open";
                        bl_feild.Type = Convert.ToInt32(grditem.SelectedRows[0].Cells["Type"].Value.ToString());
                        bl_sale.InsertSale(bl_feild);
                        BindItenName();

                        cboqty.Text = "";
                        txtshortname.Text = "";
                    }
                    else
                    {

                        cbotable.Focus();
                    }

                }
                else
                {
                    MessageBox.Show("Please select table");
                    cbotable.Focus();
                }

            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
        }
        public void BindItenName()
        
        {
            try
            {

                    DataTable dt = null;


                    pnldiscount.Visible = false;


                    if (comboseat.Text == "")
                    {
                        bl_feild.Seat = "A";
                    }
                    else
                    {
                        bl_feild.Seat = comboseat.Text;
                    }
                    if (cbotable.Text == "0" && comboseat.Text == "0")
                    {
                        chkcounter.Checked = true;
                    }
                    else
                    {
                        chkcounter.Checked = false;
                    }
                    bl_feild.TableNoS = cbotable.Text;
                    bl_feild.Status = "Open";
                    dt = bl_sale.SelectItemName(bl_feild);




                    if (dt.Rows.Count > 0)
                    {

                        string KotPrinted = "";

                        KotPrinted = common.ReturnOneValue("select KotStatus from OrderDetail where TableNo='" + cbotable.Text + "' and Status='Open' and KotStatus='1'");

                        if (KotPrinted == "1")
                        {
                            btnprint.Enabled = false;
                        }
                        else
                        {
                            btnprint.Enabled = true;
                        }
                        pnltotal.Visible = true;
                        grdOrder.DataSource = null;

                        grdOrder.DataSource = dt;
                        grdOrder.Columns["OrderDetailId"].Visible = false;
                        grdOrder.Columns["OrderId"].Visible = false;
                        grdOrder.Columns["SubCatId"].Visible = false;
                        grdOrder.Columns["TableNo"].Visible = false;
                        grdOrder.Columns["Person"].Visible = false;
                        grdOrder.Columns["KotStatus"].Visible = false;
                        grdOrder.Columns["ParsalNo"].Visible = false;
                        grdOrder.Columns["StartTime"].Visible = false;
                        grdOrder.Columns["KotOrderNo"].Visible = false;
                        grdOrder.Columns["WaitorName"].Visible = false;
                        grdOrder.Columns["DpId"].Visible = false;
                        grdOrder.Columns["DeliveryCharge"].Visible = false;
                        grdOrder.Columns["ItemName"].Width = 150;
                        grdOrder.Columns["Price"].Width = 60;
                        decimal price = 0;
                        decimal total = 0;
                        decimal qty = 0;
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {

                            qty = Convert.ToDecimal(dt.Rows[i][6].ToString());
                            price = Convert.ToDecimal(dt.Rows[i][5].ToString());
                            if (qty != 1)
                            {

                                total = total + (price * Convert.ToDecimal(qty));

                            }
                            else
                            {
                                total = total + Convert.ToDecimal(dt.Rows[i][5].ToString());
                            }


                        }
                        txtperson.Text = dt.Rows[0][7].ToString();
                        lbltable.Text = dt.Rows[0][3].ToString();
                        lblperson.Text = dt.Rows[0][7].ToString();
                        lbltotal.Text = Convert.ToString(total);
                        cbowaitor.Text = dt.Rows[0]["WaitorName"].ToString();
                        combodepartment.SelectedValue = dt.Rows[0]["DpId"].ToString();
                        if (cbotable.Text == "0")
                        {
                            pnldelivery.Visible = true;
                            btnDeliveryCharge.Enabled = true;
                            lblDeliverycharge.Text = dt.Rows[0]["DeliveryCharge"].ToString();
                        }
                        else
                        {
                            btnDeliveryCharge.Enabled = false;
                            pnldelivery.Visible = false;
                        }

                        BindTable();

                    }
                    else
                    {
                        txtperson.Text = "";
                        grdOrder.DataSource = null;
                        pnltotal.Visible = false;
                        combodepartment.SelectedValue = "7";
                        btnDeliveryCharge.Enabled = false;
                        pnldelivery.Visible = false;
                        pnlcustomername.Visible = false;
                        BindTable();
                    }

          
            }
            catch
            {

              
            }


      }

        private void chkBoxChange(object sender, EventArgs e)
        {
            for (int k = 0; k <= grdOrder.RowCount - 1; k++)
            {
                this.grdOrder[0, k].Value = this.chkbox.Checked;
            }
            this.grdOrder.EndEdit();
        }

        private void btnRemoveItem_Click(object sender, EventArgs e)
        {



      
            try
            {
                if (grdOrder.Rows.Count > 0)
                {

                    bl_feild.SaleId = grdOrder.SelectedRows[0].Cells["OrderDetailId"].Value.ToString();
                    if (bl_feild.SaleId != "")
                    {
                        string CheckQt = bl_sale.CheckQt(bl_feild);
                            if (CheckQt != "2")
                            {
                                    RemoveItem();
                            }
                            else
                            {

                                MessageBox.Show("Cant remove item after KOT is Print,Please cancel this Item");
                            }
                      
                    }

                }
            }
            catch 
            {
                
                
            }
 
  


        }

        public void RemoveItem()
        {
            DialogResult msgg = MessageBox.Show("Are you sure you want to Remove selected Item?", "Delete", MessageBoxButtons.YesNo);
            if (msgg == DialogResult.Yes)
            {

                    bl_feild.Status = "Open";
             
                bl_feild.SaleId = grdOrder.SelectedRows[0].Cells["OrderDetailId"].Value.ToString();
                bl_sale.DeleteSaleItem(bl_feild);
                BindItenName();
            }
        }

        private void btnChangeQty_Click(object sender, EventArgs e)
        {

            if (grdOrder.Rows.Count > 0)
            {

                try
                {
                    bl_feild.SaleId = grdOrder.SelectedRows[0].Cells["OrderDetailId"].Value.ToString();
                      string CheckQt = bl_sale.CheckQt(bl_feild);
                      if (CheckQt != "2")
                      {
                          string qty = grdOrder.SelectedRows[0].Cells["Qty"].Value.ToString();
                          string saleid = grdOrder.SelectedRows[0].Cells["OrderDetailId"].Value.ToString();
                          string price = grdOrder.SelectedRows[0].Cells["Price"].Value.ToString();

                          Qty quanti = new Qty(saleid, qty, price, this);
                          quanti.Show();
                      }
                      else {
                          MessageBox.Show("Can't Change Quantity after KOT is print,please cancel item?");
                      }

                    
                }
                catch
                {
               }
            }


            else
            {
                MessageBox.Show("Please select order first?");
            }

   

        }

        private void btnchnageperson_Click(object sender, EventArgs e)
        {
            if (grdOrder.Rows.Count > 0)
            {
                string tblno = cbotable.Text;
                ChangePerson change = new ChangePerson(tblno, txtperson.Text, this);
                change.Show();
            }
            else
            {
                MessageBox.Show("Please select order first!");
            }

        }

        private void btndis_Click(object sender, EventArgs e)
        {

                if (cbotable.Text == "")
                {
                    MessageBox.Show("Please select table no?");
                }
                else {
                    Discount dis = new Discount(this);
                    dis.ShowDialog();
                }
           


        }

        private void button3_Click(object sender, EventArgs e)
        {

               PrintBill();

        }

        public void PrintBill()
        {
            try
            {

              if (grdOrder.Rows.Count > 0)
                {
                    decimal total = 0;
                    int billno = 0;
                        string pp = common.ReturnOneValue("select BillPrinter from PrinterSetup");
                        if (pp != "0")
                        {
                           string msg = common.ReturnOneValue("select Message from Message where Status='YES'");
                            
                           
                            DataSet dsresto = null;
  
                                bl_feild.Status = "Open";
                                bl_feild.TableNoPrintBill = cbotable.Text;
                                dsresto = bl_sale.SelectRestoPrint(bl_feild);


                      


                            foreach (DataRow dt in dsresto.Tables[0].Rows)
                            {
                                SubCatidToPrint.Add(Convert.ToInt32(dt["SubCatId"].ToString()), Convert.ToInt32(dt["Qty"].ToString()));
                            }

                            if (dsresto.Tables[0].Rows.Count > 0)
                            {


                                rptPrintBill rptItem = new rptPrintBill();
                                billno = common.ReceiptNo();
                                bl_feild.BillNo = Convert.ToString(billno);
                                rptItem.SetDataSource(dsresto.Tables[0]);




                                rptItem.SetParameterValue("WaiterName", cbowaitor.Text);
                                rptItem.SetParameterValue("Message", msg);
                                rptItem.SetParameterValue("HotelName", bl_feild.HotelName);
                                rptItem.SetParameterValue("HotelAddress", bl_feild.HotelAddress);

                                bl_feild.Total = Convert.ToDecimal(lbltotal.Text);

                                rptItem.SetParameterValue("ReceiptNo", billno);
                                rptItem.SetParameterValue("Date", bl_feild.GlobalDate);
                                rptItem.SetParameterValue("BillTime", DateTime.Now.ToString("hh:mm:ss"));
                                bl_feild.GrandTotal = Convert.ToDecimal(lbltotal.Text);
                                bl_feild.CashierName = lblcashierName.Text;
                                bl_feild.Name = cbowaitor.Text;
                                bl_feild.Dis =lbldiscount.Text;
                                string discount = lbldiscount.Text;
                                string[] sdis = discount.Split('-');
                                string disco = sdis[0].ToString();
                                

                                if (pnldiscount.Visible == true)
                                {
                                    decimal discountamt = 0;
                                    if (sdis[1].ToString() == "Per")
                                    {
                                        discountamt = Convert.ToDecimal(lbltotal.Text) * Convert.ToDecimal(disco) / 100;

                                    }
                                    else
                                    {
                                        discountamt = Convert.ToDecimal(sdis[0].ToString());
                                    }
                                    total= Convert.ToDecimal(lbltotal.Text) - discountamt;
                                    bl_feild.DisAmount = discountamt;
                                    rptItem.SetParameterValue("discount", lbldiscount.Text);
                                    rptItem.SetParameterValue("lbldiscount", "DisCount");
                                    rptItem.SetParameterValue("lblsubtotal", "SubTotal");
                                    rptItem.SetParameterValue("PrintTotal", total);
                                    rptItem.SetParameterValue("SubTotal", lbltotal.Text);
                                }
                                else {
                                    total = Convert.ToDecimal(lbltotal.Text);
                                    rptItem.SetParameterValue("discount", "");
                                    rptItem.SetParameterValue("lbldiscount", "");
                                    rptItem.SetParameterValue("lblsubtotal", "");
                                    rptItem.SetParameterValue("SubTotal", "");
                                    rptItem.SetParameterValue("PrintTotal", total);
                                    bl_feild.SubTotal = Convert.ToDecimal(0);
                                    bl_feild.Dis = "00";
                                    bl_feild.DisAmount = 00;

                                }

    
                                    
                                    if (pnlcustomername.Visible == true)
                                    {
                                        bl_feild.CustId = bl_feild.CustIdStatic;
                                        rptItem.SetParameterValue("lblName", "Name");
                                        rptItem.SetParameterValue("lblMobileNo", "MobileNo");
                                        rptItem.SetParameterValue("lblAddress", "Address");
                                        rptItem.SetParameterValue("Name", lblCname.Text);
                                        rptItem.SetParameterValue("MobileNo", lblCMobileNo.Text);
                                        rptItem.SetParameterValue("Address",lblSociety.Text+" "+label13.Text+" "+lblCAddress.Text);
                                    }
                                    else
                                    {
                                        bl_feild.CustId = "0";
                                        rptItem.SetParameterValue("lblName", "");
                                        rptItem.SetParameterValue("lblMobileNo", "");
                                        rptItem.SetParameterValue("lblAddress", "");
                                        rptItem.SetParameterValue("Name", "");
                                        rptItem.SetParameterValue("MobileNo", "");
                                        rptItem.SetParameterValue("Address", "");
                                    }

                                    if (pnldelivery.Visible == true)
                                    {
                                        rptItem.SetParameterValue("lblDeliveryCharge", "DeleveryCharge");
                                        rptItem.SetParameterValue("DeliveryCharge", lblDeliverycharge.Text);
                                    }
                                    else
                                    {
                                        rptItem.SetParameterValue("lblDeliveryCharge", "");
                                        rptItem.SetParameterValue("DeliveryCharge", "");
                                    }
                                string word = common.changeToWords(Convert.ToString(total), true);
                                rptItem.SetParameterValue("Word", word);
                                bl_feild.Date=dsresto.Tables[0].Rows[0]["Date"].ToString();
                                string id=bl_sale.InsertBill(bl_feild);
                                bl_feild.OrderId = id;
                                bl_feild.Status = "P";
                                DialogResult msgg = MessageBox.Show("Are you sure you want to Print Restaurant Bill?", "Restaurant Bill", MessageBoxButtons.YesNo);
                                if (msgg == DialogResult.Yes)
                                {

                                    rptItem.PrintOptions.PrinterName = pp;
                                    rptItem.PrintToPrinter(1, false, 0, 0);
                                }

 
                                    bl_sale.CloseOrderResto(bl_feild);

                                    StockMinus();
                            }

                           
                           
                            
                            SubCatidToPrint.Clear();
                            //comboseat.Text = "";
                            cbotable.Text = "";
                            
                            BindItenName();
                            
                                        //sms code
                                        //BL_SmsEmail bl_smsemail = new BL_SmsEmail();
                                        //DataTable dtsms = bl_smsemail.Selectsms();
                                        //if (dtsms.Rows.Count > 0)
                                        //{
                                        //    if (dtsms.Rows[0]["Switch"].ToString() == "1")
                                        //    {
                                        //        if (dtsms.Rows[0]["BillSms"].ToString() == "1")
                                        //        {
                                        //            EmailSms.SendSms("9096234202", "Bill NO:-" + billno + ",Amount:-" + total + "");
                                        //        }
                                        //    }
                                        //}
                                        //sms code
                          
                        }
                        else {
                            MessageBox.Show("Printer not found");
                        }
                }
            }
            catch
            {


            }
        }


        private void txtqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            {
                e.Handled = true;
                
            }
        }




        private void purchasingToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void workerToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }



        private void btncancelBill_Click(object sender, EventArgs e)
        {


            if (grdOrder.Rows.Count > 0)
            {
                bl_feild.SaleId = grdOrder.SelectedRows[0].Cells["OrderDetailId"].Value.ToString();
                if (bl_feild.SaleId != "")
                {
                    string CheckQt = bl_sale.CheckQt(bl_feild);
                    if (CheckQt == "2")
                    {
                        DialogResult msgg = MessageBox.Show("Are you sure you want to Cancel Item?", "Delete", MessageBoxButtons.YesNo);
                        if (msgg == DialogResult.Yes)
                        {

                            try
                            {

                                string SaleId = grdOrder.SelectedRows[0].Cells["OrderDetailId"].Value.ToString();
                                string TableNo = cbotable.Text;
                                string orderno = common.ReturnOneValue("select KotOrderNo from OrderDetail where OrderDetailId='" + SaleId + "'");
                                string CashierName = lblcashierName.Text;
                                string ItemName = grdOrder.SelectedRows[0].Cells["ItemName"].Value.ToString();
                                string rate = grdOrder.SelectedRows[0].Cells["Price"].Value.ToString();
                                string curqty = grdOrder.SelectedRows[0].Cells["Qty"].Value.ToString();

                                string Amount = grdOrder.SelectedRows[0].Cells["Price"].Value.ToString();
                                CancelItem quanti = new CancelItem(SaleId, TableNo, orderno, CashierName, ItemName, curqty, rate, Amount, this);
                                quanti.ShowDialog();


                            }
                            catch
                            {
                            }


                        }
                    }
                    else
                    {

                        MessageBox.Show("Please Note Cancel Item After KOT Print?You Can Remove This");
                    }

                }
            }
            else {
                MessageBox.Show("no order");
            }


        }

        private void calculatorToolStripMenuItem_Click(object sender, EventArgs e)
        {
        
        }

        private void txtsearch_TextChanged(object sender, EventArgs e)
        {
            if (txtsearch.Text != "")
            {
                bl_feild.DpId = combodepartment.SelectedValue.ToString();
                bl_feild.TextSearch = txtsearch.Text;
                DataTable dt = bl_catagory.SelectItemBySearch(bl_feild);
                if (dt.Rows.Count > 0)
                {
                    grditem.DataSource = dt;
                    grditem.Columns["SubCatId"].Visible = false;
                    grditem.Columns["CatId"].Visible = false;
                    grditem.Columns["Type"].Visible = false;
                    grditem.Columns["ItemName"].Width = 110;
                    grditem.Columns["Price"].Width = 30;
                    grditem.Columns["ShortName"].Width = 50;
                }
            }
            else
            {
                //string catid = bl_feild.SaticCatId;
                //BindItemName(catid);
            }
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helps helpmy = new Helps();
            helpmy.ShowDialog();

        }

        private void btnQT_Click(object sender, EventArgs e)
        {
          
            PrintKOT();
            BindTable();

        }

        private void PrintKOT()
        {
            try
            {
                if (grdOrder.Rows.Count > 0)
                {
                    string pp = common.ReturnOneValue("select KotPrinter from PrinterSetup");
                    if (pp != "0")
                    {
                        DataSet ds = null;
                        bl_feild.KOTPRINT = "";
                        bl_feild.Status = "Open";
                        if (comboseat.Text != "")
                        {
                            bl_feild.Seat = comboseat.Text;
                        }
                        else {
                            bl_feild.Seat = "A";
                        }
                        bl_feild.TableNoPrintBill = cbotable.Text;
                        bl_feild.Time = DateTime.Now.ToString("hh:mm");
                        bl_feild.KotOrderNo = Convert.ToString(common.KotNo());

           
                                ds = bl_sale.PrintQt(bl_feild);
                                bl_sale.UpdateQt(bl_feild);
                         
                            
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                PrintKOT(ds);
                            }

                            else
                            {

                                MessageBox.Show("All Order Printed?Please enter New Order");
                            }

                    }
                    else
                    {
                        MessageBox.Show("Printer not found");
                    }
                }
                string KotPrinted = "";

                KotPrinted = common.ReturnOneValue("select KotStatus from OrderDetail where TableNo='" + cbotable.Text + "' and Status='Open' and KotStatus='1'");
               
                if (KotPrinted == "1")
                {
                    btnprint.Enabled = false;
                }
                else
                {
                    btnprint.Enabled = true;
                }
            
            }
            catch(Exception ex)
            {
                MessageBox.Show("this error"+ex);
            }
        }
        private void PrintKOT(DataSet ds)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to Print KOT?", "Print KOT", MessageBoxButtons.YesNo);
                                if (msgg == DialogResult.Yes)
                                {
                                    rptKotPrint rptItem = new rptKotPrint();
                                    rptItem.SetDataSource(ds.Tables[0]);


                                    if (cbotable.Text == "0")
                                    {
                                        rptItem.SetParameterValue("lblname", "Parsal");
                                        rptItem.SetParameterValue("no", cbotable.Text + "" + comboseat.Text);
                                    }
                                    else
                                    {
                                        rptItem.SetParameterValue("lblname", "TableNo");
                                        rptItem.SetParameterValue("no", cbotable.Text+""+comboseat.Text);
                                    }

                                
                                    
                                    if (bl_feild.KOTPRINT=="yes")
                                    {
                                        rptItem.SetParameterValue("KotOrderNo","RePrint");
                                    }else{
                                    rptItem.SetParameterValue("KotOrderNo",bl_feild.KotOrderNo);
                                    }
                                    string pp = common.ReturnOneValue("select KotPrinter from PrinterSetup");
                                    rptItem.PrintOptions.PrinterName = pp;
                                            
                                    rptItem.PrintToPrinter(1, false, 0, 0);

                                }

                                //cbotable.Text = "";
                                cbotable.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("this error" + ex);
            }



        }


        private void txtSearchByNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtSearchByNo.Text != "")
                {
                    bl_feild.DpId = combodepartment.SelectedValue.ToString();
                    bl_feild.TextSearch = txtSearchByNo.Text;
                    DataTable dt = bl_catagory.SelectItemBySearchNo(bl_feild);

                    if (dt.Rows.Count > 0)
                    {
                        grditem.DataSource = dt;
                        grditem.Columns["SubCatId"].Visible = false;
                        grditem.Columns["CatId"].Visible = false;
                        grditem.Columns["ItemName"].Width = 110;
                        grditem.Columns["Price"].Width = 30;
                        grditem.Columns["ShortName"].Width = 50;

                    }
                    else
                    {
                        grditem.DataSource = null;
                    }
                }
            }
            catch 
            {
                 
            }

        }





        private void grditem_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (cbotable.Text != "")
            {
                bl_feild.TableNo = cbotable.Text;
                if (comboseat.Text != "")
                {
                    bl_feild.Seat = comboseat.Text;
                }
                else
                {
                    bl_feild.Seat = "A";
                }
                DataTable dt = bl_sale.CheckPendingStatus(bl_feild);

                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["PendingStatus"].ToString() == "0")
                    {
                        string billno = dt.Rows[0]["BillNo"].ToString();
                        string Total = dt.Rows[0]["Total"].ToString();

                        AmountPay am = new AmountPay(Total, billno, cbotable.Text, comboseat.Text, this);
                        am.ShowDialog();
                    }
                }
                else
                {

                    AddToOrder();
                }

            }
            else
            {
                MessageBox.Show("Please select tableno");
            }

          
        }


        private void grdOrder_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {

            try
            {

                int columnIndex = grdOrder.CurrentCell.ColumnIndex;
                string columnName = grdOrder.Columns[columnIndex].Name;

                if (columnName != "chkcol")
                {
                    bl_feild.SaleId = grdOrder.SelectedRows[0].Cells["OrderDetailId"].Value.ToString();
                    if (grdOrder.SelectedRows[0].Cells["OrderDetailId"].Value.ToString() != "")
                    {
                        string kotremark = grdOrder.SelectedRows[0].Cells["KotRemark"].Value.ToString();
                        string selectedQty = bl_sale.GetQty(bl_feild);
                        string qty = grdOrder.SelectedRows[0].Cells["Qty"].Value.ToString();

                        string CheckQt = bl_sale.CheckQt(bl_feild);
                        if (Convert.ToInt32(qty) >= Convert.ToInt32(selectedQty) || CheckQt != "2")
                        {
                            QtyChange(qty);
                        }
                        else
                        {
                            MessageBox.Show("Can not Change Less Qty");
                            BindItenName();
                        }

                    }
                    else
                    {

                    }
                }

               
            }
            catch
            {
         
            }
        }


        private void QtyChange(string qty)
        {

            bl_feild.SaleId = grdOrder.SelectedRows[0].Cells["OrderDetailId"].Value.ToString();
            bl_feild.QtyS = Convert .ToInt32(qty);
            bl_feild.KotRemark = grdOrder.SelectedRows[0].Cells["KotRemark"].Value.ToString();
            bl_sale.UpdateQty(bl_feild);
            BindItenName();
        }


        



        private void txtSearchByNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            {
                e.Handled = true;
               
            }
        }


        private void main_KeyDown(object sender, KeyEventArgs e)
        
        {
            if (e.KeyCode == Keys.F1)
            {
                Customer cm = new Customer(this);
                cm.ShowDialog();
            }
            if (e.KeyCode == Keys.F2)
            {
                cbotable.Focus();
            }
            if (e.KeyCode == Keys.F3)
            {
                txtSearchByNo.Focus();
            }
            if (e.KeyCode == Keys.F4)
            {
                cboqty.Focus();
            }
            if (e.KeyCode == Keys.F5)
            {
                txtsearch.Focus();
            }
            if (e.KeyCode == Keys.F6)
            {
                cboqty.Focus();
            }
            if (e.KeyCode == Keys.F7)
            {
                txtshortname.Focus();
            }
            if (e.Control && e.KeyCode == Keys.P)
            {

                    PrintBill();
               
            }
            if (e.Control && e.KeyCode == Keys.K)
            {

                    PrintKOT();
              
            }
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }

        }


        private void btnCustomer_Click(object sender, EventArgs e)
        {

        }

        private void hotelSettingToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void securityToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void kotReprintToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }




        private void todayToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void yesturdayToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void saleByTableToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }


       private void parsalReportsToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Report2DateSelect report = new Report2DateSelect("ParsalReports");
            report.Show();
        }

        private void customerListToolStripMenuItem_Click(object sender, EventArgs e)
       {

       }


        private void grditem_KeyDown(object sender, KeyEventArgs e)
        {

        }



        private void txtsearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = Char.ToUpper(e.KeyChar);
        }


        private void billReprintToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }


        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Report2DateSelect report = new Report2DateSelect("DisCountReports");
            report.Show();
        }

        private void cashBookReportsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Report2DateSelect report = new Report2DateSelect("CashBookReports");
            report.Show();
        }



        //private void txttableno_TextChanged(object sender, EventArgs e)
        //{



        //    if (txttableno.Text != "" || txttableno.Focused)
        //    {
        //        if (chkparsal.Checked == true)
        //        {
        //            chkparsal.Checked = false;
        //            pnlcustomername.Visible = false;

        //        }
        //        else
        //        {
        //            pnlcustomername.Visible = false;
        //        }

        //        BindTableNo();
        //        BindItenName();


        //    }

        //    else
        //    {
        //        BindItenName();
        //        lsttableno.Hide();

        //    }




        //}

        //private void txttableno_KeyDown(object sender, KeyEventArgs e)
        //{


        //    if (e.KeyCode == Keys.Enter)
        //    {
        //        if (lsttableno.Rows.Count > 0)
        //        {
        //            txttableno.Text = lsttableno.SelectedRows[0].Cells[1].Value.ToString();
                   
        //            lsttableno.Hide();

        //            txtworker.Focus();
        //        }
        //        else
        //        {
        //            lsttableno.Visible = false;
        //            lstwaiter.Visible = false;
        //            txtworker.Focus();
        //        }

        //    }

        //    if (e.KeyCode == Keys.Up)
        //    {
        //        DataTable dtTemp = lsttableno.DataSource as DataTable;

        //        object[] arr = dtTemp.Rows[dtTemp.Rows.Count - 1].ItemArray;
        //        for (int i = dtTemp.Rows.Count - 2; i >= 0; i--)
        //        {
        //            dtTemp.Rows[i + 1].ItemArray = dtTemp.Rows[i].ItemArray;
        //        }
        //        dtTemp.Rows[0].ItemArray = arr;



        //    }

        //    if (e.KeyCode == Keys.Down)
        //    {

        //        DataTable dtTemp = lsttableno.DataSource as DataTable;

        //        object[] arr = dtTemp.Rows[0].ItemArray;
        //        for (int i = 1; i < dtTemp.Rows.Count; i++)
        //        {
        //            dtTemp.Rows[i - 1].ItemArray = dtTemp.Rows[i].ItemArray;
        //        }
        //        dtTemp.Rows[dtTemp.Rows.Count - 1].ItemArray = arr;



        //    }
 
        //}

        private void txtSearchByNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AddToOrder();
                txtSearchByNo.Text = "";
                string catid = bl_feild.SaticCatId;

            }

        }

        private void txtqty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtshortname.Focus();
            }
        }

        private void txtsearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AddToOrder();
                txtsearch.Text = "";
                //string catid = bl_feild.SaticCatId;
                //string dpid = bl_feild.DpId;
                //BindItemName(catid);
                //e.Handled = true;
                
            }

            if (e.KeyCode == Keys.Up)
            {
                DataTable dtTemp = grditem.DataSource as DataTable;

                object[] arr = dtTemp.Rows[dtTemp.Rows.Count - 1].ItemArray;
                for (int i = dtTemp.Rows.Count - 2; i >= 0; i--)
                {
                    dtTemp.Rows[i + 1].ItemArray = dtTemp.Rows[i].ItemArray;
                }
                dtTemp.Rows[0].ItemArray = arr;



            }

            if (e.KeyCode == Keys.Down)
            {

                DataTable dtTemp = grditem.DataSource as DataTable;

                object[] arr = dtTemp.Rows[0].ItemArray;
                for (int i = 1; i < dtTemp.Rows.Count; i++)
                {
                    dtTemp.Rows[i - 1].ItemArray = dtTemp.Rows[i].ItemArray;
                }
                dtTemp.Rows[dtTemp.Rows.Count - 1].ItemArray = arr;



            }
        }

        private void txtshortname_KeyDown(object sender, KeyEventArgs e)
    {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtshortname.Text != "")
                {

                    cbokotremark.Focus();

                }

   
            }

            if (e.KeyCode == Keys.Up)
            {
                DataTable dtTemp = grditem.DataSource as DataTable;

                object[] arr = dtTemp.Rows[dtTemp.Rows.Count - 1].ItemArray;
                for (int i = dtTemp.Rows.Count - 2; i >= 0; i--)
                {
                    dtTemp.Rows[i + 1].ItemArray = dtTemp.Rows[i].ItemArray;
                }
                dtTemp.Rows[0].ItemArray = arr;



            }

            if (e.KeyCode == Keys.Down)
            {

                DataTable dtTemp = grditem.DataSource as DataTable;

                object[] arr = dtTemp.Rows[0].ItemArray;
                for (int i = 1; i < dtTemp.Rows.Count; i++)
                {
                    dtTemp.Rows[i - 1].ItemArray = dtTemp.Rows[i].ItemArray;
                }
                dtTemp.Rows[dtTemp.Rows.Count - 1].ItemArray = arr;



            }
        }

        private void txtperson_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                cbotable.Focus();
            }
        }
//
        private void txtshortname_TextChanged(object sender, EventArgs e)
        {
            if (txtshortname.Text != "")
            {
                bl_feild.DpId = combodepartment.SelectedValue.ToString();
                bl_feild.TextSearch = txtshortname.Text;
                DataTable dt = bl_catagory.SearchShortName(bl_feild);
                if (dt.Rows.Count > 0)
                {
                    grditem.DataSource = dt;
                    grditem.Columns["SubCatId"].Visible = false;
                    grditem.Columns["CatId"].Visible = false;
                    grditem.Columns["Type"].Visible = false;
                    grditem.Columns["ItemName"].Width = 110;
                    grditem.Columns["Price"].Width = 30;
                    grditem.Columns["ShortName"].Width = 50;
                }
                else {
                    grditem.DataSource = null;
                    
                }
            }
            else
            {
                grditem.DataSource = null;
            }
        }

        private void addCatagoryToolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void addItemToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void addTableToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void addSupplierToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void workerToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void purchasingToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void calculatorToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void viewStockToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        //private void txtworker_TextChanged(object sender, EventArgs e)
        //{
        //    if (txtworker.Text !="" )
        //    {
             
        //        if (txtworker.Focused)
        //        {
        //            bl_feild.TextSearch = txtworker.Text;
        //            DataTable dt = bl_worker.SelectWaiterName(bl_feild);

        //            if (dt.Rows.Count > 0)
        //            {
        //                lstwaiter.Visible = true;
        //                lstwaiter.DataSource = dt;
        //            }
        //        }
        //    }
        //    else
        //    {
        //        if (txtworker.Focused)
        //        {
                   
         //          DataTable dt = bl_worker.SelectWaiterNameAll(bl_feild);

        //            if (dt.Rows.Count > 0)
        //            {
        //                lstwaiter.Visible = true;
        //                lstwaiter.DataSource = dt;
        //            }
        //        }

          
        //    }
           

        //}

        private void BindWaitorName()
        {
            DataTable dt = bl_worker.SelectWaiterNameAll(bl_feild);
            cbowaitor.DataSource = dt;
            cbowaitor.DisplayMember = "Name";
            cbowaitor.ValueMember = "Name";
        }

        //private void txtworker_KeyDown(object sender, KeyEventArgs e)
        //{


        //    if (e.KeyCode == Keys.Enter)
        //    {
        //        if (lstwaiter.Rows.Count > 0 && lstwaiter.Visible==true)
        //        {
                   
        //            txtworker.Text = lstwaiter.SelectedRows[0].Cells[0].Value.ToString();

        //            lstwaiter.Hide();
        //            txtqty.Focus();
                    
        //        }
        //        else
        //        {
        //            lstwaiter.Visible = false;
        //            lstwaiter.Visible = false;
        //            txtqty.Focus();
        //        }
                
               

        //    }

        //    if (e.KeyCode == Keys.Up)
        //    {
        //        DataTable dtTemp = lstwaiter.DataSource as DataTable;

        //        object[] arr = dtTemp.Rows[dtTemp.Rows.Count - 1].ItemArray;
        //        for (int i = dtTemp.Rows.Count - 2; i >= 0; i--)
        //        {
        //            dtTemp.Rows[i + 1].ItemArray = dtTemp.Rows[i].ItemArray;
        //        }
        //        dtTemp.Rows[0].ItemArray = arr;



        //    }

        //    if (e.KeyCode == Keys.Down)
        //    {

        //        DataTable dtTemp = lstwaiter.DataSource as DataTable;

        //        object[] arr = dtTemp.Rows[0].ItemArray;
        //        for (int i = 1; i < dtTemp.Rows.Count; i++)
        //        {
        //            dtTemp.Rows[i - 1].ItemArray = dtTemp.Rows[i].ItemArray;
        //        }
        //        dtTemp.Rows[dtTemp.Rows.Count - 1].ItemArray = arr;



        //    }

    
        //}

        private void grdTableDetails_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void grdPendingBill_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void shiftingToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }


        private void notePadToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        //private void lstwaiter_DoubleClick(object sender, EventArgs e)
        //{
        //    txtworker.Text = lstwaiter.Text.ToString();
        //    lstwaiter.Visible = false;
        //    txtqty.Focus();
        //}



        private void StockMinus()
        {
            //for (int j = 0; j < SubCatidToPrint.Count; j++)
            //{
            foreach (KeyValuePair<int, int> subcatid in SubCatidToPrint)
            {
                bl_feild.SubcatId = Convert.ToString((subcatid.Key));
                DataTable dtrecipeItemId = bl_newitem.SelectStarRecipe(bl_feild);

                for (int i = 0; i < dtrecipeItemId.Rows.Count; i++)
                {

                    bl_feild.ItemId = dtrecipeItemId.Rows[i]["ItemId"].ToString();
                   // int UnitIdtoRecipe = Convert.ToInt32(dtrecipeItemId.Rows[i]["UnitId"].ToString());

                   // DataTable dt = bl_newitem.SelectIdNewItem(bl_feild);

                    DataTable dtstock = bl_newitem.SelectStockById(bl_feild);

                    if (dtstock.Rows.Count > 0)
                    {

                        decimal stock = Convert.ToDecimal(dtstock.Rows[0]["Stock"].ToString());
                        decimal newstock = 0;



                        //int primeryid = Convert.ToInt32(dt.Rows[0]["PrimeryId"].ToString());
                        //DataTable dtstock = bl_newitem.SelectStockById(bl_feild);
                        //decimal stock = Convert.ToDecimal(dtstock.Rows[0]["Stock"].ToString());
                        //decimal newstock = 0;
                        //if (UnitIdtoRecipe == primeryid)
                        //{

                        //    if (dtstock.Rows.Count > 0)
                        //    {
                        //        newstock = stock - Convert.ToDecimal(dtrecipeItemId.Rows[i]["Qty"].ToString());
                            
                        //    }

                        //}
                        //else
                        //{

                        //    if (dtstock.Rows.Count > 0)
                        //    {
                        //        int conversion = Convert.ToInt32(dt.Rows[0]["ConversionUnit"].ToString());
                        //        decimal calstock = Convert.ToDecimal(dtrecipeItemId.Rows[i]["Qty"].ToString()) / conversion;
                        //        newstock = stock - calstock;
                             
                        //    }
                        //}
                        //decimal aa = (Convert.ToDecimal(dtrecipeItemId.Rows[i]["Qty"].ToString()) * (subcatid.Value));
                        //newstock = stock - aa;
                        newstock = stock - Convert.ToDecimal(dtrecipeItemId.Rows[i]["Qty"].ToString()) * (subcatid.Value);
                        bl_feild.UnitId = dtrecipeItemId.Rows[i][4].ToString();
                        bl_feild.Stock = Convert.ToString(newstock);
                        bl_feild.ItemId = dtrecipeItemId.Rows[i][3].ToString();
            
                        bl_newitem.StockMinus(bl_feild);
                    }

                }

            }
        }

        private void rdoresto_CheckedChanged(object sender, EventArgs e)
        {
            BindCatagory();
        }

        private void rdobar_CheckedChanged_1(object sender, EventArgs e)
        {
            BindCatagory();
        }

        private void periodWiseToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void billWiseToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }


        private void cashierWiseToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }


        private void saleByItemToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }






        private void messageToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void kotWiseToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void kotItemToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }



        private void grdPendingBill_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {

                }

            }
            catch
            {


            }
        }



        private void txtshortname_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = Char.ToUpper(e.KeyChar);
        }

        private void printerSettingToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        //private void txttableno_Leave(object sender, EventArgs e)
        //{
        //    lsttableno.Visible = false;
        //    if (txttableno.Text != "")
        //    {
        //        string table = common.ReturnOneValue("select TableNo From TableCount where TableNo='" + txttableno.Text + "'");
        //        if (table != "0")
        //        {

        //        }
        //        else
        //        {
        //            MessageBox.Show("Table No not found?");
        //            txttableno.Text = "";
        //            txttableno.Focus();
        //        }
        //    }
        //}

        //private void txtworker_Leave(object sender, EventArgs e)
        //{
        //    lstwaiter.Visible = false;
        //    if (txtworker.Text != "")
        //    {
        //        string worker = common.ReturnOneValue("select UserName From Worker where UserName='" + txtworker.Text + "'");
        //        if (worker != "0")
        //        {

        //        }
        //        else
        //        {
        //            MessageBox.Show("worker not found?");
        //            txtworker.Text = "";
        //            txtworker.Focus();
        //        }
        //    }
        //}

        private void billSummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }


        private void button2_Click(object sender, EventArgs e)
        {
            try
            {

                    if (grdOrder.Rows.Count > 0)
                    {
                        string saleid = grdOrder.SelectedRows[0].Cells["OrderDetailId"].Value.ToString();
                        string itemname = grdOrder.SelectedRows[0].Cells["ItemName"].Value.ToString();
                        ChangeItemName change = new ChangeItemName(saleid, itemname, this);
                        change.Show();
                    }
                    else
                    {
                        MessageBox.Show("Please select order first!");
                    }
            

            }
            catch 
            {
                
               
            }

        }

        private void adminPartToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        public void Discount()
        {
            pnldiscount.Visible = true;
            lbldiscount.Text = bl_feild.DiscountStatic;
        }

        //private void chkparsal_Click(object sender, EventArgs e)
        //{
        //    if (chkparsal.Checked == true)
        //    {
        //        cbotable.Text = "";
        //        cbowaitor.Text = "";
        //        cboqty.Text = "";
        //        txtshortname.Text = "";
        //        BindParsalNo();

        //        ddlparsalNo.Text = "";
        //        int par = common.PasalNo();
        //        ddlparsalNo.SelectedText = Convert.ToString(par);
        //        grdOrder.DataSource = null;
        //        pnlcustomername.Visible = false;
        //        pnltotal.Visible = false;

        //        ddlparsalNo.Enabled = true;
        //        btnnewParsal.Enabled = true;
        //        btnCustomer.Enabled = true;
        //    }
        //    else
        //    {
        //        ddlparsalNo.DataSource = null;
        //        ddlparsalNo.Text = "";
        //        grdOrder.DataSource = null;
        //        ddlparsalNo.Enabled = false;
        //        btnnewParsal.Enabled = false;
        //        btnCustomer.Enabled = false;
               
        //    }
        //}


        //private void BindParsalNo()
        //{
        //    try
        //    {

        //        string date = bl_changeofday.SelectDate();
        //        bl_feild.Date = date;
        //        DataTable dt = bl_sale.SelectParsalNo(bl_feild);
        //        if (dt.Rows.Count > 0)
        //        {
        //            ddlparsalNo.DataSource = dt;
        //        }
        //        ddlparsalNo.DisplayMember = "ParsalNo";
        //        ddlparsalNo.ValueMember = "ParsalNo";
        //    }
        //    catch
        //    {

        //        MessageBox.Show("Please Check Parsal");
        //    }

        //}

        //private void btnnewParsal_Click(object sender, EventArgs e)
        //{
        //    NewParsal();
        //}
        //private void NewParsal()
        //{
        //    try
        //    {


        //        if (chkparsal.Checked == true)
        //        {
        //            BindParsalNo();
        //            ddlparsalNo.Text = "";
        //            int par = common.PasalNo();
        //            ddlparsalNo.SelectedText = Convert.ToString(par);
        //            grdOrder.DataSource = null;
        //            pnltotal.Visible = false;
        //            pnlcustomername.Visible = false;
        //        }
        //        else
        //        {
        //            MessageBox.Show("Please Check Parsal");
        //        }
        //    }
        //    catch
        //    {

        //        MessageBox.Show("Error occured Please Contact Administrator");
        //    }
        //}

        //private void ddlparsalNo_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //    try
        //    {


        //        if (chkparsal.Checked == true)
        //        {
        //            if (ddlparsalNo.Text != "System.Data.DataRowView")
        //            {

        //                BindItenName();
        //                SelectCustomerById();
        //            }
        //        }
        //    }
        //    catch
        //    {

        //        MessageBox.Show("Error occured Please Contact Administrator");
        //    }
        //}

        private void btnCustomer_Click_1(object sender, EventArgs e)
        {
            Customer cm = new Customer(this);
            cm.ShowDialog();
        }


        //public void CheckData()
        //{
        //    if (ddlparsalNo.Text != "")
        //    {
        //        bl_feild.Status = "Parsal";
        //        bl_feild.ParsalNoStatic = Convert.ToInt32(ddlparsalNo.Text);
        //        bl_sale.ParsalToCustomer(bl_feild);
        //        SelectCustomerById();
        //    }

        //}


        //private void SelectCustomerById()
        //{
        //    try
        //    {


        //        if (ddlparsalNo.Text != "")
        //        {
        //            bl_feild.ParsalNo = Convert.ToInt32(ddlparsalNo.Text);
        //            bl_feild.Status = "Parsal";
        //             string date = bl_changeofday.SelectDate();
        //            bl_feild.Date = date;
        //            bl_feild.CustId = bl_customer.GetIdCustId(bl_feild);

        //            if (bl_feild.CustId != "")
        //            {
        //                DataTable dt = bl_customer.SelectCustomerById(bl_feild);
        //                if (dt.Rows.Count > 0)
        //                {
        //                    pnlcustomername.Visible = true;
        //                    lblCname.Text = dt.Rows[0][1].ToString();
        //                    lblCMobileNo.Text = dt.Rows[0][2].ToString();
        //                    lblCAddress.Text = dt.Rows[0][3].ToString();

        //                }
        //                else
        //                {
        //                    pnlcustomername.Visible = false;
        //                }
        //            }
        //            else {
        //                pnlcustomername.Visible = false;
        //            }
        //        }

        //    }
        //    catch
        //    {
        //        MessageBox.Show("Error occured Please Contact Administrator");

        //    }
        //}



        private void btnKotRemark_Click(object sender, EventArgs e)
        {
            if (grdOrder.Rows.Count > 0)
            {
                try
                {
                    if (grdOrder.Rows.Count > 0)
                    {
                        string Remark = grdOrder.SelectedRows[0].Cells["KotRemark"].Value.ToString();
                        string saleid = grdOrder.SelectedRows[0].Cells["OrderDetailId"].Value.ToString();

                        KotRemark kotremark = new KotRemark(Remark,saleid, this);
                        kotremark.ShowDialog();
                    }
                }
                catch
                {

                }
            }
            else
            {
                MessageBox.Show("Please select order first?");
            }
        }


        //private void txtKotRemark_KeyDown(object sender, KeyEventArgs e)
        //{
            // if (e.KeyCode == Keys.Enter)
            //{


            //    bl_feild.TableNo = cbotable.Text;
            //    DataTable dt = bl_sale.CheckPendingStatus(bl_feild);

            //    if (dt.Rows.Count > 0)
            //    {
            //        if (dt.Rows[0]["PendingStatus"].ToString() == "0")
            //        {
            //            string billno = dt.Rows[0]["BillNo"].ToString();
            //            string Total = dt.Rows[0]["Total"].ToString();

            //            AmountPay am = new AmountPay(Total, billno, cbotable.Text, this);
            //            am.ShowDialog();
            //        }
            //    }
            //    else
            //    {
            //    if (grdkotremark.Rows.Count > 0)
            //    {
            //        txtKotRemark.Text = grdkotremark.SelectedRows[0].Cells[1].Value.ToString();
                   
            //        AddToOrder();
            //        grdkotremark.Visible = false;
            //        cboqty.Focus();
            //        txtKotRemark.Text = "";
            //        txtshortname.Text = "";
            //        grdkotremark.DataSource = null;
            //    }
            //    else
            //    {
            //        grdkotremark.Visible = false;
            //        AddToOrder();

            //        cboqty.Focus();
            //        txtKotRemark.Text = "";
            //        txtshortname.Text = "";
            //        grdkotremark.DataSource = null;
            //    }
            // }

            //}

            //if (e.KeyCode == Keys.Up)
            //{
            //    DataTable dtTemp = grdkotremark.DataSource as DataTable;

            //    object[] arr = dtTemp.Rows[dtTemp.Rows.Count - 1].ItemArray;
            //    for (int i = dtTemp.Rows.Count - 2; i >= 0; i--)
            //    {
            //        dtTemp.Rows[i + 1].ItemArray = dtTemp.Rows[i].ItemArray;
            //    }
            //    dtTemp.Rows[0].ItemArray = arr;



            //}

            //if (e.KeyCode == Keys.Down)
            //{

            //    DataTable dtTemp = grdkotremark.DataSource as DataTable;

            //    object[] arr = dtTemp.Rows[0].ItemArray;
            //    for (int i = 1; i < dtTemp.Rows.Count; i++)
            //    {
            //        dtTemp.Rows[i - 1].ItemArray = dtTemp.Rows[i].ItemArray;
            //    }
            //    dtTemp.Rows[dtTemp.Rows.Count - 1].ItemArray = arr;

            //}



       // }

        //private void txtKotRemark_TextChanged(object sender, EventArgs e)
        //{

        //    if (txtKotRemark.Text != "" || txtKotRemark.Focused)
        //    {
        //        BindKotRemark();

        //    }
        //    else {
        //        grdkotremark.Visible = false;
        //        DataTable dt = bl_catagory.SelectKotRemark();
        //        grdkotremark.DataSource = dt;
        //        grdkotremark.Columns["KotId"].Visible = false;
        //    }
         

        //}

        private void BindKotRemark()
        {
            //if (txtKotRemark.Focused)
            //{
            //    if (txtKotRemark.Text != "")
            //    {
            //        bl_feild.TextSearch = txtKotRemark.Text;
            //        DataTable dt = bl_catagory.SearchKotRemark(bl_feild);
            //        if (dt.Rows.Count > 0)
            //        {
            //            grdkotremark.Visible = true;
            //            grdkotremark.DataSource = dt;
            //            grdkotremark.Columns["KotId"].Visible = false;
            //        }
            //        else
            //        {
            //            grdkotremark.DataSource = null;
            //            grdkotremark.Visible = false;
            //        }

            //    }


            //}
            DataTable dt = bl_catagory.SelectKotRemark();
            DataRow row = dt.NewRow();
            row["KotName"] = "";
            dt.Rows.InsertAt(row, 0);

            cbokotremark.DataSource = dt;
            cbokotremark.DisplayMember = "KotName";
            cbokotremark.ValueMember = "KotId";

        }

        //private void txttableno_KeyPress(object sender, KeyPressEventArgs e)
        //{

        //    if (txttableno.Text != "")
        //    {

        //        try
        //        {
        //            bl_feild.TableNo = txttableno.Text;
        //            DataTable dt = bl_sale.CheckPendingStatus(bl_feild);

        //            if (dt.Rows[0]["PendingStatus"].ToString() == "0")
        //            {
        //                string billno = dt.Rows[0]["BillNo"].ToString();
        //                string Total = dt.Rows[0]["Total"].ToString();

        //                AmountPay am = new AmountPay(Total, billno,txttableno.Text, this);
        //                am.ShowDialog();
        //            }


        //        }
        //        catch
        //        {


        //        }

        //    }
        //}


        public void BindTable()
        {

            try
            {

                //flowLayoutPanelTable.Controls.Clear();
                //DataTable dt = bl_table.CheckTable();
                //if (dt.Rows.Count > 0)
                //{
                //    for (int i = 0; i < dt.Rows.Count; i++)
                //    {
                //        DataGridView bb = new DataGridView();
                //        bb.Location = new Point(160, 100 * i + 10);
                //        bb.Name = dt.Rows[i]["TableNo"].ToString();
                //        bb.Size = new Size(70, 120);
                //        string a = (dt.Rows[i]["TableNo"].ToString());
                //        bb.Text = dt.Rows[i]["TableNo"].ToString();
                //        bb.ForeColor = Color.Black;
                //        bb.Font = new Font("Lucida Console", 15);
                //        bb.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
                //        bb.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
                //        bb.ReadOnly = true;
                //        bb.AllowUserToAddRows = false;
                //        bb.AllowUserToDeleteRows = false;
                //        bb.RowHeadersVisible = false;
                //        bb.EnableHeadersVisualStyles = false;
                //        bb.MultiSelect = false;
                //        bb.BackgroundColor = Color.White;
                //        DataTable dtseat = bl_table.CheckTableSeat(dt.Rows[i]["TableNo"].ToString());
                //        bb.DataSource = dtseat;
                //        this.flowLayoutPanelTable.Controls.Add(bb);
                //        bb.Columns["Status"].Visible = false;

                //        for (int j = 0; j < bb.Rows.Count; j++)
                //        {
                //            if ((String)bb.Rows[j].Cells["Status"].Value != String.Empty)
                //            {
                //                if ((String)bb.Rows[j].Cells["Status"].Value == "Open")
                //                {
                //                    bb.Rows[j].DefaultCellStyle.BackColor = Color.Green;

                //                    bb.Rows[j].DefaultCellStyle.SelectionBackColor = bb.Rows[j].DefaultCellStyle.BackColor;
                //                    bb.Rows[j].DefaultCellStyle.SelectionForeColor = bb.Rows[j].DefaultCellStyle.ForeColor;
                //                }
                //                else
                //                {
                //                    bb.Rows[j].DefaultCellStyle.BackColor = Color.Red;
                //                    bb.Rows[j].DefaultCellStyle.SelectionBackColor = bb.Rows[j].DefaultCellStyle.BackColor;
                //                    bb.Rows[j].DefaultCellStyle.SelectionForeColor = bb.Rows[j].DefaultCellStyle.ForeColor;

                //                }
                //            }

                //        }
                //       bb.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Table_CellClick);
                //   }

                //}

                flowLayoutPanelTable.Controls.Clear();
                DataTable dt = bl_table.CheckTable();
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataGridView bb = new DataGridView();
                        bb.Location = new Point(160, 100 * i + 10);
                        bb.Name = dt.Rows[i]["TableNo"].ToString();
                        bb.Size = new Size(70, 120);
                        string a = (dt.Rows[i]["TableNo"].ToString());
                        bb.Text = dt.Rows[i]["TableNo"].ToString();
                        bb.ForeColor = Color.Black;
                        bb.DefaultCellStyle.SelectionForeColor = Color.Black;
                        bb.Font = new Font("Lucida Console", 15);
                        bb.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
                        bb.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
                        bb.ReadOnly = true;
                        bb.AllowUserToAddRows = false;
                        bb.AllowUserToDeleteRows = false;
                        bb.RowHeadersVisible = false;
                        bb.EnableHeadersVisualStyles = false;
                        bb.MultiSelect = false;
                        bb.BackgroundColor = Color.White;
                        bb.ColumnHeadersVisible = false;
                        DataTable dtseat = bl_table.CheckTableSeat(dt.Rows[i]["TableNo"].ToString());


                        if (dtseat.Rows.Count > 0)
                        {
                            bb.DataSource = dtseat;
                        }
                        else
                        {
                            DataRow row = dtseat.NewRow();
                            row["TableNo"] = dt.Rows[i]["TableNo"].ToString();
                            dtseat.Rows.InsertAt(row, 0);
                            bb.DataSource = dtseat;
                            bb.DefaultCellStyle.BackColor = Color.White;
                            bb.DefaultCellStyle.SelectionBackColor = bb.DefaultCellStyle.BackColor;
                            bb.DefaultCellStyle.SelectionForeColor = bb.DefaultCellStyle.ForeColor;
                           
                        }
                        
                      

                        this.flowLayoutPanelTable.Controls.Add(bb);
                        bb.Columns["Status"].Visible = false;

                        for (int j = 0; j < bb.Rows.Count; j++)
                        {
                            if ((String)bb.Rows[j].Cells["Status"].Value.ToString() != String.Empty)
                            {
                                if ((String)bb.Rows[j].Cells["Status"].Value == "Open")
                                {
                                   string KotPrinted = common.ReturnOneValue("select KotStatus from OrderDetail where TableNo='" + bb.Rows[j].Cells["TableNo"].Value.ToString() + "' and Seat='" + bb.Rows[j].Cells["Seat"].Value.ToString() + "' and Status='Open' and KotStatus='1'");
                                   if (KotPrinted == "1")
                                    {
                                        bb.Rows[j].DefaultCellStyle.BackColor = Color.Yellow;
                                        bb.Rows[j].DefaultCellStyle.SelectionBackColor = bb.Rows[j].DefaultCellStyle.BackColor;
                                        bb.Rows[j].DefaultCellStyle.SelectionForeColor = bb.Rows[j].DefaultCellStyle.ForeColor;
                                    }
                                    else
                                    {
                                        bb.Rows[j].DefaultCellStyle.BackColor = Color.GreenYellow;
                                        bb.Rows[j].DefaultCellStyle.SelectionBackColor = bb.Rows[j].DefaultCellStyle.BackColor;
                                        bb.Rows[j].DefaultCellStyle.SelectionForeColor = bb.Rows[j].DefaultCellStyle.ForeColor;
                                    }

                                }
                                else
                                {
                                    bb.Rows[j].DefaultCellStyle.BackColor = Color.Red;
                                    bb.Rows[j].DefaultCellStyle.SelectionBackColor = bb.Rows[j].DefaultCellStyle.BackColor;
                                    bb.Rows[j].DefaultCellStyle.SelectionForeColor = bb.Rows[j].DefaultCellStyle.ForeColor;

                                }
                            }

                        }
                        bb.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Table_CellClick);
                    }

                }



            }
            catch
            {
              
            }
        }

  

        void Table_CellClick(object sender, EventArgs e)
        {

            DataGridView btn = (DataGridView)sender;
            string tableno = btn.SelectedRows[0].Cells["TableNo"].Value.ToString();
            string seat = btn.SelectedRows[0].Cells["Seat"].Value.ToString();
            try
            {
                bl_feild.TableNo = tableno;
                bl_feild.Seat = seat;
                DataSet PendingDetails = bl_sale.GetSettleBills(bl_feild);
                if (PendingDetails.Tables[0].Rows.Count > 0)
                {
                    string billno = PendingDetails.Tables[0].Rows[0]["BillNo"].ToString();
                    string Total = PendingDetails.Tables[0].Rows[0]["Total"].ToString();

                    AmountPay am = new AmountPay(Total, billno, tableno, seat, this);
                    am.ShowDialog();
                }
                else
                {
                    comboseat.Text = seat;
                    cbotable.Text = tableno;


                }
            }
            catch
            {


            }
        }

        private void comboseat_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbotable.Text != "System.Data.DataRowView")
            {

                BindItenName();
                SelectCustomer();
            }


        }

        private void cbotable_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cbotable.Text != "System.Data.DataRowView")
            {
               
                BindItenName();
                SelectCustomer();
            }
        }

        private void cbotable_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                comboseat.Focus();
                //cmbUnit.DroppedDown = true;
                comboseat.DroppedDown = true;
            }
        }

        private void ComboBoxCheckValue(object sender, KeyPressEventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            cb.DroppedDown = true;
            string strFindStr = "";
            if (e.KeyChar == (char)8)
            {
                if (cb.SelectionStart <= 1)
                {
                    cb.Text = "";
                    return;
                }

                if (cb.SelectionLength == 0)
                    strFindStr = cb.Text.Substring(0, cb.Text.Length - 1);
                else
                    strFindStr = cb.Text.Substring(0, cb.SelectionStart - 1);
            }
            else
            {
                if (cb.SelectionLength == 0)
                    strFindStr = cb.Text + e.KeyChar;
                else
                    strFindStr = cb.Text.Substring(0, cb.SelectionStart) + e.KeyChar;
            }
            int intIdx = -1;
            intIdx = cb.FindString(strFindStr);
            if (intIdx != -1)
            {
                cb.SelectedText = "";
                cb.SelectedIndex = intIdx;
                cb.SelectionStart = strFindStr.Length;
                cb.SelectionLength = cb.Text.Length;
                e.Handled = true;
            }
            else
                e.Handled = true;
        }

        private void cbotable_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBoxCheckValue(cbotable,e);
            
        }

        private void combodepartment_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBoxCheckValue(combodepartment, e);
        }

        private void cbowaitor_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBoxCheckValue(cbowaitor, e);
        }

        private void cbowaitor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                cboqty.Focus();
            }
        }

        private void comboseat_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                cbowaitor.Focus();
            }
        }

        private void cboqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBoxCheckValue(cboqty, e);
        }

        private void cboqty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtshortname.Focus();
            }
        }

        private void cbokotremark_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBoxCheckValue(cbokotremark, e);
        }

        private void cbokotremark_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                if (cbotable.Text != "")
                {
                    bl_feild.TableNo = cbotable.Text;
                    if (comboseat.Text != "")
                    {
                        bl_feild.Seat = comboseat.Text;
                    }
                    else
                    {
                        bl_feild.Seat = "A";
                    }
                    DataTable dt = bl_sale.CheckPendingStatus(bl_feild);

                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows[0]["PendingStatus"].ToString() == "0")
                        {
                            string billno = dt.Rows[0]["BillNo"].ToString();
                            string Total = dt.Rows[0]["Total"].ToString();

                            AmountPay am = new AmountPay(Total, billno, cbotable.Text, comboseat.Text, this);
                            am.ShowDialog();
                        }
                    }
                    else
                    {

                        AddToOrder();
                        cboqty.Focus();
                        cbokotremark.Text = "";
                        txtshortname.Text = "";

                    }

                }
                else
                {
                    MessageBox.Show("Please select tableno");
                }


            }

  
        }

        private void cbotable_Enter(object sender, EventArgs e)
        {
        
           // cbotable.DroppedDown = true;
        }

        private void comboseat_Enter(object sender, EventArgs e)
        {
           // comboseat.DroppedDown = true;
        }

        private void cbowaitor_Enter(object sender, EventArgs e)
        {
            //cbowaitor.DroppedDown = true;
        }

        private void cboqty_Enter(object sender, EventArgs e)
        {
          //  cboqty.DroppedDown = true;
        }

        private void cbokotremark_Enter(object sender, EventArgs e)
        {
           // cbokotremark.DroppedDown = true;
        }

        private void combodepartment_Enter(object sender, EventArgs e)
        {
           // combodepartment.DroppedDown = true;
        }




        public void InsertCustomerById()
        {

            try
            {


                    bl_feild.TableNoS = cbotable.Text;
                    if (comboseat.Text != "")
                    {
                        bl_feild.Seat = comboseat.Text;
                    }
                    else
                    {
                        bl_feild.Seat = "A";
                    }
                    bl_sale.ParsalToCustomer(bl_feild);
                    SelectCustomer();
              

            }
            catch
            {
                MessageBox.Show("Error occured Please Contact Administrator");

            }
        }

        private void SelectCustomer()
        {
            bl_feild.TableNoS = cbotable.Text;
            if (comboseat.Text != "")
            {
                bl_feild.Seat = comboseat.Text;
            }
            else {
                bl_feild.Seat = "A"; 
            }
            DataTable dt = bl_customer.SelectCustomerById(bl_feild);
            if (dt.Rows.Count > 0)
            {
                pnlcustomername.Visible = true;
                lblCname.Text = dt.Rows[0]["Name"].ToString();
                lblCMobileNo.Text = dt.Rows[0]["MobileNo"].ToString();
                lblCAddress.Text = dt.Rows[0]["Address"].ToString();
                lblSociety.Text = dt.Rows[0]["Society"].ToString();
                label13.Text = dt.Rows[0]["FlatName"].ToString();

            }
            else
            {
                pnlcustomername.Visible = false;
            }
        }

        private void btnshifting_Click(object sender, EventArgs e)
        {
            Shifting shi = new Shifting(this);
            shi.ShowDialog();
        }

        private void btnmerge_Click(object sender, EventArgs e)
        {
            Mearg m = new Mearg(this);
            m.ShowDialog();
            
        }

        private void btndone_Click(object sender, EventArgs e)
        {
            if (chkcounter.Checked == true)
            {
                if (grdOrder.Rows.Count > 0)
                {
                    DialogResult msgg = MessageBox.Show("Are you sure you want to Counter Bill?", "Counter", MessageBoxButtons.YesNo);
                    if (msgg == DialogResult.Yes)
                    {

                        DataSet dsCounter = new DataSet();

                        bl_feild.Status = "Open";
                        bl_feild.TableNoPrintBill = cbotable.Text;
                        bl_feild.Time = DateTime.Now.ToString("hh:mm");


                        bl_feild.KotOrderNo = Convert.ToString(common.KotNo());

                        bl_sale.UpdateQt(bl_feild);


                        dsCounter = bl_sale.SelectRestoPrint(bl_feild);


                        foreach (DataRow dt in dsCounter.Tables[0].Rows)
                        {
                            SubCatidToPrint.Add(Convert.ToInt32(dt["SubCatId"].ToString()), Convert.ToInt32(dt["Qty"].ToString()));
                        }


                        int billno = common.ReceiptNo();
                        bl_feild.BillNo = Convert.ToString(billno);
                        bl_feild.CashierName = lblcashierName.Text;
                        bl_feild.PayModeId = 10;
                        bl_feild.Name = cbowaitor.Text;
                        bl_feild.Date = dsCounter.Tables[0].Rows[0]["Date"].ToString();
                        string id = bl_sale.InsertBill(bl_feild);
                        bl_feild.OrderId = id;





                        bl_sale.CloseCounter(bl_feild);


                        StockMinus();
                        SubCatidToPrint.Clear();
                        BindItenName();
                        chkcounter.Checked = false;
                    }
                }
            }
            else
            {
                MessageBox.Show("Please Check Counter Option");
            }


        }

        private void btnmodify_Click(object sender, EventArgs e)
        {
            DialogResult msgg = MessageBox.Show("Are you sure you want to Modify Bill?", "Modify Bill", MessageBoxButtons.YesNo);
            if (msgg == DialogResult.Yes)
            {
                Password pw = new Password("ModifyBill");
                pw.ShowDialog();
            }
        }





        private void combodepartment_SelectedIndexChanged(object sender, EventArgs e)
        {


        }

        private void btnKotReprint_Click(object sender, EventArgs e)
        {
            ReprintKOT rk = new ReprintKOT();
            rk.ShowDialog();
        }

        private void btnbillreprint_Click(object sender, EventArgs e)
        {
            Sale s = new Sale();
            s.ShowDialog();
        }

        private void btnclose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDeliveryCharge_Click(object sender, EventArgs e)
        {
            string seat="";
            if (comboseat.Text!="")
            {
                seat=comboseat.Text;
            }
            else
            {
                seat="A";
            }
            DeliveryCharge dc = new DeliveryCharge(cbotable.Text, seat,this);
            dc.ShowDialog();

        }

        private void chkcounter_CheckedChanged(object sender, EventArgs e)
        {
            if (chkcounter.Checked == true)
            {
                cbotable.Text = "0";
                comboseat.Text = "0";
            }
            else
            {
                cbotable.Text = "0";
                comboseat.Text = "";
            }
            BindItenName();
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            ShowTable st = new ShowTable(this);
            st.ShowDialog();
        }







    }
}

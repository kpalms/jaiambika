﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using Restaurant.BL;


namespace Restaurant.PL
{
    public partial class PaymentMode : Form
    {

        BL_PaymentMode bl_paymentmode = new BL_PaymentMode();

        public PaymentMode()
        {
            InitializeComponent();
        }

        private void PaymentMode_Load(object sender, EventArgs e)
        {
            BindPaymentMode();
            btndelete.Enabled = false;
            
            btnUpdate.Enabled = false;
        }

     

        private void btnadd_Click(object sender, EventArgs e)
        {
            
                if (txtPayMode.Text == "")
                {
                    MessageBox.Show("Please Insert PaymentMode? ");
                    txtPayMode.Focus();
                   
                }
               
            else 
            {
                bl_paymentmode.PaymentMode = txtPayMode.Text;
                //bl_paymentmode.UpdatePaymentMode(bl_paymentmode);
                bl_paymentmode.InsertPaymentMode(bl_paymentmode);
                MessageBox.Show("Record Updated Successfully ");
                BindPaymentMode();
                txtPayMode.Focus();
                txtPayMode.Text = "";

            }
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure to delete selected Record?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                  
                    bl_paymentmode.DeletePaymentMode(bl_paymentmode);
                    MessageBox.Show("Record Deleted Successfully");
                    BindPaymentMode();
                  //  _main.BindCatagory();
                    txtPayMode.Text="";
                    
                    txtPayMode.Focus();
                    btndelete.Enabled = false;
                    btnUpdate.Enabled = false;
                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
     
        }

        private void BindPaymentMode()
        {
            DataTable dt = bl_paymentmode.SelectPaymentMode();
            dataGridView1.DataSource = dt;
            dataGridView1.Columns["PayModeId"].Visible = false;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
           

            bl_paymentmode.PayModeId = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells["PayModeId"].Value.ToString());
            txtPayMode.Text = dataGridView1.SelectedRows[0].Cells["PaymentMode"].Value.ToString();
            btndelete.Enabled = true;
            btnUpdate.Enabled = true;
            btnadd.Enabled = false;
        }

        private void PaymentMode_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void txtPayMode_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = Char.ToUpper(e.KeyChar);
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            txtPayMode.Text = "";
            btndelete.Enabled = false;
            btnUpdate.Enabled = false;
            btnadd.Enabled = true;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            
                {
                    bl_paymentmode.PaymentMode = txtPayMode.Text;
                    bl_paymentmode.UpdatePaymentMode(bl_paymentmode);
                    BindPaymentMode();
                    MessageBox.Show("Record Inserted Successfully ");
                    txtPayMode.Focus();
                    txtPayMode.Text = "";
                }

        }

    }
}

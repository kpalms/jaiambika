﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.Data.SqlClient;
using Restaurant.BL;

namespace Restaurant.PL
{
    public partial class Types : Form
    {

        BL_Field bl_field = new BL_Field();
        BL_Catagory bl_catagory = new BL_Catagory();
        AddSubCatagory _Subcatagory;
        public Types(AddSubCatagory subcat)
        {
            InitializeComponent();
            this.KeyPreview = true;
            _Subcatagory = subcat;
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            try
            {
                if (btnadd.Text == "Add")
                {
                    if (txtname.Text != "")
                    {
                       // bl_field.Type = txtname.Text;
                        bl_catagory.InsertType(bl_field);
                        txtname.Text = "";
                        MessageBox.Show("Record inserted Successfully");
                        BindType();

                    }
                    else
                    {
                        MessageBox.Show("Please enter Name?");
                    }
                }
                if (btnadd.Text == "Update")
                {
                   // bl_field.Type = txtname.Text;
                    bl_catagory.UpdateType(bl_field);
                    txtname.Text = "";
                    MessageBox.Show("Record Updated Successfully");
                    BindType();
                }
            }
            catch
            {
                MessageBox.Show("error occured!Please contact Administration?");

            }
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            txtname.Text = "";
            btnadd.Text = "Add";
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Catagory?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_catagory.DeleteType(bl_field);
                    txtname.Text = "";
                    MessageBox.Show("Record Delete Successfully");
                    BindType();

                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            bl_field.TypeId = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            txtname.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            btnadd.Text = "Update";
        }


        private void BindType()
        {
            DataTable dt = bl_catagory.SelectType();
            if (dt.Rows.Count > 0)
            {
                dataGridView1.DataSource = dt;
                dataGridView1.Columns["TypeId"].Visible = false;
                dataGridView1.Columns["Status"].Visible = false;
            }
            else
            {
                dataGridView1.DataSource = null;
            }
        }

        private void Type_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void Type_Load(object sender, EventArgs e)
        {
            BindType();
        }
    }
}

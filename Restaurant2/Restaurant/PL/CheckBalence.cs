﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using Restaurant.PL.Reports;
using System.Globalization;
using System.Data;

namespace Restaurant.PL
{
    public partial class CheckBalence : Form
    {


        BL_Installment blInstallment = new BL_Installment();

        public CheckBalence()
        {
            InitializeComponent();
        }

        private void CheckBalence_Load(object sender, EventArgs e)
        {
          DataTable dt=  blInstallment.SelectBalence();
          grdinstallment.DataSource = dt;
          grdinstallment.Columns["CustId"].Visible = false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;


namespace Restaurant.PL
{
    public partial class DeliveryCharge : Form
    {
        BL_Sale bl_sale = new BL_Sale();
        BL_Field bl_feild = new BL_Field();
        main main;
        public DeliveryCharge(string tableno,string seat,main _main)
        {
            InitializeComponent();
            main = _main;
            bl_feild.TableNoS = tableno;
            bl_feild.Seat = seat;
        }

        private void btnDelivery_Click(object sender, EventArgs e)
        {
            if (txtprice.Text != "")
            {
                bl_feild.DeliveryCharge = txtprice.Text;
                bl_sale.DeleveryCharge(bl_feild);
                main.PageRefreshEvent();
                txtprice.Text = "";
                this.Close();
            }
            else
            {
                MessageBox.Show("please enter Rate");
            }

        }

        private void txtprice_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Common cm = new Common();
            //cm.NumberOnly(e);
            Common common = new Common();
            common.OneDecimalPoint(txtprice, e);
        }
    }
}

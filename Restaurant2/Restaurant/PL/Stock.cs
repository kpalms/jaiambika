﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using Restaurant.BL;
using Restaurant.DL;

    
namespace Restaurant.PL
{
    public partial class Stock : Form
    {
        BL_Field bl_field = new BL_Field();
        BL_NewItem bl_newitem = new BL_NewItem();
        Common common = new Common();

        public Stock()
        {
            InitializeComponent();
            this.KeyPreview = true;
        }

        private void Stock_Load(object sender, EventArgs e)
        {
            btndelete.Enabled = false;
            btnUpdate.Enabled = false;
            BindUnit();
            BindStock();
            BindItemName();
            cboitemname.Focus();
        }

        

        private void BindStock()
        {
            DataTable dt = bl_newitem.SelectStock();
            dataGridView1.DataSource = dt;
            dataGridView1.Columns["StockId"].Visible = false;

        }

        private void BindItemName()
        {
            DataTable dt = bl_newitem.SearchNewItemAll();
            DataRow row = dt.NewRow();
            row["ItemNames"] = "Select";
            dt.Rows.InsertAt(row, 0);
            cboitemname.DataSource = dt;
            cboitemname.DisplayMember = "ItemNames";
            cboitemname.ValueMember = "ItemId";

        }


        private void BindUnit()
        {

            common.BindUnitName(ddlunit);
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
           
            try
            {
                if (btnadd.Text == "Save")
                {
                    if (cboitemname.Text == "Select")
                    {
                        MessageBox.Show("Please Select the Item Name");
                        cboitemname.Focus();
                    }
                    else if (ddlunit.Text == "Select")
                    {
                        MessageBox.Show("Please Select the Unit");
                        ddlunit.Focus();
                    }
                    else if (txtStock.Text == "")
                    {
                        MessageBox.Show("Please Enter the Stock");
                        txtStock.Focus();
                    }
                     else if (Convert.ToDouble(txtStock.Text) == 0)                      
                     {
                         MessageBox.Show("Please Enter Valid Stock Value");
                         txtStock.Focus();
                     }
                    else if (txtminQty.Text == "")
                    {
                        MessageBox.Show("Please Enter the Minimum Stock value");
                        txtminQty.Focus();
                    }
                    else
                    {
                        bl_field.ItemId = cboitemname.SelectedValue.ToString();
                        string id = bl_newitem.CheckItemId(bl_field);
                        if (id == "")
                        {

                            SetField();
                            bl_newitem.InsertStock(bl_field);
                            MessageBox.Show("Record inserted Successfully");
                            BindStock();
                            Clear();
                        }

                        else
                        {

                            MessageBox.Show("This Item already exist!");
                        }
                    }
                    
                }
                

            }
                 
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
            
        }


        private void SetField()
        {
            bl_field.ItemId = cboitemname.SelectedValue.ToString();
           // bl_field.GroupId = ddlgroup.SelectedValue.ToString();
            bl_field.UnitId = ddlunit.SelectedValue.ToString();
            bl_field.Stock = txtStock.Text;
            bl_field.MinQty = txtminQty.Text;
            bl_field.Closing = txtStock.Text;
        }

        private void Clear()
        {
            cboitemname.Text = "Select";
            ddlunit.Text = "Select";
            txtStock.Text = "";
            txtminQty.Text = "";
            btnUpdate.Enabled = false;
            btndelete.Enabled = false;
            btnadd.Enabled=true;
        }
        private void btncancel_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Stock?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_field.StockId = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[0].Value.ToString());
                    bl_newitem.DeleteStock(bl_field);
                    MessageBox.Show("Record Delete Successfully");
                    BindStock();
                    Clear();
                }
            }
            catch
            {
                MessageBox.Show("error occured!Please contact Administration?");
            }
        }

        private void Stock_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void grditemName_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

       // private void txtitemname_TextChanged(object sender, EventArgs e)
       // {
            //if (cboitemname.Text != "Select")
            //{
            //    bl_field.TextSearch = txtitemname.Text;
            //    DataTable dt = bl_newitem.SearchNewItemBySearch(bl_field);
            //    if (dt.Rows.Count > 0)
            //    {
            //        grditemName.Visible = true;
            //        grditemName.DataSource = dt;
            //        grditemName.Columns["ItemId"].Visible = false;
            //        grditemName.Columns["Rate"].Visible = false;

            //    }
            //}
            //else
            //{
            //    DataTable dt = bl_newitem.SearchNewItemAll();
            //    if (dt.Rows.Count > 0)
            //    {
            //        grditemName.Visible = true;
            //        grditemName.DataSource = dt;
            //        grditemName.Columns["ItemId"].Visible = false;
            //        grditemName.Columns["Rate"].Visible = false;
            //        //grditemName.Columns["Inventory"].Visible = false;
            //        //grditemName.Columns["PrimeryId"].Visible = false;
            //        //grditemName.Columns["SeconderyId"].Visible = false;
            //        //grditemName.Columns["ConversionUnit"].Visible = false;

            //    }
            //}
     //   }

        //private void txtitemname_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyCode == Keys.Enter)
        //    {

        //        if (grditemName.Rows.Count > 0)
        //        {

        //            txtitemname.Text = grditemName.SelectedRows[0].Cells["ItemNames"].Value.ToString();
                   
        //            bl_field.ItemId = grditemName.SelectedRows[0].Cells["ItemId"].Value.ToString();

                  
        //            grditemName.Hide();
   

        //        }
        //        else
        //        {

        //            grditemName.Visible = false;
        //            txtitemname.Focus();
        //        }

        //    }

        //    if (e.KeyCode == Keys.Up)
        //    {
        //        DataTable dtTemp = grditemName.DataSource as DataTable;


        //        object[] arr = dtTemp.Rows[dtTemp.Rows.Count - 1].ItemArray;
        //        for (int i = dtTemp.Rows.Count - 2; i >= 0; i--)
        //        {
        //            dtTemp.Rows[i + 1].ItemArray = dtTemp.Rows[i].ItemArray;
        //        }
        //        dtTemp.Rows[0].ItemArray = arr;



        //    }

        //    if (e.KeyCode == Keys.Down)
        //    {

        //        DataTable dtTemp = grditemName.DataSource as DataTable;

        //        object[] arr = dtTemp.Rows[0].ItemArray;
        //        for (int i = 1; i < dtTemp.Rows.Count; i++)
        //        {
        //            dtTemp.Rows[i - 1].ItemArray = dtTemp.Rows[i].ItemArray;
        //        }
        //        dtTemp.Rows[dtTemp.Rows.Count - 1].ItemArray = arr;



        //    }
        //}

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Rows.Count>0)
            {
                cboitemname.Text = dataGridView1.SelectedRows[0].Cells["ItemNames"].Value.ToString();
                //grditemName.Visible = false;
               // ddlgroup.Text = dataGridView1.SelectedRows[0].Cells["GroupName"].Value.ToString();              
                ddlunit.Text = dataGridView1.SelectedRows[0].Cells["UnitName"].Value.ToString();
                txtStock.Text = dataGridView1.SelectedRows[0].Cells["Stock"].Value.ToString();
                txtminQty.Text = dataGridView1.SelectedRows[0].Cells["MinQty"].Value.ToString();
               
            }
            btndelete.Enabled = true;
            btnUpdate.Enabled=true;
            btnadd.Enabled = false;
        }

        private void cboitemname_KeyPress(object sender, KeyPressEventArgs e)
        {
            common.ComboBoxCheckValue(cboitemname, e);
        }

        private void txtStock_KeyPress(object sender, KeyPressEventArgs e)
        {
            common.OneDecimalPoint(txtStock, e);
        }

        private void txtminQty_KeyPress(object sender, KeyPressEventArgs e)
        {
            common.NumberOnly(e);

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (cboitemname.Text == "Select")
                {
                    MessageBox.Show("Please Select the Item Name");
                    cboitemname.Focus();
                }
                else if (ddlunit.Text == "Select")
                {
                    MessageBox.Show("Please Select the Unit");
                    ddlunit.Focus();
                }
                else if (txtStock.Text == "")
                {
                    MessageBox.Show("Please Enter the Stock");
                    txtStock.Focus();
                }
                else if (Convert.ToDouble(txtStock.Text) == 0)
                {
                    MessageBox.Show("Please Enter Valid Stock Value");
                    txtStock.Focus();
                }
                else if (txtminQty.Text == "")
                {
                    MessageBox.Show("Please Enter the Minimum Stock value");
                    txtminQty.Focus();
                }
                else
                {

                    SetField();
                    bl_field.StockId = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells["StockId"].Value.ToString());
                    bl_newitem.UpdateStock(bl_field);
                    MessageBox.Show("Record Updated Successfully");
                    BindStock();
                    Clear();
                }

            }
            catch
            {
                   MessageBox.Show("error occured!Please contact Administration?");
            }
        }

        private void cboitemname_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ddlunit.Focus();

            }
        }

        private void ddlunit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtStock.Focus();

            }
        }

        private void txtminQty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnadd.Focus();

            }
        }

        private void ddlunit_KeyPress(object sender, KeyPressEventArgs e)
        {
            common.ComboBoxCheckValue(ddlunit, e);
        }

        private void txtStock_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtminQty.Focus();

            }
        }
    }
}

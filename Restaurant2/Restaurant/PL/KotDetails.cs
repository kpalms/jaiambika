﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Restaurant.BL;
namespace Restaurant.PL
{
    public partial class KotDetails : Form
    {
        public KotDetails()
        {
            InitializeComponent();
        }
        BL_Worker bl_worker = new BL_Worker();
        BL_Table bl_table = new BL_Table();
      
     // DataGridView bb = new DataGridView();
        private void dgvkotdetails_CellClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }
        private void KotDetails_Load(object sender, EventArgs e)
        {
            BindItemQuantity();
            BindKot();
           // Binddemo();
        }

        private void BindItemQuantity()
        {
            DataTable dt = bl_worker.SelectItemQuantity();
            dgvsumquantity.DataSource = dt;
        }



        private void BindKot()
        {

            try
            {

                DataTable dt = bl_table.CheckTableKot();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataGridView grid = new DataGridView();
                    Label lbl = new Label();


                    grid.Name = dt.Rows[i]["TableNo"].ToString();
                    grid.Size = new Size(300, 130);
                    grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
                    grid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
                    grid.ReadOnly = true;
                    grid.AllowUserToAddRows = false;
                    grid.AllowUserToDeleteRows = false;
                    grid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(253)))), ((int)(((byte)(244)))));
              

                    lbl.Text = dt.Rows[i]["TableNo"].ToString();
                    lbl.Font = new Font("Microsoft Sans Serif",15);
                    lbl.Size = new Size(40, 30);


                 
                    DataTable dtt = bl_worker.SelectKotDetails(dt.Rows[i]["TableNo"].ToString());
                   

                    grid.DataSource = dtt;



                    if (dtt.Rows.Count > 0)
                    {
                        this.flowLayoutPanel1.Controls.Add(lbl);
                        this.flowLayoutPanel1.Controls.Add(grid);



                        grid.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grid_CellDoubleClick);

                        DataGridViewButtonColumn btn = new DataGridViewButtonColumn();
                        grid.Columns.Add(btn);
                        btn.HeaderText = "Complated";
                        btn.Text = "Complated";
                        btn.UseColumnTextForButtonValue = true;
                     

                        grid.Columns["ItemName"].Width = 150;
                        grid.Columns["Qty"].Width = 20;
                        grid.Columns["OrderDetailId"].Visible = false;
                        grid.RowHeadersVisible = false;
                        grid.EnableHeadersVisualStyles = false;
                        grid.MultiSelect = false;
                    }


                  
                 }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }




        }

        void grid_CellDoubleClick(object sender, EventArgs e)
        {
            DataGridView btn = (DataGridView)sender;
            string OrderDetailId = btn.SelectedRows[0].Cells["OrderDetailId"].Value.ToString();
            bl_worker.UpdateKotDetails(OrderDetailId);
            flowLayoutPanel1.Controls.Clear();
            BindKot();
            BindItemQuantity();
        }



    }
}

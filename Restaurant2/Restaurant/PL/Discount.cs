﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;

namespace Restaurant.PL
{
    public partial class Discount : Form
    {
        BL_Field bl_field = new BL_Field();
        BL_Sale bl_sale = new BL_Sale();
        main _main;
        public Discount(main mainn)
        {
            InitializeComponent();
            this.KeyPreview = true;
            _main = mainn;
            
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            //try
            //{
            //    if (txtdis.Text != "")
            //    {
            //        if (listBox1.SelectedIndex == 0)
            //        {
            //            bl_field.Dis = txtdis.Text + "-Per";
            //        }
            //        else
            //        {
            //            bl_field.Dis = txtdis.Text + "-Rs";
            //        }

            //        bl_sale.ChangeDis(bl_field);
            //        this.Hide();
            //        _main.PageRefreshEvent();
            //    }
            //    else
            //    {
            //        MessageBox.Show("Please select Option");
            //    }
            //}
            //catch
            //{

            //    MessageBox.Show("error occured!Please contact Administration?");
            //}

            discount();

 
        }

        private void txtdis_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            //{
            //    e.Handled = true;
             
            //}
            Common common = new Common();
            common.OneDecimalPoint(txtdis, e);
        }

        private void Discount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
            if (e.KeyCode == Keys.Enter)
            {
                discount();
               // this.Close();
            }
        }

        private void Discount_Load(object sender, EventArgs e)
        {

        }




        private void discount()
        {
            if(listBox1.SelectedIndex <0)
            {
                MessageBox.Show("Please select Option");
                listBox1.Focus();
            }
            else if (txtdis.Text == "" || txtdis.Text == "0")
            {
                MessageBox.Show("Please Enter Discount!");
                txtdis.Focus();

            }
            else
            {
                if (listBox1.SelectedIndex == 0)
                {
                    bl_field.DiscountStatic = txtdis.Text + "-Per";
                }
                else
                {
                    bl_field.DiscountStatic = txtdis.Text + "-Rs";
                }
                this.Hide();
                _main.Discount();


            }
        
      
           

        
        }

        //private void discount()
        //{
        //    //this.Hide();
        //    //_main.Discount();

        //    if (txtdis.Text != "" )
        //    {
        //        if (listBox1.SelectedIndex == 0)
        //        {
        //            bl_field.DiscountStatic = txtdis.Text + "-Per";
        //        }
        //        else
        //        {
        //            bl_field.DiscountStatic = txtdis.Text + "-Rs";
        //        }
        //        this.Hide();
        //        _main.Discount();

        //    }
        //    else if(listBox1.SelectedIndex <0)
        //    {
        //        MessageBox.Show("Please select Option");
        //        listBox1.Focus();
        //    }

        //    else if (txtdis.Text == "" && txtdis.Text == "0")
        //    {
        //        MessageBox.Show("Please Enter Discount!");
        //        txtdis.Focus();
        //    }
          
           
        //}
    }
}

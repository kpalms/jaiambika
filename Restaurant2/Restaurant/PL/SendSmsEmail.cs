﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Collections;

using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using Restaurant.PL.Reports;




namespace Restaurant.PL
{
    public partial class SendSmsEmail : Form
    {
        BL_Customer bl_customer = new BL_Customer();
        BL_Field bl_field = new BL_Field();
        CheckBox chkbox = new CheckBox();


        List<string> list = new List<string>();
        EmailSms email = new EmailSms();
        public SendSmsEmail()
        {
            InitializeComponent();
        }

        private void btnsend_Click(object sender, EventArgs e)
        {

            ArrayList arrMobileNo = new ArrayList();
            ArrayList arrEmail = new ArrayList();
            DataTable dt = bl_customer.SelectCustomer();

            try
            {


                if (chksms.Checked != false || chkemail.Checked != false)
                {

   
                    if (dt != null)

                    {
                        for (int i = 0; i <= dt.Rows.Count - 1; i++)
                        {

                            if (Convert.ToBoolean(grdcustomer.Rows[i].Cells["chkcol"].Value) == true)
                            {
                                if (dt.Rows[i]["MobileNo"].ToString() != "")
                                {
                                    arrMobileNo.Add(dt.Rows[i]["MobileNo"].ToString());
                                }
                                if (dt.Rows[i]["Email"].ToString() != "")
                                {
                                    arrEmail.Add(dt.Rows[i]["Email"].ToString());
                                }

                            }

                        }
                        if (arrMobileNo.Count != 0 )
                        {

                            if (chksms.Checked == true)
                            {
                                string no = string.Join(",", (string[])arrMobileNo.ToArray(Type.GetType("System.String")));

                                EmailSms.SendSms(no, txtbody.Text);
                                MessageBox.Show("Sms Send Successfully");
                            }


                       }
                        else
                        {
                            MessageBox.Show("Please select number");
                        }


                        if (arrEmail.Count != 0)
                        {
                            if (chkemail.Checked == true)
                            {

                                email.SendMail(txtsubject.Text, txtbody.Text, arrEmail);
                                MessageBox.Show("Email Send Successfully");

                            }

                        }
                        else
                        {
                            MessageBox.Show("Please select email");
                        }


                    }
                    else
                    {
                        MessageBox.Show("Please Import Number");
                    }

                }
                else
                {
                    MessageBox.Show("Please tick sms/email checkbox");
                }

            }
            catch (Exception ex)
            {

               // throw ex;
            }

        }
        private void BindCustomer()
        {
            grdcustomer.AllowUserToAddRows = false;
            grdcustomer.Columns.Clear();
            DataGridViewCheckBoxColumn colCB = new DataGridViewCheckBoxColumn();
            colCB.Name = "chkcol";
            colCB.HeaderText = "";
            colCB.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grdcustomer.Columns.Add(colCB);
            Rectangle rect = this.grdcustomer.GetCellDisplayRectangle(0, -1, true);    
            chkbox.Size = new Size(18, 18);
            rect.Offset(40, 2);
            chkbox.Location = rect.Location;
            chkbox.CheckedChanged += chkBoxChange;
            this.grdcustomer.Controls.Add(chkbox);

            DataTable dt = bl_customer.SelectCustomer();
            grdcustomer.DataSource = dt;
            grdcustomer.Columns["CustId"].Visible = false;
            grdcustomer.Columns["Status"].Visible = false;
        }
        private void chkBoxChange(object sender, EventArgs e)
        {
            for (int k = 0; k <= grdcustomer.RowCount - 1; k++)
            {
                this.grdcustomer[0, k].Value = this.chkbox.Checked;

                            
            }
            this.grdcustomer.EndEdit();
        }


        private void SendSmsEmail_Load(object sender, EventArgs e)
        {
            BindCustomer();
        }

        private void txtsearchMobile_TextChanged(object sender, EventArgs e)
        {
            if (txtsearchMobile.Text != "")
            {
               
                    bl_field.TextSearch = txtsearchMobile.Text;
                    DataTable dt = bl_customer.SelectCustomerMobileNo(bl_field);
                    if (dt.Rows.Count > 0)
                    {
                        grdcustomer.DataSource = dt;

                    }
                    else
                    {
                        grdcustomer.DataSource = null;
                    }
            

            }
            else
            {
                BindCustomer();
            }
        }

        private void txtsearchName_TextChanged(object sender, EventArgs e)
        {
            if (txtsearchName.Text != "")
            {
                
                    bl_field.TextSearch = txtsearchName.Text;
                    DataTable dt = bl_customer.SelectCustomerName(bl_field);
                    if (dt.Rows.Count > 0)
                    {
                        grdcustomer.DataSource = dt;

                    }
                    else
                    {
                        grdcustomer.DataSource = null;
                    }
               

            }
            else
            {
                BindCustomer();
            }
        }

        private void SendSmsEmail_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }






    }
}

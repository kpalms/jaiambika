﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Globalization;


namespace Restaurant.PL
{

    public partial class Installment : Form
    {


        BL_Installment blInstallment = new BL_Installment();
        BL_Sale bl_sale = new BL_Sale();
        BL_Customer bl_customer = new BL_Customer();
        Common common = new Common();


        public Installment()
        {
            InitializeComponent();
        }


        private void Installment_Load(object sender, EventArgs e)
        {
            dtpSaleDate.CustomFormat = "dd/MM/yyyy";

            btnupdate.Enabled = false;
            btndelete.Enabled = false;
            BindCustomerName();


        }

       

        private void BindInstallment()
        {
            if (cbocustomer.Text != "Select")
            {
                bl_customer.CustomerIdStatic = Convert.ToInt32(cbocustomer.SelectedValue.ToString());
                DataTable dt = blInstallment.SelectInstallment(bl_customer);

                grdinstallment.DataSource = dt;

                grdinstallment.Columns["InstId"].Visible = false;
                grdinstallment.Columns["CustId"].Visible = false;
                decimal Debittot = 0;
                decimal Credittot = 0;
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Credittot = Credittot + Convert.ToDecimal(grdinstallment.Rows[i].Cells["Credit"].Value);
                        Debittot = Debittot + Convert.ToDecimal(grdinstallment.Rows[i].Cells["Debit"].Value);
                    }
                    lbltotalamt.Text = Convert.ToString(Debittot);
                    lblpaidamount.Text = Convert.ToString(Credittot);

                    lblramount.Text = Convert.ToString(Convert.ToDecimal(lbltotalamt.Text) - Convert.ToDecimal(lblpaidamount.Text));
                }
                else
                {
                    lbltotalamt.Text = "0.00";
                    lblpaidamount.Text = "0.00";
                    lblramount.Text = "0.00";
                }
            }

        }
        //private void BindSaleDetails()
        //{
        //    DataTable dt = blInstallment.GetSaleDetails(bl_customer);
        //    grdsaledata.DataSource = dt;
         
        //    decimal tot = 0;
        //    if (dt.Rows.Count > 0)
        //    {
        //        for (int i = 0; i < dt.Rows.Count; i++)
        //        {
        //            tot = tot + Convert.ToDecimal(grdsaledata.Rows[i].Cells["Total"].Value);
        //        }
        //        lbltotalamt.Text=Convert.ToString(tot);
        //    }

        //}



        private void btnPrintBill_Click(object sender, EventArgs e)
        {
            if (cbocustomer.Text == "Select")
            {
                MessageBox.Show("Please enter customer name");
                cbocustomer.Focus();
            }
            else if (txtreceiptno.Text == "")
            {
                MessageBox.Show("Please enter Receipt No");
                txtreceiptno.Focus();
            }
            else if (txtamount.Text == "")
            {
                MessageBox.Show("Please enter Installment Amount");
                txtamount.Focus();
            }
             
            else
            {
                SetField();
                blInstallment.ReceiptNo = Convert.ToInt32(txtreceiptno.Text);
                blInstallment.InsertInstallment(blInstallment);

                MessageBox.Show("Record save successfully?");
                Clear();
                BindInstallment();
                
            }

        }


           

 


        private void Calculation()
        {
            decimal total = 0;
            decimal subtotal = 0;
            decimal FinalTotal = 0;
            decimal vatamt = 0;
            decimal vatamount = 0;
             decimal amtTotal = 0;


            FinalTotal = subtotal + vatamount;

            for (int i = 0; i < grdinstallment.Rows.Count; i++)
            {
                amtTotal = amtTotal + Convert.ToDecimal(grdinstallment.Rows[i].Cells["Amount"].Value);
            }
            decimal amount=Convert.ToDecimal(txtamount.Text);
            decimal totalamt = 0;
            totalamt=amtTotal+amount;
            if (FinalTotal == totalamt)
            {
               blInstallment.InstStatus = "Clear";
               blInstallment.NextInstDate = "-";

            }
            else
            {
                blInstallment.InstStatus = "Installment";
            }

            
         }


        private void Clear()
        {

             txtamount.Text = "";
            txtreceiptno.Text = "";
            dtpSaleDate.Text = "";

            btnPrintBill.Enabled = true;
            btnupdate.Enabled = false;
            btndelete.Enabled = false;

        }

        private void SetField()
        {
            blInstallment.ReceiptNo = Convert.ToInt32(txtreceiptno.Text);
            blInstallment.PayDate = dtpSaleDate.Text;
            blInstallment.Credit =Convert.ToDecimal(txtamount.Text);
            blInstallment.CustomerIdStatic = cbocustomer.SelectedValue.ToString();
        }

        private void grdinstallment_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (grdinstallment.Rows.Count > 0)
            {

                txtamount.Text = grdinstallment.SelectedRows[0].Cells["Credit"].Value.ToString();
                txtreceiptno.Text = grdinstallment.SelectedRows[0].Cells["ReceiptNo"].Value.ToString();
              
                string d = grdinstallment.SelectedRows[0].Cells["Date"].Value.ToString();
                DateTime date = DateTime.ParseExact(d, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                dtpSaleDate.Value = date;
           
                btnupdate.Enabled = true;

                btnPrintBill.Enabled = false;

                btndelete.Enabled = true;
                blInstallment.InstId = Convert.ToInt32(grdinstallment.SelectedRows[0].Cells["InstId"].Value.ToString());

            }
        }

        private void btnupdate_Click(object sender, EventArgs e)
        {
            
            if (txtreceiptno.Text == "")
            {
                MessageBox.Show("Please enter Receipt No");
            }
            else if (txtamount.Text == "")
            {
                MessageBox.Show("Please enter Installment Amount");
            }
             else
            {
                SetField();
                blInstallment.InstallmentUpdate(blInstallment);
                MessageBox.Show("Records updated");
                BindInstallment();
                btnPrintBill.Enabled = true;
                btnupdate.Enabled = false;
                btndelete.Enabled = true;
                Clear();
                BindInstallment();
             }
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            if (grdinstallment.Rows.Count > 0)
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Record?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    blInstallment.InstallmentDelete(blInstallment);

                    MessageBox.Show("Record Deleted successfully");
                    Clear();
                    BindInstallment();
                    btndelete.Enabled = false;
                    btnupdate.Enabled = false;
                }
            }
        }

        private void btnclear_Click(object sender, EventArgs e)
        {
            Clear();
            btndelete.Enabled = false;
        }

        private void Installment_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }

        }

        private void cmbbill_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode==Keys.Enter)
            {
                txtreceiptno.Focus();
            }
        }

        private void txtreceiptno_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
            }

        }

        private void grdSale_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtamount.Focus();
            }


        }

        private void txtamount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnPrintBill.Focus();
            }

        }

        private void txtamount_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtreceiptno_TextChanged(object sender, EventArgs e)
        {
          
        }

        private void txtreceiptno_KeyPress(object sender, KeyPressEventArgs e)
        {
            //common.NumberOnly(e);
        }

        private void txtamount_KeyPress(object sender, KeyPressEventArgs e)
        {
            common.OneDecimalPoint(txtamount, e);
         }

       private void BindCustomerName()
        {
            // DataTable dt = bl_sale.SelectCustomerAll();

            //DataRow row = dt.NewRow();
            //row["Name"] = "Select";
            //dt.Rows.InsertAt(row, 0);

            //cbocustomer.DataSource = dt;
            //cbocustomer.DisplayMember = "Name";
            //cbocustomer.ValueMember = "CustId";
            common.BindCustomerCombo(cbocustomer);

      
        }

        private void cbocustomer_KeyPress(object sender, KeyPressEventArgs e)
        {
            common.ComboBoxCheckValue(cbocustomer,e);
        }

        private void cbocustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbocustomer.Text != "System.Data.DataRowView")
            {
                if (cbocustomer.ValueMember == "CustId")
                {
                    BindInstallment();
                }
            }

        }






    }
}

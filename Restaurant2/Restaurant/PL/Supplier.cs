﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using Restaurant.PL.Reports;

namespace Restaurant.PL
{
    public partial class Supplier : Form
    {
        BL_Field bl_field = new BL_Field();
        BL_Vendor bl_vendor = new BL_Vendor();

        public Supplier()
        {
            InitializeComponent();
            this.KeyPreview = true;
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            try
            {
                string email = txtemail.Text;
                if (txtname.Text == "")
                {
                     MessageBox.Show("Please enter Name?");
                }
                else if (txtphoneno.Text != "" && txtphoneno.Text.Length < 10)
                {
                    MessageBox.Show("Invalid Phone No.");
                    txtphoneno.Focus();

                }
                else if (txtmobileno.Text != "" && txtmobileno.Text.Length < 10)
                {
                    MessageBox.Show("Invalid Mobile No.");
                    txtmobileno.Focus();

                }
                else if (txtemail.Text != "" && (email.IndexOf('@') == -1 || email.IndexOf('.') == -1))
                {                      
                        MessageBox.Show("Invalid Email Id");
                        txtemail.Focus();
                    
                }
                     
                    else
                    {
                        SetField();
                        bl_vendor.InsertVendor(bl_field);
                        MessageBox.Show("Record inserted Successfully");
                        BindVendor();
                        Clear();
                    }
                }
                             
            catch
            {
                MessageBox.Show("error occured!Please contact Administration?");

            }
        }

        private void SetField()
        {
            bl_field.SupplierName = txtname.Text;
            bl_field.ContactPerson = txtcontactp.Text;
            bl_field.Country = txtcountry.Text;
            bl_field.State = txtstate.Text;
            bl_field.City = txtcity.Text;
            bl_field.Address = txtaddress.Text;
            bl_field.PhoneNo = txtphoneno.Text;
            bl_field.Mobile = txtmobileno.Text;
            bl_field.Email = txtemail.Text;
            bl_field.Note = txtnotes.Text;


        }

        private void Clear()
        {

            txtname.Text = "";
            txtcontactp.Text = "";
            txtcountry.Text = "";
            txtstate.Text = "";
            txtcity.Text = "";
            txtaddress.Text = "";
            txtphoneno.Text = "";
            txtmobileno.Text = "";
            txtemail.Text = "";
            txtnotes.Text = "";
           
        }
        private void btncancel_Click(object sender, EventArgs e)
        {
            Clear();
            btnUpdate.Enabled = false;
            btndelete.Enabled = false;
            btnadd.Enabled = true;
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure to delete selected Record?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_vendor.DeleteVendor(bl_field);
                    txtname.Text = "";
                    MessageBox.Show("Record Delete Successfully");
                    BindVendor();
                    Clear();
                    btnUpdate.Enabled = false;
                    btndelete.Enabled = false;
                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
        }

        private void Supplier_Load(object sender, EventArgs e)
        {
            BindVendor();
            btndelete.Enabled = false;
            btnUpdate.Enabled = false;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            bl_field.SupplierId = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            txtname.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            txtcontactp.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
            txtcountry.Text = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
            txtstate.Text = dataGridView1.SelectedRows[0].Cells[4].Value.ToString();
            txtcity.Text = dataGridView1.SelectedRows[0].Cells[5].Value.ToString();
            txtaddress.Text = dataGridView1.SelectedRows[0].Cells[6].Value.ToString();
            txtphoneno.Text = dataGridView1.SelectedRows[0].Cells[7].Value.ToString();
            txtmobileno.Text = dataGridView1.SelectedRows[0].Cells[8].Value.ToString();
            txtemail.Text = dataGridView1.SelectedRows[0].Cells[9].Value.ToString();
            txtnotes.Text = dataGridView1.SelectedRows[0].Cells[10].Value.ToString();
            btnUpdate.Enabled = true;
            btndelete.Enabled = true;
            btnadd.Enabled = false;
        }

        private void BindVendor()
        {
            DataTable dt = bl_vendor.SelectVendor();
            if (dt.Rows.Count > 0)
            {
                dataGridView1.DataSource = dt;
                dataGridView1.Columns["SupplierId"].Visible = false;
                dataGridView1.Columns["Status"].Visible = false;
            }
            else
            {
                dataGridView1.DataSource = null;
            }
        }

        private void Supplier_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void txtphoneno_KeyPress(object sender, KeyPressEventArgs e)
        {
            Common common = new Common();
            common.NumberOnly(e);
            txtphoneno.MaxLength = 10;
        }

        private void txtmobileno_KeyPress(object sender, KeyPressEventArgs e)
        {
            Common common = new Common();
            common.NumberOnly(e);
            txtmobileno.MaxLength = 10;

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
                string email = txtemail.Text;
                if (txtname.Text == "")
                {
                    MessageBox.Show("Please Select Record");
                }
                else if (txtphoneno.Text != "" && txtphoneno.Text.Length < 10)
                {
                    MessageBox.Show("Invalid Phone No.");
                    txtphoneno.Focus();

                }
                else if (txtmobileno.Text != "" && txtmobileno.Text.Length < 10)
                {
                    MessageBox.Show("Invalid Mobile No.");
                    txtmobileno.Focus();

                }
                else if (txtemail.Text != "" && (email.IndexOf('@') == -1 || email.IndexOf('.') == -1))
                {
                    MessageBox.Show("Invalid Email Id");
                    txtemail.Focus();

                }
                else
                {
                    SetField();
                    bl_vendor.UpdateVendor(bl_field);
                    MessageBox.Show("Record Updated Successfully");
                    BindVendor();
                    Clear();
                }
                btnUpdate.Enabled = false;
                btndelete.Enabled = false;
               
        }

    }
}

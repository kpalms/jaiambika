﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using Restaurant.BL;
namespace Restaurant.PL
{
    public partial class DepartmentTable : Form
    {
        public DepartmentTable()
        {
            InitializeComponent();
        }
        BL_Worker bl_worker = new BL_Worker();
        int id;
        private void DepartmentTable_Load(object sender, EventArgs e)
        {
            BindDepartment();
            BindTABLE();
            Bind();
            btnupdate.Enabled = false;
            btndelete.Enabled = false;
        }

        private void BindDepartment()
        {
            DataTable dt = bl_worker.SelectDepartment();
            DataRow row = dt.NewRow();
            row["Department"] = "SELECT";
            dt.Rows.InsertAt(row, 0);
            combodepartment.DataSource = dt;
            combodepartment.DisplayMember = "Department";
            combodepartment.ValueMember = "DpId";
            

        }

        private void BindTABLE()
        {
            try
            {
                DataTable dt = bl_worker.SelectTable();
                combotable.DataSource = dt;
                combotable.DisplayMember = "TableNo";
                combotable.ValueMember = "TblId";
            }
            catch (Exception ex)
            {
                MessageBox.Show("erro"+ex);
            }


        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            if (combodepartment.Text == "SELECT")
            {
                MessageBox.Show("Please SELECT Department");
                combodepartment.Focus();

            }
            else if (combotable.Text == null)
            {
                MessageBox.Show("Please SELECT TABLE");
                combotable.Focus();

            }
            else
            {
                SetFill();
                string returnvalue = bl_worker.CheckDepartmenttable(bl_worker);
                 if (returnvalue != "")
                 {
                     MessageBox.Show("This record already exists!");
                    
                 }
                 else
                 {
                     bl_worker.DepartmentTableInsert(bl_worker);
                     Bind();
                     Clear();
                 }
            }
        }


        private void Bind()
        {
            DataTable dt = bl_worker.SelectDepartmentTable();
            dgvdepartmenttable.DataSource = dt;
            dgvdepartmenttable.Columns["DptId"].Visible = false;
        }

        private void dgvdepartmenttable_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgvdepartmenttable.Rows.Count > 0)
                {
                    id = Convert.ToInt32(dgvdepartmenttable.SelectedRows[0].Cells[0].Value.ToString());
                    combodepartment.Text = dgvdepartmenttable.SelectedRows[0].Cells[1].Value.ToString();
                    combotable.Text = dgvdepartmenttable.SelectedRows[0].Cells[2].Value.ToString();
                    btnsave.Enabled = false;
                    btnupdate.Enabled = true;
                    btndelete.Enabled = true;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("error occured!Please contact Administration?"+ex);
            }
        }

        private void btnupdate_Click(object sender, EventArgs e)
        {
            if (combodepartment.Text == "SELECT")
            {
                MessageBox.Show("Please SELECT Department");
                combodepartment.Focus();

            }
            else if (combotable.Text == "SELECT")
            {
                MessageBox.Show("Please SELECT TABLE");
                combotable.Focus();

            }
            else
            {
                string returnvalue = bl_worker.CheckDepartmenttable(bl_worker);
                if (returnvalue != "")
                {
                    MessageBox.Show("This record already exists!");

                }
                else
                {
                    SetFill();
                    bl_worker.DptId = id;
                    bl_worker.DepartmentTableUpdate(bl_worker);
                    Bind();
                    Clear();
                    btnsave.Enabled = true;
                    btnupdate.Enabled = false;
                    btndelete.Enabled = false;
                }
            }
        }

        private void SetFill()
        {
            bl_worker.DpId = Convert.ToInt32(combodepartment.SelectedValue.ToString());
            bl_worker.TableNo = Convert.ToInt32(combotable.Text);
        }

        private void Clear()
        {
            combotable.Text = null;
            combodepartment.Text = "SELECT";
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            Clear();
            btnupdate.Enabled = false;
            btndelete.Enabled = false;
            btnsave.Enabled = true;
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {

                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Department?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_worker.DptId = id;
                    bl_worker.DepartmentTableDelete(bl_worker);
                    Clear();
                    Bind();
                    btnsave.Enabled = true;
                    btnupdate.Enabled = false;
                    btndelete.Enabled = false;
                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
        }
    }
}

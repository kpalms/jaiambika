﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;



using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using Restaurant.PL.Reports;
using System.Collections;


namespace Restaurant.PL
{
    public partial class Home : Form
    {

        BL_Catagory bl_catagory = new BL_Catagory();
        BL_Table bl_table = new BL_Table();
        BL_Field bl_feild = new BL_Field();
        BL_Sale bl_sale = new BL_Sale();
        Common common = new Common();
        BL_Customer bl_customer = new BL_Customer();
        BL_Worker bl_worker = new BL_Worker();
        BL_NewItem bl_newitem = new BL_NewItem();
        CheckBox chkbox = new CheckBox();
        public static string LoginId = "";
        ArrayList SubCatidToPrint = new ArrayList();


        public Home()
        {
            InitializeComponent();
            this.KeyPreview = true;
        }

        private void saleStatusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Sale sale = new Sale();
            sale.ShowDialog();
        }

        private void hotelSettingToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            HotelSetting hs = new HotelSetting();
            hs.ShowDialog();
        }

        private void securityToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Password pw = new Password("Security");
            pw.Show();
        }

        private void kotReprintToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReprintKOT rk = new ReprintKOT();
            rk.Show();
            //try
            //{
            //    if (grdOrder.Rows.Count > 0)
            //    {

            //        DataSet ds = null;
            //        bl_feild.KOTPRINT = "yes";
            //        bl_feild.TableNoPrintBill = txttableno.Text;
            //        ds = bl_sale.RePrintQt(bl_feild);


            //        PrintKOT(ds);

            //    }
            //}
            //catch
            //{


            //}
        }

        private void billReprintToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Sale ss = new Sale();
            ss.ShowDialog();
        }

        private void shiftingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //string tableno = txttableno.Text;
            main mm = new main();
            Shifting sf = new Shifting(mm);
            sf.ShowDialog();
        }

        private void manageAccessLevelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Password pw = new Password("ManageAccess");
            pw.Show();
        }

        private void addCatagoryToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            AddCatagory addcatagory = new AddCatagory();
            addcatagory.ShowDialog();
        }

        private void addItemToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AddSubCatagory addsubcat = new AddSubCatagory();
            addsubcat.ShowDialog();
        }

        private void addTableToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AddTable add = new AddTable();
            add.ShowDialog();
        }

        private void addSupplierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Supplier sp = new Supplier();
            sp.ShowDialog();
        }

        private void workerToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Password pw = new Password("Worker");
            pw.Show();
        }

        private void messageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Message ff = new Message();
            ff.Show();
        }

        private void printerSettingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PrinterSetup ps = new PrinterSetup();
            ps.ShowDialog();
        }

        private void adminPartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Password pw = new Password("AdminPart");
            pw.Show();
        }

        private void menuAccessToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MenuHead mh = new MenuHead();
            mh.ShowDialog();
        }

        private void itemAccessToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ItemAccess ia = new ItemAccess();
            ia.ShowDialog();
        }

        private void sMSSettingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SMSSetting ss = new SMSSetting();
            ss.ShowDialog();
        }

        private void emailSettingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EmailSetting em = new EmailSetting();
            em.ShowDialog();
        }

        private void paymentModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PaymentMode paymod = new PaymentMode();
            paymod.ShowDialog();
        }

        private void purchasingToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Purchasing purchasing = new Purchasing(null);
            purchasing.ShowDialog();
        }

        private void wastageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Wastage ww = new Wastage();
            ww.ShowDialog();
        }

        private void expencessToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Expenses p = new Expenses();
            p.ShowDialog();
        }

        private void createUnitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Unit un = new Unit();
            un.ShowDialog();
        }



        private void createItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateItem ci = new CreateItem();
            ci.ShowDialog();
        }

        private void recipieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Recipie rc = new Recipie();
            rc.ShowDialog();
        }

        private void viewStockToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Stock st = new Stock();
            st.ShowDialog();
        }

        private void viewStockToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ViewStock vs = new ViewStock();
            vs.ShowDialog();
        }

        private void calculatorToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("calc");
        }

        private void notePadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("notepad");
        }

        private void sendSmsEmailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SendSmsEmail se = new SendSmsEmail();
            se.ShowDialog();
        }

        private void periodWiseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bl_feild.TypeStatic = "Restaurant";
            bl_feild.HeaderName = "Period Wise Reports Restaurant";
            Report2DateSelect report = new Report2DateSelect("PeriodWise");
            report.Show();
        }

        private void billWiseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bl_feild.TypeStatic = "Restaurant";
            bl_feild.HeaderName = "Bill Wise Reports Restaurant";
            BillWiseSelect report = new BillWiseSelect("BillWise");
            report.Show();
        }

        private void cashierWiseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bl_feild.TypeStatic = "Restaurant";
            bl_feild.HeaderName = "CashierWise Reports Restaurant";
            Report2DateSelect report = new Report2DateSelect("CashierWise");
            report.Show();
        }

        private void saleByItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bl_feild.TypeStatic = "Restaurant";
            bl_feild.HeaderName = "item Wise Reports Restaurant";
            Report2DateSelect report = new Report2DateSelect("salebyitem");
            report.Show();
        }

        private void billSummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bl_feild.HeaderName = "Restaurant Bill Summary Reports";
            bl_feild.TypeStatic = "Restaurant";
            Report2DateSelect report = new Report2DateSelect("BillSummary");
            report.Show();
        }

        private void billDetailsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            bl_feild.HeaderName = "BillDetailsReport";
            Report2DateSelect report = new Report2DateSelect("BillDetailsReport");
            report.Show();
        }

        private void customerHistoryToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            bl_feild.HeaderName = "CustomerHistory";
            SelectCustomer report = new SelectCustomer();
            report.Show();
        }

        private void discountReportToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            bl_feild.HeaderName = "DisCountReports";
            Report2DateSelect report = new Report2DateSelect("DisCountReports");
            report.Show();
        }

        private void parsalReportsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            bl_feild.HeaderName = "ParsalReports";
            Report2DateSelect report = new Report2DateSelect("ParsalReports");
            report.Show();
        }

        private void cancelItemListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Report2DateSelect report = new Report2DateSelect("CancelItem");
            report.Show();
        }

        private void cancelBillReportToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            bl_feild.HeaderName = "CancelBill";
            Report2DateSelect report = new Report2DateSelect("CancelBill");
            report.Show();
        }

        private void kotWiseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bl_feild.HeaderName = "KotWise Reports";
            bl_feild.TypeStatic = "Restaurant";
            Report2DateSelect report = new Report2DateSelect("KotWise");
            report.Show();
        }

        private void kotItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bl_feild.HeaderName = "KotWise Item Reports";
            bl_feild.TypeStatic = "Restaurant";
            Report2DateSelect report = new Report2DateSelect("KotWiseItem");
            report.Show();
        }

        private void todayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReportsViewer reportviewer = new ReportsViewer("today");
            reportviewer.ShowDialog();
        }

        private void yesturdayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReportsViewer reportviewer = new ReportsViewer("yesturday");
            reportviewer.ShowDialog();
        }

        private void saleByTableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Report2DateSelect report = new Report2DateSelect("salebyTable");
            report.Show();
        }

        private void itemPriceListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bl_feild.TypeStatic = "Restaurant";
            ReportsViewer report = new ReportsViewer("ItemPriceList");
            report.ShowDialog();
        }

        private void customerListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReportsViewer report = new ReportsViewer("CustomerList");
            report.ShowDialog();
        }

        private void purchasingReportsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bl_feild.HeaderName = "Purchasing/Expecess Reports";
            Report2DateSelect report = new Report2DateSelect("PurchasingExp");
            report.Show();
        }

        private void stockEntryReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bl_feild.HeaderName = "Purchasing Report";
            Report2DateSelect report = new Report2DateSelect("PurchasingReport");
            report.Show();
        }

        private void wastageReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bl_feild.HeaderName = "WastageReport";
            Report2DateSelect report = new Report2DateSelect("WastageReport");
            report.Show();
        }

        private void stockReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReportsViewer report = new ReportsViewer("StockReport");
            report.ShowDialog();
        }

        private void recipieReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReportsViewer report = new ReportsViewer("RecipieReport");
            report.ShowDialog();
        }

        private void purchasingReportToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            AddCatagory addcatagory = new AddCatagory();
            addcatagory.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            AddSubCatagory addsubcat = new AddSubCatagory();
            addsubcat.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            main m = new main();
            m.ShowDialog();
            

        }

        private void btnpurchase_Click(object sender, EventArgs e)
        {
            Purchasing purchasing = new Purchasing(null);
            purchasing.ShowDialog();
        }

        private void btnstock_Click(object sender, EventArgs e)
        {
            ViewStock vs = new ViewStock();
            vs.ShowDialog();
        }

        private void btncustomer_Click(object sender, EventArgs e)
        {
            AddTable at = new AddTable();
            at.ShowDialog();
        }

        private void btnAddProduct_Click(object sender, EventArgs e)
        {
            CreateItem ci = new CreateItem();
            ci.ShowDialog();
        }


        private void addKotRemarkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddKotRemark ak = new AddKotRemark();
            ak.ShowDialog();
        }

        private void purchaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PurchaseSecurity ps = new PurchaseSecurity();
            ps.ShowDialog();
        }

        private void customerPaymentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Installment inns = new Installment();
            inns.ShowDialog();
        }

        private void customerBalenceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CheckBalence cb = new CheckBalence();
            cb.ShowDialog();
        }

        private void addCustomerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            main mm = new main();
            Customer cm = new Customer(mm);
            cm.ShowDialog();
        }

        private void barItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bl_feild.TypeStatic = "Restaurant";
            bl_feild.HeaderName = "Bar item Wise Reports";
            Report2DateSelect report = new Report2DateSelect("salebyitemBar");
            report.Show();
        }

        private void periodWiseBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bl_feild.TypeStatic = "Restaurant";
            bl_feild.HeaderName = "Period Wise Reports Restaurant";
            Report2DateSelect report = new Report2DateSelect("PeriodWiseBar");
            report.Show();
        }

        private void salebyPaymentModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaleByPaymentMode s = new SaleByPaymentMode();
            s.ShowDialog();

        }

        private void saleSummeryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaleSummery ss = new SaleSummery();
            ss.ShowDialog();
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Application.Exit();
            this.Close();
            Login l = new Login();
            l.Show();

        }

        private void addWorkerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Worker w = new Worker();
            w.ShowDialog();
        }

        private void workerAttendancToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Attendanc a = new Attendanc();
            a.ShowDialog();
        }

        private void salleryCalculateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SalaryCalculator sc = new SalaryCalculator();
            sc.ShowDialog();
        }

        private void paymentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Payment p = new Payment();
            p.ShowDialog();
        }

        private void salaryHiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SalaryHistory sh = new SalaryHistory();
            sh.ShowDialog();
        }


        private void supplierAccountToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SupplierAccount sa = new SupplierAccount();
            sa.ShowDialog();
        }

        private void rateSlapToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void kotDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            KotDetails kd = new KotDetails();
            kd.ShowDialog();
        }

        private void rateCategaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RatCategary rc = new RatCategary();
            rc.ShowDialog();
        }

        private void departmentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Department dp = new Department();
            dp.ShowDialog();
        }

       

        private void rateAssignToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RateAssign ra = new RateAssign();
            ra.ShowDialog();
        }

        private void demoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RateSlap rs = new RateSlap();
            rs.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            KotDetails kd = new KotDetails();
            kd.ShowDialog();
        }

        private void demoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            demo dd = new demo();
            dd.ShowDialog();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void dayCloseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string TableOpen = common.ReturnOneValue("select status from OrderDetail where Status in ('Open','P')");
            if (TableOpen == "0")
            {
                Password p = new Password("DayClose");
                p.ShowDialog();
            }
            else
            {
                MessageBox.Show("Please Check Your Table First");
            }
        }

        private void dayOpenToolStripMenuItem_Click(object sender, EventArgs e)
        {

            string TableOpen = common.ReturnOneValue("select status from OrderDetail where Status in ('Open','P')");
            if (TableOpen == "0")
            {
                Password p = new Password("DayOpen");
                p.ShowDialog();
            }
            else
            {
                MessageBox.Show("Please Check Your Table First");
            }
        }

        private void feedBackToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Feedback fd = new Feedback();
            fd.ShowDialog();
        }


        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helps h = new Helps();
            h.ShowDialog();
        }

        private void workderAttendaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WorkerAttendanc wa = new WorkerAttendanc();
            wa.ShowDialog();
        }

        private void reprintKotToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReprintKOT rk = new ReprintKOT();
            rk.ShowDialog();
        }

        private void btnReprintKot_Click(object sender, EventArgs e)
        {
            ReprintKOT rk = new ReprintKOT();
            rk.ShowDialog();
        }

        private void securityToolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            Security ss = new Security();
            ss.ShowDialog();
        }

        private void btnReprintKot_MouseEnter(object sender, EventArgs e)
        {
            //reprintKotToolStripMenuItem.BackgroundImage = Properties.Resources.WithGlow;
        }

        private void btnAttendance_Click(object sender, EventArgs e)
        {
            WorkerAttendanc wa = new WorkerAttendanc();
            wa.ShowDialog();
        }

        private void btncustome_Click(object sender, EventArgs e)
        {
            Customer cm = new Customer(null);
            cm.ShowDialog();
        }

        private void btnbillreprint_Click(object sender, EventArgs e)
        {
            Sale s = new Sale();
            s.ShowDialog();
        }

        private void btnsalesummery_Click(object sender, EventArgs e)
        {
            SaleSummery ss = new SaleSummery();
            ss.ShowDialog();
        }

        private void btnSaleByPaymentmode_Click(object sender, EventArgs e)
        {
            SaleByPaymentMode s = new SaleByPaymentMode();
            s.ShowDialog();
        }


        private void Home_Load(object sender, EventArgs e)
        {

        }

        private void Home_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.S && Control.ModifierKeys == Keys.Control)
            {
                main m = new main();
                m.ShowDialog();
            }

            if (e.KeyCode == Keys.I && Control.ModifierKeys == Keys.Control)
            {

                //m.ShowDialog();
            }
            if (e.KeyCode == Keys.P && Control.ModifierKeys == Keys.Control)
            {
                Purchasing m = new Purchasing(null);
                m.ShowDialog();
            }
            if (e.KeyCode == Keys.C && Control.ModifierKeys == Keys.Control)
            {
                main m = new main();
                m.ShowDialog();
            }

        }
        private void securityToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Security s = new Security();
            s.ShowDialog();

        }

        private void counterReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bl_feild.HeaderName = "CounterReport";
            Report2DateSelect report = new Report2DateSelect("CounterReport");
            report.Show();
        }

        private void btnsupplier_Click(object sender, EventArgs e)
        {
            Supplier s = new Supplier();
            s.ShowDialog();
        }



     
    }
}

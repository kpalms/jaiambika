﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
   using Restaurant.BL;
using Restaurant.DL;
using Restaurant.PL.Reports; 

namespace Restaurant.PL
{

    public partial class ReprintKOT : Form
    {
        public ReprintKOT()
        {
            InitializeComponent();
        }

        BL_ReprintKOT bl_reprintkot = new BL_ReprintKOT();
        Common common = new Common();

        private void ReprintKOT_Load(object sender, EventArgs e)
        {
            BindKot();
        }

        private void BindKot()
        {

            try
            {

                DataTable dt = bl_reprintkot.SelectKotTable(bl_reprintkot);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataGridView grid = new DataGridView();
                    Label lbl = new Label();
                    Button btn = new Button();
                    Panel pnl = new Panel();
                    pnl.Size = new Size(150, 150);
                    btn.Size = new Size(70, 30);
                    btn.BackColor = Color.Gold;


                    grid.Name = dt.Rows[i]["TableNo"].ToString();
                    grid.Size = new Size(200, 150);
                    grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
                    grid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
                    grid.ReadOnly = true;
                    grid.AllowUserToAddRows = false;
                    grid.AllowUserToDeleteRows = false;
                    grid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(253)))), ((int)(((byte)(244)))));
                    grid.Dock = DockStyle.Fill;
                    grid.BackgroundColor = Color.White;
                    DataGridViewCellStyle rowStyle = grid.RowHeadersDefaultCellStyle;
                    rowStyle.BackColor = Color.Gold;
                    grid.BorderStyle = BorderStyle.Fixed3D;

                    lbl.Text = dt.Rows[i]["TableNo"].ToString();
                    lbl.Font = new Font("Microsoft Sans Serif",10);
                    lbl.Size = new Size(30, 20);
                    pnl.Location = new Point(10, 10);


                    btn.Name = dt.Rows[i]["KotOrderNo"].ToString();


                    DataTable dtt = bl_reprintkot.SelectKotDetails(dt.Rows[i]["KotOrderNo"].ToString());
                   

                    grid.DataSource = dtt;



                    if (dtt.Rows.Count > 0)
                    {
                       pnl.Controls.Add(grid);
                        pnl.Controls.Add(lbl);
                       pnl.Controls.Add(btn);
                       this.flowLayoutPanel1.Controls.Add(lbl);
                       this.flowLayoutPanel1.Controls.Add(grid);
                       this.flowLayoutPanel1.Controls.Add(btn);
                        this.flowLayoutPanel1.Controls.Add(pnl);
                        btn.Text = "Print";
                        btn.Click += new EventHandler(btnPrint_Click);



                       // grid.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grid_CellDoubleClick);

                        //DataGridViewButtonColumn btn = new DataGridViewButtonColumn();
                        //grid.Columns.Add(btn);
                        //btn.HeaderText = "Complated";
                        //btn.Text = "Complated";
                        //btn.UseColumnTextForButtonValue = true;
                     

                        grid.Columns["ItemName"].Width = 150;
                        grid.Columns["Qty"].Width = 20;
                        //grid.Columns["OrderDetailId"].Visible = false;
                        grid.RowHeadersVisible = false;
                        grid.EnableHeadersVisualStyles = false;
                        grid.MultiSelect = false;

                        
                    }


                  
                 }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }




        }



        void btnPrint_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string kotorderno = btn.Name;
            DataSet ds = bl_reprintkot.ReprintKot(kotorderno);
             PrintKOT(ds);

        }


        private void PrintKOT(DataSet ds)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to Print KOT?", "Print KOT", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    rptKotPrint rptItem = new rptKotPrint();
                    rptItem.SetDataSource(ds.Tables[0]);


                    rptItem.SetParameterValue("lblname", "TableNo");
                    string tableno = ds.Tables[0].Rows[0]["TableNo"].ToString();
                    rptItem.SetParameterValue("no", tableno);


                    //if (bl_feild.KOTPRINT == "yes")
                   //{
                        rptItem.SetParameterValue("KotOrderNo", "RePrint");
                   // }
                   // else
                   // {
                        string kotorderno = ds.Tables[0].Rows[0]["KotOrderNo"].ToString();
                        rptItem.SetParameterValue("KotOrderNo", kotorderno);
                   // }
                    string pp = common.ReturnOneValue("select KotPrinter from PrinterSetup");
                    rptItem.PrintOptions.PrinterName = pp;

                    rptItem.PrintToPrinter(1, false, 0, 0);

                }


                
            }
            catch (Exception ex)
            {
                MessageBox.Show("this error" + ex);
            }



        }


    }
}

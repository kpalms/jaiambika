﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.Data.SqlClient;
using Restaurant.BL;

namespace Restaurant.PL
{
    public partial class Message : Form
    {

        BL_Field bl_field = new BL_Field();
        BL_NewItem bl_newitem = new BL_NewItem();
        public Message()
        {
            InitializeComponent();
            this.KeyPreview = true;
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtname.Text != "")
                    {
                        bl_field.Message = txtname.Text;
                        bl_newitem.InsertMessage(bl_field);
                        txtname.Text = "";
                        MessageBox.Show("Record inserted Successfully");
                        BindUnitName();
                        txtname.Focus();

                    }
                    else
                    {
                        MessageBox.Show("Please enter Message?");
                        txtname.Focus();
                    }
                                
            }
            catch
            {
                MessageBox.Show("error occured!Please contact Administration?");

            }
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            txtname.Text = "";
            txtname.Focus();
            btnUpdate.Enabled = false;
            btndelete.Enabled = false;
            btnadd.Enabled = true;
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Message?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_newitem.DeleteMessage(bl_field);
                    txtname.Text = "";
                    MessageBox.Show("Record Deleted Successfully");
                    BindUnitName();
                    txtname.Focus();
                    btnUpdate.Enabled = false;
                    btndelete.Enabled = false;

                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
        }

        private void Message_Load(object sender, EventArgs e)
        {
            BindUnitName();
            Select1Message();
            btndelete.Enabled = false;
            btnUpdate.Enabled = false;
                
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            bl_field.MessageId = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            txtname.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            btnUpdate.Enabled = true;
            btndelete.Enabled = true;
            btnadd.Enabled = false;
        }

        private void BindUnitName()
        {
            DataTable dt = bl_newitem.SelectMessage();
            if (dt.Rows.Count > 0)
            {
                dataGridView1.DataSource = dt;
                dataGridView1.Columns["MessageId"].Visible = false;
                dataGridView1.Columns["Status"].Visible = false;
                
            }
            else
            {
                dataGridView1.DataSource = null;
            }

        }
        private void Select1Message()
        {
            DataTable dt = bl_newitem.Select1Message();
            lblMessage.Text= dt.Rows[0]["Message"].ToString();

        }

        private void Message_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void btnselect_Click(object sender, EventArgs e)
        {
            if (txtname.Text != "")
            {
                bl_newitem.SelectMessage(bl_field);
                txtname.Text = "";
                MessageBox.Show("Message Selected");
                BindUnitName();
                btnUpdate.Enabled = false;
                btndelete.Enabled = false;
                Select1Message();
            }

            else
            {
                MessageBox.Show("Please Select the Message");
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {    
                bl_field.Message = txtname.Text;
                bl_newitem.UpdateMessage(bl_field);
                txtname.Text = "";
                MessageBox.Show("Record Updated Successfully");
                BindUnitName();
                txtname.Focus();
                btnUpdate.Enabled = false;
                btndelete.Enabled = false;
            
        }
    }
}

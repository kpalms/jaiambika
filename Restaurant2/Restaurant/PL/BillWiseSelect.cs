﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using Restaurant.PL.Reports;

namespace Restaurant.PL
{
    public partial class BillWiseSelect : Form
    {

        public string frm = "";
        public static String dt1 = "", dt2 = "";
        BL_Sale bl_sale = new BL_Sale();
        BL_Field bl_field = new BL_Field();

        public BillWiseSelect(string fm)
        {
            InitializeComponent();
            frm = fm;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            bl_field.FromDate = cboFrom.Text;
            bl_field.Todate = cboTo.Text;
     
            if (frm == "BillWise")
            {
                this.Close();
                ReportsViewer reportviewer = new ReportsViewer("BillWise");
                reportviewer.Show();
            }
            if (frm == "BillWiseLodge")
            {
                this.Close();
                ReportsViewer reportviewer = new ReportsViewer("BillWiseCheckOut");
                reportviewer.Show();
            }
        }

        private void BillWiseSelect_Load(object sender, EventArgs e)
        {
            if (frm == "BillWise")
            {
                lblheader.Text = "Sale Bill Wise";
                //BindBill();
                //BindBillTO();
                BindBillFrom();
                BindBillTo();
            }
            if (frm == "BillWiseLodge")
            {
                lblheader.Text = "Bill Wise CheckOut Reports";
                BindBillcheckOutFrom();
                BindBillcheckOutTo();
            }

            //lstfrom.Visible = false;
            //lstto.Visible = false;

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void BindBill()
        {
            //DataSet PendingDetails = bl_sale.GetBill(bl_field);
            //if (PendingDetails.Tables[0].Rows.Count > 0)
            //{
            //    lstfrom.DataSource = PendingDetails.Tables[0];
            //    lstfrom.ValueMember = "BillNo";
            //    lstfrom.DisplayMember = "BillNo";
            //}
            //else
            //{
            //    lstfrom.DataSource = null;
            //}
        }


        public void BindBillTO()
        {
            //DataSet PendingDetails = bl_sale.GetBill(bl_field);
            //if (PendingDetails.Tables[0].Rows.Count > 0)
            //{
            //    lstto.DataSource = PendingDetails.Tables[0];
            //    lstto.ValueMember = "BillNo";
            //    lstto.DisplayMember = "BillNo";
            //}
            //else
            //{
            //    lstfrom.DataSource = null;
            //}
        }


        public void BindBillcheckOutFrom()
        {
            //DataSet PendingDetails = bl_sale.GetBillCheckOut(bl_field);
            //if (PendingDetails.Tables[0].Rows.Count > 0)
            //{
            //    lstfrom.DataSource = PendingDetails.Tables[0];
            //    lstfrom.ValueMember = "CheckOutBillNo";
            //    lstfrom.DisplayMember = "CheckOutBillNo";
            //}
            //else
            //{
            //    lstfrom.DataSource = null;
            //}
        }


        public void BindBillcheckOutTo()
        {
            //DataSet PendingDetails = bl_sale.GetBillCheckOut(bl_field);
            //if (PendingDetails.Tables[0].Rows.Count > 0)
            //{
            //    lstto.DataSource = PendingDetails.Tables[0];
            //    lstto.ValueMember = "CheckOutBillNo";
            //    lstto.DisplayMember = "CheckOutBillNo";
            //}
            //else
            //{
            //    lstfrom.DataSource = null;
            //}
        }



        private void txtfrom_Leave(object sender, EventArgs e)
        {
            //if (txtfrom.Text != "")
            //{
            //    lstfrom.Visible = false;
            //}
        }

        private void txtto_Leave(object sender, EventArgs e)
        {
            //if (txtto.Text != "")
            //{
            //    lstto.Visible = false;
            //}
        }

        private void txtfrom_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.OemPeriod)
            //{
            //}
            //else if (lstfrom.Visible && e.KeyCode == Keys.Up)
            //{

            //    if (lstfrom.SelectedIndex > 0)
            //        lstfrom.SelectedIndex -= 1;

            //    e.Handled = true;
            //}
            //else if (lstfrom.Visible && e.KeyCode == Keys.Down)
            //{

            //    if (lstfrom.SelectedIndex < lstfrom.Items.Count - 1)
            //        lstfrom.SelectedIndex += 1;

            //    e.Handled = true;
            //}
            //else if (e.KeyCode == Keys.Enter)
            //{
            //    if (lstfrom.SelectedIndex > 0)
            //    {
            //        txtfrom.Text = "";
            //        txtfrom.Text = lstfrom.Text.ToString();
            //        lstfrom.Hide();

            //        txtto.Focus();
            //    }
            //    else
            //    {
            //        lstfrom.Visible = false;
            //        if (txtfrom.Text != "")
            //        {
            //            txtto.Focus();
            //        }
            //    }
            //}
        }

        private void lstfrom_DoubleClick(object sender, EventArgs e)
        {
            //txtfrom.Text = lstfrom.Text.ToString();
            //lstfrom.Visible = false;
            //txtto.Focus();
        }

        private void txtto_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.OemPeriod)
            //{
            //}
            //else if (lstto.Visible && e.KeyCode == Keys.Up)
            //{

            //    if (lstto.SelectedIndex > 0)
            //        lstto.SelectedIndex -= 1;

            //    e.Handled = true;
            //}
            //else if (lstto.Visible && e.KeyCode == Keys.Down)
            //{

            //    if (lstto.SelectedIndex < lstto.Items.Count - 1)
            //        lstto.SelectedIndex += 1;

            //    e.Handled = true;
            //}
            //else if (e.KeyCode == Keys.Enter)
            //{
            //    if (lstto.SelectedIndex > 0)
            //    {
            //        txtto.Text = "";
            //        txtto.Text = lstto.Text.ToString();
            //        lstto.Hide();

                   
            //    }
            //    else
            //    {
            //        lstto.Visible = false;
                  
            //    }
            //}
        }

        private void lstto_DoubleClick(object sender, EventArgs e)
        {
            //txtto.Text = lstfrom.Text.ToString();
            //lstto.Visible = false;
        
        }

        private void txtfrom_TextChanged(object sender, EventArgs e)
        {
            //if (txtfrom.Text != "")
            //{
            //    lstfrom.Visible = true;
            //}
        }

        private void txtto_TextChanged(object sender, EventArgs e)
        {
            //if (txtto.Text != "")
            //{
            //    lstto.Visible = true;
            //}
        }
        private void BindBillFrom()
        {
            DataSet PendingDetails = bl_sale.GetBill(bl_field);
            if (PendingDetails.Tables[0].Rows.Count > 0)
            {
                cboFrom.DataSource = PendingDetails.Tables[0];
                cboFrom.ValueMember = "BillNo";
                cboFrom.DisplayMember = "BillNo";
            }                 
             else
             {
                 cboFrom.DataSource = null;
             }   
        }

        private void BindBillTo()
        {
            DataSet PendingDetails2 = bl_sale.GetBill(bl_field);
            if (PendingDetails2.Tables[0].Rows.Count > 0)
            {
                cboTo.DataSource = PendingDetails2.Tables[0];
                cboTo.ValueMember = "BillNo";
                cboTo.DisplayMember = "BillNo";
            }
            else
            {
                cboTo.DataSource = null;
            }
        }
    }
}

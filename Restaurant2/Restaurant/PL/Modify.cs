﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using Restaurant.PL.Reports;
using System.Collections;


namespace Restaurant.PL
{
    public partial class Modify : Form
    {
        BL_Worker bl_worker = new BL_Worker();
        BL_Field bl_field = new BL_Field();
     
        public Modify()
        {
            InitializeComponent();
           
        }

        private void Modify_Load(object sender, EventArgs e)
        {
            BindModifyTable();
        }


        private void BindModifyTable()
        {
            try
            {
                DataTable dt = bl_worker.SelectModifyTable();

                DataRow row = dt.NewRow();
                row["TableNo"] = "SELECT";
                dt.Rows.InsertAt(row, 0);

                cbomodify.DataSource = dt;
                cbomodify.DisplayMember = "TableNo";
                cbomodify.ValueMember = "TableNo";

            }
            catch (Exception ex)
            {
                MessageBox.Show("erro" + ex);
            }


        }

        private void btnok_Click(object sender, EventArgs e)
        {
            if (cbomodify.Text == "SELECT")
            {
                MessageBox.Show("Please select table no");
            }
            else
            {
                string s = cbomodify.Text;
                string[] words = s.Split(' ');
                string TableNo = words[0];
                string SeatFrom = words[1];

                Common comm = new Common();
                string orderid = comm.ReturnOneValue("select distinct OrderId from OrderDetail where TableNo='" + TableNo + "' and Seat='" + SeatFrom + "' and Status='P'");
                bl_worker.ModifyTable(orderid);
                main mm = new main();
                mm.BindTable();
                MessageBox.Show("Modify Bill successfully");
                this.Close();

            }

        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }



    }
}

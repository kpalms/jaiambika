﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using Restaurant.PL.Reports;

namespace Restaurant.PL
{
    public partial class Purchasing : Form
    {
        BL_Field bl_field = new BL_Field();
        BL_Purchase bl_purchase = new BL_Purchase();
        BL_Vendor bl_vendor = new BL_Vendor();
        BL_NewItem bl_newitem = new BL_NewItem();
        Common common = new Common();

        BL_Worker bl_worker = new BL_Worker();

        //BL_ChangeOfDay bl_changeofday = new BL_ChangeOfDay();
        DataTable selectnewitem = new DataTable();

        string BillNocheck = "";


        public Purchasing(string billnocheck)
        {
            InitializeComponent();
            this.KeyPreview = true;
            BillNocheck = billnocheck;
        }

        private void btnadd_Click_1(object sender, EventArgs e)
        {
            Add();
            CalculateSubTotal();
           
        }

        private void Add()
        {
            try
            {

                if (btnadd.Text == "Add")
                {

                    if (txtitemname.Text == "")
                    {
                        MessageBox.Show("Please enter item name");
                        txtitemname.Focus();
                    }
                    else if (txtqty.Text == "")
                    {
                        MessageBox.Show("Please enter qty");
                        txtqty.Focus();
                    }
                    else if (Convert.ToInt16(txtqty.Text) == 0)
                    {
                        MessageBox.Show("Please enter valid qty");
                        txtqty.Focus();
                    }

                    else if (ddlunit.Text == "Select")
                    {
                        MessageBox.Show("Please Select Unit");
                        ddlunit.Focus();
                    }
                    else if (txtrate.Text == "")
                    {
                        MessageBox.Show("Please enter Rate");
                        txtrate.Focus();
                    }
                    else if (Convert.ToInt16(txtrate.Text) == 0)
                    {
                        MessageBox.Show("Please enter valid Rate");
                        txtrate.Focus();
                    }

                    else
                    {
                        SetField();
                        bl_purchase.InsertPurchaseDetail(bl_field);


                        BindPurchase();
                        Clear();
                        grditemName.Visible = false;
                        //MessageBox.Show("Recods Inserted Successfully");
                        //kk


                    }
   
                }
                else
                {

                    SetField();
                    bl_field.BillNo = BillNocheck;
                    bl_field.SupplierId = cbosupplier.SelectedValue.ToString();
                    bl_field.PurchaseDetailsId = Convert.ToInt32(grdpurchase.SelectedRows[0].Cells["PurchaseDetailsId"].Value.ToString());

                    if (BillNocheck != null)
                    {
                        bl_purchase.UpdatePurchaseDetail(bl_field);                       
                        bl_purchase.UpdatePurchase(bl_field);
                        MessageBox.Show("Record Updated Successfully");
                        SelectFromPurchaseSecurity();
                        btnadd.Text = "Add";
                        Clear();

                    }
                    else
                    {
                        bl_purchase.UpdatePurchaseDetail(bl_field);
                        MessageBox.Show("Record Updated Successfully");
                        BindPurchase();
                        btnadd.Text = "Add";
                        Clear();
                    }


                    bl_purchase.UpdatePurchaseDetail(bl_field);

                }



            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }


        }

        private void SetField()
        {
            bl_field.BillNo = txtbillNo.Text;
            bl_field.SupplierId = cbosupplier.SelectedValue.ToString();
            bl_field.Date = ddldate.Text;
            //bl_field.ItemId = grditemName.SelectedRows[0].Cells["ItemId"].Value.ToString();
            bl_field.Qty = txtqty.Text;
            bl_field.UnitId = ddlunit.SelectedValue.ToString();
            bl_field.Rate = txtrate.Text;
            bl_field.Amount = txtamount.Text;
            bl_field.Notes = txtnotes.Text;
            if (txtDiscount.Text != "")
            {
                bl_field.Discount = txtDiscount.Text;
            }
            else {
                bl_field.Discount = "0";
            }
            bl_field.Status = "Open";
          


        }
        private void Clear()
        {
          
            txtitemname.Text = "";
            txtqty.Text = "";
 

            txtpaid.Text = "0.00";

            //common.BindUnitName(ddlunit);

            ddldate.Text = DateTime.Now.ToString();
            //common.BindUnitName(ddlunit);


            txtrate.Text = "";
            txtamount.Text = "";
            txtnotes.Text = "";
            grditemName.Visible = false;
            txtDiscount.Text = "0";
            

        }
        private void Purchasing_Load(object sender, EventArgs e)
        {

            if (BillNocheck != null)
            {
                bl_field.BillNo = BillNocheck;
                SelectFromPurchaseSecurity();

                btnadd.Text = "Update";

                CalculateSubTotal();


                //CalculateSubTotal();
               

            }
            else
            {
               BindPurchase();
               CalculateSubTotal();
            }
          

            grditemName.Visible = false;
  

            common.BindUnitName(ddlunit);
 
            BindSupplier();
            btndelete.Enabled = false;
           // BindItemName();
        }



        private void SelectFromPurchaseSecurity()
        {
            DataTable dt = bl_purchase.SelectDataByInvoice(bl_field);
            grdpurchase.DataSource = dt;
            txtbillNo.Text = dt.Rows[0][1].ToString();
            cbosupplier.Text = Convert.ToString(dt.Rows[0]["SupplierName"].ToString());
            ddldate.Text = Convert.ToString(dt.Rows[0]["Date"].ToString());
           

            grdpurchase.Columns["PurchaseDetailsId"].Visible = false;
            grdpurchase.Columns["BillNo"].Visible = false;
            grdpurchase.Columns["SupplierName"].Visible = false;
            grdpurchase.Columns["Date"].Visible = false;
            grdpurchase.Columns["UnitId"].Visible = false;

          
        }


        private void BindPurchase()
        {
            DataTable dt = bl_purchase.SelectPurchaseDetail(bl_field);
            grdpurchase.DataSource = dt;
            grdpurchase.Columns["PurchaseDetailsId"].Visible = false;
            grdpurchase.Columns["Status"].Visible = false;
            grdpurchase.Columns["ItemId"].Visible = false;

            grditemName.Visible = false;

        }

        private void grdpurchase_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (grdpurchase.Rows.Count>0)
                {
                //bl_field.PurchaseId = Convert.ToInt32(grdpurchase.SelectedRows[0].Cells[0].Value.ToString());
                //string d = grdpurchase.SelectedRows[0].Cells[1].Value.ToString();

                txtitemname.Text = grdpurchase.SelectedRows[0].Cells["ItemNames"].Value.ToString();
                grditemName.Visible = false;
                ddlunit.Text = grdpurchase.SelectedRows[0].Cells["UnitName"].Value.ToString();
                txtqty.Text = grdpurchase.SelectedRows[0].Cells["Qty"].Value.ToString();
                //ddldate.Text = grdpurchase.SelectedRows[0].Cells["Date"].Value.ToString();
                txtnotes.Text = grdpurchase.SelectedRows[0].Cells["Notes"].Value.ToString();
                txtrate.Text = grdpurchase.SelectedRows[0].Cells["Rate"].Value.ToString();

                btnadd.Text = "Update";
                btndelete.Enabled = true;
                }
            }
            catch 
            {
                MessageBox.Show("error occured ?please contact administrator");
             
            }

        
        
        }

        private void btndelete_Click_1(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure to delete selected Record(s)?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_field.PurchaseDetailsId =Convert.ToInt32(grdpurchase.SelectedRows[0].Cells["PurchaseDetailsId"].Value.ToString());
                    bl_purchase.DeletePurchaseDetail(bl_field);
                    BindPurchase();
                    grditemName.Visible = false;
                    Clear();
                    btnadd.Text = "Add";
                    CalculateSubTotal();
                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
        }

        private void txtqty_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtcost_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            {
                e.Handled = true;
                MessageBox.Show("Please enter number only");
            }
        }

        private void Purchasing_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void btncancel_Click_1(object sender, EventArgs e)
        {
            btnadd.Text = "Add";
            Clear();
            btndelete.Enabled = false;
        }

        private void btnshow_Click_1(object sender, EventArgs e)
        {

            DataTable dt = bl_purchase.SelectExpensesByDate(bl_field);
            grdpurchase.DataSource = dt;
            grdpurchase.Columns["PurchaseId"].Visible = false;
            grdpurchase.Columns["Status"].Visible = false;

            decimal tot = 0;
            lblTotal.Text = "";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                decimal price = Convert.ToDecimal(dt.Rows[i][4].ToString());
                tot = tot + price;
                lblTotal.Text = Convert.ToString(tot);

            }

        }

        private void btnprint_Click_1(object sender, EventArgs e)
        {
            if (grdpurchase.Rows.Count == 0)
            {
                MessageBox.Show("Please add some record first ");
            }
            else if (txtbillNo.Text == "")
            {
                MessageBox.Show("Please enter Bill no.");
                txtbillNo.Focus();
            }

            else if (cbosupplier.Text == "Select")
            {
                MessageBox.Show("Please select supplier name");
                cbosupplier.Focus();
            }
            else if (txtpaid.Text == "")
            {
                MessageBox.Show("Please Enter Paid Amount ");
                txtpaid.Focus();
            }
             

            else
            {
                SetField();
                string id = bl_purchase.InsertPurchase(bl_field);
                bl_field.PurchaseId = Convert.ToInt32(id);
                bl_purchase.UpdatePurchaseId(bl_field);
                StockAdd();
                Supplieraccountsetfill();
                bl_worker.SupplierAccount(bl_field);
                BindPurchase();

                Clear();
                cbosupplier.Text = "Select";
                txtbillNo.Text = "";
                lblSubTotal.Text = "0.00";
                lblTotal.Text = "0.00";
                btnadd.Text = "Add";
                MessageBox.Show("Record Saved Successfully");


            }

        }

        private void BindSupplier()
        {
            DataTable dt = bl_vendor.SearchVendorAll(bl_field);
            DataRow row = dt.NewRow();
            row["SupplierName"] = "Select";
            dt.Rows.InsertAt(row, 0);
            cbosupplier.DataSource = dt;
            cbosupplier.DisplayMember = "SupplierName";
            cbosupplier.ValueMember = "SupplierId";


        }
 


        private void StockEntry_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }


        private void btnnewItem_Click(object sender, EventArgs e)
        {
            CreateItem ci = new CreateItem();
            ci.ShowDialog();
        }

        private void txtbillNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                cbosupplier.Focus();

            }
        }

        private void txtqty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ddlunit.Focus();
                CalculateItemAmount();

            }

        }

        private void ddlunit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                txtrate.Focus();
                CalculateItemAmount();
            }

        }

        private void txtrate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtamount.Focus();
                //CalculateItemAmount();

            }
        }

        private void txtamount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnadd.Focus();

            }
        }

        private void btnadd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Add();

            }
        }

        private void name_Click(object sender, EventArgs e)
        {

        }

        private void txtitemname_Leave(object sender, EventArgs e)
        {
            grditemName.Visible = false;
        }



        private void txtbillNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            common.NumberOnly(e);
        }




        private void CalculateItemAmount()
        {
            try
            {
                bl_field.ItemId = grditemName.SelectedRows[0].Cells["ItemId"].Value.ToString();
               
               txtamount.Text = Convert.ToString(Convert.ToDecimal(txtqty.Text) * Convert.ToDecimal(txtrate.Text));


            }
            catch 
            {
                
               
            }


       
        }

        private void  CalculateSubTotal()
        {
            decimal total = 0;
            decimal subtotal = 0;
            for (int i = 0; i < grdpurchase.Rows.Count; i++)
            {

                bl_field.ItemId = grdpurchase.Rows[i].Cells["ItemId"].Value.ToString();
                
                subtotal = Convert.ToDecimal(grdpurchase.Rows[i].Cells["Qty"].Value.ToString()) * Convert.ToDecimal(grdpurchase.Rows[i].Cells["Rate"].Value.ToString());
                total = total + subtotal;
   

            }
            lblSubTotal.Text = Convert.ToString(total);
            if (txtDiscount.Text != "0" && txtDiscount.Text != "")
            {
                decimal dis = total * Convert.ToDecimal(txtDiscount.Text) / 100;
                lblTotal.Text = Convert.ToString(total - dis);
            }
            else
            {
                lblTotal.Text = Convert.ToString(total);
            }
        }


        private void StockAdd()
        {
            for (int i = 0; i < grdpurchase.Rows.Count; i++)
            {

                bl_field.ItemId = grdpurchase.Rows[i].Cells["ItemId"].Value.ToString();
                //DataTable dt = bl_newitem.SelectIdNewItem(bl_field);
                //if (dt.Rows.Count > 0)
                //{
                    //int primeryid = Convert.ToInt32(dt.Rows[0][3].ToString());
                    DataTable dtstock = bl_newitem.SelectStockById(bl_field);

                    if (dtstock.Rows.Count > 0)
                    {
                        decimal stock = Convert.ToDecimal(dtstock.Rows[0]["Stock"].ToString());
                        decimal newstock = 0;
                        //int UnitIdtoRecipe = Convert.ToInt32(grdpurchase.Rows[i].Cells["UnitId"].Value.ToString());
                        //if (UnitIdtoRecipe == primeryid)
                        //{

                        //    if (dtstock.Rows.Count > 0)
                        //    {
                        //        newstock = Convert.ToDecimal(grdpurchase.Rows[i].Cells["Qty"].Value.ToString()) + stock;
                        //    }

                        //}
                        //else
                        //{

                        //    if (dtstock.Rows.Count > 0)
                        //    {
                        //        int conversion = Convert.ToInt32(dt.Rows[0]["ConversionUnit"].ToString());
                        //        decimal addstock = Convert.ToDecimal(grdpurchase.Rows[i].Cells["Qty"].Value.ToString()) / conversion;
                        //        newstock = addstock + stock;
                        //    }
                        //}
                        newstock = Convert.ToDecimal(grdpurchase.Rows[i].Cells["Qty"].Value.ToString()) + stock;
                        bl_field.Stock = Convert.ToString(newstock);
                        bl_field.ItemId = grdpurchase.Rows[i].Cells["ItemId"].Value.ToString();
                        bl_newitem.UpdateStockValue(bl_field);
                    //}

                }
            }
        }





        private void txtrate_TextChanged(object sender, EventArgs e)
        {
            CalculateItemAmount();
        }

        private void ddlunit_SelectedIndexChanged(object sender, EventArgs e)
        {
             CalculateItemAmount();
        }

        private void txtqty_TextChanged(object sender, EventArgs e)
        {
            CalculateItemAmount();
        }

        private void txtDiscount_TextChanged(object sender, EventArgs e)
        {
            if (txtDiscount.Text != "0" || txtDiscount.Text != "00")
            {
                lblTotal.Text = "0";
                CalculateSubTotal();
            }
           
        }

      
      private void Supplieraccountsetfill()
      {
          bl_field.Date = ddldate.Text;
          bl_field.SupplierId = cbosupplier.SelectedValue.ToString();
          bl_field.BillNo = txtbillNo.Text;
          bl_field.ReceiptNo = 0;
          bl_field.Debit = Convert.ToDecimal(lblTotal.Text);
          bl_field.Credit = Convert.ToDecimal(txtpaid.Text);

      }

      private void cbosupplier_KeyPress(object sender, KeyPressEventArgs e)
      {
          common.ComboBoxCheckValue(cbosupplier, e);
         // ComboBoxCheckValue(cbosupplier, e);
      }

      private void txtitemname_TextChanged(object sender, EventArgs e)
      {
          DataTable dt = null;
          if (txtitemname.Focus())
          {
              if (txtitemname.Text != "")
              {

                  bl_field.TextSearch = txtitemname.Text;
                  dt = bl_newitem.SearchNewItemBySearch(bl_field);
                  if (dt.Rows.Count > 0)
                  {
                      grditemName.Visible = true;
                      grditemName.DataSource = dt;
                      grditemName.Columns["ItemId"].Visible = false;
                 
 
                  }


              }
              else
              {

                  dt = bl_newitem.SearchNewItemAll();
                  if (dt.Rows.Count > 0)
                  {
                      grditemName.Visible = true;
                      grditemName.DataSource = dt;
                      grditemName.Columns["ItemId"].Visible = false;
                
  
                  }

              }
          }
          else
          {
              grditemName.Visible = false;

          }
      }

      private void txtitemname_KeyDown(object sender, KeyEventArgs e)
      {

          if (e.KeyCode == Keys.Enter)
          {
              if (grditemName.Rows.Count > 0)
              {

                  txtitemname.Text = grditemName.SelectedRows[0].Cells["ItemNames"].Value.ToString();

                  bl_field.ItemId = grditemName.SelectedRows[0].Cells["ItemId"].Value.ToString();

                    txtrate.Text = grditemName.SelectedRows[0].Cells["Rate"].Value.ToString();
                  grditemName.Visible = false;
                  txtqty.Focus();


              }
              else
              {

                  grditemName.Visible = false;
                  txtitemname.Focus();
              }


          }

          if (e.KeyCode == Keys.Up)
          {
              DataTable dtTemp = grditemName.DataSource as DataTable;


              object[] arr = dtTemp.Rows[dtTemp.Rows.Count - 1].ItemArray;
              for (int i = dtTemp.Rows.Count - 2; i >= 0; i--)
              {
                  dtTemp.Rows[i + 1].ItemArray = dtTemp.Rows[i].ItemArray;
              }
              dtTemp.Rows[0].ItemArray = arr;



          }

          if (e.KeyCode == Keys.Down)
          {

              DataTable dtTemp = grditemName.DataSource as DataTable;

              object[] arr = dtTemp.Rows[0].ItemArray;
              for (int i = 1; i < dtTemp.Rows.Count; i++)
              {
                  dtTemp.Rows[i - 1].ItemArray = dtTemp.Rows[i].ItemArray;
              }
              dtTemp.Rows[dtTemp.Rows.Count - 1].ItemArray = arr;



          }


      }

      private void cbosupplier_KeyDown(object sender, KeyEventArgs e)
      {
          if (e.KeyCode == Keys.Enter)
          {
              txtitemname.Focus();
              //cbosupplier.DroppedDown = true; ;
          }
      }



      private void ddlunit_KeyPress(object sender, KeyPressEventArgs e)
      {
          common.ComboBoxCheckValue(ddlunit, e);
      }

      private void txtrate_KeyPress(object sender, KeyPressEventArgs e)
      {

          common.OneDecimalPoint(txtrate, e);
      }

      private void txtDiscount_KeyPress(object sender, KeyPressEventArgs e)
      {
         
      }

      private void ddlunit_Enter(object sender, EventArgs e)
      {
          ddlunit.DroppedDown = true;
      }
       
     
     
      




 
       
     
      



    }
}

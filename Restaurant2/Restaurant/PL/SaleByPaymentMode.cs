﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;



using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using Restaurant.PL.Reports;
using System.Globalization;
using System.Data;


namespace Restaurant.PL
{
    public partial class SaleByPaymentMode : Form
    {
        BL_Sale bl_sale = new BL_Sale();
        BL_Field bl_field = new BL_Field();


        public SaleByPaymentMode()
        {
            InitializeComponent();
        }

        private void SaleByPaymentMode_Load(object sender, EventArgs e)
        {
            dtpfrom.CustomFormat = "dd/MM/yyyy";
            dtpto.CustomFormat = "dd/MM/yyyy";
            Sale();
           
 

        }

        private void btnbillshow_Click(object sender, EventArgs e)
        {
            Sale();
        }

        private void Sale()
        {
            bl_field.FromDate = dtpfrom.Text;
            bl_field.Todate = dtpto.Text;

            DataTable dt = bl_sale.SaleByPaymentMode(bl_field);
            grdsale.DataSource = dt;
            decimal amtTotal = 0;
            for (int i = 0; i < grdsale.Rows.Count; i++)
            {
                amtTotal = amtTotal + Convert.ToDecimal(grdsale.Rows[i].Cells["Total"].Value);
            }
            lbltotalamt.Text = Convert.ToString(amtTotal);

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using Restaurant.BL;
using Restaurant.DL;
using System.Configuration;

namespace Restaurant.PL
{
   
    public partial class AccessLevel : Form
    {
        BL_Security bl_security = new BL_Security();
        BL_TreeView bl_treeview = new BL_TreeView();
        BL_Field bl_field = new BL_Field();
        public static string constr = ConfigurationManager.ConnectionStrings["MuktanganConnectionString"].ConnectionString;
        main _main;
        public AccessLevel(main mm)
        {
            InitializeComponent();
            _main=mm;

        }

        private void AccessLevel_Load(object sender, EventArgs e)
        {


            string constr = ConfigurationManager.ConnectionStrings["MuktanganConnectionString"].ConnectionString;

            SqlConnection conn = new SqlConnection(constr);

           
            SqlDataAdapter da = new SqlDataAdapter("Select * from TreeViewItem", conn);
           
            SqlDataAdapter daCategories = new SqlDataAdapter("Select * from TreeViewMenu order by Sequance asc", conn);


            DataSet ds = new DataSet();


            treeView1.CheckBoxes = true;
            treeView1.BeginUpdate();

            da.Fill(ds, "TreeViewItem");
            daCategories.Fill(ds, "TreeViewMenu");
            ds.Relations.Add("Cat_Product", ds.Tables["TreeViewMenu"].Columns["MenuId"], ds.Tables["TreeViewItem"].Columns["MenuId"]);
            foreach (DataRow dr in ds.Tables["TreeViewMenu"].Rows)
            {
                TreeNode tn = new TreeNode(dr["MenuName"].ToString());
                foreach (DataRow drChild in dr.GetChildRows("Cat_Product"))
                {
                    TreeNode child = new TreeNode();
                    child.Text = drChild["ItemName"].ToString();

                    //tn.Nodes.Add(drChild["ItemName"].ToString());
                    tn.Nodes.Add(child);
                    if (drChild["Status"].ToString() == "1")
                    {
                       child.Checked = true;
                      
                    }
                    
                 }
                treeView1.Nodes.Add(tn);
            }

            treeView1.EndUpdate();
            treeView1.ExpandAll();
        }

   
        private void treeView1_AfterCheck(object sender, TreeViewEventArgs e)
        {
            treeView1.BeginUpdate();

            foreach (TreeNode Node in e.Node.Nodes)
            {
                Node.Checked = e.Node.Checked;
            }

            treeView1.EndUpdate();

            //+++++++++++++++
            TreeNode _node = e.Node;

            bl_field.ItemName=e.Node.Text;
            if (e.Node.Checked == true)
            {
                bl_field.Status = "1";
               
            }
            else
            {
                bl_field.Status = "0";

            }
            bl_treeview.TreeViewItemChange(bl_field);
            //main mm = new main();
            //_main.BindManageAccess();
        }

        private void AccessLevel_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }





 



    }
}

﻿namespace Restaurant.PL
{
    partial class CancelItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btncancel = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lblitemname = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtresion = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lbltableno = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblkotbot = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblcashier = new System.Windows.Forms.Label();
            this.lblcurrentqty = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblrate = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lblamount = new System.Windows.Forms.Label();
            this.lblqty = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblmsg = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(92, 159);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "CurrentQuantity";
            // 
            // btncancel
            // 
            this.btncancel.BackgroundImage = global::Restaurant.Properties.Resources.button;
            this.btncancel.Location = new System.Drawing.Point(254, 368);
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(112, 23);
            this.btncancel.TabIndex = 5;
            this.btncancel.Text = "ok";
            this.btncancel.UseVisualStyleBackColor = true;
            this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(92, 125);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Item Name";
            // 
            // lblitemname
            // 
            this.lblitemname.AutoSize = true;
            this.lblitemname.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblitemname.Location = new System.Drawing.Point(251, 122);
            this.lblitemname.Name = "lblitemname";
            this.lblitemname.Size = new System.Drawing.Size(41, 13);
            this.lblitemname.TabIndex = 7;
            this.lblitemname.Text = "label3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(90, 215);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Cancel Quantity";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(92, 278);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Reason";
            // 
            // txtresion
            // 
            this.txtresion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtresion.Location = new System.Drawing.Point(254, 275);
            this.txtresion.Multiline = true;
            this.txtresion.Name = "txtresion";
            this.txtresion.Size = new System.Drawing.Size(191, 84);
            this.txtresion.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(92, 43);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "TableNo";
            // 
            // lbltableno
            // 
            this.lbltableno.AutoSize = true;
            this.lbltableno.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltableno.Location = new System.Drawing.Point(251, 40);
            this.lbltableno.Name = "lbltableno";
            this.lbltableno.Size = new System.Drawing.Size(41, 13);
            this.lbltableno.TabIndex = 13;
            this.lbltableno.Text = "label7";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(92, 74);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Kot No";
            // 
            // lblkotbot
            // 
            this.lblkotbot.AutoSize = true;
            this.lblkotbot.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblkotbot.Location = new System.Drawing.Point(251, 71);
            this.lblkotbot.Name = "lblkotbot";
            this.lblkotbot.Size = new System.Drawing.Size(41, 13);
            this.lblkotbot.TabIndex = 15;
            this.lblkotbot.Text = "label9";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(92, 99);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "CashierName";
            // 
            // lblcashier
            // 
            this.lblcashier.AutoSize = true;
            this.lblcashier.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcashier.Location = new System.Drawing.Point(251, 96);
            this.lblcashier.Name = "lblcashier";
            this.lblcashier.Size = new System.Drawing.Size(48, 13);
            this.lblcashier.TabIndex = 17;
            this.lblcashier.Text = "label11";
            // 
            // lblcurrentqty
            // 
            this.lblcurrentqty.AutoSize = true;
            this.lblcurrentqty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcurrentqty.Location = new System.Drawing.Point(251, 156);
            this.lblcurrentqty.Name = "lblcurrentqty";
            this.lblcurrentqty.Size = new System.Drawing.Size(48, 13);
            this.lblcurrentqty.TabIndex = 18;
            this.lblcurrentqty.Text = "label12";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(92, 190);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(34, 13);
            this.label13.TabIndex = 19;
            this.label13.Text = "Rate";
            // 
            // lblrate
            // 
            this.lblrate.AutoSize = true;
            this.lblrate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblrate.Location = new System.Drawing.Point(251, 187);
            this.lblrate.Name = "lblrate";
            this.lblrate.Size = new System.Drawing.Size(48, 13);
            this.lblrate.TabIndex = 20;
            this.lblrate.Text = "label14";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(92, 245);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(49, 13);
            this.label15.TabIndex = 21;
            this.label15.Text = "Amount";
            // 
            // lblamount
            // 
            this.lblamount.AutoSize = true;
            this.lblamount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblamount.Location = new System.Drawing.Point(256, 245);
            this.lblamount.Name = "lblamount";
            this.lblamount.Size = new System.Drawing.Size(48, 13);
            this.lblamount.TabIndex = 22;
            this.lblamount.Text = "label16";
            // 
            // lblqty
            // 
            this.lblqty.AutoSize = true;
            this.lblqty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblqty.Location = new System.Drawing.Point(256, 215);
            this.lblqty.Name = "lblqty";
            this.lblqty.Size = new System.Drawing.Size(14, 13);
            this.lblqty.TabIndex = 23;
            this.lblqty.Text = "1";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblqty);
            this.panel1.Controls.Add(this.txtresion);
            this.panel1.Controls.Add(this.lblamount);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.btncancel);
            this.panel1.Controls.Add(this.lblrate);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.lblitemname);
            this.panel1.Controls.Add(this.lblcurrentqty);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.lblcashier);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.lblkotbot);
            this.panel1.Controls.Add(this.lbltableno);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Location = new System.Drawing.Point(40, 25);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(566, 487);
            this.panel1.TabIndex = 24;
            //this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // lblmsg
            // 
            this.lblmsg.AutoSize = true;
            this.lblmsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmsg.Location = new System.Drawing.Point(168, 235);
            this.lblmsg.Name = "lblmsg";
            this.lblmsg.Size = new System.Drawing.Size(92, 31);
            this.lblmsg.TabIndex = 24;
            this.lblmsg.Text = "label3";
            // 
            // CancelItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(253)))), ((int)(((byte)(244)))));
            this.ClientSize = new System.Drawing.Size(654, 532);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblmsg);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CancelItem";
            this.Text = "CancelItem";
            this.Load += new System.EventHandler(this.CancelItem_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CancelItem_KeyDown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btncancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblitemname;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtresion;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbltableno;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblkotbot;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblcashier;
        private System.Windows.Forms.Label lblcurrentqty;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblrate;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblamount;
        private System.Windows.Forms.Label lblqty;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblmsg;
    }
}
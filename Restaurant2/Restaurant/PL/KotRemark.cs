﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;


namespace Restaurant.PL
{
    public partial class KotRemark : Form
    {

        BL_Field bl_field = new BL_Field();
        BL_Sale bl_sale = new BL_Sale();
        main _mainForm;
        BL_Catagory bl_catagory = new BL_Catagory();


        public KotRemark(string Remark,string saleid, main mainfrm)
        {
            _mainForm = mainfrm;
            InitializeComponent();
            cbokotremark.Text = Remark;
            bl_field.SaleId = saleid;

        }

        private void Remark()
        {
            bl_field.KotRemark = cbokotremark.Text;
            bl_sale.KotRemark(bl_field);
            this.Hide();
            _mainForm.PageRefreshEvent();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Remark();
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void KotRemark_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void txtKOTRemark_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Remark();
            }
        }

        private void KotRemark_Load(object sender, EventArgs e)
        {
            BindKotRemark();
        }

        private void BindKotRemark()
        {
            DataTable dt = bl_catagory.SelectKotRemark();
            DataRow row = dt.NewRow();
            row["KotName"] = "";
            dt.Rows.InsertAt(row, 0);

            cbokotremark.DataSource = dt;
            cbokotremark.DisplayMember = "KotName";
            cbokotremark.ValueMember = "KotId";
        }
       
    }
}

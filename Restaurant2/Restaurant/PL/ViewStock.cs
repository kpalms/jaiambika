﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using Restaurant.BL;
using Restaurant.DL;

namespace Restaurant.PL
{
    public partial class ViewStock : Form
    {
        BL_Field bl_field = new BL_Field();
        BL_NewItem bl_newitem = new BL_NewItem();


        public ViewStock()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void ViewStock_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void ViewStock_Load(object sender, EventArgs e)
        {
            BindStock();
        }

        private void BindStock()
        {

            DataTable dt = bl_newitem.SelectStock();
            grdViewStock.DataSource = dt;
            if (grdViewStock.Rows.Count > 0)
            {
                grdViewStock.Columns["StockId"].Visible = false;


            }

 
        }

    }
}

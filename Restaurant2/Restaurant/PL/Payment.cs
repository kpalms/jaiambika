﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Restaurant.BL;
using Restaurant.DL;

namespace Restaurant.PL
{
    public partial class Payment : Form
    {
        public Payment()
        {
            InitializeComponent();
        }
        BL_Worker bl_worker = new BL_Worker();
        int id;
       
        private void btnsave_Click(object sender, EventArgs e)
        {
            if (comboworkers.Text == "SELECT")
            {
                MessageBox.Show("Please Select Worker Name");
                comboworkers.Focus();
            }
            else
            {
                Setfill();
                bl_worker.InsertSalaryCalculator(bl_worker);
                Clear();
                BindCustomerDetails();
            }
        }

        private void Payment_Load(object sender, EventArgs e)
        {
      
            //dgvpayment.Columns["SalaryCalsId"].Visible = false;
            //dgvpayment.Columns["WorkerId"].Visible = false;
            Selectworkerpaymentdetailsdatewise();
            DTP.Text = DateTime.Today.Date.ToString();
            BindWorkers();
            Selectworkerpaymentdetailsdatewise();
            btndelete.Enabled = false;
            button1.Enabled = false;
        }
        private void BindWorkers()
        {
            
            DataTable dt = bl_worker.SelectWorker();
            DataRow row = dt.NewRow();
            row["Name"] = "SELECT";
            dt.Rows.InsertAt(row, 0);
            comboworkers.DataSource = dt;
            comboworkers.DisplayMember = "Name";
            comboworkers.ValueMember = "WorkerId";

        }

        private void Setfill()
        {
           
            bl_worker.Date = DTP.Text;
            bl_worker.WorkerId = Convert.ToInt32(comboworkers.SelectedValue.ToString());
            bl_worker.Credit = Convert.ToDecimal(txtpadiamount.Text);
            bl_worker.Remark = txtremark.Text;
        }

        private void Clear()
        {
            txtremark.Text = "";
            txtpadiamount.Text = "";
           // comboworkers.Text = "";
            btndelete.Enabled = false;
            button1.Enabled = false;
            btnsave.Enabled = true;
           
            DTP.Text =DateTime.Today.Date.ToString();
        }


      

        private void comboworkers_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (comboworkers.Text != "System.Data.DataRowView")
                {
                    if (comboworkers.ValueMember == "WorkerId")
                    {

                        BindCustomerDetails();

                    }

                }
            }
            catch
            {

            }
        }


        private void BindCustomerDetails()
        {
            bl_worker.WorkerId = Convert.ToInt32(comboworkers.SelectedValue.ToString());
            DataTable dt = bl_worker.Selectworkerpaymentdetails(bl_worker);
            dgvpayment.DataSource = dt;
            //dgvpayment.Columns["WorkerId"].Visible = false;
            dgvpayment.Columns["SalaryCalsId"].Visible = false;
            Clear();
            decimal debitamt = 0;
            decimal creditamt = 0;
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["Debit"].ToString() != "")
                    {
                        debitamt = debitamt + Convert.ToDecimal(dt.Rows[i]["Debit"].ToString());
                    }
                    if (dt.Rows[i]["Credit"].ToString() != "")
                    {
                        creditamt = creditamt + Convert.ToDecimal(dt.Rows[i]["Credit"].ToString());
                    }
                }
                lbldebit.Text = Convert.ToString(debitamt);
                lblcredit.Text = Convert.ToString(creditamt);
                lblremaing.Text = Convert.ToString(Convert.ToDecimal(debitamt) - Convert.ToDecimal(creditamt));
            }
            else
            {
                lbldebit.Text = "00";
                lblcredit.Text = "00";
                lblremaing.Text = "00";
            }
        }

        private void txtpadiamount_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            //{
            //    e.Handled = true;
            //    MessageBox.Show("Please enter number only");
            //}
            Common common = new Common();
            common.OneDecimalPoint(txtpadiamount, e);
        }

        private void dgvpayment_CellClick(object sender, DataGridViewCellEventArgs e)
        {
           if(dgvpayment.Rows.Count>0)
           {
               id = Convert.ToInt32(dgvpayment.SelectedRows[0].Cells[0].Value.ToString());
              DTP.Text= dgvpayment.SelectedRows[0].Cells[1].Value.ToString();
              txtpadiamount.Text = dgvpayment.SelectedRows[0].Cells[9].Value.ToString();
              txtremark.Text = dgvpayment.SelectedRows[0].Cells[10].Value.ToString();
           }
           btndelete.Enabled = true;
           button1.Enabled = true;
           btnsave.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboworkers.Text == "SELECT")
            {
                MessageBox.Show("Please Select Worker Name");
                comboworkers.Focus();
            }
            else
            {
                Setfill();
                bl_worker.SalaryCalsId = id;
                bl_worker.UpdateSalaryCalculator(bl_worker);
                Clear();
                BindCustomerDetails();
            }
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {

                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Catagory?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_worker.SalaryCalsId = id;
                    bl_worker.DeleteSalaryCalculator(bl_worker);
                    Clear();
                    BindCustomerDetails();
                    
                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
          
        }

        private void btncancle_Click(object sender, EventArgs e)
        {
            Clear();
            comboworkers.Text = "SELECT";
            lbldebit.Text = "00";
            lblcredit.Text = "00";
            lblremaing.Text = "00";
            dgvpayment.DataSource = null;
            
        }

        private void Selectworkerpaymentdetailsdatewise()
        {
            bl_worker.Date = DTP.Text;
            DataTable dt = bl_worker.Selectworkerpaymentdetailsdatewise(bl_worker);
            dgvpayment.DataSource = dt;
        }

        private void DTP_ValueChanged(object sender, EventArgs e)
        {
            Selectworkerpaymentdetailsdatewise();
        }
    }
}

﻿namespace Restaurant.PL
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        //#region Windows Form Designer generated code

        ///// <summary>
        ///// Required method for Designer support - do not modify
        ///// the contents of this method with the code editor.
        ///// </summary>
        //private void InitializeComponent()
        //{
        //    this.components = new System.ComponentModel.Container();
        //    System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
        //    System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
        //    System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
        //    System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
        //    System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
        //    System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
        //    System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
        //    System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
        //    System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
        //    System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
        //    System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(main));
        //    this.addCatagoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        //    this.addSubCatagoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.addTableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.purchasingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.workerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.catagoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.addCatagoryToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        //    this.addSubCatagoryToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        //    this.vvvToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.label4 = new System.Windows.Forms.Label();
        //    this.txtqty = new System.Windows.Forms.TextBox();
        //    this.printDocument1 = new System.Drawing.Printing.PrintDocument();
        //    this.panel1 = new System.Windows.Forms.Panel();
        //    this.pnltotal = new System.Windows.Forms.Panel();
        //    this.lbldisc2 = new System.Windows.Forms.Label();
        //    this.lblsubtext = new System.Windows.Forms.Label();
        //    this.lblsubtotal = new System.Windows.Forms.Label();
        //    this.lbldistext = new System.Windows.Forms.Label();
        //    this.lbldis = new System.Windows.Forms.Label();
        //    this.lblperson = new System.Windows.Forms.Label();
        //    this.label5 = new System.Windows.Forms.Label();
        //    this.label7 = new System.Windows.Forms.Label();
        //    this.label8 = new System.Windows.Forms.Label();
        //    this.lbltable = new System.Windows.Forms.Label();
        //    this.lbltotal = new System.Windows.Forms.Label();
        //    this.grdOrder = new System.Windows.Forms.DataGridView();
        //    this.lbldisc = new System.Windows.Forms.Label();
        //    this.label3 = new System.Windows.Forms.Label();
        //    this.txtdis = new System.Windows.Forms.TextBox();
        //    this.label2 = new System.Windows.Forms.Label();
        //    this.txtperson = new System.Windows.Forms.TextBox();
        //    this.label1 = new System.Windows.Forms.Label();
        //    this.panel3 = new System.Windows.Forms.Panel();
        //    this.tabControl1 = new System.Windows.Forms.TabControl();
        //    this.tabPage1 = new System.Windows.Forms.TabPage();
        //    this.grdTableDetails = new System.Windows.Forms.DataGridView();
        //    this.tabPage2 = new System.Windows.Forms.TabPage();
        //    this.grdKot = new System.Windows.Forms.DataGridView();
        //    this.tabPage3 = new System.Windows.Forms.TabPage();
        //    this.grdPendingBill = new System.Windows.Forms.DataGridView();
        //    this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
        //    this.grditem = new System.Windows.Forms.DataGridView();
        //    this.txtSearchByNo = new System.Windows.Forms.TextBox();
        //    this.label9 = new System.Windows.Forms.Label();
        //    this.txtsearch = new System.Windows.Forms.TextBox();
        //    this.label6 = new System.Windows.Forms.Label();
        //    this.alertToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.panel4 = new System.Windows.Forms.Panel();
        //    this.rdobar = new System.Windows.Forms.RadioButton();
        //    this.rdoresto = new System.Windows.Forms.RadioButton();
        //    this.label16 = new System.Windows.Forms.Label();
        //    this.label15 = new System.Windows.Forms.Label();
        //    this.lblcashierName = new System.Windows.Forms.Label();
        //    this.txtworker = new System.Windows.Forms.TextBox();
        //    this.txttableno = new System.Windows.Forms.TextBox();
        //    this.lbltime = new System.Windows.Forms.Label();
        //    this.txtshortname = new System.Windows.Forms.TextBox();
        //    this.label14 = new System.Windows.Forms.Label();
        //    this.btndis = new System.Windows.Forms.Button();
        //    this.btnshowtable = new System.Windows.Forms.Button();
        //    this.lsttableno = new System.Windows.Forms.ListBox();
        //    this.lstwaiter = new System.Windows.Forms.ListBox();
        //    this.timer1 = new System.Windows.Forms.Timer(this.components);
        //    this.btnQT = new System.Windows.Forms.Button();
        //    this.btncancelBill = new System.Windows.Forms.Button();
        //    this.btnchnageperson = new System.Windows.Forms.Button();
        //    this.btnChangeQty = new System.Windows.Forms.Button();
        //    this.button3 = new System.Windows.Forms.Button();
        //    this.btnRemoveItem = new System.Windows.Forms.Button();
        //    this.btnChangePrice = new System.Windows.Forms.Button();
        //    this.btnAddOrder = new System.Windows.Forms.Button();
        //    this.panel2 = new System.Windows.Forms.Panel();
        //    this.button1 = new System.Windows.Forms.Button();
        //    this.menuStrip2 = new System.Windows.Forms.MenuStrip();
        //    this.saleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.toolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.hotelSettingToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        //    this.securityToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        //    this.kotReprintToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.billReprintToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.cashBookToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.shiftingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.setupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.addCatagoryToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
        //    this.addItemToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        //    this.addTableToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        //    this.addSupplierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.workerToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
        //    this.createUnitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.creatPackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.messageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.printerSettingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.transactionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.purchasingToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        //    this.inventoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.viewStockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.reportsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        //    this.restaurantToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.periodWiseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.billWiseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.cashierWiseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.saleByItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.bToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.periodWiseToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        //    this.billWiseToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        //    this.cashierWiseToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        //    this.itemWiseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.lodgeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.checkInReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.checkOutReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.billWiseToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
        //    this.cashierWiseToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
        //    this.roomWiseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.kotAuditBotAuditToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.kotWiseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.botWiseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.kotItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.botItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.todayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.yesturdayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.saleByTableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.cancelBillListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.itemPriceListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.restaurantToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        //    this.barToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.customerListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.purchasingReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.cashBookReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.windowsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.calculatorToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        //    this.notePadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.lodgingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.addRoomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.checkInToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.checkOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.viewAllCheckOutCustomerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.billSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        //    this.panel1.SuspendLayout();
        //    this.pnltotal.SuspendLayout();
        //    ((System.ComponentModel.ISupportInitialize)(this.grdOrder)).BeginInit();
        //    this.panel3.SuspendLayout();
        //    this.tabControl1.SuspendLayout();
        //    this.tabPage1.SuspendLayout();
        //    ((System.ComponentModel.ISupportInitialize)(this.grdTableDetails)).BeginInit();
        //    this.tabPage2.SuspendLayout();
        //    ((System.ComponentModel.ISupportInitialize)(this.grdKot)).BeginInit();
        //    this.tabPage3.SuspendLayout();
        //    ((System.ComponentModel.ISupportInitialize)(this.grdPendingBill)).BeginInit();
        //    ((System.ComponentModel.ISupportInitialize)(this.grditem)).BeginInit();
        //    this.panel4.SuspendLayout();
        //    this.panel2.SuspendLayout();
        //    this.menuStrip2.SuspendLayout();
        //    this.SuspendLayout();
        //    // 
        //    // addCatagoryToolStripMenuItem
        //    // 
        //    this.addCatagoryToolStripMenuItem.Name = "addCatagoryToolStripMenuItem";
        //    this.addCatagoryToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
        //    // 
        //    // toolStripMenuItem1
        //    // 
        //    this.toolStripMenuItem1.Name = "toolStripMenuItem1";
        //    this.toolStripMenuItem1.Size = new System.Drawing.Size(167, 22);
        //    // 
        //    // addSubCatagoryToolStripMenuItem
        //    // 
        //    this.addSubCatagoryToolStripMenuItem.Name = "addSubCatagoryToolStripMenuItem";
        //    this.addSubCatagoryToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
        //    // 
        //    // addTableToolStripMenuItem
        //    // 
        //    this.addTableToolStripMenuItem.Name = "addTableToolStripMenuItem";
        //    this.addTableToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
        //    // 
        //    // purchasingToolStripMenuItem
        //    // 
        //    this.purchasingToolStripMenuItem.Name = "purchasingToolStripMenuItem";
        //    this.purchasingToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
        //    // 
        //    // workerToolStripMenuItem
        //    // 
        //    this.workerToolStripMenuItem.Name = "workerToolStripMenuItem";
        //    this.workerToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
        //    // 
        //    // reportsToolStripMenuItem
        //    // 
        //    this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
        //    this.reportsToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
        //    // 
        //    // catagoryToolStripMenuItem
        //    // 
        //    this.catagoryToolStripMenuItem.Name = "catagoryToolStripMenuItem";
        //    this.catagoryToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
        //    this.catagoryToolStripMenuItem.Text = "Catagory";
        //    // 
        //    // addCatagoryToolStripMenuItem1
        //    // 
        //    this.addCatagoryToolStripMenuItem1.Name = "addCatagoryToolStripMenuItem1";
        //    this.addCatagoryToolStripMenuItem1.Size = new System.Drawing.Size(32, 19);
        //    // 
        //    // addSubCatagoryToolStripMenuItem1
        //    // 
        //    this.addSubCatagoryToolStripMenuItem1.Name = "addSubCatagoryToolStripMenuItem1";
        //    this.addSubCatagoryToolStripMenuItem1.Size = new System.Drawing.Size(32, 19);
        //    // 
        //    // vvvToolStripMenuItem
        //    // 
        //    this.vvvToolStripMenuItem.Name = "vvvToolStripMenuItem";
        //    this.vvvToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
        //    // 
        //    // label4
        //    // 
        //    this.label4.AutoSize = true;
        //    this.label4.BackColor = System.Drawing.Color.Transparent;
        //    this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.label4.Location = new System.Drawing.Point(323, 0);
        //    this.label4.Name = "label4";
        //    this.label4.Size = new System.Drawing.Size(44, 13);
        //    this.label4.TabIndex = 30;
        //    this.label4.Text = "Qty F6";
        //    // 
        //    // txtqty
        //    // 
        //    this.txtqty.BackColor = System.Drawing.Color.AntiqueWhite;
        //    this.txtqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        //    this.txtqty.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.txtqty.Location = new System.Drawing.Point(326, 17);
        //    this.txtqty.Name = "txtqty";
        //    this.txtqty.Size = new System.Drawing.Size(82, 35);
        //    this.txtqty.TabIndex = 3;
        //    this.txtqty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtqty_KeyDown);
        //    this.txtqty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtqty_KeyPress);
        //    // 
        //    // panel1
        //    // 
        //    this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        //    this.panel1.Controls.Add(this.pnltotal);
        //    this.panel1.Controls.Add(this.grdOrder);
        //    this.panel1.Location = new System.Drawing.Point(18, 135);
        //    this.panel1.Name = "panel1";
        //    this.panel1.Size = new System.Drawing.Size(374, 576);
        //    this.panel1.TabIndex = 37;
        //    // 
        //    // pnltotal
        //    // 
        //    this.pnltotal.BackColor = System.Drawing.Color.AntiqueWhite;
        //    this.pnltotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        //    this.pnltotal.Controls.Add(this.lbldisc2);
        //    this.pnltotal.Controls.Add(this.lblsubtext);
        //    this.pnltotal.Controls.Add(this.lblsubtotal);
        //    this.pnltotal.Controls.Add(this.lbldistext);
        //    this.pnltotal.Controls.Add(this.lbldis);
        //    this.pnltotal.Controls.Add(this.lblperson);
        //    this.pnltotal.Controls.Add(this.label5);
        //    this.pnltotal.Controls.Add(this.label7);
        //    this.pnltotal.Controls.Add(this.label8);
        //    this.pnltotal.Controls.Add(this.lbltable);
        //    this.pnltotal.Controls.Add(this.lbltotal);
        //    this.pnltotal.Location = new System.Drawing.Point(8, 391);
        //    this.pnltotal.Name = "pnltotal";
        //    this.pnltotal.Size = new System.Drawing.Size(359, 168);
        //    this.pnltotal.TabIndex = 34;
        //    // 
        //    // lbldisc2
        //    // 
        //    this.lbldisc2.AutoSize = true;
        //    this.lbldisc2.BackColor = System.Drawing.Color.Transparent;
        //    this.lbldisc2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.lbldisc2.Location = new System.Drawing.Point(239, 44);
        //    this.lbldisc2.Name = "lbldisc2";
        //    this.lbldisc2.Size = new System.Drawing.Size(77, 24);
        //    this.lbldisc2.TabIndex = 26;
        //    this.lbldisc2.Text = "label10";
        //    // 
        //    // lblsubtext
        //    // 
        //    this.lblsubtext.AutoSize = true;
        //    this.lblsubtext.BackColor = System.Drawing.Color.Transparent;
        //    this.lblsubtext.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.lblsubtext.Location = new System.Drawing.Point(7, 8);
        //    this.lblsubtext.Name = "lblsubtext";
        //    this.lblsubtext.Size = new System.Drawing.Size(90, 24);
        //    this.lblsubtext.TabIndex = 25;
        //    this.lblsubtext.Text = "Sub Total";
        //    // 
        //    // lblsubtotal
        //    // 
        //    this.lblsubtotal.AutoSize = true;
        //    this.lblsubtotal.BackColor = System.Drawing.Color.Transparent;
        //    this.lblsubtotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.lblsubtotal.Location = new System.Drawing.Point(191, 11);
        //    this.lblsubtotal.Name = "lblsubtotal";
        //    this.lblsubtotal.Size = new System.Drawing.Size(93, 24);
        //    this.lblsubtotal.TabIndex = 24;
        //    this.lblsubtotal.Text = "SubTotal";
        //    // 
        //    // lbldistext
        //    // 
        //    this.lbldistext.AutoSize = true;
        //    this.lbldistext.BackColor = System.Drawing.Color.Transparent;
        //    this.lbldistext.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.lbldistext.Location = new System.Drawing.Point(7, 39);
        //    this.lbldistext.Name = "lbldistext";
        //    this.lbldistext.Size = new System.Drawing.Size(83, 24);
        //    this.lbldistext.TabIndex = 23;
        //    this.lbldistext.Text = "Discount";
        //    // 
        //    // lbldis
        //    // 
        //    this.lbldis.AutoSize = true;
        //    this.lbldis.BackColor = System.Drawing.Color.Transparent;
        //    this.lbldis.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.lbldis.Location = new System.Drawing.Point(192, 44);
        //    this.lbldis.Name = "lbldis";
        //    this.lbldis.Size = new System.Drawing.Size(77, 24);
        //    this.lbldis.TabIndex = 22;
        //    this.lbldis.Text = "label10";
        //    // 
        //    // lblperson
        //    // 
        //    this.lblperson.AutoSize = true;
        //    this.lblperson.BackColor = System.Drawing.Color.Transparent;
        //    this.lblperson.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.lblperson.Location = new System.Drawing.Point(191, 74);
        //    this.lblperson.Name = "lblperson";
        //    this.lblperson.Size = new System.Drawing.Size(77, 24);
        //    this.lblperson.TabIndex = 8;
        //    this.lblperson.Text = "label12";
        //    // 
        //    // label5
        //    // 
        //    this.label5.AutoSize = true;
        //    this.label5.BackColor = System.Drawing.Color.Transparent;
        //    this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.label5.Location = new System.Drawing.Point(7, 134);
        //    this.label5.Name = "label5";
        //    this.label5.Size = new System.Drawing.Size(51, 24);
        //    this.label5.TabIndex = 1;
        //    this.label5.Text = "Total";
        //    // 
        //    // label7
        //    // 
        //    this.label7.AutoSize = true;
        //    this.label7.BackColor = System.Drawing.Color.Transparent;
        //    this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.label7.Location = new System.Drawing.Point(7, 100);
        //    this.label7.Name = "label7";
        //    this.label7.Size = new System.Drawing.Size(58, 24);
        //    this.label7.TabIndex = 3;
        //    this.label7.Text = "Table";
        //    // 
        //    // label8
        //    // 
        //    this.label8.AutoSize = true;
        //    this.label8.BackColor = System.Drawing.Color.Transparent;
        //    this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.label8.Location = new System.Drawing.Point(7, 70);
        //    this.label8.Name = "label8";
        //    this.label8.Size = new System.Drawing.Size(70, 24);
        //    this.label8.TabIndex = 4;
        //    this.label8.Text = "Person";
        //    // 
        //    // lbltable
        //    // 
        //    this.lbltable.AutoSize = true;
        //    this.lbltable.BackColor = System.Drawing.Color.Transparent;
        //    this.lbltable.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.lbltable.Location = new System.Drawing.Point(191, 103);
        //    this.lbltable.Name = "lbltable";
        //    this.lbltable.Size = new System.Drawing.Size(77, 24);
        //    this.lbltable.TabIndex = 7;
        //    this.lbltable.Text = "label11";
        //    // 
        //    // lbltotal
        //    // 
        //    this.lbltotal.AutoSize = true;
        //    this.lbltotal.BackColor = System.Drawing.Color.Transparent;
        //    this.lbltotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.lbltotal.ForeColor = System.Drawing.Color.Red;
        //    this.lbltotal.Location = new System.Drawing.Point(191, 134);
        //    this.lbltotal.Name = "lbltotal";
        //    this.lbltotal.Size = new System.Drawing.Size(77, 24);
        //    this.lbltotal.TabIndex = 9;
        //    this.lbltotal.Text = "label13";
        //    // 
        //    // grdOrder
        //    // 
        //    this.grdOrder.AllowUserToDeleteRows = false;
        //    this.grdOrder.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
        //    this.grdOrder.BackgroundColor = System.Drawing.Color.AntiqueWhite;
        //    dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        //    dataGridViewCellStyle31.BackColor = System.Drawing.Color.PeachPuff;
        //    dataGridViewCellStyle31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    dataGridViewCellStyle31.ForeColor = System.Drawing.SystemColors.WindowText;
        //    dataGridViewCellStyle31.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        //    dataGridViewCellStyle31.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        //    dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        //    this.grdOrder.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle31;
        //    this.grdOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        //    dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        //    dataGridViewCellStyle32.BackColor = System.Drawing.Color.White;
        //    dataGridViewCellStyle32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    dataGridViewCellStyle32.ForeColor = System.Drawing.SystemColors.ControlText;
        //    dataGridViewCellStyle32.SelectionBackColor = System.Drawing.Color.Orange;
        //    dataGridViewCellStyle32.SelectionForeColor = System.Drawing.Color.Black;
        //    dataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
        //    this.grdOrder.DefaultCellStyle = dataGridViewCellStyle32;
        //    this.grdOrder.EnableHeadersVisualStyles = false;
        //    this.grdOrder.Location = new System.Drawing.Point(8, 12);
        //    this.grdOrder.MultiSelect = false;
        //    this.grdOrder.Name = "grdOrder";
        //    this.grdOrder.RowHeadersVisible = false;
        //    this.grdOrder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
        //    this.grdOrder.Size = new System.Drawing.Size(359, 209);
        //    this.grdOrder.TabIndex = 27;
        //    this.grdOrder.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdOrder_CellValueChanged);
        //    this.grdOrder.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdOrder_CellClick);
        //    this.grdOrder.SelectionChanged += new System.EventHandler(this.grdOrder_SelectionChanged);
        //    // 
        //    // lbldisc
        //    // 
        //    this.lbldisc.AutoSize = true;
        //    this.lbldisc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.lbldisc.Location = new System.Drawing.Point(695, 33);
        //    this.lbldisc.Name = "lbldisc";
        //    this.lbldisc.Size = new System.Drawing.Size(16, 13);
        //    this.lbldisc.TabIndex = 35;
        //    this.lbldisc.Text = "%";
        //    this.lbldisc.Visible = false;
        //    // 
        //    // label3
        //    // 
        //    this.label3.AutoSize = true;
        //    this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.label3.Location = new System.Drawing.Point(635, 5);
        //    this.label3.Name = "label3";
        //    this.label3.Size = new System.Drawing.Size(57, 13);
        //    this.label3.TabIndex = 32;
        //    this.label3.Text = "Discount";
        //    // 
        //    // txtdis
        //    // 
        //    this.txtdis.Location = new System.Drawing.Point(638, 23);
        //    this.txtdis.Multiline = true;
        //    this.txtdis.Name = "txtdis";
        //    this.txtdis.ReadOnly = true;
        //    this.txtdis.Size = new System.Drawing.Size(53, 33);
        //    this.txtdis.TabIndex = 31;
        //    // 
        //    // label2
        //    // 
        //    this.label2.AutoSize = true;
        //    this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.label2.Location = new System.Drawing.Point(545, 4);
        //    this.label2.Name = "label2";
        //    this.label2.Size = new System.Drawing.Size(46, 13);
        //    this.label2.TabIndex = 30;
        //    this.label2.Text = "Person";
        //    // 
        //    // txtperson
        //    // 
        //    this.txtperson.BackColor = System.Drawing.Color.AntiqueWhite;
        //    this.txtperson.Location = new System.Drawing.Point(548, 21);
        //    this.txtperson.Multiline = true;
        //    this.txtperson.Name = "txtperson";
        //    this.txtperson.Size = new System.Drawing.Size(53, 33);
        //    this.txtperson.TabIndex = 6;
        //    this.txtperson.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtperson_KeyDown);
        //    // 
        //    // label1
        //    // 
        //    this.label1.AutoSize = true;
        //    this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.label1.Location = new System.Drawing.Point(116, 2);
        //    this.label1.Name = "label1";
        //    this.label1.Size = new System.Drawing.Size(57, 13);
        //    this.label1.TabIndex = 26;
        //    this.label1.Text = "Table F2";
        //    // 
        //    // panel3
        //    // 
        //    this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        //    this.panel3.Controls.Add(this.tabControl1);
        //    this.panel3.Controls.Add(this.flowLayoutPanel1);
        //    this.panel3.Controls.Add(this.grditem);
        //    this.panel3.Controls.Add(this.txtSearchByNo);
        //    this.panel3.Controls.Add(this.label9);
        //    this.panel3.Controls.Add(this.txtsearch);
        //    this.panel3.Controls.Add(this.label6);
        //    this.panel3.Location = new System.Drawing.Point(484, 135);
        //    this.panel3.Name = "panel3";
        //    this.panel3.Size = new System.Drawing.Size(520, 576);
        //    this.panel3.TabIndex = 36;
        //    // 
        //    // tabControl1
        //    // 
        //    this.tabControl1.Controls.Add(this.tabPage1);
        //    this.tabControl1.Controls.Add(this.tabPage2);
        //    this.tabControl1.Controls.Add(this.tabPage3);
        //    this.tabControl1.Location = new System.Drawing.Point(341, 16);
        //    this.tabControl1.Name = "tabControl1";
        //    this.tabControl1.SelectedIndex = 0;
        //    this.tabControl1.Size = new System.Drawing.Size(174, 544);
        //    this.tabControl1.TabIndex = 65;
        //    // 
        //    // tabPage1
        //    // 
        //    this.tabPage1.BackColor = System.Drawing.Color.AntiqueWhite;
        //    this.tabPage1.Controls.Add(this.grdTableDetails);
        //    this.tabPage1.Location = new System.Drawing.Point(4, 22);
        //    this.tabPage1.Name = "tabPage1";
        //    this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
        //    this.tabPage1.Size = new System.Drawing.Size(166, 518);
        //    this.tabPage1.TabIndex = 0;
        //    this.tabPage1.Text = "Table";
        //    this.tabPage1.UseVisualStyleBackColor = true;
        //    // 
        //    // grdTableDetails
        //    // 
        //    this.grdTableDetails.AllowUserToAddRows = false;
        //    this.grdTableDetails.AllowUserToDeleteRows = false;
        //    this.grdTableDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
        //    this.grdTableDetails.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
        //    this.grdTableDetails.BackgroundColor = System.Drawing.Color.AntiqueWhite;
        //    dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        //    dataGridViewCellStyle33.BackColor = System.Drawing.Color.PeachPuff;
        //    dataGridViewCellStyle33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    dataGridViewCellStyle33.ForeColor = System.Drawing.SystemColors.WindowText;
        //    dataGridViewCellStyle33.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        //    dataGridViewCellStyle33.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        //    dataGridViewCellStyle33.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        //    this.grdTableDetails.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle33;
        //    this.grdTableDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        //    this.grdTableDetails.Cursor = System.Windows.Forms.Cursors.Hand;
        //    dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        //    dataGridViewCellStyle34.BackColor = System.Drawing.Color.White;
        //    dataGridViewCellStyle34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    dataGridViewCellStyle34.ForeColor = System.Drawing.SystemColors.ControlText;
        //    dataGridViewCellStyle34.SelectionBackColor = System.Drawing.Color.Orange;
        //    dataGridViewCellStyle34.SelectionForeColor = System.Drawing.Color.Black;
        //    dataGridViewCellStyle34.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
        //    this.grdTableDetails.DefaultCellStyle = dataGridViewCellStyle34;
        //    this.grdTableDetails.EnableHeadersVisualStyles = false;
        //    this.grdTableDetails.Location = new System.Drawing.Point(8, 14);
        //    this.grdTableDetails.MultiSelect = false;
        //    this.grdTableDetails.Name = "grdTableDetails";
        //    this.grdTableDetails.ReadOnly = true;
        //    this.grdTableDetails.RowHeadersVisible = false;
        //    this.grdTableDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
        //    this.grdTableDetails.Size = new System.Drawing.Size(153, 489);
        //    this.grdTableDetails.TabIndex = 30;
        //    this.grdTableDetails.TabStop = false;
        //    this.grdTableDetails.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdTableDetails_CellClick);
        //    // 
        //    // tabPage2
        //    // 
        //    this.tabPage2.Controls.Add(this.grdKot);
        //    this.tabPage2.Location = new System.Drawing.Point(4, 22);
        //    this.tabPage2.Name = "tabPage2";
        //    this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
        //    this.tabPage2.Size = new System.Drawing.Size(166, 518);
        //    this.tabPage2.TabIndex = 1;
        //    this.tabPage2.Text = "KOT";
        //    this.tabPage2.UseVisualStyleBackColor = true;
        //    // 
        //    // grdKot
        //    // 
        //    this.grdKot.AllowUserToAddRows = false;
        //    this.grdKot.AllowUserToDeleteRows = false;
        //    this.grdKot.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
        //    this.grdKot.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
        //    this.grdKot.BackgroundColor = System.Drawing.Color.AntiqueWhite;
        //    dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        //    dataGridViewCellStyle35.BackColor = System.Drawing.Color.PeachPuff;
        //    dataGridViewCellStyle35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    dataGridViewCellStyle35.ForeColor = System.Drawing.SystemColors.WindowText;
        //    dataGridViewCellStyle35.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        //    dataGridViewCellStyle35.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        //    dataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        //    this.grdKot.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle35;
        //    this.grdKot.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        //    this.grdKot.Cursor = System.Windows.Forms.Cursors.Hand;
        //    dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        //    dataGridViewCellStyle36.BackColor = System.Drawing.Color.White;
        //    dataGridViewCellStyle36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    dataGridViewCellStyle36.ForeColor = System.Drawing.SystemColors.ControlText;
        //    dataGridViewCellStyle36.SelectionBackColor = System.Drawing.Color.Orange;
        //    dataGridViewCellStyle36.SelectionForeColor = System.Drawing.Color.Black;
        //    dataGridViewCellStyle36.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
        //    this.grdKot.DefaultCellStyle = dataGridViewCellStyle36;
        //    this.grdKot.EnableHeadersVisualStyles = false;
        //    this.grdKot.Location = new System.Drawing.Point(10, 15);
        //    this.grdKot.MultiSelect = false;
        //    this.grdKot.Name = "grdKot";
        //    this.grdKot.ReadOnly = true;
        //    this.grdKot.RowHeadersVisible = false;
        //    this.grdKot.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
        //    this.grdKot.Size = new System.Drawing.Size(153, 489);
        //    this.grdKot.TabIndex = 31;
        //    this.grdKot.TabStop = false;
        //    // 
        //    // tabPage3
        //    // 
        //    this.tabPage3.Controls.Add(this.grdPendingBill);
        //    this.tabPage3.Location = new System.Drawing.Point(4, 22);
        //    this.tabPage3.Name = "tabPage3";
        //    this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
        //    this.tabPage3.Size = new System.Drawing.Size(166, 518);
        //    this.tabPage3.TabIndex = 2;
        //    this.tabPage3.Text = "PendingBill";
        //    this.tabPage3.UseVisualStyleBackColor = true;
        //    // 
        //    // grdPendingBill
        //    // 
        //    this.grdPendingBill.AllowUserToAddRows = false;
        //    this.grdPendingBill.AllowUserToDeleteRows = false;
        //    this.grdPendingBill.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
        //    this.grdPendingBill.BackgroundColor = System.Drawing.Color.AntiqueWhite;
        //    dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        //    dataGridViewCellStyle37.BackColor = System.Drawing.Color.PeachPuff;
        //    dataGridViewCellStyle37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    dataGridViewCellStyle37.ForeColor = System.Drawing.SystemColors.WindowText;
        //    dataGridViewCellStyle37.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        //    dataGridViewCellStyle37.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        //    dataGridViewCellStyle37.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        //    this.grdPendingBill.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle37;
        //    this.grdPendingBill.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        //    this.grdPendingBill.Cursor = System.Windows.Forms.Cursors.Hand;
        //    dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        //    dataGridViewCellStyle38.BackColor = System.Drawing.Color.White;
        //    dataGridViewCellStyle38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    dataGridViewCellStyle38.ForeColor = System.Drawing.SystemColors.ControlText;
        //    dataGridViewCellStyle38.SelectionBackColor = System.Drawing.Color.Orange;
        //    dataGridViewCellStyle38.SelectionForeColor = System.Drawing.Color.Black;
        //    dataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
        //    this.grdPendingBill.DefaultCellStyle = dataGridViewCellStyle38;
        //    this.grdPendingBill.EnableHeadersVisualStyles = false;
        //    this.grdPendingBill.Location = new System.Drawing.Point(10, 15);
        //    this.grdPendingBill.MultiSelect = false;
        //    this.grdPendingBill.Name = "grdPendingBill";
        //    this.grdPendingBill.ReadOnly = true;
        //    this.grdPendingBill.RowHeadersVisible = false;
        //    this.grdPendingBill.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
        //    this.grdPendingBill.Size = new System.Drawing.Size(153, 489);
        //    this.grdPendingBill.TabIndex = 31;
        //    this.grdPendingBill.TabStop = false;
        //    this.grdPendingBill.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdPendingBill_CellClick);
        //    this.grdPendingBill.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grdPendingBill_KeyDown);
        //    // 
        //    // flowLayoutPanel1
        //    // 
        //    this.flowLayoutPanel1.AutoScroll = true;
        //    this.flowLayoutPanel1.BackColor = System.Drawing.Color.AntiqueWhite;
        //    this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        //    this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 13);
        //    this.flowLayoutPanel1.Name = "flowLayoutPanel1";
        //    this.flowLayoutPanel1.Size = new System.Drawing.Size(334, 209);
        //    this.flowLayoutPanel1.TabIndex = 30;
        //    // 
        //    // grditem
        //    // 
        //    this.grditem.AllowUserToAddRows = false;
        //    this.grditem.AllowUserToDeleteRows = false;
        //    this.grditem.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
        //    this.grditem.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
        //    this.grditem.BackgroundColor = System.Drawing.Color.AntiqueWhite;
        //    dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        //    dataGridViewCellStyle39.BackColor = System.Drawing.Color.PeachPuff;
        //    dataGridViewCellStyle39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    dataGridViewCellStyle39.ForeColor = System.Drawing.SystemColors.WindowText;
        //    dataGridViewCellStyle39.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        //    dataGridViewCellStyle39.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        //    dataGridViewCellStyle39.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        //    this.grditem.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle39;
        //    this.grditem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        //    this.grditem.Cursor = System.Windows.Forms.Cursors.Hand;
        //    dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        //    dataGridViewCellStyle40.BackColor = System.Drawing.Color.White;
        //    dataGridViewCellStyle40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    dataGridViewCellStyle40.ForeColor = System.Drawing.SystemColors.ControlText;
        //    dataGridViewCellStyle40.SelectionBackColor = System.Drawing.Color.Orange;
        //    dataGridViewCellStyle40.SelectionForeColor = System.Drawing.Color.Black;
        //    dataGridViewCellStyle40.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
        //    this.grditem.DefaultCellStyle = dataGridViewCellStyle40;
        //    this.grditem.EnableHeadersVisualStyles = false;
        //    this.grditem.Location = new System.Drawing.Point(2, 282);
        //    this.grditem.MultiSelect = false;
        //    this.grditem.Name = "grditem";
        //    this.grditem.ReadOnly = true;
        //    this.grditem.RowHeadersVisible = false;
        //    this.grditem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
        //    this.grditem.Size = new System.Drawing.Size(334, 278);
        //    this.grditem.TabIndex = 29;
        //    this.grditem.TabStop = false;
        //    this.grditem.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grditem_CellDoubleClick);
        //    this.grditem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grditem_KeyDown);
        //    // 
        //    // txtSearchByNo
        //    // 
        //    this.txtSearchByNo.BackColor = System.Drawing.Color.AntiqueWhite;
        //    this.txtSearchByNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        //    this.txtSearchByNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.txtSearchByNo.Location = new System.Drawing.Point(2, 241);
        //    this.txtSearchByNo.Name = "txtSearchByNo";
        //    this.txtSearchByNo.Size = new System.Drawing.Size(167, 35);
        //    this.txtSearchByNo.TabIndex = 3;
        //    this.txtSearchByNo.TextChanged += new System.EventHandler(this.txtSearchByNo_TextChanged);
        //    this.txtSearchByNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearchByNo_KeyDown);
        //    this.txtSearchByNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSearchByNo_KeyPress);
        //    // 
        //    // label9
        //    // 
        //    this.label9.AutoSize = true;
        //    this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.label9.Location = new System.Drawing.Point(-1, 225);
        //    this.label9.Name = "label9";
        //    this.label9.Size = new System.Drawing.Size(85, 13);
        //    this.label9.TabIndex = 32;
        //    this.label9.Text = "Search No F3";
        //    // 
        //    // txtsearch
        //    // 
        //    this.txtsearch.BackColor = System.Drawing.Color.AntiqueWhite;
        //    this.txtsearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        //    this.txtsearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.txtsearch.Location = new System.Drawing.Point(175, 242);
        //    this.txtsearch.Name = "txtsearch";
        //    this.txtsearch.Size = new System.Drawing.Size(161, 35);
        //    this.txtsearch.TabIndex = 4;
        //    this.txtsearch.TextChanged += new System.EventHandler(this.txtsearch_TextChanged);
        //    this.txtsearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtsearch_KeyDown);
        //    this.txtsearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtsearch_KeyPress);
        //    // 
        //    // label6
        //    // 
        //    this.label6.AutoSize = true;
        //    this.label6.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.label6.Location = new System.Drawing.Point(223, 225);
        //    this.label6.Name = "label6";
        //    this.label6.Size = new System.Drawing.Size(85, 14);
        //    this.label6.TabIndex = 28;
        //    this.label6.Text = "ItemsName F5";
        //    // 
        //    // alertToolStripMenuItem
        //    // 
        //    this.alertToolStripMenuItem.Name = "alertToolStripMenuItem";
        //    this.alertToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
        //    // 
        //    // panel4
        //    // 
        //    this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        //    this.panel4.Controls.Add(this.rdobar);
        //    this.panel4.Controls.Add(this.rdoresto);
        //    this.panel4.Controls.Add(this.label16);
        //    this.panel4.Controls.Add(this.label15);
        //    this.panel4.Controls.Add(this.lblcashierName);
        //    this.panel4.Controls.Add(this.txtworker);
        //    this.panel4.Controls.Add(this.txttableno);
        //    this.panel4.Controls.Add(this.lbltime);
        //    this.panel4.Controls.Add(this.txtshortname);
        //    this.panel4.Controls.Add(this.label14);
        //    this.panel4.Controls.Add(this.btndis);
        //    this.panel4.Controls.Add(this.lbldisc);
        //    this.panel4.Controls.Add(this.btnshowtable);
        //    this.panel4.Controls.Add(this.label1);
        //    this.panel4.Controls.Add(this.label3);
        //    this.panel4.Controls.Add(this.label4);
        //    this.panel4.Controls.Add(this.txtqty);
        //    this.panel4.Controls.Add(this.txtdis);
        //    this.panel4.Controls.Add(this.txtperson);
        //    this.panel4.Controls.Add(this.label2);
        //    this.panel4.Location = new System.Drawing.Point(18, 34);
        //    this.panel4.Name = "panel4";
        //    this.panel4.Size = new System.Drawing.Size(985, 95);
        //    this.panel4.TabIndex = 39;
        //    // 
        //    // rdobar
        //    // 
        //    this.rdobar.AutoSize = true;
        //    this.rdobar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.rdobar.Location = new System.Drawing.Point(612, 70);
        //    this.rdobar.Name = "rdobar";
        //    this.rdobar.Size = new System.Drawing.Size(86, 17);
        //    this.rdobar.TabIndex = 72;
        //    this.rdobar.TabStop = true;
        //    this.rdobar.Text = "Bar(Ctrl+B)";
        //    this.rdobar.UseVisualStyleBackColor = true;
        //    this.rdobar.CheckedChanged += new System.EventHandler(this.rdobar_CheckedChanged_1);
        //    // 
        //    // rdoresto
        //    // 
        //    this.rdoresto.AutoSize = true;
        //    this.rdoresto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.rdoresto.Location = new System.Drawing.Point(465, 70);
        //    this.rdoresto.Name = "rdoresto";
        //    this.rdoresto.Size = new System.Drawing.Size(130, 17);
        //    this.rdoresto.TabIndex = 71;
        //    this.rdoresto.TabStop = true;
        //    this.rdoresto.Text = "Restaurant(Ctrl+R)";
        //    this.rdoresto.UseVisualStyleBackColor = true;
        //    this.rdoresto.CheckedChanged += new System.EventHandler(this.rdoresto_CheckedChanged);
        //    // 
        //    // label16
        //    // 
        //    this.label16.AutoSize = true;
        //    this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.label16.Location = new System.Drawing.Point(4, 2);
        //    this.label16.Name = "label16";
        //    this.label16.Size = new System.Drawing.Size(59, 13);
        //    this.label16.TabIndex = 70;
        //    this.label16.Text = "Welcome";
        //    // 
        //    // label15
        //    // 
        //    this.label15.AutoSize = true;
        //    this.label15.BackColor = System.Drawing.Color.Transparent;
        //    this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.label15.Location = new System.Drawing.Point(205, 2);
        //    this.label15.Name = "label15";
        //    this.label15.Size = new System.Drawing.Size(80, 13);
        //    this.label15.TabIndex = 69;
        //    this.label15.Text = "Waiter Name";
        //    // 
        //    // lblcashierName
        //    // 
        //    this.lblcashierName.AutoSize = true;
        //    this.lblcashierName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.lblcashierName.Location = new System.Drawing.Point(3, 19);
        //    this.lblcashierName.Name = "lblcashierName";
        //    this.lblcashierName.Size = new System.Drawing.Size(93, 13);
        //    this.lblcashierName.TabIndex = 67;
        //    this.lblcashierName.Text = "lblcashierName";
        //    // 
        //    // txtworker
        //    // 
        //    this.txtworker.BackColor = System.Drawing.Color.AntiqueWhite;
        //    this.txtworker.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        //    this.txtworker.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.txtworker.Location = new System.Drawing.Point(204, 17);
        //    this.txtworker.Name = "txtworker";
        //    this.txtworker.Size = new System.Drawing.Size(116, 35);
        //    this.txtworker.TabIndex = 1;
        //    this.txtworker.TextChanged += new System.EventHandler(this.txtworker_TextChanged);
        //    this.txtworker.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtworker_KeyDown);
        //    this.txtworker.Leave += new System.EventHandler(this.txtworker_Leave);
        //    // 
        //    // txttableno
        //    // 
        //    this.txttableno.BackColor = System.Drawing.Color.AntiqueWhite;
        //    this.txttableno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        //    this.txttableno.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.txttableno.Location = new System.Drawing.Point(119, 19);
        //    this.txttableno.Name = "txttableno";
        //    this.txttableno.Size = new System.Drawing.Size(77, 35);
        //    this.txttableno.TabIndex = 2;
        //    this.txttableno.TextChanged += new System.EventHandler(this.txttableno_TextChanged);
        //    this.txttableno.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttableno_KeyDown);
        //    this.txttableno.Leave += new System.EventHandler(this.txttableno_Leave);
        //    // 
        //    // lbltime
        //    // 
        //    this.lbltime.AutoSize = true;
        //    this.lbltime.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.lbltime.Location = new System.Drawing.Point(845, 17);
        //    this.lbltime.Name = "lbltime";
        //    this.lbltime.Size = new System.Drawing.Size(60, 25);
        //    this.lbltime.TabIndex = 66;
        //    this.lbltime.Text = "Time";
        //    // 
        //    // txtshortname
        //    // 
        //    this.txtshortname.BackColor = System.Drawing.Color.AntiqueWhite;
        //    this.txtshortname.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.txtshortname.Location = new System.Drawing.Point(414, 19);
        //    this.txtshortname.Name = "txtshortname";
        //    this.txtshortname.Size = new System.Drawing.Size(128, 35);
        //    this.txtshortname.TabIndex = 4;
        //    this.txtshortname.TextChanged += new System.EventHandler(this.txtshortname_TextChanged);
        //    this.txtshortname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtshortname_KeyDown);
        //    this.txtshortname.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtshortname_KeyPress);
        //    // 
        //    // label14
        //    // 
        //    this.label14.AutoSize = true;
        //    this.label14.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.label14.Location = new System.Drawing.Point(411, 2);
        //    this.label14.Name = "label14";
        //    this.label14.Size = new System.Drawing.Size(86, 14);
        //    this.label14.TabIndex = 64;
        //    this.label14.Text = "Short Name F7";
        //    // 
        //    // btndis
        //    // 
        //    this.btndis.BackgroundImage = global::Restaurant.Properties.Resources.button21;
        //    this.btndis.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
        //    this.btndis.Location = new System.Drawing.Point(715, 23);
        //    this.btndis.Name = "btndis";
        //    this.btndis.Size = new System.Drawing.Size(34, 23);
        //    this.btndis.TabIndex = 36;
        //    this.btndis.Text = "...";
        //    this.btndis.UseVisualStyleBackColor = true;
        //    this.btndis.Click += new System.EventHandler(this.btndis_Click);
        //    // 
        //    // btnshowtable
        //    // 
        //    this.btnshowtable.BackgroundImage = global::Restaurant.Properties.Resources.button;
        //    this.btnshowtable.Cursor = System.Windows.Forms.Cursors.Hand;
        //    this.btnshowtable.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
        //    this.btnshowtable.Location = new System.Drawing.Point(755, 23);
        //    this.btnshowtable.Name = "btnshowtable";
        //    this.btnshowtable.Size = new System.Drawing.Size(75, 23);
        //    this.btnshowtable.TabIndex = 28;
        //    this.btnshowtable.Text = "ShowTable";
        //    this.btnshowtable.UseVisualStyleBackColor = true;
        //    this.btnshowtable.Click += new System.EventHandler(this.button1_Click);
        //    // 
        //    // lsttableno
        //    // 
        //    this.lsttableno.FormattingEnabled = true;
        //    this.lsttableno.Location = new System.Drawing.Point(139, 90);
        //    this.lsttableno.Name = "lsttableno";
        //    this.lsttableno.Size = new System.Drawing.Size(78, 95);
        //    this.lsttableno.TabIndex = 68;
        //    // 
        //    // lstwaiter
        //    // 
        //    this.lstwaiter.FormattingEnabled = true;
        //    this.lstwaiter.Location = new System.Drawing.Point(225, 88);
        //    this.lstwaiter.Name = "lstwaiter";
        //    this.lstwaiter.Size = new System.Drawing.Size(112, 95);
        //    this.lstwaiter.TabIndex = 68;
        //    this.lstwaiter.DoubleClick += new System.EventHandler(this.lstwaiter_DoubleClick);
        //    // 
        //    // timer1
        //    // 
        //    this.timer1.Enabled = true;
        //    this.timer1.Interval = 1000;
        //    this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
        //    // 
        //    // btnQT
        //    // 
        //    this.btnQT.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnQT.BackgroundImage")));
        //    this.btnQT.Cursor = System.Windows.Forms.Cursors.Hand;
        //    this.btnQT.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
        //    this.btnQT.Location = new System.Drawing.Point(399, 482);
        //    this.btnQT.Name = "btnQT";
        //    this.btnQT.Size = new System.Drawing.Size(77, 51);
        //    this.btnQT.TabIndex = 38;
        //    this.btnQT.Text = "KOT\r\n(ctrl+k)";
        //    this.btnQT.UseVisualStyleBackColor = true;
        //    this.btnQT.Click += new System.EventHandler(this.btnQT_Click);
        //    // 
        //    // btncancelBill
        //    // 
        //    this.btncancelBill.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btncancelBill.BackgroundImage")));
        //    this.btncancelBill.Cursor = System.Windows.Forms.Cursors.Hand;
        //    this.btncancelBill.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
        //    this.btncancelBill.Location = new System.Drawing.Point(398, 135);
        //    this.btncancelBill.Name = "btncancelBill";
        //    this.btncancelBill.Size = new System.Drawing.Size(77, 51);
        //    this.btncancelBill.TabIndex = 33;
        //    this.btncancelBill.Text = "CancelItem";
        //    this.btncancelBill.UseVisualStyleBackColor = true;
        //    this.btncancelBill.Click += new System.EventHandler(this.btncancelBill_Click);
        //    // 
        //    // btnchnageperson
        //    // 
        //    this.btnchnageperson.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnchnageperson.BackgroundImage")));
        //    this.btnchnageperson.Cursor = System.Windows.Forms.Cursors.Hand;
        //    this.btnchnageperson.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
        //    this.btnchnageperson.Location = new System.Drawing.Point(398, 311);
        //    this.btnchnageperson.Name = "btnchnageperson";
        //    this.btnchnageperson.Size = new System.Drawing.Size(77, 51);
        //    this.btnchnageperson.TabIndex = 32;
        //    this.btnchnageperson.Text = "Change Persons";
        //    this.btnchnageperson.UseVisualStyleBackColor = true;
        //    this.btnchnageperson.Click += new System.EventHandler(this.btnchnageperson_Click);
        //    // 
        //    // btnChangeQty
        //    // 
        //    this.btnChangeQty.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnChangeQty.BackgroundImage")));
        //    this.btnChangeQty.Cursor = System.Windows.Forms.Cursors.Hand;
        //    this.btnChangeQty.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
        //    this.btnChangeQty.Location = new System.Drawing.Point(399, 194);
        //    this.btnChangeQty.Name = "btnChangeQty";
        //    this.btnChangeQty.Size = new System.Drawing.Size(77, 51);
        //    this.btnChangeQty.TabIndex = 31;
        //    this.btnChangeQty.Text = "Change Qty";
        //    this.btnChangeQty.UseVisualStyleBackColor = true;
        //    this.btnChangeQty.Click += new System.EventHandler(this.btnChangeQty_Click);
        //    // 
        //    // button3
        //    // 
        //    this.button3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button3.BackgroundImage")));
        //    this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
        //    this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
        //    this.button3.Location = new System.Drawing.Point(399, 539);
        //    this.button3.Name = "button3";
        //    this.button3.Size = new System.Drawing.Size(77, 51);
        //    this.button3.TabIndex = 28;
        //    this.button3.Text = "Print Bill\r\n(ctrl+p)";
        //    this.button3.UseVisualStyleBackColor = true;
        //    this.button3.Click += new System.EventHandler(this.button3_Click);
        //    // 
        //    // btnRemoveItem
        //    // 
        //    this.btnRemoveItem.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRemoveItem.BackgroundImage")));
        //    this.btnRemoveItem.Cursor = System.Windows.Forms.Cursors.Hand;
        //    this.btnRemoveItem.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
        //    this.btnRemoveItem.Location = new System.Drawing.Point(399, 368);
        //    this.btnRemoveItem.Name = "btnRemoveItem";
        //    this.btnRemoveItem.Size = new System.Drawing.Size(77, 51);
        //    this.btnRemoveItem.TabIndex = 27;
        //    this.btnRemoveItem.Text = "Remove Item";
        //    this.btnRemoveItem.UseVisualStyleBackColor = true;
        //    this.btnRemoveItem.Click += new System.EventHandler(this.btnRemoveItem_Click);
        //    // 
        //    // btnChangePrice
        //    // 
        //    this.btnChangePrice.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnChangePrice.BackgroundImage")));
        //    this.btnChangePrice.Cursor = System.Windows.Forms.Cursors.Hand;
        //    this.btnChangePrice.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
        //    this.btnChangePrice.Location = new System.Drawing.Point(398, 254);
        //    this.btnChangePrice.Name = "btnChangePrice";
        //    this.btnChangePrice.Size = new System.Drawing.Size(77, 51);
        //    this.btnChangePrice.TabIndex = 26;
        //    this.btnChangePrice.Text = "ChangePrice";
        //    this.btnChangePrice.UseVisualStyleBackColor = true;
        //    this.btnChangePrice.Click += new System.EventHandler(this.btnChangePrice_Click);
        //    // 
        //    // btnAddOrder
        //    // 
        //    this.btnAddOrder.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAddOrder.BackgroundImage")));
        //    this.btnAddOrder.Cursor = System.Windows.Forms.Cursors.Hand;
        //    this.btnAddOrder.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
        //    this.btnAddOrder.Location = new System.Drawing.Point(399, 425);
        //    this.btnAddOrder.Name = "btnAddOrder";
        //    this.btnAddOrder.Size = new System.Drawing.Size(77, 51);
        //    this.btnAddOrder.TabIndex = 25;
        //    this.btnAddOrder.Text = "Add to Order";
        //    this.btnAddOrder.UseVisualStyleBackColor = true;
        //    this.btnAddOrder.Click += new System.EventHandler(this.btnAddOrder_Click);
        //    // 
        //    // panel2
        //    // 
        //    this.panel2.BackgroundImage = global::Restaurant.Properties.Resources.header2;
        //    this.panel2.Controls.Add(this.button1);
        //    this.panel2.Controls.Add(this.menuStrip2);
        //    this.panel2.Location = new System.Drawing.Point(0, -2);
        //    this.panel2.Name = "panel2";
        //    this.panel2.Size = new System.Drawing.Size(1024, 30);
        //    this.panel2.TabIndex = 2;
        //    // 
        //    // button1
        //    // 
        //    this.button1.BackgroundImage = global::Restaurant.Properties.Resources.button21;
        //    this.button1.Location = new System.Drawing.Point(950, 2);
        //    this.button1.Name = "button1";
        //    this.button1.Size = new System.Drawing.Size(53, 24);
        //    this.button1.TabIndex = 36;
        //    this.button1.Text = "LogOut";
        //    this.button1.UseVisualStyleBackColor = true;
        //    this.button1.Click += new System.EventHandler(this.button1_Click_1);
        //    // 
        //    // menuStrip2
        //    // 
        //    this.menuStrip2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
        //    this.menuStrip2.Font = new System.Drawing.Font("Papyrus", 9.75F, System.Drawing.FontStyle.Bold);
        //    this.menuStrip2.Location = new System.Drawing.Point(0, 0);
        //    this.menuStrip2.Name = "menuStrip2";
        //    this.menuStrip2.Size = new System.Drawing.Size(1024, 29);
        //    this.menuStrip2.TabIndex = 2;
        //    this.menuStrip2.Text = "menuStrip1";
        //    // 
        //    // saleToolStripMenuItem
        //    // 
        //    this.saleToolStripMenuItem.Name = "saleToolStripMenuItem";
        //    this.saleToolStripMenuItem.Size = new System.Drawing.Size(107, 25);
        //    this.saleToolStripMenuItem.Text = "Sale Status";
        //    this.saleToolStripMenuItem.Click += new System.EventHandler(this.saleToolStripMenuItem_Click);
        //    // 
        //    // toolToolStripMenuItem
        //    // 
        //    this.toolToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
        //    this.hotelSettingToolStripMenuItem1,
        //    this.securityToolStripMenuItem1,
        //    this.kotReprintToolStripMenuItem,
        //    this.billReprintToolStripMenuItem,
        //    this.cashBookToolStripMenuItem,
        //    this.shiftingToolStripMenuItem});
        //    this.toolToolStripMenuItem.Name = "toolToolStripMenuItem";
        //    this.toolToolStripMenuItem.Size = new System.Drawing.Size(57, 25);
        //    this.toolToolStripMenuItem.Text = "Tool";
        //    // 
        //    // hotelSettingToolStripMenuItem1
        //    // 
        //    this.hotelSettingToolStripMenuItem1.Name = "hotelSettingToolStripMenuItem1";
        //    this.hotelSettingToolStripMenuItem1.Size = new System.Drawing.Size(171, 26);
        //    this.hotelSettingToolStripMenuItem1.Text = "HotelSetting";
        //    this.hotelSettingToolStripMenuItem1.Click += new System.EventHandler(this.hotelSettingToolStripMenuItem1_Click);
        //    // 
        //    // securityToolStripMenuItem1
        //    // 
        //    this.securityToolStripMenuItem1.Name = "securityToolStripMenuItem1";
        //    this.securityToolStripMenuItem1.Size = new System.Drawing.Size(171, 26);
        //    this.securityToolStripMenuItem1.Text = "Security";
        //    this.securityToolStripMenuItem1.Click += new System.EventHandler(this.securityToolStripMenuItem1_Click);
        //    // 
        //    // kotReprintToolStripMenuItem
        //    // 
        //    this.kotReprintToolStripMenuItem.Name = "kotReprintToolStripMenuItem";
        //    this.kotReprintToolStripMenuItem.Size = new System.Drawing.Size(171, 26);
        //    this.kotReprintToolStripMenuItem.Text = "Kot Reprint";
        //    this.kotReprintToolStripMenuItem.Click += new System.EventHandler(this.kotReprintToolStripMenuItem_Click);
        //    // 
        //    // billReprintToolStripMenuItem
        //    // 
        //    this.billReprintToolStripMenuItem.Name = "billReprintToolStripMenuItem";
        //    this.billReprintToolStripMenuItem.Size = new System.Drawing.Size(171, 26);
        //    this.billReprintToolStripMenuItem.Text = "Bill Reprint";
        //    this.billReprintToolStripMenuItem.Click += new System.EventHandler(this.billReprintToolStripMenuItem_Click);
        //    // 
        //    // cashBookToolStripMenuItem
        //    // 
        //    this.cashBookToolStripMenuItem.Name = "cashBookToolStripMenuItem";
        //    this.cashBookToolStripMenuItem.Size = new System.Drawing.Size(171, 26);
        //    this.cashBookToolStripMenuItem.Text = "CashBook";
        //    this.cashBookToolStripMenuItem.Click += new System.EventHandler(this.cashBookToolStripMenuItem_Click);
        //    // 
        //    // shiftingToolStripMenuItem
        //    // 
        //    this.shiftingToolStripMenuItem.Name = "shiftingToolStripMenuItem";
        //    this.shiftingToolStripMenuItem.Size = new System.Drawing.Size(171, 26);
        //    this.shiftingToolStripMenuItem.Text = "Shifting";
        //    this.shiftingToolStripMenuItem.Click += new System.EventHandler(this.shiftingToolStripMenuItem_Click);
        //    // 
        //    // setupToolStripMenuItem
        //    // 
        //    this.setupToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
        //    this.addCatagoryToolStripMenuItem3,
        //    this.addItemToolStripMenuItem1,
        //    this.addTableToolStripMenuItem1,
        //    this.addSupplierToolStripMenuItem,
        //    this.workerToolStripMenuItem2,
        //    this.createUnitToolStripMenuItem,
        //    this.creatPackToolStripMenuItem,
        //    this.messageToolStripMenuItem,
        //    this.printerSettingToolStripMenuItem});
        //    this.setupToolStripMenuItem.Name = "setupToolStripMenuItem";
        //    this.setupToolStripMenuItem.Size = new System.Drawing.Size(66, 25);
        //    this.setupToolStripMenuItem.Text = "Setup";
        //    // 
        //    // addCatagoryToolStripMenuItem3
        //    // 
        //    this.addCatagoryToolStripMenuItem3.Name = "addCatagoryToolStripMenuItem3";
        //    this.addCatagoryToolStripMenuItem3.Size = new System.Drawing.Size(182, 26);
        //    this.addCatagoryToolStripMenuItem3.Text = "Add Catagory";
        //    this.addCatagoryToolStripMenuItem3.Click += new System.EventHandler(this.addCatagoryToolStripMenuItem3_Click);
        //    // 
        //    // addItemToolStripMenuItem1
        //    // 
        //    this.addItemToolStripMenuItem1.Name = "addItemToolStripMenuItem1";
        //    this.addItemToolStripMenuItem1.Size = new System.Drawing.Size(182, 26);
        //    this.addItemToolStripMenuItem1.Text = "Add Item";
        //    this.addItemToolStripMenuItem1.Click += new System.EventHandler(this.addItemToolStripMenuItem1_Click);
        //    // 
        //    // addTableToolStripMenuItem1
        //    // 
        //    this.addTableToolStripMenuItem1.Name = "addTableToolStripMenuItem1";
        //    this.addTableToolStripMenuItem1.Size = new System.Drawing.Size(182, 26);
        //    this.addTableToolStripMenuItem1.Text = "Add Table";
        //    this.addTableToolStripMenuItem1.Click += new System.EventHandler(this.addTableToolStripMenuItem1_Click);
        //    // 
        //    // addSupplierToolStripMenuItem
        //    // 
        //    this.addSupplierToolStripMenuItem.Name = "addSupplierToolStripMenuItem";
        //    this.addSupplierToolStripMenuItem.Size = new System.Drawing.Size(182, 26);
        //    this.addSupplierToolStripMenuItem.Text = "Add Supplier";
        //    this.addSupplierToolStripMenuItem.Click += new System.EventHandler(this.addSupplierToolStripMenuItem_Click);
        //    // 
        //    // workerToolStripMenuItem2
        //    // 
        //    this.workerToolStripMenuItem2.Name = "workerToolStripMenuItem2";
        //    this.workerToolStripMenuItem2.Size = new System.Drawing.Size(182, 26);
        //    this.workerToolStripMenuItem2.Text = "Worker";
        //    this.workerToolStripMenuItem2.Click += new System.EventHandler(this.workerToolStripMenuItem2_Click);
        //    // 
        //    // createUnitToolStripMenuItem
        //    // 
        //    this.createUnitToolStripMenuItem.Name = "createUnitToolStripMenuItem";
        //    this.createUnitToolStripMenuItem.Size = new System.Drawing.Size(182, 26);
        //    this.createUnitToolStripMenuItem.Text = "CreatUnit";
        //    this.createUnitToolStripMenuItem.Click += new System.EventHandler(this.createUnitToolStripMenuItem_Click);
        //    // 
        //    // creatPackToolStripMenuItem
        //    // 
        //    this.creatPackToolStripMenuItem.Name = "creatPackToolStripMenuItem";
        //    this.creatPackToolStripMenuItem.Size = new System.Drawing.Size(182, 26);
        //    this.creatPackToolStripMenuItem.Text = "CreatPack";
        //    this.creatPackToolStripMenuItem.Click += new System.EventHandler(this.creatPackToolStripMenuItem_Click);
        //    // 
        //    // messageToolStripMenuItem
        //    // 
        //    this.messageToolStripMenuItem.Name = "messageToolStripMenuItem";
        //    this.messageToolStripMenuItem.Size = new System.Drawing.Size(182, 26);
        //    this.messageToolStripMenuItem.Text = "Message";
        //    this.messageToolStripMenuItem.Click += new System.EventHandler(this.messageToolStripMenuItem_Click);
        //    // 
        //    // printerSettingToolStripMenuItem
        //    // 
        //    this.printerSettingToolStripMenuItem.Name = "printerSettingToolStripMenuItem";
        //    this.printerSettingToolStripMenuItem.Size = new System.Drawing.Size(182, 26);
        //    this.printerSettingToolStripMenuItem.Text = "Printer Setting";
        //    this.printerSettingToolStripMenuItem.Click += new System.EventHandler(this.printerSettingToolStripMenuItem_Click);
        //    // 
        //    // transactionToolStripMenuItem
        //    // 
        //    this.transactionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
        //    this.purchasingToolStripMenuItem1});
        //    this.transactionToolStripMenuItem.Name = "transactionToolStripMenuItem";
        //    this.transactionToolStripMenuItem.Size = new System.Drawing.Size(105, 25);
        //    this.transactionToolStripMenuItem.Text = "Transaction";
        //    // 
        //    // purchasingToolStripMenuItem1
        //    // 
        //    this.purchasingToolStripMenuItem1.Name = "purchasingToolStripMenuItem1";
        //    this.purchasingToolStripMenuItem1.Size = new System.Drawing.Size(153, 26);
        //    this.purchasingToolStripMenuItem1.Text = "Purchasing";
        //    this.purchasingToolStripMenuItem1.Click += new System.EventHandler(this.purchasingToolStripMenuItem1_Click);
        //    // 
        //    // inventoryToolStripMenuItem
        //    // 
        //    this.inventoryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
        //    this.viewStockToolStripMenuItem});
        //    this.inventoryToolStripMenuItem.Name = "inventoryToolStripMenuItem";
        //    this.inventoryToolStripMenuItem.Size = new System.Drawing.Size(84, 25);
        //    this.inventoryToolStripMenuItem.Text = "Inventory";
        //    // 
        //    // viewStockToolStripMenuItem
        //    // 
        //    this.viewStockToolStripMenuItem.Name = "viewStockToolStripMenuItem";
        //    this.viewStockToolStripMenuItem.Size = new System.Drawing.Size(154, 26);
        //    this.viewStockToolStripMenuItem.Text = "ViewStock";
        //    this.viewStockToolStripMenuItem.Click += new System.EventHandler(this.viewStockToolStripMenuItem_Click);
        //    // 
        //    // reportsToolStripMenuItem1
        //    // 
        //    this.reportsToolStripMenuItem1.Font = new System.Drawing.Font("Papyrus", 9.75F, System.Drawing.FontStyle.Bold);
        //    this.reportsToolStripMenuItem1.Name = "reportsToolStripMenuItem1";
        //    this.reportsToolStripMenuItem1.Size = new System.Drawing.Size(78, 25);
        //    this.reportsToolStripMenuItem1.Text = "Reports";
        //    // 
        //    // restaurantToolStripMenuItem
        //    // 
        //    this.restaurantToolStripMenuItem.Name = "restaurantToolStripMenuItem";
        //    this.restaurantToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
        //    this.restaurantToolStripMenuItem.Text = "Restaurant";
        //    // 
        //    // periodWiseToolStripMenuItem
        //    // 
        //    this.periodWiseToolStripMenuItem.Name = "periodWiseToolStripMenuItem";
        //    this.periodWiseToolStripMenuItem.Size = new System.Drawing.Size(170, 26);
        //    this.periodWiseToolStripMenuItem.Text = "PeriodWise";
        //    this.periodWiseToolStripMenuItem.Click += new System.EventHandler(this.periodWiseToolStripMenuItem_Click);
        //    // 
        //    // billWiseToolStripMenuItem
        //    // 
        //    this.billWiseToolStripMenuItem.Name = "billWiseToolStripMenuItem";
        //    this.billWiseToolStripMenuItem.Size = new System.Drawing.Size(170, 26);
        //    this.billWiseToolStripMenuItem.Text = "BillWise";
        //    this.billWiseToolStripMenuItem.Click += new System.EventHandler(this.billWiseToolStripMenuItem_Click);
        //    // 
        //    // cashierWiseToolStripMenuItem
        //    // 
        //    this.cashierWiseToolStripMenuItem.Name = "cashierWiseToolStripMenuItem";
        //    this.cashierWiseToolStripMenuItem.Size = new System.Drawing.Size(170, 26);
        //    this.cashierWiseToolStripMenuItem.Text = "CashierWise";
        //    this.cashierWiseToolStripMenuItem.Click += new System.EventHandler(this.cashierWiseToolStripMenuItem_Click);
        //    // 
        //    // saleByItemToolStripMenuItem
        //    // 
        //    this.saleByItemToolStripMenuItem.Name = "saleByItemToolStripMenuItem";
        //    this.saleByItemToolStripMenuItem.Size = new System.Drawing.Size(170, 26);
        //    this.saleByItemToolStripMenuItem.Text = "ItemWise";
        //    this.saleByItemToolStripMenuItem.Click += new System.EventHandler(this.saleByItemToolStripMenuItem_Click);
        //    // 
        //    // bToolStripMenuItem
        //    // 
        //    this.bToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
        //    this.periodWiseToolStripMenuItem1,
        //    this.billWiseToolStripMenuItem1,
        //    this.cashierWiseToolStripMenuItem1,
        //    this.itemWiseToolStripMenuItem});
        //    this.bToolStripMenuItem.Name = "bToolStripMenuItem";
        //    this.bToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
        //    this.bToolStripMenuItem.Text = "Bar";
        //    // 
        //    // periodWiseToolStripMenuItem1
        //    // 
        //    this.periodWiseToolStripMenuItem1.Name = "periodWiseToolStripMenuItem1";
        //    this.periodWiseToolStripMenuItem1.Size = new System.Drawing.Size(169, 26);
        //    this.periodWiseToolStripMenuItem1.Text = "PeriodWise";
        //    this.periodWiseToolStripMenuItem1.Click += new System.EventHandler(this.periodWiseToolStripMenuItem1_Click);
        //    // 
        //    // billWiseToolStripMenuItem1
        //    // 
        //    this.billWiseToolStripMenuItem1.Name = "billWiseToolStripMenuItem1";
        //    this.billWiseToolStripMenuItem1.Size = new System.Drawing.Size(169, 26);
        //    this.billWiseToolStripMenuItem1.Text = "BillWise";
        //    this.billWiseToolStripMenuItem1.Click += new System.EventHandler(this.billWiseToolStripMenuItem1_Click);
        //    // 
        //    // cashierWiseToolStripMenuItem1
        //    // 
        //    this.cashierWiseToolStripMenuItem1.Name = "cashierWiseToolStripMenuItem1";
        //    this.cashierWiseToolStripMenuItem1.Size = new System.Drawing.Size(169, 26);
        //    this.cashierWiseToolStripMenuItem1.Text = "CashierWise";
        //    this.cashierWiseToolStripMenuItem1.Click += new System.EventHandler(this.cashierWiseToolStripMenuItem1_Click);
        //    // 
        //    // itemWiseToolStripMenuItem
        //    // 
        //    this.itemWiseToolStripMenuItem.Name = "itemWiseToolStripMenuItem";
        //    this.itemWiseToolStripMenuItem.Size = new System.Drawing.Size(169, 26);
        //    this.itemWiseToolStripMenuItem.Text = "ItemWise";
        //    this.itemWiseToolStripMenuItem.Click += new System.EventHandler(this.itemWiseToolStripMenuItem_Click);
        //    // 
        //    // lodgeToolStripMenuItem
        //    // 
        //    this.lodgeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
        //    this.checkInReportsToolStripMenuItem,
        //    this.checkOutReportsToolStripMenuItem,
        //    this.billWiseToolStripMenuItem2,
        //    this.cashierWiseToolStripMenuItem2,
        //    this.roomWiseToolStripMenuItem});
        //    this.lodgeToolStripMenuItem.Name = "lodgeToolStripMenuItem";
        //    this.lodgeToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
        //    this.lodgeToolStripMenuItem.Text = "Lodge";
        //    // 
        //    // checkInReportsToolStripMenuItem
        //    // 
        //    this.checkInReportsToolStripMenuItem.Name = "checkInReportsToolStripMenuItem";
        //    this.checkInReportsToolStripMenuItem.Size = new System.Drawing.Size(209, 26);
        //    this.checkInReportsToolStripMenuItem.Text = "CheckInReports";
        //    this.checkInReportsToolStripMenuItem.Click += new System.EventHandler(this.checkInReportsToolStripMenuItem_Click);
        //    // 
        //    // checkOutReportsToolStripMenuItem
        //    // 
        //    this.checkOutReportsToolStripMenuItem.Name = "checkOutReportsToolStripMenuItem";
        //    this.checkOutReportsToolStripMenuItem.Size = new System.Drawing.Size(209, 26);
        //    this.checkOutReportsToolStripMenuItem.Text = "CheckOutReports";
        //    this.checkOutReportsToolStripMenuItem.Click += new System.EventHandler(this.checkOutReportsToolStripMenuItem_Click);
        //    // 
        //    // billWiseToolStripMenuItem2
        //    // 
        //    this.billWiseToolStripMenuItem2.Name = "billWiseToolStripMenuItem2";
        //    this.billWiseToolStripMenuItem2.Size = new System.Drawing.Size(209, 26);
        //    this.billWiseToolStripMenuItem2.Text = "BillWise";
        //    this.billWiseToolStripMenuItem2.Click += new System.EventHandler(this.billWiseToolStripMenuItem2_Click);
        //    // 
        //    // cashierWiseToolStripMenuItem2
        //    // 
        //    this.cashierWiseToolStripMenuItem2.Name = "cashierWiseToolStripMenuItem2";
        //    this.cashierWiseToolStripMenuItem2.Size = new System.Drawing.Size(209, 26);
        //    this.cashierWiseToolStripMenuItem2.Text = "CashierWise";
        //    this.cashierWiseToolStripMenuItem2.Click += new System.EventHandler(this.cashierWiseToolStripMenuItem2_Click);
        //    // 
        //    // roomWiseToolStripMenuItem
        //    // 
        //    this.roomWiseToolStripMenuItem.Name = "roomWiseToolStripMenuItem";
        //    this.roomWiseToolStripMenuItem.Size = new System.Drawing.Size(209, 26);
        //    this.roomWiseToolStripMenuItem.Text = "RoomWise";
        //    this.roomWiseToolStripMenuItem.Click += new System.EventHandler(this.roomWiseToolStripMenuItem_Click);
        //    // 
        //    // kotAuditBotAuditToolStripMenuItem
        //    // 
        //    this.kotAuditBotAuditToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
        //    this.kotWiseToolStripMenuItem,
        //    this.botWiseToolStripMenuItem,
        //    this.kotItemToolStripMenuItem,
        //    this.botItemToolStripMenuItem});
        //    this.kotAuditBotAuditToolStripMenuItem.Name = "kotAuditBotAuditToolStripMenuItem";
        //    this.kotAuditBotAuditToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
        //    this.kotAuditBotAuditToolStripMenuItem.Text = "KotAudit/BotAudit";
        //    // 
        //    // kotWiseToolStripMenuItem
        //    // 
        //    this.kotWiseToolStripMenuItem.Name = "kotWiseToolStripMenuItem";
        //    this.kotWiseToolStripMenuItem.Size = new System.Drawing.Size(142, 26);
        //    this.kotWiseToolStripMenuItem.Text = "KotWise";
        //    this.kotWiseToolStripMenuItem.Click += new System.EventHandler(this.kotWiseToolStripMenuItem_Click);
        //    // 
        //    // botWiseToolStripMenuItem
        //    // 
        //    this.botWiseToolStripMenuItem.Name = "botWiseToolStripMenuItem";
        //    this.botWiseToolStripMenuItem.Size = new System.Drawing.Size(142, 26);
        //    this.botWiseToolStripMenuItem.Text = "BotWise";
        //    this.botWiseToolStripMenuItem.Click += new System.EventHandler(this.botWiseToolStripMenuItem_Click);
        //    // 
        //    // kotItemToolStripMenuItem
        //    // 
        //    this.kotItemToolStripMenuItem.Name = "kotItemToolStripMenuItem";
        //    this.kotItemToolStripMenuItem.Size = new System.Drawing.Size(142, 26);
        //    this.kotItemToolStripMenuItem.Text = "KotItem";
        //    this.kotItemToolStripMenuItem.Click += new System.EventHandler(this.kotItemToolStripMenuItem_Click);
        //    // 
        //    // botItemToolStripMenuItem
        //    // 
        //    this.botItemToolStripMenuItem.Name = "botItemToolStripMenuItem";
        //    this.botItemToolStripMenuItem.Size = new System.Drawing.Size(142, 26);
        //    this.botItemToolStripMenuItem.Text = "BotItem";
        //    this.botItemToolStripMenuItem.Click += new System.EventHandler(this.botItemToolStripMenuItem_Click);
        //    // 
        //    // todayToolStripMenuItem
        //    // 
        //    this.todayToolStripMenuItem.Name = "todayToolStripMenuItem";
        //    this.todayToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
        //    this.todayToolStripMenuItem.Text = "Today";
        //    this.todayToolStripMenuItem.Click += new System.EventHandler(this.todayToolStripMenuItem_Click);
        //    // 
        //    // yesturdayToolStripMenuItem
        //    // 
        //    this.yesturdayToolStripMenuItem.Name = "yesturdayToolStripMenuItem";
        //    this.yesturdayToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
        //    this.yesturdayToolStripMenuItem.Text = "Yesturday";
        //    this.yesturdayToolStripMenuItem.Click += new System.EventHandler(this.yesturdayToolStripMenuItem_Click);
        //    // 
        //    // saleByTableToolStripMenuItem
        //    // 
        //    this.saleByTableToolStripMenuItem.Name = "saleByTableToolStripMenuItem";
        //    this.saleByTableToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
        //    this.saleByTableToolStripMenuItem.Text = "Sale by Table";
        //    this.saleByTableToolStripMenuItem.Click += new System.EventHandler(this.saleByTableToolStripMenuItem_Click);
        //    // 
        //    // cancelBillListToolStripMenuItem
        //    // 
        //    this.cancelBillListToolStripMenuItem.Name = "cancelBillListToolStripMenuItem";
        //    this.cancelBillListToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
        //    this.cancelBillListToolStripMenuItem.Text = "Cancel Item List";
        //    this.cancelBillListToolStripMenuItem.Click += new System.EventHandler(this.cancelBillListToolStripMenuItem_Click);
        //    // 
        //    // itemPriceListToolStripMenuItem
        //    // 
        //    this.itemPriceListToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
        //    this.restaurantToolStripMenuItem1,
        //    this.barToolStripMenuItem});
        //    this.itemPriceListToolStripMenuItem.Name = "itemPriceListToolStripMenuItem";
        //    this.itemPriceListToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
        //    this.itemPriceListToolStripMenuItem.Text = "Item Price List";
        //    // 
        //    // restaurantToolStripMenuItem1
        //    // 
        //    this.restaurantToolStripMenuItem1.Name = "restaurantToolStripMenuItem1";
        //    this.restaurantToolStripMenuItem1.Size = new System.Drawing.Size(155, 26);
        //    this.restaurantToolStripMenuItem1.Text = "Restaurant";
        //    this.restaurantToolStripMenuItem1.Click += new System.EventHandler(this.restaurantToolStripMenuItem1_Click);
        //    // 
        //    // barToolStripMenuItem
        //    // 
        //    this.barToolStripMenuItem.Name = "barToolStripMenuItem";
        //    this.barToolStripMenuItem.Size = new System.Drawing.Size(155, 26);
        //    this.barToolStripMenuItem.Text = "Bar";
        //    this.barToolStripMenuItem.Click += new System.EventHandler(this.barToolStripMenuItem_Click);
        //    // 
        //    // customerListToolStripMenuItem
        //    // 
        //    this.customerListToolStripMenuItem.Name = "customerListToolStripMenuItem";
        //    this.customerListToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
        //    this.customerListToolStripMenuItem.Text = "Customer List";
        //    this.customerListToolStripMenuItem.Click += new System.EventHandler(this.customerListToolStripMenuItem_Click);
        //    // 
        //    // purchasingReportsToolStripMenuItem
        //    // 
        //    this.purchasingReportsToolStripMenuItem.Name = "purchasingReportsToolStripMenuItem";
        //    this.purchasingReportsToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
        //    this.purchasingReportsToolStripMenuItem.Text = "Purchasing Reports";
        //    this.purchasingReportsToolStripMenuItem.Click += new System.EventHandler(this.purchasingReportsToolStripMenuItem_Click);
        //    // 
        //    // cashBookReportsToolStripMenuItem
        //    // 
        //    this.cashBookReportsToolStripMenuItem.Name = "cashBookReportsToolStripMenuItem";
        //    this.cashBookReportsToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
        //    this.cashBookReportsToolStripMenuItem.Text = "CashBookReports";
        //    this.cashBookReportsToolStripMenuItem.Click += new System.EventHandler(this.cashBookReportsToolStripMenuItem_Click);
        //    // 
        //    // windowsToolStripMenuItem
        //    // 
        //    this.windowsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
        //    this.calculatorToolStripMenuItem1,
        //    this.notePadToolStripMenuItem});
        //    this.windowsToolStripMenuItem.Name = "windowsToolStripMenuItem";
        //    this.windowsToolStripMenuItem.Size = new System.Drawing.Size(83, 25);
        //    this.windowsToolStripMenuItem.Text = "Windows";
        //    // 
        //    // calculatorToolStripMenuItem1
        //    // 
        //    this.calculatorToolStripMenuItem1.Name = "calculatorToolStripMenuItem1";
        //    this.calculatorToolStripMenuItem1.Size = new System.Drawing.Size(153, 26);
        //    this.calculatorToolStripMenuItem1.Text = "Calculator";
        //    this.calculatorToolStripMenuItem1.Click += new System.EventHandler(this.calculatorToolStripMenuItem1_Click);
        //    // 
        //    // notePadToolStripMenuItem
        //    // 
        //    this.notePadToolStripMenuItem.Name = "notePadToolStripMenuItem";
        //    this.notePadToolStripMenuItem.Size = new System.Drawing.Size(153, 26);
        //    this.notePadToolStripMenuItem.Text = "NotePad";
        //    this.notePadToolStripMenuItem.Click += new System.EventHandler(this.notePadToolStripMenuItem_Click);
        //    // 
        //    // lodgingToolStripMenuItem
        //    // 
        //    this.lodgingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
        //    this.addRoomToolStripMenuItem,
        //    this.checkInToolStripMenuItem,
        //    this.checkOutToolStripMenuItem,
        //    this.viewAllCheckOutCustomerToolStripMenuItem});
        //    this.lodgingToolStripMenuItem.Name = "lodgingToolStripMenuItem";
        //    this.lodgingToolStripMenuItem.Size = new System.Drawing.Size(77, 25);
        //    this.lodgingToolStripMenuItem.Text = "Lodging";
        //    // 
        //    // addRoomToolStripMenuItem
        //    // 
        //    this.addRoomToolStripMenuItem.Name = "addRoomToolStripMenuItem";
        //    this.addRoomToolStripMenuItem.Size = new System.Drawing.Size(193, 26);
        //    this.addRoomToolStripMenuItem.Text = "Add Room";
        //    this.addRoomToolStripMenuItem.Click += new System.EventHandler(this.addRoomToolStripMenuItem_Click);
        //    // 
        //    // checkInToolStripMenuItem
        //    // 
        //    this.checkInToolStripMenuItem.Name = "checkInToolStripMenuItem";
        //    this.checkInToolStripMenuItem.Size = new System.Drawing.Size(193, 26);
        //    this.checkInToolStripMenuItem.Text = "CheckIn";
        //    this.checkInToolStripMenuItem.Click += new System.EventHandler(this.checkInToolStripMenuItem_Click);
        //    // 
        //    // checkOutToolStripMenuItem
        //    // 
        //    this.checkOutToolStripMenuItem.Name = "checkOutToolStripMenuItem";
        //    this.checkOutToolStripMenuItem.Size = new System.Drawing.Size(193, 26);
        //    this.checkOutToolStripMenuItem.Text = "CheckOut";
        //    this.checkOutToolStripMenuItem.Click += new System.EventHandler(this.checkOutToolStripMenuItem_Click);
        //    // 
        //    // viewAllCheckOutCustomerToolStripMenuItem
        //    // 
        //    this.viewAllCheckOutCustomerToolStripMenuItem.Name = "viewAllCheckOutCustomerToolStripMenuItem";
        //    this.viewAllCheckOutCustomerToolStripMenuItem.Size = new System.Drawing.Size(193, 26);
        //    this.viewAllCheckOutCustomerToolStripMenuItem.Text = "ShowCheckOut";
        //    this.viewAllCheckOutCustomerToolStripMenuItem.Click += new System.EventHandler(this.viewAllCheckOutCustomerToolStripMenuItem_Click);
        //    // 
        //    // helpToolStripMenuItem
        //    // 
        //    this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
        //    this.helpToolStripMenuItem.Size = new System.Drawing.Size(57, 25);
        //    this.helpToolStripMenuItem.Text = "Help";
        //    this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
        //    // 
        //    // billSummaryToolStripMenuItem
        //    // 
        //    this.billSummaryToolStripMenuItem.Name = "billSummaryToolStripMenuItem";
        //    this.billSummaryToolStripMenuItem.Size = new System.Drawing.Size(170, 26);
        //    this.billSummaryToolStripMenuItem.Text = "BillSummary";
        //    this.billSummaryToolStripMenuItem.Click += new System.EventHandler(this.billSummaryToolStripMenuItem_Click);
        //    // 
        //    // main
        //    // 
        //    this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        //    this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        //    this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
        //    this.ClientSize = new System.Drawing.Size(1018, 744);
        //    this.Controls.Add(this.lsttableno);
        //    this.Controls.Add(this.lstwaiter);
        //    this.Controls.Add(this.panel4);
        //    this.Controls.Add(this.btnQT);
        //    this.Controls.Add(this.panel1);
        //    this.Controls.Add(this.panel3);
        //    this.Controls.Add(this.btncancelBill);
        //    this.Controls.Add(this.btnchnageperson);
        //    this.Controls.Add(this.btnChangeQty);
        //    this.Controls.Add(this.button3);
        //    this.Controls.Add(this.btnRemoveItem);
        //    this.Controls.Add(this.btnChangePrice);
        //    this.Controls.Add(this.btnAddOrder);
        //    this.Controls.Add(this.panel2);
        //    this.Name = "main";
        //    this.Text = "K-Palms Help Line 9096234202,9595163594/  www.kpalms.net";
        //    this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
        //    this.Load += new System.EventHandler(this.main_Load);
        //    this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.main_KeyDown);
        //    this.panel1.ResumeLayout(false);
        //    this.pnltotal.ResumeLayout(false);
        //    this.pnltotal.PerformLayout();
        //    ((System.ComponentModel.ISupportInitialize)(this.grdOrder)).EndInit();
        //    this.panel3.ResumeLayout(false);
        //    this.panel3.PerformLayout();
        //    this.tabControl1.ResumeLayout(false);
        //    this.tabPage1.ResumeLayout(false);
        //    ((System.ComponentModel.ISupportInitialize)(this.grdTableDetails)).EndInit();
        //    this.tabPage2.ResumeLayout(false);
        //    ((System.ComponentModel.ISupportInitialize)(this.grdKot)).EndInit();
        //    this.tabPage3.ResumeLayout(false);
        //    ((System.ComponentModel.ISupportInitialize)(this.grdPendingBill)).EndInit();
        //    ((System.ComponentModel.ISupportInitialize)(this.grditem)).EndInit();
        //    this.panel4.ResumeLayout(false);
        //    this.panel4.PerformLayout();
        //    this.panel2.ResumeLayout(false);
        //    this.panel2.PerformLayout();
        //    this.menuStrip2.ResumeLayout(false);
        //    this.menuStrip2.PerformLayout();
        //    this.ResumeLayout(false);

        //}

        //#endregion

        //private System.Windows.Forms.ToolStripMenuItem addCatagoryToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem addTableToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem purchasingToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem workerToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        //private System.Windows.Forms.ToolStripMenuItem addSubCatagoryToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem catagoryToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem addCatagoryToolStripMenuItem1;
        //private System.Windows.Forms.ToolStripMenuItem addSubCatagoryToolStripMenuItem1;
        //private System.Windows.Forms.ToolStripMenuItem vvvToolStripMenuItem;
        //private System.Windows.Forms.Panel panel2;
        //private System.Windows.Forms.MenuStrip menuStrip2;
        //private System.Windows.Forms.ToolStripMenuItem saleToolStripMenuItem;
        //private System.Windows.Forms.Button btncancelBill;
        //private System.Windows.Forms.Button btnchnageperson;
        //private System.Windows.Forms.Button btnChangeQty;
        //private System.Windows.Forms.Label label4;
        //private System.Windows.Forms.TextBox txtqty;
        //private System.Windows.Forms.Button button3;
        //private System.Windows.Forms.Button btnRemoveItem;
        //private System.Windows.Forms.Button btnChangePrice;
        //private System.Windows.Forms.Button btnAddOrder;
        //private System.Windows.Forms.Button button1;
        //private System.Windows.Forms.Panel panel3;
        //private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        //private System.Windows.Forms.TextBox txtsearch;
        //private System.Windows.Forms.DataGridView grditem;
        //private System.Windows.Forms.Label label6;
        //private System.Windows.Forms.Panel panel1;
        //private System.Windows.Forms.Panel pnltotal;
        //private System.Windows.Forms.Label lbldisc2;
        //private System.Windows.Forms.Label lblsubtext;
        //private System.Windows.Forms.Label lblsubtotal;
        //private System.Windows.Forms.Label lbldistext;
        //private System.Windows.Forms.Label lbldis;
        //private System.Windows.Forms.Label lblperson;
        //private System.Windows.Forms.Label label5;
        //private System.Windows.Forms.Label label7;
        //private System.Windows.Forms.Label label8;
        //private System.Windows.Forms.Label lbltable;
        //private System.Windows.Forms.Label lbltotal;
        //private System.Windows.Forms.Button btndis;
        //private System.Windows.Forms.Label lbldisc;
        //private System.Windows.Forms.Label label3;
        //private System.Windows.Forms.DataGridView grdOrder;
        //private System.Windows.Forms.TextBox txtdis;
        //private System.Windows.Forms.Label label2;
        //private System.Windows.Forms.TextBox txtperson;
        //private System.Windows.Forms.Button btnshowtable;
        //private System.Windows.Forms.Label label1;
        //private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        //private System.Windows.Forms.Button btnQT;
        //private System.Drawing.Printing.PrintDocument printDocument1;
        //private System.Windows.Forms.TextBox txtSearchByNo;
        //private System.Windows.Forms.Label label9;
        //private System.Windows.Forms.ToolStripMenuItem alertToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem toolToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem hotelSettingToolStripMenuItem1;
        //private System.Windows.Forms.ToolStripMenuItem securityToolStripMenuItem1;
        //private System.Windows.Forms.ToolStripMenuItem kotReprintToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem1;
        //private System.Windows.Forms.ToolStripMenuItem todayToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem yesturdayToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem saleByTableToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem cancelBillListToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem itemPriceListToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem customerListToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem purchasingReportsToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem billReprintToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem cashBookToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem cashBookReportsToolStripMenuItem;
        //private System.Windows.Forms.Panel panel4;
        //private System.Windows.Forms.Label lbltime;
        //private System.Windows.Forms.TextBox txtshortname;
        //private System.Windows.Forms.Label label14;
        //private System.Windows.Forms.TabControl tabControl1;
        //private System.Windows.Forms.TabPage tabPage1;
        //private System.Windows.Forms.TabPage tabPage2;
        //private System.Windows.Forms.Timer timer1;
        //private System.Windows.Forms.TextBox txttableno;
        //private System.Windows.Forms.Label lblcashierName;
        //private System.Windows.Forms.ToolStripMenuItem setupToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem addCatagoryToolStripMenuItem3;
        //private System.Windows.Forms.ToolStripMenuItem addItemToolStripMenuItem1;
        //private System.Windows.Forms.ToolStripMenuItem addTableToolStripMenuItem1;
        //private System.Windows.Forms.ToolStripMenuItem addSupplierToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem workerToolStripMenuItem2;
        //private System.Windows.Forms.ToolStripMenuItem transactionToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem purchasingToolStripMenuItem1;
        //private System.Windows.Forms.ToolStripMenuItem windowsToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem calculatorToolStripMenuItem1;
        //private System.Windows.Forms.ToolStripMenuItem inventoryToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem viewStockToolStripMenuItem;
        //private System.Windows.Forms.Label label15;
        //private System.Windows.Forms.TextBox txtworker;
        //private System.Windows.Forms.ListBox lstwaiter;
        //private System.Windows.Forms.TabPage tabPage3;
        //private System.Windows.Forms.DataGridView grdTableDetails;
        //private System.Windows.Forms.DataGridView grdPendingBill;
        //private System.Windows.Forms.DataGridView grdKot;
        //private System.Windows.Forms.ToolStripMenuItem shiftingToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem lodgingToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem addRoomToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem checkInToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem checkOutToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem viewAllCheckOutCustomerToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem notePadToolStripMenuItem;
        //private System.Windows.Forms.ListBox lsttableno;
        //private System.Windows.Forms.Label label16;
        //private System.Windows.Forms.ToolStripMenuItem createUnitToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem creatPackToolStripMenuItem;
        //private System.Windows.Forms.RadioButton rdobar;
        //private System.Windows.Forms.RadioButton rdoresto;
        //private System.Windows.Forms.ToolStripMenuItem restaurantToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem bToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem lodgeToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem periodWiseToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem billWiseToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem cashierWiseToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem periodWiseToolStripMenuItem1;
        //private System.Windows.Forms.ToolStripMenuItem billWiseToolStripMenuItem1;
        //private System.Windows.Forms.ToolStripMenuItem cashierWiseToolStripMenuItem1;
        //private System.Windows.Forms.ToolStripMenuItem saleByItemToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem itemWiseToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem checkInReportsToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem checkOutReportsToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem billWiseToolStripMenuItem2;
        //private System.Windows.Forms.ToolStripMenuItem cashierWiseToolStripMenuItem2;
        //private System.Windows.Forms.ToolStripMenuItem roomWiseToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem messageToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem kotAuditBotAuditToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem kotWiseToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem botWiseToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem kotItemToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem botItemToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem restaurantToolStripMenuItem1;
        //private System.Windows.Forms.ToolStripMenuItem barToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem printerSettingToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem BillSummeryToolStripMenuItem;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.addCatagoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.addSubCatagoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addTableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchasingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.workerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.catagoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addCatagoryToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.addSubCatagoryToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.vvvToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label4 = new System.Windows.Forms.Label();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnshifting = new System.Windows.Forms.Button();
            this.btnmodify = new System.Windows.Forms.Button();
            this.btnmerge = new System.Windows.Forms.Button();
            this.btncounter = new System.Windows.Forms.Button();
            this.btnKotRemark = new System.Windows.Forms.Button();
            this.pnlcustomername = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.lblSociety = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblCAddress = new System.Windows.Forms.Label();
            this.lblCMobileNo = new System.Windows.Forms.Label();
            this.lblCname = new System.Windows.Forms.Label();
            this.pnltotal = new System.Windows.Forms.Panel();
            this.pnldelivery = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.lblDeliverycharge = new System.Windows.Forms.Label();
            this.pnldiscount = new System.Windows.Forms.Panel();
            this.lbldiscount = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.lblperson = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lbltable = new System.Windows.Forms.Label();
            this.lbltotal = new System.Windows.Forms.Label();
            this.btnbillreprint = new System.Windows.Forms.Button();
            this.btnQT = new System.Windows.Forms.Button();
            this.grdOrder = new System.Windows.Forms.DataGridView();
            this.btnprint = new System.Windows.Forms.Button();
            this.btndis = new System.Windows.Forms.Button();
            this.btnKotReprint = new System.Windows.Forms.Button();
            this.btnCustomer = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtperson = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.flowLayoutPanelTable = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.grditem = new System.Windows.Forms.DataGridView();
            this.txtSearchByNo = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtsearch = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.alertToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel4 = new System.Windows.Forms.Panel();
            this.cbokotremark = new System.Windows.Forms.ComboBox();
            this.cboqty = new System.Windows.Forms.ComboBox();
            this.cbowaitor = new System.Windows.Forms.ComboBox();
            this.cbotable = new System.Windows.Forms.ComboBox();
            this.comboseat = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtshortname = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblcashierName = new System.Windows.Forms.Label();
            this.btnshowtable = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btncancelBill = new System.Windows.Forms.Button();
            this.btnchnageperson = new System.Windows.Forms.Button();
            this.btnChangeQty = new System.Windows.Forms.Button();
            this.btnRemoveItem = new System.Windows.Forms.Button();
            this.btnChangePrice = new System.Windows.Forms.Button();
            this.btnAddOrder = new System.Windows.Forms.Button();
            this.combodepartment = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.chkcounter = new System.Windows.Forms.CheckBox();
            this.lblopendate = new System.Windows.Forms.Label();
            this.btnclose = new System.Windows.Forms.Button();
            this.btnDeliveryCharge = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.pnlcustomername.SuspendLayout();
            this.pnltotal.SuspendLayout();
            this.pnldelivery.SuspendLayout();
            this.pnldiscount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdOrder)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grditem)).BeginInit();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // addCatagoryToolStripMenuItem
            // 
            this.addCatagoryToolStripMenuItem.Name = "addCatagoryToolStripMenuItem";
            this.addCatagoryToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(167, 22);
            // 
            // addSubCatagoryToolStripMenuItem
            // 
            this.addSubCatagoryToolStripMenuItem.Name = "addSubCatagoryToolStripMenuItem";
            this.addSubCatagoryToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // addTableToolStripMenuItem
            // 
            this.addTableToolStripMenuItem.Name = "addTableToolStripMenuItem";
            this.addTableToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // purchasingToolStripMenuItem
            // 
            this.purchasingToolStripMenuItem.Name = "purchasingToolStripMenuItem";
            this.purchasingToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // workerToolStripMenuItem
            // 
            this.workerToolStripMenuItem.Name = "workerToolStripMenuItem";
            this.workerToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // catagoryToolStripMenuItem
            // 
            this.catagoryToolStripMenuItem.Name = "catagoryToolStripMenuItem";
            this.catagoryToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
            this.catagoryToolStripMenuItem.Text = "Catagory";
            // 
            // addCatagoryToolStripMenuItem1
            // 
            this.addCatagoryToolStripMenuItem1.Name = "addCatagoryToolStripMenuItem1";
            this.addCatagoryToolStripMenuItem1.Size = new System.Drawing.Size(32, 19);
            // 
            // addSubCatagoryToolStripMenuItem1
            // 
            this.addSubCatagoryToolStripMenuItem1.Name = "addSubCatagoryToolStripMenuItem1";
            this.addSubCatagoryToolStripMenuItem1.Size = new System.Drawing.Size(32, 19);
            // 
            // vvvToolStripMenuItem
            // 
            this.vvvToolStripMenuItem.Name = "vvvToolStripMenuItem";
            this.vvvToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(237, -1);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 30;
            this.label4.Text = "Qty F6";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnshifting);
            this.panel1.Controls.Add(this.btnmodify);
            this.panel1.Controls.Add(this.btnmerge);
            this.panel1.Controls.Add(this.btncounter);
            this.panel1.Controls.Add(this.btnKotRemark);
            this.panel1.Controls.Add(this.pnlcustomername);
            this.panel1.Controls.Add(this.pnltotal);
            this.panel1.Controls.Add(this.btnbillreprint);
            this.panel1.Controls.Add(this.btnQT);
            this.panel1.Controls.Add(this.grdOrder);
            this.panel1.Controls.Add(this.btnprint);
            this.panel1.Controls.Add(this.btndis);
            this.panel1.Location = new System.Drawing.Point(19, 111);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(331, 621);
            this.panel1.TabIndex = 37;
            // 
            // btnshifting
            // 
            this.btnshifting.BackColor = System.Drawing.Color.Gold;
            this.btnshifting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnshifting.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnshifting.Location = new System.Drawing.Point(219, 539);
            this.btnshifting.Name = "btnshifting";
            this.btnshifting.Size = new System.Drawing.Size(89, 24);
            this.btnshifting.TabIndex = 72;
            this.btnshifting.Text = "Shifting";
            this.btnshifting.UseVisualStyleBackColor = false;
            this.btnshifting.Click += new System.EventHandler(this.btnshifting_Click);
            // 
            // btnmodify
            // 
            this.btnmodify.BackColor = System.Drawing.Color.Gold;
            this.btnmodify.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnmodify.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnmodify.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmodify.Location = new System.Drawing.Point(119, 566);
            this.btnmodify.Name = "btnmodify";
            this.btnmodify.Size = new System.Drawing.Size(89, 24);
            this.btnmodify.TabIndex = 77;
            this.btnmodify.Text = "Modify";
            this.btnmodify.UseVisualStyleBackColor = false;
            this.btnmodify.Click += new System.EventHandler(this.btnmodify_Click);
            // 
            // btnmerge
            // 
            this.btnmerge.BackColor = System.Drawing.Color.Gold;
            this.btnmerge.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnmerge.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmerge.Location = new System.Drawing.Point(119, 538);
            this.btnmerge.Name = "btnmerge";
            this.btnmerge.Size = new System.Drawing.Size(89, 24);
            this.btnmerge.TabIndex = 73;
            this.btnmerge.Text = "Merge";
            this.btnmerge.UseVisualStyleBackColor = false;
            this.btnmerge.Click += new System.EventHandler(this.btnmerge_Click);
            // 
            // btncounter
            // 
            this.btncounter.BackColor = System.Drawing.Color.Gold;
            this.btncounter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btncounter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btncounter.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncounter.Location = new System.Drawing.Point(19, 567);
            this.btncounter.Name = "btncounter";
            this.btncounter.Size = new System.Drawing.Size(89, 24);
            this.btncounter.TabIndex = 75;
            this.btncounter.Text = "Counter";
            this.btncounter.UseVisualStyleBackColor = false;
            this.btncounter.Click += new System.EventHandler(this.btndone_Click);
            // 
            // btnKotRemark
            // 
            this.btnKotRemark.BackColor = System.Drawing.Color.Gold;
            this.btnKotRemark.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnKotRemark.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnKotRemark.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKotRemark.Location = new System.Drawing.Point(219, 567);
            this.btnKotRemark.Name = "btnKotRemark";
            this.btnKotRemark.Size = new System.Drawing.Size(89, 24);
            this.btnKotRemark.TabIndex = 71;
            this.btnKotRemark.Text = "KOT Remark";
            this.btnKotRemark.UseVisualStyleBackColor = false;
            this.btnKotRemark.Click += new System.EventHandler(this.btnKotRemark_Click);
            // 
            // pnlcustomername
            // 
            this.pnlcustomername.BackColor = System.Drawing.Color.AntiqueWhite;
            this.pnlcustomername.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlcustomername.Controls.Add(this.label13);
            this.pnlcustomername.Controls.Add(this.lblSociety);
            this.pnlcustomername.Controls.Add(this.label10);
            this.pnlcustomername.Controls.Add(this.label11);
            this.pnlcustomername.Controls.Add(this.label12);
            this.pnlcustomername.Controls.Add(this.lblCAddress);
            this.pnlcustomername.Controls.Add(this.lblCMobileNo);
            this.pnlcustomername.Controls.Add(this.lblCname);
            this.pnlcustomername.Location = new System.Drawing.Point(7, 280);
            this.pnlcustomername.Name = "pnlcustomername";
            this.pnlcustomername.Size = new System.Drawing.Size(313, 75);
            this.pnlcustomername.TabIndex = 38;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(128, 47);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(14, 13);
            this.label13.TabIndex = 9;
            this.label13.Text = "fl";
            // 
            // lblSociety
            // 
            this.lblSociety.AutoSize = true;
            this.lblSociety.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSociety.Location = new System.Drawing.Point(75, 46);
            this.lblSociety.Name = "lblSociety";
            this.lblSociety.Size = new System.Drawing.Size(48, 13);
            this.lblSociety.TabIndex = 8;
            this.lblSociety.Text = "Society";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(6, 46);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 13);
            this.label10.TabIndex = 5;
            this.label10.Text = "Address";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(6, 26);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "Mobile No";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(6, 6);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(38, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "Name";
            // 
            // lblCAddress
            // 
            this.lblCAddress.AutoSize = true;
            this.lblCAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCAddress.Location = new System.Drawing.Point(150, 46);
            this.lblCAddress.Name = "lblCAddress";
            this.lblCAddress.Size = new System.Drawing.Size(52, 13);
            this.lblCAddress.TabIndex = 2;
            this.lblCAddress.Text = "Address";
            // 
            // lblCMobileNo
            // 
            this.lblCMobileNo.AutoSize = true;
            this.lblCMobileNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCMobileNo.Location = new System.Drawing.Point(75, 26);
            this.lblCMobileNo.Name = "lblCMobileNo";
            this.lblCMobileNo.Size = new System.Drawing.Size(63, 13);
            this.lblCMobileNo.TabIndex = 1;
            this.lblCMobileNo.Text = "Mobile No";
            // 
            // lblCname
            // 
            this.lblCname.AutoSize = true;
            this.lblCname.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCname.Location = new System.Drawing.Point(75, 8);
            this.lblCname.Name = "lblCname";
            this.lblCname.Size = new System.Drawing.Size(38, 13);
            this.lblCname.TabIndex = 0;
            this.lblCname.Text = "Name";
            // 
            // pnltotal
            // 
            this.pnltotal.BackColor = System.Drawing.Color.AntiqueWhite;
            this.pnltotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnltotal.Controls.Add(this.pnldelivery);
            this.pnltotal.Controls.Add(this.pnldiscount);
            this.pnltotal.Controls.Add(this.lblperson);
            this.pnltotal.Controls.Add(this.label5);
            this.pnltotal.Controls.Add(this.label7);
            this.pnltotal.Controls.Add(this.label8);
            this.pnltotal.Controls.Add(this.lbltable);
            this.pnltotal.Controls.Add(this.lbltotal);
            this.pnltotal.Location = new System.Drawing.Point(8, 361);
            this.pnltotal.Name = "pnltotal";
            this.pnltotal.Size = new System.Drawing.Size(312, 118);
            this.pnltotal.TabIndex = 34;
            // 
            // pnldelivery
            // 
            this.pnldelivery.Controls.Add(this.label19);
            this.pnldelivery.Controls.Add(this.lblDeliverycharge);
            this.pnldelivery.Location = new System.Drawing.Point(14, 82);
            this.pnldelivery.Name = "pnldelivery";
            this.pnldelivery.Size = new System.Drawing.Size(297, 31);
            this.pnldelivery.TabIndex = 13;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(10, 5);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(109, 17);
            this.label19.TabIndex = 11;
            this.label19.Text = "Delivery Charge";
            // 
            // lblDeliverycharge
            // 
            this.lblDeliverycharge.AutoSize = true;
            this.lblDeliverycharge.BackColor = System.Drawing.Color.Transparent;
            this.lblDeliverycharge.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDeliverycharge.Location = new System.Drawing.Point(194, 5);
            this.lblDeliverycharge.Name = "lblDeliverycharge";
            this.lblDeliverycharge.Size = new System.Drawing.Size(124, 17);
            this.lblDeliverycharge.TabIndex = 12;
            this.lblDeliverycharge.Text = "Delivery Charge";
            // 
            // pnldiscount
            // 
            this.pnldiscount.Controls.Add(this.lbldiscount);
            this.pnldiscount.Controls.Add(this.label18);
            this.pnldiscount.Location = new System.Drawing.Point(12, 1);
            this.pnldiscount.Name = "pnldiscount";
            this.pnldiscount.Size = new System.Drawing.Size(291, 23);
            this.pnldiscount.TabIndex = 10;
            // 
            // lbldiscount
            // 
            this.lbldiscount.AutoSize = true;
            this.lbldiscount.BackColor = System.Drawing.Color.Transparent;
            this.lbldiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldiscount.Location = new System.Drawing.Point(191, 1);
            this.lbldiscount.Name = "lbldiscount";
            this.lbldiscount.Size = new System.Drawing.Size(69, 17);
            this.lbldiscount.TabIndex = 12;
            this.lbldiscount.Text = "discount";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(12, 2);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(63, 17);
            this.label18.TabIndex = 11;
            this.label18.Text = "Discount";
            // 
            // lblperson
            // 
            this.lblperson.AutoSize = true;
            this.lblperson.BackColor = System.Drawing.Color.Transparent;
            this.lblperson.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblperson.Location = new System.Drawing.Point(208, 24);
            this.lblperson.Name = "lblperson";
            this.lblperson.Size = new System.Drawing.Size(58, 17);
            this.lblperson.TabIndex = 8;
            this.lblperson.Text = "person";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(24, 59);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 17);
            this.label5.TabIndex = 1;
            this.label5.Text = "Total";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(24, 39);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 17);
            this.label7.TabIndex = 3;
            this.label7.Text = "Table";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(24, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 17);
            this.label8.TabIndex = 4;
            this.label8.Text = "Person";
            // 
            // lbltable
            // 
            this.lbltable.AutoSize = true;
            this.lbltable.BackColor = System.Drawing.Color.Transparent;
            this.lbltable.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltable.Location = new System.Drawing.Point(208, 41);
            this.lbltable.Name = "lbltable";
            this.lbltable.Size = new System.Drawing.Size(49, 17);
            this.lbltable.TabIndex = 7;
            this.lbltable.Text = "Table";
            // 
            // lbltotal
            // 
            this.lbltotal.AutoSize = true;
            this.lbltotal.BackColor = System.Drawing.Color.Transparent;
            this.lbltotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltotal.ForeColor = System.Drawing.Color.Red;
            this.lbltotal.Location = new System.Drawing.Point(208, 61);
            this.lbltotal.Name = "lbltotal";
            this.lbltotal.Size = new System.Drawing.Size(61, 17);
            this.lbltotal.TabIndex = 9;
            this.lbltotal.Text = "label13";
            // 
            // btnbillreprint
            // 
            this.btnbillreprint.BackColor = System.Drawing.Color.Gold;
            this.btnbillreprint.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnbillreprint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnbillreprint.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnbillreprint.Location = new System.Drawing.Point(19, 537);
            this.btnbillreprint.Name = "btnbillreprint";
            this.btnbillreprint.Size = new System.Drawing.Size(89, 24);
            this.btnbillreprint.TabIndex = 82;
            this.btnbillreprint.Text = "Bill Reprint";
            this.btnbillreprint.UseVisualStyleBackColor = false;
            this.btnbillreprint.Click += new System.EventHandler(this.btnbillreprint_Click);
            // 
            // btnQT
            // 
            this.btnQT.BackColor = System.Drawing.Color.Gold;
            this.btnQT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnQT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnQT.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQT.Location = new System.Drawing.Point(119, 483);
            this.btnQT.Name = "btnQT";
            this.btnQT.Size = new System.Drawing.Size(90, 51);
            this.btnQT.TabIndex = 38;
            this.btnQT.Text = "KOT\r\nPrint(ctrl+k)";
            this.btnQT.UseVisualStyleBackColor = false;
            this.btnQT.Click += new System.EventHandler(this.btnQT_Click);
            // 
            // grdOrder
            // 
            this.grdOrder.AllowUserToDeleteRows = false;
            this.grdOrder.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdOrder.BackgroundColor = System.Drawing.Color.AntiqueWhite;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.PeachPuff;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdOrder.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grdOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Orange;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdOrder.DefaultCellStyle = dataGridViewCellStyle2;
            this.grdOrder.EnableHeadersVisualStyles = false;
            this.grdOrder.Location = new System.Drawing.Point(10, 3);
            this.grdOrder.MultiSelect = false;
            this.grdOrder.Name = "grdOrder";
            this.grdOrder.ReadOnly = true;
            this.grdOrder.RowHeadersVisible = false;
            this.grdOrder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdOrder.Size = new System.Drawing.Size(310, 273);
            this.grdOrder.TabIndex = 27;
            this.grdOrder.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdOrder_CellValueChanged);
            // 
            // btnprint
            // 
            this.btnprint.BackColor = System.Drawing.Color.Gold;
            this.btnprint.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnprint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnprint.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnprint.Location = new System.Drawing.Point(220, 483);
            this.btnprint.Name = "btnprint";
            this.btnprint.Size = new System.Drawing.Size(90, 51);
            this.btnprint.TabIndex = 28;
            this.btnprint.Text = "Print Bill\r\n(ctrl+p)";
            this.btnprint.UseVisualStyleBackColor = false;
            this.btnprint.Click += new System.EventHandler(this.button3_Click);
            // 
            // btndis
            // 
            this.btndis.BackColor = System.Drawing.Color.Gold;
            this.btndis.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btndis.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btndis.Location = new System.Drawing.Point(19, 483);
            this.btndis.Name = "btndis";
            this.btndis.Size = new System.Drawing.Size(90, 51);
            this.btndis.TabIndex = 36;
            this.btndis.Text = "Discount";
            this.btndis.UseVisualStyleBackColor = false;
            this.btndis.Click += new System.EventHandler(this.btndis_Click);
            // 
            // btnKotReprint
            // 
            this.btnKotReprint.BackColor = System.Drawing.Color.Gold;
            this.btnKotReprint.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnKotReprint.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKotReprint.Location = new System.Drawing.Point(356, 546);
            this.btnKotReprint.Name = "btnKotReprint";
            this.btnKotReprint.Size = new System.Drawing.Size(79, 24);
            this.btnKotReprint.TabIndex = 81;
            this.btnKotReprint.Text = "Kot Reprint";
            this.btnKotReprint.UseVisualStyleBackColor = false;
            this.btnKotReprint.Click += new System.EventHandler(this.btnKotReprint_Click);
            // 
            // btnCustomer
            // 
            this.btnCustomer.BackColor = System.Drawing.Color.Gold;
            this.btnCustomer.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCustomer.Location = new System.Drawing.Point(354, 111);
            this.btnCustomer.Name = "btnCustomer";
            this.btnCustomer.Size = new System.Drawing.Size(77, 51);
            this.btnCustomer.TabIndex = 61;
            this.btnCustomer.Text = "Customer(F1)";
            this.btnCustomer.UseVisualStyleBackColor = false;
            this.btnCustomer.Click += new System.EventHandler(this.btnCustomer_Click_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(582, 1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 30;
            this.label2.Text = "Person";
            // 
            // txtperson
            // 
            this.txtperson.BackColor = System.Drawing.Color.White;
            this.txtperson.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtperson.Location = new System.Drawing.Point(585, 18);
            this.txtperson.Multiline = true;
            this.txtperson.Name = "txtperson";
            this.txtperson.Size = new System.Drawing.Size(53, 33);
            this.txtperson.TabIndex = 6;
            this.txtperson.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtperson_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(1, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "Table F2";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.flowLayoutPanelTable);
            this.panel3.Controls.Add(this.flowLayoutPanel1);
            this.panel3.Controls.Add(this.grditem);
            this.panel3.Controls.Add(this.txtSearchByNo);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.txtsearch);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Location = new System.Drawing.Point(441, 111);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(562, 621);
            this.panel3.TabIndex = 36;
            // 
            // flowLayoutPanelTable
            // 
            this.flowLayoutPanelTable.AutoScroll = true;
            this.flowLayoutPanelTable.BackColor = System.Drawing.Color.AntiqueWhite;
            this.flowLayoutPanelTable.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanelTable.Location = new System.Drawing.Point(303, 3);
            this.flowLayoutPanelTable.Name = "flowLayoutPanelTable";
            this.flowLayoutPanelTable.Size = new System.Drawing.Size(249, 613);
            this.flowLayoutPanelTable.TabIndex = 31;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.AntiqueWhite;
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(7, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(290, 299);
            this.flowLayoutPanel1.TabIndex = 30;
            // 
            // grditem
            // 
            this.grditem.AllowUserToAddRows = false;
            this.grditem.AllowUserToDeleteRows = false;
            this.grditem.AllowUserToResizeRows = false;
            this.grditem.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grditem.BackgroundColor = System.Drawing.Color.AntiqueWhite;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.PeachPuff;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grditem.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.grditem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grditem.Cursor = System.Windows.Forms.Cursors.Hand;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Orange;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grditem.DefaultCellStyle = dataGridViewCellStyle4;
            this.grditem.EnableHeadersVisualStyles = false;
            this.grditem.Location = new System.Drawing.Point(8, 356);
            this.grditem.MultiSelect = false;
            this.grditem.Name = "grditem";
            this.grditem.ReadOnly = true;
            this.grditem.RowHeadersVisible = false;
            this.grditem.RowTemplate.Height = 30;
            this.grditem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grditem.Size = new System.Drawing.Size(289, 257);
            this.grditem.TabIndex = 29;
            this.grditem.TabStop = false;
            this.grditem.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grditem_CellDoubleClick);
            this.grditem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grditem_KeyDown);
            // 
            // txtSearchByNo
            // 
            this.txtSearchByNo.BackColor = System.Drawing.Color.AntiqueWhite;
            this.txtSearchByNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSearchByNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchByNo.Location = new System.Drawing.Point(8, 324);
            this.txtSearchByNo.Name = "txtSearchByNo";
            this.txtSearchByNo.Size = new System.Drawing.Size(141, 26);
            this.txtSearchByNo.TabIndex = 3;
            this.txtSearchByNo.TextChanged += new System.EventHandler(this.txtSearchByNo_TextChanged);
            this.txtSearchByNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearchByNo_KeyDown);
            this.txtSearchByNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSearchByNo_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(5, 308);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 13);
            this.label9.TabIndex = 32;
            this.label9.Text = "Search No F3";
            // 
            // txtsearch
            // 
            this.txtsearch.BackColor = System.Drawing.Color.AntiqueWhite;
            this.txtsearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsearch.Location = new System.Drawing.Point(155, 324);
            this.txtsearch.Name = "txtsearch";
            this.txtsearch.Size = new System.Drawing.Size(142, 26);
            this.txtsearch.TabIndex = 4;
            this.txtsearch.TextChanged += new System.EventHandler(this.txtsearch_TextChanged);
            this.txtsearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtsearch_KeyDown);
            this.txtsearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtsearch_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(152, 308);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 14);
            this.label6.TabIndex = 28;
            this.label6.Text = "ItemsName F5";
            // 
            // alertToolStripMenuItem
            // 
            this.alertToolStripMenuItem.Name = "alertToolStripMenuItem";
            this.alertToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.cbokotremark);
            this.panel4.Controls.Add(this.cboqty);
            this.panel4.Controls.Add(this.cbowaitor);
            this.panel4.Controls.Add(this.cbotable);
            this.panel4.Controls.Add(this.comboseat);
            this.panel4.Controls.Add(this.label17);
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.txtshortname);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.txtperson);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Location = new System.Drawing.Point(210, 27);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(645, 68);
            this.panel4.TabIndex = 39;
            // 
            // cbokotremark
            // 
            this.cbokotremark.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbokotremark.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbokotremark.FormattingEnabled = true;
            this.cbokotremark.Location = new System.Drawing.Point(412, 16);
            this.cbokotremark.Name = "cbokotremark";
            this.cbokotremark.Size = new System.Drawing.Size(167, 33);
            this.cbokotremark.TabIndex = 75;
            this.cbokotremark.Enter += new System.EventHandler(this.cbokotremark_Enter);
            this.cbokotremark.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbokotremark_KeyPress);
            this.cbokotremark.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbokotremark_KeyDown);
            // 
            // cboqty
            // 
            this.cboqty.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboqty.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboqty.FormattingEnabled = true;
            this.cboqty.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20"});
            this.cboqty.Location = new System.Drawing.Point(240, 14);
            this.cboqty.Name = "cboqty";
            this.cboqty.Size = new System.Drawing.Size(50, 33);
            this.cboqty.TabIndex = 76;
            this.cboqty.Enter += new System.EventHandler(this.cboqty_Enter);
            this.cboqty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboqty_KeyPress);
            this.cboqty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboqty_KeyDown);
            // 
            // cbowaitor
            // 
            this.cbowaitor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbowaitor.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbowaitor.FormattingEnabled = true;
            this.cbowaitor.Location = new System.Drawing.Point(146, 15);
            this.cbowaitor.Name = "cbowaitor";
            this.cbowaitor.Size = new System.Drawing.Size(88, 33);
            this.cbowaitor.TabIndex = 75;
            this.cbowaitor.Enter += new System.EventHandler(this.cbowaitor_Enter);
            this.cbowaitor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbowaitor_KeyPress);
            this.cbowaitor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbowaitor_KeyDown);
            // 
            // cbotable
            // 
            this.cbotable.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbotable.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbotable.FormattingEnabled = true;
            this.cbotable.Location = new System.Drawing.Point(6, 14);
            this.cbotable.Name = "cbotable";
            this.cbotable.Size = new System.Drawing.Size(85, 33);
            this.cbotable.TabIndex = 74;
            this.cbotable.SelectedIndexChanged += new System.EventHandler(this.cbotable_SelectedIndexChanged);
            this.cbotable.Enter += new System.EventHandler(this.cbotable_Enter);
            this.cbotable.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbotable_KeyPress);
            this.cbotable.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbotable_KeyDown);
            // 
            // comboseat
            // 
            this.comboseat.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboseat.FormattingEnabled = true;
            this.comboseat.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z"});
            this.comboseat.Location = new System.Drawing.Point(99, 15);
            this.comboseat.Name = "comboseat";
            this.comboseat.Size = new System.Drawing.Size(41, 33);
            this.comboseat.TabIndex = 73;
            this.comboseat.SelectedIndexChanged += new System.EventHandler(this.comboseat_SelectedIndexChanged);
            this.comboseat.Enter += new System.EventHandler(this.comboseat_Enter);
            this.comboseat.KeyDown += new System.Windows.Forms.KeyEventHandler(this.comboseat_KeyDown);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(410, -1);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(75, 13);
            this.label17.TabIndex = 72;
            this.label17.Text = "KOT Remark";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(145, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(77, 13);
            this.label15.TabIndex = 69;
            this.label15.Text = "Waiter Name";
            // 
            // txtshortname
            // 
            this.txtshortname.BackColor = System.Drawing.Color.White;
            this.txtshortname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtshortname.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtshortname.Location = new System.Drawing.Point(299, 14);
            this.txtshortname.Name = "txtshortname";
            this.txtshortname.Size = new System.Drawing.Size(108, 35);
            this.txtshortname.TabIndex = 4;
            this.txtshortname.TextChanged += new System.EventHandler(this.txtshortname_TextChanged);
            this.txtshortname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtshortname_KeyDown);
            this.txtshortname.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtshortname_KeyPress);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(296, -2);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(90, 13);
            this.label14.TabIndex = 64;
            this.label14.Text = "Short Name F7";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(3, 9);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(66, 15);
            this.label16.TabIndex = 70;
            this.label16.Text = "Welcome";
            // 
            // lblcashierName
            // 
            this.lblcashierName.AutoSize = true;
            this.lblcashierName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcashierName.Location = new System.Drawing.Point(87, 9);
            this.lblcashierName.Name = "lblcashierName";
            this.lblcashierName.Size = new System.Drawing.Size(108, 15);
            this.lblcashierName.TabIndex = 67;
            this.lblcashierName.Text = "lblcashierName";
            // 
            // btnshowtable
            // 
            this.btnshowtable.BackColor = System.Drawing.Color.Gold;
            this.btnshowtable.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnshowtable.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnshowtable.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnshowtable.Location = new System.Drawing.Point(914, 54);
            this.btnshowtable.Name = "btnshowtable";
            this.btnshowtable.Size = new System.Drawing.Size(89, 51);
            this.btnshowtable.TabIndex = 28;
            this.btnshowtable.Text = "Kot Details";
            this.btnshowtable.UseVisualStyleBackColor = false;
            this.btnshowtable.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Gold;
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(355, 330);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(77, 51);
            this.button2.TabIndex = 69;
            this.button2.Text = "Change ItemName";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btncancelBill
            // 
            this.btncancelBill.BackColor = System.Drawing.Color.Gold;
            this.btncancelBill.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btncancelBill.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btncancelBill.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btncancelBill.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncancelBill.Location = new System.Drawing.Point(355, 273);
            this.btncancelBill.Name = "btncancelBill";
            this.btncancelBill.Size = new System.Drawing.Size(77, 51);
            this.btncancelBill.TabIndex = 33;
            this.btncancelBill.Text = "Cancel\r\nItem";
            this.btncancelBill.UseVisualStyleBackColor = false;
            this.btncancelBill.Click += new System.EventHandler(this.btncancelBill_Click);
            // 
            // btnchnageperson
            // 
            this.btnchnageperson.BackColor = System.Drawing.Color.Gold;
            this.btnchnageperson.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnchnageperson.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnchnageperson.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnchnageperson.Location = new System.Drawing.Point(356, 383);
            this.btnchnageperson.Name = "btnchnageperson";
            this.btnchnageperson.Size = new System.Drawing.Size(77, 51);
            this.btnchnageperson.TabIndex = 32;
            this.btnchnageperson.Text = "Change Persons";
            this.btnchnageperson.UseVisualStyleBackColor = false;
            this.btnchnageperson.Click += new System.EventHandler(this.btnchnageperson_Click);
            // 
            // btnChangeQty
            // 
            this.btnChangeQty.BackColor = System.Drawing.Color.Gold;
            this.btnChangeQty.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnChangeQty.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChangeQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChangeQty.Location = new System.Drawing.Point(355, 165);
            this.btnChangeQty.Name = "btnChangeQty";
            this.btnChangeQty.Size = new System.Drawing.Size(77, 51);
            this.btnChangeQty.TabIndex = 31;
            this.btnChangeQty.Text = "Change Qty";
            this.btnChangeQty.UseVisualStyleBackColor = false;
            this.btnChangeQty.Click += new System.EventHandler(this.btnChangeQty_Click);
            // 
            // btnRemoveItem
            // 
            this.btnRemoveItem.BackColor = System.Drawing.Color.Gold;
            this.btnRemoveItem.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRemoveItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemoveItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemoveItem.Location = new System.Drawing.Point(355, 220);
            this.btnRemoveItem.Name = "btnRemoveItem";
            this.btnRemoveItem.Size = new System.Drawing.Size(77, 51);
            this.btnRemoveItem.TabIndex = 27;
            this.btnRemoveItem.Text = "Remove Item";
            this.btnRemoveItem.UseVisualStyleBackColor = false;
            this.btnRemoveItem.Click += new System.EventHandler(this.btnRemoveItem_Click);
            // 
            // btnChangePrice
            // 
            this.btnChangePrice.BackColor = System.Drawing.Color.Gold;
            this.btnChangePrice.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnChangePrice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChangePrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChangePrice.Location = new System.Drawing.Point(355, 493);
            this.btnChangePrice.Name = "btnChangePrice";
            this.btnChangePrice.Size = new System.Drawing.Size(79, 49);
            this.btnChangePrice.TabIndex = 26;
            this.btnChangePrice.Text = "Change\r\nPrice";
            this.btnChangePrice.UseVisualStyleBackColor = false;
            this.btnChangePrice.Click += new System.EventHandler(this.btnChangePrice_Click);
            // 
            // btnAddOrder
            // 
            this.btnAddOrder.BackColor = System.Drawing.Color.Gold;
            this.btnAddOrder.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddOrder.Location = new System.Drawing.Point(356, 439);
            this.btnAddOrder.Name = "btnAddOrder";
            this.btnAddOrder.Size = new System.Drawing.Size(77, 51);
            this.btnAddOrder.TabIndex = 25;
            this.btnAddOrder.Text = "Add to Order";
            this.btnAddOrder.UseVisualStyleBackColor = false;
            this.btnAddOrder.Click += new System.EventHandler(this.btnAddOrder_Click);
            // 
            // combodepartment
            // 
            this.combodepartment.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.combodepartment.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combodepartment.FormattingEnabled = true;
            this.combodepartment.Location = new System.Drawing.Point(18, 46);
            this.combodepartment.Name = "combodepartment";
            this.combodepartment.Size = new System.Drawing.Size(167, 33);
            this.combodepartment.TabIndex = 74;
            this.combodepartment.SelectedIndexChanged += new System.EventHandler(this.combodepartment_SelectedIndexChanged);
            this.combodepartment.Enter += new System.EventHandler(this.combodepartment_Enter);
            this.combodepartment.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.combodepartment_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(15, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 73;
            this.label3.Text = "Department :";
            // 
            // chkcounter
            // 
            this.chkcounter.AutoSize = true;
            this.chkcounter.Location = new System.Drawing.Point(18, 88);
            this.chkcounter.Name = "chkcounter";
            this.chkcounter.Size = new System.Drawing.Size(63, 17);
            this.chkcounter.TabIndex = 76;
            this.chkcounter.Text = "Counter";
            this.chkcounter.UseVisualStyleBackColor = true;
            this.chkcounter.CheckedChanged += new System.EventHandler(this.chkcounter_CheckedChanged);
            // 
            // lblopendate
            // 
            this.lblopendate.AutoSize = true;
            this.lblopendate.Location = new System.Drawing.Point(798, 9);
            this.lblopendate.Name = "lblopendate";
            this.lblopendate.Size = new System.Drawing.Size(41, 13);
            this.lblopendate.TabIndex = 78;
            this.lblopendate.Text = "label13";
            // 
            // btnclose
            // 
            this.btnclose.BackColor = System.Drawing.Color.Red;
            this.btnclose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnclose.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnclose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnclose.Location = new System.Drawing.Point(985, 1);
            this.btnclose.Name = "btnclose";
            this.btnclose.Size = new System.Drawing.Size(30, 33);
            this.btnclose.TabIndex = 81;
            this.btnclose.Text = "x";
            this.btnclose.UseVisualStyleBackColor = false;
            this.btnclose.Click += new System.EventHandler(this.btnclose_Click);
            // 
            // btnDeliveryCharge
            // 
            this.btnDeliveryCharge.BackColor = System.Drawing.Color.Gold;
            this.btnDeliveryCharge.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeliveryCharge.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeliveryCharge.Location = new System.Drawing.Point(356, 575);
            this.btnDeliveryCharge.Name = "btnDeliveryCharge";
            this.btnDeliveryCharge.Size = new System.Drawing.Size(79, 24);
            this.btnDeliveryCharge.TabIndex = 83;
            this.btnDeliveryCharge.Text = "Ex charge";
            this.btnDeliveryCharge.UseVisualStyleBackColor = false;
            this.btnDeliveryCharge.Click += new System.EventHandler(this.btnDeliveryCharge_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Gold;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(356, 604);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(79, 24);
            this.button1.TabIndex = 84;
            this.button1.Text = "Parcel";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(1024, 750);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnDeliveryCharge);
            this.Controls.Add(this.btnKotReprint);
            this.Controls.Add(this.btnclose);
            this.Controls.Add(this.lblopendate);
            this.Controls.Add(this.chkcounter);
            this.Controls.Add(this.combodepartment);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btncancelBill);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.lblcashierName);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnCustomer);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.btnchnageperson);
            this.Controls.Add(this.btnChangeQty);
            this.Controls.Add(this.btnRemoveItem);
            this.Controls.Add(this.btnshowtable);
            this.Controls.Add(this.btnChangePrice);
            this.Controls.Add(this.btnAddOrder);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "main";
            this.Text = "K-Palms Help Line 9096234202,9595163594/  www.kpalms.net";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.main_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.main_KeyDown);
            this.panel1.ResumeLayout(false);
            this.pnlcustomername.ResumeLayout(false);
            this.pnlcustomername.PerformLayout();
            this.pnltotal.ResumeLayout(false);
            this.pnltotal.PerformLayout();
            this.pnldelivery.ResumeLayout(false);
            this.pnldelivery.PerformLayout();
            this.pnldiscount.ResumeLayout(false);
            this.pnldiscount.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdOrder)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grditem)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem addCatagoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addTableToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchasingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem workerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem addSubCatagoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem catagoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addCatagoryToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem addSubCatagoryToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem vvvToolStripMenuItem;
        private System.Windows.Forms.Button btncancelBill;
        private System.Windows.Forms.Button btnchnageperson;
        private System.Windows.Forms.Button btnChangeQty;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnprint;
        private System.Windows.Forms.Button btnRemoveItem;
        private System.Windows.Forms.Button btnChangePrice;
        private System.Windows.Forms.Button btnAddOrder;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.TextBox txtsearch;
        private System.Windows.Forms.DataGridView grditem;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnltotal;
        private System.Windows.Forms.Label lblperson;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lbltable;
        private System.Windows.Forms.Label lbltotal;
        private System.Windows.Forms.Button btndis;
        private System.Windows.Forms.DataGridView grdOrder;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtperson;
        private System.Windows.Forms.Button btnshowtable;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnQT;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.TextBox txtSearchByNo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ToolStripMenuItem alertToolStripMenuItem;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox txtshortname;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblcashierName;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel pnlcustomername;
        private System.Windows.Forms.Label lblCAddress;
        private System.Windows.Forms.Label lblCMobileNo;
        private System.Windows.Forms.Label lblCname;
        private System.Windows.Forms.Button btnCustomer;
        private System.Windows.Forms.Panel pnldiscount;
        private System.Windows.Forms.Label lbldiscount;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnKotRemark;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox comboseat;
        private System.Windows.Forms.ComboBox combodepartment;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelTable;
        private System.Windows.Forms.ComboBox cbotable;
        private System.Windows.Forms.ComboBox cbowaitor;
        private System.Windows.Forms.ComboBox cboqty;
        private System.Windows.Forms.ComboBox cbokotremark;
        private System.Windows.Forms.Button btnshifting;
        private System.Windows.Forms.Button btnmerge;
        private System.Windows.Forms.Button btncounter;
        private System.Windows.Forms.CheckBox chkcounter;
        private System.Windows.Forms.Button btnmodify;
        private System.Windows.Forms.Label lblopendate;
        private System.Windows.Forms.Button btnKotReprint;
        private System.Windows.Forms.Button btnbillreprint;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblSociety;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnclose;
        private System.Windows.Forms.Button btnDeliveryCharge;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lblDeliverycharge;
        private System.Windows.Forms.Panel pnldelivery;
        private System.Windows.Forms.Button button1;
       
    }
}
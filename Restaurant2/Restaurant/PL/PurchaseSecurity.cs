﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using Restaurant.PL.Reports;



namespace Restaurant.PL
{
    public partial class PurchaseSecurity : Form
    {

        DL_Purchase dl_purchase = new DL_Purchase();
        BL_Purchase bl_purchase = new BL_Purchase();
        BL_Field bl_field = new BL_Field();


        public PurchaseSecurity()
        {
            InitializeComponent();
        }

        private void PurchaseSecurity_Load(object sender, EventArgs e)
        {
            dtpFromDate.CustomFormat = "dd/MM/yyyy";
            dtpToDate.CustomFormat = "dd/MM/yyyy";

            BindDataByDate();
            BindInvoiceNo();

        }

        

        private void Setfield()
        {
            bl_field.FromDate = dtpFromDate.Text;
            bl_field.Todate = dtpToDate.Text;
        }



        private void BindDataByDate()
        {
            Setfield();

            DataTable dt = bl_purchase.SelectPurchaseByDate(bl_field);
            grdPurchase.DataSource = dt;

            grdPurchase.Columns["PurchaseDetailsId"].Visible = false;

        }

        private void BindInvoiceNo()
        {
            Setfield();
            DataTable dt = bl_purchase.SelectInvoiceNo(bl_field);

            DataRow row = dt.NewRow();
            row["BillNo"] = Convert.ToString("SELECT");
            dt.Rows.InsertAt(row, 0);

            cmbInvoiceNo.DataSource = dt;
            cmbInvoiceNo.DisplayMember = "BillNo";
            cmbInvoiceNo.ValueMember = "BillNo";
 
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (cmbInvoiceNo.Text == "SELECT")
            {
                MessageBox.Show("Please Select Invoice No!");
            }
            else
            {
                try
                {
                    //DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Invoice No.?", "Delete", MessageBoxButtons.YesNo);
                    //if (msgg == DialogResult.Yes)
                    //{
                    //    bl_field.PurchaseDetailsId = Convert.ToInt32(grdPurchase.SelectedRows[0].Cells[0].Value.ToString());
                    //    bl_field.BillNo = cmbInvoiceNo.Text;
                    //    bl_purchase.DeletePurchaseData(bl_purchase);
                    //    MessageBox.Show("Record Deleted Successfully");
                    //    BindInvoiceNo();
                    //    BindDataByDate();



                    //}
                }
                catch
                {

                    MessageBox.Show("error occured!Please contact Administration?");
                }
            }
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            BindInvoiceNo();
            BindDataByDate();
        }
     

        //private void btnShow_Click(object sender, EventArgs e)
        //{
        //    BindInvoiceNo();
        //    BindDataByDate();
        //}

        //private void cmbInvoiceNo_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (cmbInvoiceNo.Text != "System.Data.DataRowView")
        //    {
        //        if (cmbInvoiceNo.ValueMember == "InvoiceNo")
        //        {
        //            string invoic = cmbInvoiceNo.SelectedValue.ToString();
        //            BindDataByInvoice(invoic);
        //        }
        //    }
        //}

      

       private void btnUpdate_Click(object sender, EventArgs e)
       {
           //if (cmbInvoiceNo.Text == "SELECT")
           //{
           //    MessageBox.Show("Please Select Invoice No!");
           //}
           //else
           //{
           //    this.Close();
           //    Purchasing p = new Purchasing(cmbInvoiceNo.Text);
           //    p.ShowDialog();
           //}
       }

       private void grdPurchase_CellClick(object sender, DataGridViewCellEventArgs e)
       {

       }

       private void PurchaseSecurity_KeyDown(object sender, KeyEventArgs e)
       {
           if (e.KeyCode == Keys.Escape)
           {
               this.Close();
           }

       }

       private void btnUpdate_Click_1(object sender, EventArgs e)
       {
           if (cmbInvoiceNo.Text == "SELECT")
           {
               MessageBox.Show("Please Select BillNo!");
           }
           else
           {
               this.Close();
               Purchasing p = new Purchasing(cmbInvoiceNo.Text);
               p.ShowDialog();
           }
       }

       private void cmbInvoiceNo_SelectedIndexChanged_1(object sender, EventArgs e)
       {
           if (cmbInvoiceNo.Text != "System.Data.DataRowView")
           {
               if (cmbInvoiceNo.ValueMember == "BillNo")
               {
                   string invoic = cmbInvoiceNo.SelectedValue.ToString();
                   BindDataByInvoice(invoic);
               }
           }
       }


       private void BindDataByInvoice(string ino)
       {
           bl_field.BillNo = ino;
           DataTable dt = bl_purchase.SelectDataByInvoice(bl_field);
           grdPurchase.DataSource = dt;
           grdPurchase.Columns["PurchaseDetailsId"].Visible = false;
          
       }

      

    }
}

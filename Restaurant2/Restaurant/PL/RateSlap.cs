﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Restaurant.BL;
using Restaurant.DL;

namespace Restaurant.PL
{
    public partial class RateSlap : Form
    {
        public RateSlap()
        {
            InitializeComponent();
        }
        int id;

        BL_Catagory bl_catagory = new BL_Catagory();
        BL_Field bl_field = new BL_Field();
        BL_Table bl_Table = new BL_Table();
        BL_Worker bl_worker = new BL_Worker();

        private void txtrate_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b' && e.KeyChar != '.')
            //{
            //    e.Handled = true;
            //    MessageBox.Show("Please enter number only");
            //}
            Common common = new Common();
            common.OneDecimalPoint(txtrate, e);
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
             try
            {
            if (combocatagory.Text == "SELECT")
            {
                MessageBox.Show("Please Select Catagory");
            }
            else if (comboitem.Text == "SELECT")
            {
                MessageBox.Show("Please Select Item");
            }
            else if (combotable.Text == "SELECT")
            {
                MessageBox.Show("Please Select RateCategory");
            }
            else if (txtrate.Text == "")
            {
                MessageBox.Show("Please Enter Rate");
            }
            else
            {
              
                SetFill();
                string returnvalue = bl_worker.CheckRateSlap(bl_field);
                 if (returnvalue != "")
                 {
                     MessageBox.Show("This record already exists!");

                 }
                 else
                 {

                     bl_worker.RateSlapInsert(bl_field);
                     Clear();
                     BindRateSalp();
                 }
            }
            }
             catch
             {
                 MessageBox.Show("error occured!Please contact Administration?");
             }
        }

        private void RateSlap_Load(object sender, EventArgs e)
        {
            BindCatagory();
           // BindTable();
            BindRateCat();
            BindRateSalp();
            btndelete.Enabled = false;
            btnupdate.Enabled = false;
        }

        private void BindCatagory()
        {
            DataTable dt = bl_catagory.Catagory(bl_field);
            DataRow row = dt.NewRow();
            row["CatagoryName"] = "SELECT";
            dt.Rows.InsertAt(row, 0);
            combocatagory.DataSource = dt;
            combocatagory.ValueMember = "CatId";
            combocatagory.DisplayMember = "CatagoryName";
        }


        private void BindItemName(string catid)
        {
            if (combocatagory.Text != "System.Data.DataRowView")
            {
                if (catid != "System.Data.DataRowView")
                {
                    DataTable dt = bl_catagory.SelectSubCatagory(catid);
                    DataRow row = dt.NewRow();
                    row["ItemName"] = "SELECT";
                    dt.Rows.InsertAt(row, 0);
                    comboitem.DataSource = dt;
                    comboitem.ValueMember = "SubCatId";
                    comboitem.DisplayMember = "ItemName";
                }
            }

        }

        private void combocatagory_SelectedIndexChanged(object sender, EventArgs e)
        {
            string catid = combocatagory.SelectedValue.ToString();
            BindItemName(catid);
        }


        //private void BindTable()
        //{
        //    DataTable dt = bl_Table.SelectTable();
        //    DataRow row = dt.NewRow();
        //    row["TableNo"] = "SELECT";
        //    dt.Rows.InsertAt(row, 0);
        //    combotable.DataSource = dt;
           
        //    combotable.ValueMember = "TblId";
        //    combotable.DisplayMember = "TableNo";
        //}


        private void SetFill()
        {
            bl_field.CatId = combocatagory.SelectedValue.ToString();
            bl_field.ItemId = comboitem.SelectedValue.ToString();
            bl_field.RateCatId = Convert.ToInt32( combotable.SelectedValue.ToString());
            bl_field.Rate = txtrate.Text;
        }

        private void Clear()
        {
           // combocatagory.Text = "SELECT";
           // comboitem.Text = "SELECT";
            combotable.Text = "SELECT";
            txtrate.Text = "";
        }

        private void btnupdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (combocatagory.Text == "SELECT")
                {
                    MessageBox.Show("Please Select Catagory");
                }
                else if (comboitem.Text == "SELECT")
                {
                    MessageBox.Show("Please Select Item");
                }
                else if (combotable.Text == "SELECT")
                {
                    MessageBox.Show("Please Select Table");
                }
                else if (txtrate.Text == "")
                {
                    MessageBox.Show("Please Enter Rate");
                }
                else
                {
                    SetFill();
                    bl_field.RateSlapId = id;
                    bl_worker.RateSlapUpdate(bl_field);
                    Clear();
                    BindRateSalp();

                }
            }
            catch
            {
                MessageBox.Show("error occured!Please contact Administration?");
            }
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {

                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Catagory?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_field.RateSlapId = id;
                    bl_worker.RateSlapDelete(bl_field);
                    BindRateSalp();
                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
        }

        private void dgvpayment_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            id = Convert.ToInt32(dgvpayment.SelectedRows[0].Cells[0].Value.ToString());
            combocatagory.Text = dgvpayment.SelectedRows[0].Cells[1].Value.ToString();
            comboitem.Text = dgvpayment.SelectedRows[0].Cells[2].Value.ToString();
            combotable.Text = dgvpayment.SelectedRows[0].Cells[3].Value.ToString();
            txtrate.Text = dgvpayment.SelectedRows[0].Cells[4].Value.ToString();
            btndelete.Enabled = true;
            btnupdate.Enabled = true;
            btnsave.Enabled = false;
        }

        private void btncancle_Click(object sender, EventArgs e)
        {
            Clear();
            BindRateSalp();
            //dgvpayment.DataSource = null;
            btndelete.Enabled = false;
            btnupdate.Enabled = false;
            btnsave.Enabled = true;
        }

        private void BindRateSalp()
        {
           DataTable dt= bl_worker.SelectRateSlap();
           dgvpayment.DataSource = dt;
           dgvpayment.Columns["RateSlapId"].Visible = false;
           dgvpayment.Columns["CATAGORYNAME"].Visible = false;

        }

        private void BindRateCat()
        {
            DataTable dt = bl_worker.SelectRateCategary();
            DataRow row = dt.NewRow();
            row["RateCatName"] = "SELECT";
            dt.Rows.InsertAt(row, 0);
            combotable.DataSource = dt;
            combotable.ValueMember = "RateCatId";
            combotable.DisplayMember = "RateCatName";
        }

        private void comboitem_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboitem.Text != "System.Data.DataRowView")
            {
                if (comboitem.ValueMember == "SubCatId")
                {
                    bl_field.ItemId = comboitem.SelectedValue.ToString();
                    DataTable dt = bl_worker.SelectRateSlapById(bl_field);
                    dgvpayment.DataSource = dt;
                    dgvpayment.Columns["RateSlapId"].Visible = false;
                    dgvpayment.Columns["CATAGORYNAME"].Visible = false;
                }
            }
        }
    }
}

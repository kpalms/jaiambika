﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;



using System.Data.SqlClient;
using Restaurant.BL;


namespace Restaurant.PL
{
    public partial class PrinterSetup : Form
    {
        BL_Field bl_field = new BL_Field();
        BL_NewItem bl_newitem = new BL_NewItem();

        
        public PrinterSetup()
        {
            InitializeComponent();
        }

        private void PrinterSetup_Load(object sender, EventArgs e)
        {
            foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            {
                ddlbill.Items.Add(printer);
                ddlkot.Items.Add(printer);
            }

            BindPrinter();
        }

        private void SetField()
        {
            bl_field.BillPrinter = ddlbill.Text;
            bl_field.KotPrinter = ddlkot.Text;
        }
        private void btnclose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            try
            {
                if (btnadd.Text == "Add")
                {
                    if (ddlbill.Text == "")
                    {
                        MessageBox.Show("Please select Bill Printer?");
                    }
                    else if (ddlkot.Text == "")
                    {
                        MessageBox.Show("Please select KOT Printer?");
                    }
                    else
                    {

                        SetField();
                        bl_newitem.InsertPrinter(bl_field);

                        MessageBox.Show("Printer select Successfully");
                        BindPrinter();

                    }
                }
                if (btnadd.Text == "Update")
                {

                    if (ddlbill.Text == "")
                    {
                        MessageBox.Show("Please select Bill Printer?");
                    }
                    else if (ddlkot.Text == "")
                    {
                        MessageBox.Show("Please select KOT Printer?");
                    }
                    else
                    {
                        SetField();
                        bl_newitem.UpdatePrinter(bl_field);
                        MessageBox.Show("Record Updated Successfully");
                        BindPrinter();
                    }
                }
            }
            catch
            {
                MessageBox.Show("error occured!Please contact Administration?");

            }
        }


        private void BindPrinter()
        {
            DataTable dt = bl_newitem.SelectPrinter();
            if (dt.Rows.Count > 0)
            {
                ddlbill.Text = dt.Rows[0][1].ToString();
                ddlkot.Text = dt.Rows[0][2].ToString();
                btnadd.Text = "Update";
            }
            else
            {
                btnadd.Text = "Add";
            }

        }

        private void PrinterSetup_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

    }
}

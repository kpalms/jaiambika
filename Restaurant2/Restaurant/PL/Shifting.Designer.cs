﻿namespace Restaurant.PL
{
    partial class Shifting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btncancel = new System.Windows.Forms.Button();
            this.btnok = new System.Windows.Forms.Button();
            this.cbofrom = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cboto = new System.Windows.Forms.ComboBox();
            this.cboseat = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btncancel
            // 
            this.btncancel.BackgroundImage = global::Restaurant.Properties.Resources.button21;
            this.btncancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btncancel.Location = new System.Drawing.Point(162, 156);
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(75, 37);
            this.btncancel.TabIndex = 5;
            this.btncancel.Text = "Cancel";
            this.btncancel.UseVisualStyleBackColor = true;
            this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // btnok
            // 
            this.btnok.BackgroundImage = global::Restaurant.Properties.Resources.button21;
            this.btnok.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnok.Location = new System.Drawing.Point(50, 156);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(75, 37);
            this.btnok.TabIndex = 4;
            this.btnok.Text = "Ok";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // cbofrom
            // 
            this.cbofrom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbofrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbofrom.FormattingEnabled = true;
            this.cbofrom.Location = new System.Drawing.Point(50, 34);
            this.cbofrom.Name = "cbofrom";
            this.cbofrom.Size = new System.Drawing.Size(187, 21);
            this.cbofrom.TabIndex = 57;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label3.Location = new System.Drawing.Point(12, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 56;
            this.label3.Text = "From";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label1.Location = new System.Drawing.Point(12, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 13);
            this.label1.TabIndex = 58;
            this.label1.Text = "To";
            // 
            // cboto
            // 
            this.cboto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboto.FormattingEnabled = true;
            this.cboto.Location = new System.Drawing.Point(50, 73);
            this.cboto.Name = "cboto";
            this.cboto.Size = new System.Drawing.Size(117, 21);
            this.cboto.TabIndex = 59;
            // 
            // cboseat
            // 
            this.cboseat.FormattingEnabled = true;
            this.cboseat.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D"});
            this.cboseat.Location = new System.Drawing.Point(173, 73);
            this.cboseat.Name = "cboseat";
            this.cboseat.Size = new System.Drawing.Size(64, 21);
            this.cboseat.TabIndex = 60;
            this.cboseat.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboseat_KeyPress);
            this.cboseat.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboseat_KeyDown);
            // 
            // Shifting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(253)))), ((int)(((byte)(244)))));
            this.ClientSize = new System.Drawing.Size(292, 266);
            this.Controls.Add(this.cboseat);
            this.Controls.Add(this.cboto);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbofrom);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btncancel);
            this.Controls.Add(this.btnok);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Shifting";
            this.Text = "Shifting";
            this.Load += new System.EventHandler(this.Shifting_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Shifting_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btncancel;
        private System.Windows.Forms.Button btnok;
        private System.Windows.Forms.ComboBox cbofrom;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboto;
        private System.Windows.Forms.ComboBox cboseat;
    }
}
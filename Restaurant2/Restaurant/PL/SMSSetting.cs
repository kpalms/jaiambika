﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using Restaurant.PL.Reports;
using System.Data;



namespace Restaurant.PL
{
    public partial class SMSSetting : Form
    {
        BL_Field bl_field = new BL_Field();
        BL_SmsEmail bl_smsemail= new BL_SmsEmail();

        public SMSSetting()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                if (btnsave.Text == "Save")
                {
                    if (txturl.Text == "")
                    {

                        MessageBox.Show("Please enter url?");

                    }
                   else if (txtuserid.Text == "")
                    {
                        MessageBox.Show("Please enter UserId?");
                    }
                    else if (txtpassword.Text == "")
                    {
                        MessageBox.Show("Please enter Password?");
                    }
                    else
                    {
                        SetField();
                        bl_smsemail.InsertSms(bl_field);
                        Clear();
                        MessageBox.Show("Record inserted Successfully");
                        BindHotelSetting();
                    }
                }
               else
                {
                    SetField();
                    bl_smsemail.Updatesms(bl_field);

                    MessageBox.Show("Record Updated Successfully");
                    BindHotelSetting();
                }
            }
            catch
            {
                MessageBox.Show("error occured!Please contact Administration?");

            }
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected URL?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_smsemail.Deletesms(bl_field);
                    Clear();
                    MessageBox.Show("Record Delete Successfully");
                    BindHotelSetting();

                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
        }

        private void btnclose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SMSSetting_Load(object sender, EventArgs e)
        {
            BindHotelSetting();
        }

        private void SetField()
        {
            bl_field.Url = txturl.Text;
            bl_field.UserId = txtuserid.Text;
            bl_field.Password = txtpassword.Text;

        }
        private void Clear()
        {
            txturl.Text="";
            txtuserid.Text = "";
            txtpassword.Text = "";
        }

        private void BindHotelSetting()
        {
            DataTable dt = bl_smsemail.Selectsms();
            if (dt.Rows.Count > 0)
            {
                bl_field.SmsId = Convert.ToInt32(dt.Rows[0][0].ToString());
                txturl.Text = dt.Rows[0][1].ToString();
                txtuserid.Text = dt.Rows[0][2].ToString();
                txtpassword.Text = dt.Rows[0][3].ToString();
                if (dt.Rows[0]["Switch"].ToString() == "1")
                {
                    rdoon.Checked = true;
                }
                else
                {
                    rdooff.Checked = true;
                }
                if (dt.Rows[0]["BillSms"].ToString() == "1")
                {
                    rdobOn.Checked = true;
                }
                else
                {
                    rdobOFF.Checked = true;
                }
                btnsave.Text = "Update";
            }
            else
            {
                btnsave.Text = "Save";
                Clear();
            }

        }

        private void SMSSetting_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void rdoon_CheckedChanged(object sender, EventArgs e)
        {
            Common cm = new Common();
            cm.UseExecuteNonQuery("update SmsSetting set Switch=1");

        }

        private void rdooff_CheckedChanged(object sender, EventArgs e)
        {
            Common cm = new Common();
            cm.UseExecuteNonQuery("update SmsSetting set Switch=0");
        }

        private void rdobOn_CheckedChanged(object sender, EventArgs e)
        {
            Common cm = new Common();
            cm.UseExecuteNonQuery("update SmsSetting set BillSms=1");
        }

        private void rdobOFF_CheckedChanged(object sender, EventArgs e)
        {
            Common cm = new Common();
            cm.UseExecuteNonQuery("update SmsSetting set BillSms=0");
        }
    }
}

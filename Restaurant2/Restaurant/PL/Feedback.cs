﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Restaurant.BL;
using Restaurant.DL;
using Restaurant.PL;


namespace Restaurant.PL
{
    public partial class Feedback : Form
    {
        BL_Feedback bl_feedback = new BL_Feedback();
        Common common = new Common();

        public Feedback()
        {
            InitializeComponent();
        }

        private void Feedback_Load(object sender, EventArgs e)
        {
            BindCustomer();
            BindServer();
            BindFeedback();
            btndelete.Enabled = false;
            btnUpdate.Enabled = false;
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
           
            if(dtpDate.Text=="")
            {
                MessageBox.Show("Enter Date");
            }
            else if (cboCustomer.Text == "")
            {
                MessageBox.Show("Select Customer");
            }
            else if (cboServer.Text == "")
            {
                MessageBox.Show("Select Server");
            }
            else if (cboCustomer.Text == "Select")
            {
                MessageBox.Show("Select Name");
            }
            else if (cboCustomer.Text == "Select")
            {
                MessageBox.Show("Select Name");
            }
            else if (cboServer.Text == "Select")
            {
                MessageBox.Show("Select Server");
            }


                var cntls = GetAll(this, typeof(RadioButton));
                int i = 0;
            foreach (Control cntrl in cntls)
            {
                RadioButton _rb = (RadioButton)cntrl;
                if (_rb.Checked == true)
                {
                    i = 1;
                }
                
            }
            if(i==0)
            {
                MessageBox.Show("Kindly Give Us A Feedback.");
            }

            else          
            {
            SetField();
            bl_feedback.InsertFeedback(bl_feedback);
            BindFeedback();
            MessageBox.Show("Record Inserted Succesfully");
            Clear();
            }
            

        }


        private void SetField()
        {

            bl_feedback.Date = dtpDate.Text;
            bl_feedback.CustId = Convert.ToInt16(cboCustomer.SelectedValue.ToString());
            bl_feedback.ServersName = Convert.ToInt16(cboServer.SelectedValue.ToString());
            bl_feedback.Comments = txtComment.Text;

            if (frdoExcellent.Checked == true)
            {
                bl_feedback.QualityOfFood = 1;
            }
            else if (frdoGood.Checked == true)
            {
                bl_feedback.QualityOfFood = 2;
            }
            else if (frdoAverage.Checked == true)
            {
                bl_feedback.QualityOfFood = 3;
            }
            else if (frdoPoor.Checked == true)
            {
                bl_feedback.QualityOfFood = 4;
            }


            if (srdoExcellent.Checked == true)
            {
                bl_feedback.Service = 1;
            }
            else if (srdoGood.Checked == true)
            {
                bl_feedback.Service = 2;
            }
            else if (srdoAverage.Checked == true)
            {
                bl_feedback.Service = 3;
            }
            else if (srdoPoor.Checked == true)
            {
                bl_feedback.Service = 4;
            }


            if (ardoExcellent.Checked == true)
            {
                bl_feedback.Ambience = 1;
            }
            else if (ardoGood.Checked == true)
            {
                bl_feedback.Ambience = 2;
            }
            else if (ardoAverage.Checked == true)
            {
                bl_feedback.Ambience = 3;
            }
            else if (ardoPoor.Checked == true)
            {
                bl_feedback.Ambience = 4;
            }

 
        }


        private void BindCustomer()
        {
            DataTable dt = bl_feedback.SelectCustomer();
            DataRow row = dt.NewRow();
            row["Name"] = "Select";
            dt.Rows.InsertAt(row, 0);
            cboCustomer.DataSource = dt;
            cboCustomer.DisplayMember = "Name";
            cboCustomer.ValueMember = "CustId";
            
            
        }
        private void BindFeedback()
        {

            DataTable dt = bl_feedback.SelectFeedback();
            grdFeedback.DataSource = dt;
            grdFeedback.Columns["WorkerId"].Visible = false;
            grdFeedback.Columns["FeedbackId"].Visible = false;
            grdFeedback.Columns["ServersName"].Visible = false;
            grdFeedback.Columns["CustId"].Visible = false;
        }

        private void BindServer()
        {
            DataTable dt = bl_feedback.SelectServer();
            DataRow row = dt.NewRow();
            row["Name"] = "Select";
            dt.Rows.InsertAt(row, 0);
            cboServer.DataSource = dt;
            cboServer.DisplayMember = "Name";
            cboServer.ValueMember = "WorkerId";

        }

        private void btnCustomerAdd_Click(object sender, EventArgs e)
        {
            main mm = new main();
            Customer ca = new Customer(mm);
            ca.ShowDialog();
        }
        private void Clear()
        {
            dtpDate.Text = DateTime.Now.ToString();
            txtComment.Text = "";
            var cntls = GetAll(this, typeof(RadioButton));
            foreach (Control cntrl in cntls)
            {
                RadioButton _rb = (RadioButton)cntrl;
                if (_rb.Checked)
                {
                    _rb.Checked = false;

                }
            }
            cboServer.SelectedIndex = 0;
            cboCustomer.SelectedIndex = 0;
            bl_feedback.FeedbacpkId = 0;
            btnUpdate.Enabled = false;
            btndelete.Enabled = false;
            btnadd.Enabled = true;


        }
        public IEnumerable<Control> GetAll(Control control, Type type)
        {
            var controls = control.Controls.Cast<Control>();
            return controls.SelectMany(ctrls => GetAll(ctrls, type)).Concat(controls).Where(c => c.GetType() == type);
        }

        private void cboCustomer_KeyPress(object sender, KeyPressEventArgs e)
        {
            common.ComboBoxCheckValue(cboCustomer, e);
        }

        private void cboServer_KeyPress(object sender, KeyPressEventArgs e)
        {
            common.ComboBoxCheckValue(cboServer, e);
        }

        private void cboCustomer_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                cboServer.Focus();

            }
        }

        private void cboServer_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                panel1.Focus();
        
            }
        }

        private void grdFeedback_CellClick(object sender, DataGridViewCellEventArgs e)
        {
           // Clear();
            if (grdFeedback.Rows.Count > 0)
            {
                  dtpDate.Text = grdFeedback.SelectedRows[0].Cells["Date"].Value.ToString();
                  cboCustomer.SelectedValue = Convert.ToInt16(grdFeedback.SelectedRows[0].Cells["CustId"].Value.ToString());
                  cboServer.SelectedValue = Convert.ToInt16(grdFeedback.SelectedRows[0].Cells["WorkerId"].Value.ToString());
                  txtComment.Text = grdFeedback.SelectedRows[0].Cells["Comments"].Value.ToString();

                  if (grdFeedback.SelectedRows[0].Cells["QualityOfFood"].Value.ToString()=="Excellent")
                  {
                      frdoExcellent.Checked = true;
                  }
                  else if (grdFeedback.SelectedRows[0].Cells["QualityOfFood"].Value.ToString()== "Good")
                  {
                      frdoGood.Checked = true;
                  }
                  else if (grdFeedback.SelectedRows[0].Cells["QualityOfFood"].Value.ToString() == "Average")
                  {
                      frdoAverage.Checked = true;
                  }
                  else if (grdFeedback.SelectedRows[0].Cells["QualityOfFood"].Value.ToString() == "Poor")
                  {
                      frdoPoor.Checked = true;
                  }

                  if (grdFeedback.SelectedRows[0].Cells["Service"].Value.ToString() == "Excellent")
                  {
                      srdoExcellent.Checked = true;
                  }
                  else if (grdFeedback.SelectedRows[0].Cells["Service"].Value.ToString() == "Good")
                  {
                      srdoGood.Checked = true;
                  }
                  else if (grdFeedback.SelectedRows[0].Cells["Service"].Value.ToString() == "Average")
                  {
                      srdoAverage.Checked = true;
                  }
                  else if (grdFeedback.SelectedRows[0].Cells["Service"].Value.ToString() == "Poor")
                  {
                      srdoPoor.Checked = true;
                  }

                  if (grdFeedback.SelectedRows[0].Cells["Ambience"].Value.ToString() == "Excellent")
                  {
                      ardoExcellent.Checked = true;
                  }
                  else if (grdFeedback.SelectedRows[0].Cells["Ambience"].Value.ToString()== "Good")
                  {
                      ardoGood.Checked = true;
                  }
                  else if (grdFeedback.SelectedRows[0].Cells["Ambience"].Value.ToString() == "Average")
                  {
                      ardoAverage.Checked = true;
                  }
                  else if (grdFeedback.SelectedRows[0].Cells["Ambience"].Value.ToString() == "Poor")
                  {
                      ardoPoor.Checked = true;
                  }
                  bl_feedback.FeedbacpkId = Convert.ToInt16(grdFeedback.SelectedRows[0].Cells["FeedbackId"].Value.ToString());
                  btndelete.Enabled = true;
                  btnUpdate.Enabled = true;
                  btnadd.Enabled = false;

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Catagory?", "Delete", MessageBoxButtons.YesNo);
            if (msgg == DialogResult.Yes)
            {
                bl_feedback.DeleteFeedback(bl_feedback);
                BindFeedback();
                MessageBox.Show("Deleted Succesffully");
                Clear();
            }
            
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
            btndelete.Enabled = false;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (dtpDate.Text == "")
            {
                MessageBox.Show("Enter Date");
            }
            else if (cboCustomer.Text == "")
            {
                MessageBox.Show("Select Customer");
            }
            else if (cboServer.Text == "")
            {
                MessageBox.Show("Select Server");
            }
            else if (cboCustomer.Text == "Select")
            {
                MessageBox.Show("Select Name");
            }
            else if (cboCustomer.Text == "Select")
            {
                MessageBox.Show("Select Name");
            }
            else if (cboServer.Text == "Select")
            {
                MessageBox.Show("Select Server");
            }
            else
            {
                SetField();
                bl_feedback.UpdateFeedback(bl_feedback);
                BindFeedback();
                MessageBox.Show("Record Updated Succesfully");
                Clear();                
            }
        }


    }
}

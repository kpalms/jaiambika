﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;



using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;

namespace Restaurant.PL
{
    public partial class ChangeItemName : Form
    {
        BL_Sale bl_sale = new BL_Sale();
        BL_Field bl_field = new BL_Field();
        main _main;

        public ChangeItemName(string saleid, string itemname, main main)
        {
            InitializeComponent();
            _main = main;
            bl_field.SaleId = saleid;
            //bl_field.ItemName = itemname;
            txtchagep.Text = itemname;
            this.KeyPreview = true;
        }

        private void ChangeItemName_Load(object sender, EventArgs e)
        {

        }

        private void Change()
        {
            try
            {
                if (txtchagep.Text=="")
                {
                    MessageBox.Show("Please Enter Item Name");
                    txtchagep.Focus();
                }
                else
                {

                bl_field.ItemName = txtchagep.Text;
                bl_sale.UpdateItemName(bl_field);
                this.Hide();
                _main.PageRefreshEvent();

                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void txtchagep_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
            if (e.KeyCode == Keys.Enter)
            {
                Change();
            }
        }

        private void btnchage_Click(object sender, EventArgs e)
        {
            Change();
        }

        private void txtchagep_TextChanged(object sender, EventArgs e)
        {
            txtchagep.CharacterCasing = CharacterCasing.Upper;
        }

    }
}

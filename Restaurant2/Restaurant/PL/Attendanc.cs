﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Restaurant.BL;
using Restaurant.DL;

using System.Data.SqlClient;
namespace Restaurant.PL
{
    public partial class Attendanc : Form
    {

        BL_Worker bl_worker = new BL_Worker();
        Common common = new Common();


        public Attendanc()
        {
            InitializeComponent();
        }
        int id;
        private int result;

        private void btnsave_Click(object sender, EventArgs e)
        {


            if (combowokers.Text == "SELECT")
            {
                MessageBox.Show("Please Select Worker Name");
                combowokers.Focus();

            }else
            if (comboaddtype.Text == "")
            {
                MessageBox.Show("Please Select Type");
                comboaddtype.Focus();

            }
            else
            {
                bl_worker.WorkerId = Convert.ToInt32(combowokers.SelectedValue.ToString());
                string dr = bl_worker.CheckAttandance(bl_worker);
                if (dr == "1")
                {
                    MessageBox.Show("Record is Already Present");

                }
                else
                {

                    Setfill();
                    bl_worker.InsertAttendanc(bl_worker);
                    selectAttendacedatewise();
                    Clear();
                }
            }
        }


        private void Setfill()
        {
            bl_worker.Date = dtpwokeradde.Text;
            bl_worker.WorkerId=Convert.ToInt32(combowokers.SelectedValue.ToString());
            if (comboaddtype.Text == "Absent")
            {
                result = 0;
            }

            else
            {
                result=1;
            }
            //bl_worker.AttendanceType=Convert.ToInt32(comboaddtype.SelectedValue.ToString());
            bl_worker.AttendanceType = result;
            bl_worker.Reasion=rtxtreasion.Text;  
        }


        private void Clear()
        {
            dtpwokeradde.Text=DateTime.Today.ToString();
            comboaddtype.Text = "";
            combowokers.Text = "SELECT";
            rtxtreasion.Text = "";
            btnupdate.Enabled = false;
            btndelete.Enabled = false;
            btnsave.Enabled = true;
        }

        private void BindWorkers()
        {
            DataTable dt = bl_worker.SelectWorker();
            DataRow row = dt.NewRow();
            row["Name"] = "SELECT";
            dt.Rows.InsertAt(row, 0);
            combowokers.DataSource = dt;
            combowokers.DisplayMember = "Name";
            combowokers.ValueMember = "WorkerId";
            
        }

        private void Attendanc_Load(object sender, EventArgs e)
        {
            BindWorkers();
            selectAttendacedatewise();
            btndelete.Enabled = false;
            btnupdate.Enabled = false;
            dtpwokeradde.Text = DateTime.Today.Date.ToString();
               
           
        }

        private void dgvattendance_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(dgvattendance.Rows.Count > 0)
            {
                     id = Convert.ToInt32(dgvattendance.SelectedRows[0].Cells[0].Value.ToString());
                dtpwokeradde.Text = dgvattendance.SelectedRows[0].Cells[1].Value.ToString();
              combowokers.Text=  dgvattendance.SelectedRows[0].Cells[2].Value.ToString();
            comboaddtype.Text=   dgvattendance.SelectedRows[0].Cells[3].Value.ToString();
            rtxtreasion.Text = dgvattendance.SelectedRows[0].Cells[4].Value.ToString();
            }


            btnsave.Enabled = false;
            btnupdate.Enabled = true;
            btndelete.Enabled = true;
        }

        private void BindAttendance()
        {
            comboaddtype.Text = "";
            rtxtreasion.Text = "";
            bl_worker.WorkerId = Convert.ToInt32(combowokers.SelectedValue.ToString());
            DataTable dt = bl_worker.SelectAttendance(bl_worker);
            dgvattendance.DataSource = dt;
            //dgvattendance.Columns["WorkerId"].Visible = false;
            dgvattendance.Columns["AttendanceId"].Visible = false;
        }

        private void combowokers_SelectedIndexChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    if (combowokers.Text != "System.Data.DataRowView")
            //    {
            //        if (combowokers.ValueMember == "WorkerId")
            //        {

            //            BindAttendance();

                        

            //        }

            //    }

            //}
            //catch
            //{

            //}
        }

        private void btnupdate_Click(object sender, EventArgs e)
        {
            try
            {

             if (combowokers.Text == "SELECT")
            {
                MessageBox.Show("Please Select Worker Name");
                combowokers.Focus();

            }
             else if (comboaddtype.Text == "")
             {
                 MessageBox.Show("Please Select Type");
                 comboaddtype.Focus();

             }
             else
             {
                 Setfill();
                 bl_worker.AttendanceId = id;
                 bl_worker.UpdateAttendance(bl_worker);
                 selectAttendacedatewise();
                 Clear();
             }
             btnsave.Enabled = true;
            btndelete.Enabled = false;
            btnupdate.Enabled = false;
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
        }

        private void btndelete_Click(object sender, EventArgs e)
        {

            try
            {
                
                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Catagory?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_worker.AttendanceId = id;
                    bl_worker.DeleteAttendance(bl_worker);
                    selectAttendacedatewise();
                    Clear();
                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
          
        }


        private void selectAttendacedatewise()
        {
            bl_worker.Date = dtpwokeradde.Text;
            DataTable dt = bl_worker.SelectAttandancedatewise(bl_worker);
            dgvattendance.DataSource = dt;
            dgvattendance.Columns["AttendanceId"].Visible = false;
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void combowokers_KeyPress(object sender, KeyPressEventArgs e)
        {
            common.ComboBoxCheckValue(combowokers, e);
        }

        private void comboaddtype_KeyPress(object sender, KeyPressEventArgs e)
        {
            common.ComboBoxCheckValue(comboaddtype, e);
        }

        private void combowokers_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                comboaddtype.Focus();
            }
        }

        private void comboaddtype_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                rtxtreasion.Focus();
            }
        }

        private void dtpwokeradde_ValueChanged_1(object sender, EventArgs e)
        {
            selectAttendacedatewise();
        }


    }
}

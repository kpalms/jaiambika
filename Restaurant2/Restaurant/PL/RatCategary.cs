﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using Restaurant.BL;
namespace Restaurant.PL
{
    public partial class RatCategary : Form
    {
        public RatCategary()
        {
            InitializeComponent();
        }
        BL_Worker bl_worker = new BL_Worker();
        int id;
        private void btnsave_Click(object sender, EventArgs e)
        {
            if (txtratecat.Text == "")
            {
                MessageBox.Show("Please Enter RateCategory");
                txtratecat.Focus();

            }
            else 
            {
                    bl_worker.RateCatName = txtratecat.Text;
                    string returnvalue = bl_worker.CheckRateCategary(bl_worker);

                    if (returnvalue == txtratecat.Text)
                   {
                       MessageBox.Show("This record already exists!");
                       txtratecat.Text = "";
                       txtratecat.Focus();
                   }
                    else
                    {
                        
                        bl_worker.RateCategaryInsert(bl_worker);
                        txtratecat.Text = "";
                        Bind();
                    }

            }
            
        }

        private void btnupdate_Click(object sender, EventArgs e)
        {
            if (txtratecat.Text == "")
            {
                MessageBox.Show("Please Enter RateCategory");
                txtratecat.Focus();

            }
            else
            {
                     bl_worker.RateCatName = txtratecat.Text;
                    string returnvalue = bl_worker.CheckRateCategary(bl_worker);
                    if (returnvalue == txtratecat.Text)
                    {
                        MessageBox.Show("This record already exists!");
                        txtratecat.Text = "";
                        txtratecat.Focus();
                    }
                    else
                    {
                        bl_worker.RateCatId = id;
                       // bl_worker.RateCatName = txtratecat.Text;
                        bl_worker.RateCategaryUpdate(bl_worker);
                        txtratecat.Text = "";
                        Bind();
                        btnsave.Enabled = true;
                        btnupdate.Enabled = false;
                        btndelete.Enabled = false;
                    }
            }
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {

                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Catagory?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_worker.RateCatId = id;
                    bl_worker.RateCategaryDelete(bl_worker);
                    txtratecat.Text = "";
                    Bind();
                    btnsave.Enabled = true;
                    btnupdate.Enabled = false;
                    btndelete.Enabled = false;
                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
        }

        private void Bind()
        {
            DataTable dt = bl_worker. SelectRateCategary();
            dgvratecat.DataSource = dt;
            dgvratecat.Columns["RateCatId"].Visible = false;
        }

        private void RatCategary_Load(object sender, EventArgs e)
        {
            Bind();
            btnupdate.Enabled = false;
            btndelete.Enabled = false;

        }

        private void dgvratecat_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvratecat.Rows.Count > 0)
            {
                id = Convert.ToInt32(dgvratecat.SelectedRows[0].Cells[0].Value.ToString());
                txtratecat.Text = dgvratecat.SelectedRows[0].Cells[1].Value.ToString();
                btnsave.Enabled = false;
                btnupdate.Enabled = true;
                btndelete.Enabled = true;
            }
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            txtratecat.Text = "";
            btnupdate.Enabled = false;
            btndelete.Enabled = false;
            btnsave.Enabled = true;
        }

        private void txtratecat_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = Char.ToUpper(e.KeyChar);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.Data.SqlClient;
using Restaurant.BL;


namespace Restaurant.PL
{
    public partial class AddKotRemark : Form
    {

        BL_Field bl_field = new BL_Field();
        BL_Catagory bl_catagory = new BL_Catagory();

        public AddKotRemark()
        {
            InitializeComponent();
            this.KeyPreview = true;
        }

        private void AddKotRemark_Load(object sender, EventArgs e)
        {
            BindCatagory();
            btnUpdate.Enabled = false;
            btndelete.Enabled = false;
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            try
            {
                
                if (txtname.Text == "")
                {
                    MessageBox.Show("Please enter Name?");
                }
                else
                {

                    SetField();
                    bl_catagory.InsertKotRemark(bl_field);
                    Clear();
                    MessageBox.Show("Record inserted Successfully");
                    BindCatagory();
                    txtname.Focus();
                }

            }
            catch
            {
                MessageBox.Show("error occured!Please contact Administration?");

            }
        }


        private void SetField()
        {
            bl_field.AddKotRemark = txtname.Text;

        }
        private void Clear()
        {
            txtname.Text = "";

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            bl_field.AddKotId = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[0].Value.ToString());
            txtname.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            btnUpdate.Enabled = true;
            btndelete.Enabled = true;
            btnadd.Enabled = false;
        }

        private void BindCatagory()
        {
            DataTable dt = bl_catagory.SelectKotRemark();
            dataGridView1.DataSource = dt;
            dataGridView1.Columns["KotId"].Visible = false;
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            txtname.Text = "";
            txtname.Focus();
            btndelete.Enabled = false;
            btnUpdate.Enabled = false;
            btnadd.Enabled = true;
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure to delete selected Record?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_catagory.DeleteKotRemark(bl_field);
                    MessageBox.Show("Record Delete Successfully");
                    BindCatagory();
                    Clear();
                    txtname.Focus();
                    btnUpdate.Enabled = false;
                    btndelete.Enabled = false;
                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
        }

        private void AddKotRemark_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void AddKotRemark_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            
            if (txtname.Text == "")
            {
                MessageBox.Show("Please enter Name?");
            }
            else
            {
                SetField();
                bl_catagory.UpdateKotRemark(bl_field);
                Clear();
                MessageBox.Show("Record Updated Successfully");
                BindCatagory();
                txtname.Focus();
                btnUpdate.Enabled = false;
                btndelete.Enabled = false;

            }
            
        }

    }
}

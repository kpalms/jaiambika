﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Restaurant.BL;
namespace Restaurant.PL
{
    public partial class SalaryHistory : Form
    {
        public SalaryHistory()
        {
            InitializeComponent();
        }
        BL_Worker bl_worker = new BL_Worker();

        private void comboworkers_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (comboworkers.Text != "System.Data.DataRowView")
                {
                    if (comboworkers.ValueMember == "WorkerId")
                    {

                        //BindCustomerDetails();

                    }

                }
            }
            catch
            {

            }
        }


        private void SalaryHistory_Load(object sender, EventArgs e)
        {
            BindWorkers();
            //BindWorkerAttandance();
        }

        private void DTP1_ValueChanged(object sender, EventArgs e)
        {
            //BindCustomerDetails();
            //BindWorkerAttandance();
        }

        private void DTP2_ValueChanged(object sender, EventArgs e)
        {
            //BindCustomerDetails();
            //BindWorkerAttandance();
        }

        private void BindWorkers()
        {
            
            DataTable dt = bl_worker.SelectWorker();
            DataRow row = dt.NewRow();
            row["Name"] = "SELECT";
            dt.Rows.InsertAt(row, 0);
            comboworkers.DataSource = dt;
            comboworkers.DisplayMember = "Name";
            comboworkers.ValueMember = "WorkerId";

        }
        private void BindCustomerDetails()
        {
            bl_worker.WorkerId = Convert.ToInt32(comboworkers.SelectedValue.ToString());
            bl_worker.Date = DTP1.Text;
            bl_worker.Date1 = DTP2.Text;
            DataTable dt = bl_worker.Selectworkerhistory(bl_worker);
            dgvhistory.DataSource = dt;
           
            decimal debitamt = 0;
            decimal creditamt = 0;
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["Debit"].ToString() != "")
                    {
                        debitamt = debitamt + Convert.ToDecimal(dt.Rows[i]["Debit"].ToString());
                    }
                    if (dt.Rows[i]["Credit"].ToString() != "")
                    {
                        creditamt = creditamt + Convert.ToDecimal(dt.Rows[i]["Credit"].ToString());
                    }
                }
                lbldebit.Text = Convert.ToString(debitamt);
                lblcredit.Text = Convert.ToString(creditamt);
                lblremaing.Text = Convert.ToString(Convert.ToDecimal(debitamt) - Convert.ToDecimal(creditamt));
                lblpresentdays.Text = dt.Rows[0]["PresentDays"].ToString();
                lblapsentdays.Text = dt.Rows[0]["AbsentDays"].ToString();
                lblhalfdays.Text = dt.Rows[0]["Halfdays"].ToString();
            }
        }


        //private void BindWorkerAttandance()
        //{
        //    DataTable dt = bl_worker.Selectworkerattendance(bl_worker);
        //    lblpresentdays.Text = dt.Rows[0]["PresentDay"].ToString();
        //    lblapsentdays.Text = dt.Rows[0]["AbsentDay"].ToString();
        //    lblhalfdays.Text = dt.Rows[0]["HalfDay"].ToString();
        //}

        private void btnshow_Click(object sender, EventArgs e)
        {
           // BindWorkerAttandance();
            if (comboworkers.Text == "SELECT")
            {
                MessageBox.Show("Please Select the Name form List");
            }
            else

            BindCustomerDetails();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;


namespace Restaurant.PL
{
    public partial class Login : Form
    {
        BL_Worker bl_worker = new BL_Worker();
        BL_Field bl_field = new BL_Field();
        BL_Serial bl_serial = new BL_Serial();
        GenerateKey Genkey = new GenerateKey();

        BL_HotelSetting bl_hotelsett = new BL_HotelSetting();
        Common common = new Common();

        public Login()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
       
        }

        private void lnklogin_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            LoginUser();

        }

        private void LoginUser()
        {
            try
            {

                string dateopen = common.ReturnOneValue("select Date from Date where  Status=1");

                if (dateopen == dtptodaydate.Text)
                {

                    if (txtusername.Text != "" && txtpassword.Text != "")
                    {
                        bl_field.Username = txtusername.Text;
                        bl_field.Password = txtpassword.Text;
                        bl_field.LoginUser = txtusername.Text;
                        SqlDataReader dr = bl_worker.CheckLogin(bl_field);
                        if (dr.Read())
                        {

                            bl_field.RoleLogin = dr[2].ToString();
                            bl_field.WorkerId = dr[3].ToString();
                            bl_field.LoginIdStatic = dr[3].ToString();
                            bl_field.LoginUser = txtusername.Text;
                            bl_field.Date = DateTime.Now.ToString("dd/MM/yy");
                            bl_field.Time = DateTime.Now.ToString("hh:mm");
                            // bl_worker.InsertLoginDetails(bl_field);
                            this.Hide();
                            //main main = new main();
                            //main.Show();

                            Home h = new Home();
                            h.ShowDialog();
                        }
                        else
                        {
                            MessageBox.Show("Please check username and password?");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please enter Username and Password");
                    }
                }
                else
                {
                    MessageBox.Show("Your Billing date is '" + dateopen + "'");
                }


            }

            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
        }
        private void Login_Load(object sender, EventArgs e)
        {

            string strDates = common.GetDate(DateTime.Now);

            dtptodaydate.CustomFormat = "dd/MM/yyyy";

            DataTable hname = bl_hotelsett.SelectHotelSetting();
            if (hname.Rows.Count > 0)
            {
                lblhotelname.Text = hname.Rows[0][1].ToString();
            }
            else
            {
                lblhotelname.Text = "Hotel Name";
            }
            //kk
            txtusername.Text = "kundan";
            txtpassword.Text = "admin";



            string dateopen = common.ReturnOneValue("select Date from Date where  Status=1");
            bl_field.GlobalDate = dateopen;

            if (dateopen == "0")
            {
                bl_field.Status = "1";
                bl_field.Date = strDates;
                bl_hotelsett.InsertDate(bl_field);
                bl_field.GlobalDate = strDates;
            }
            else
            {
                // MessageBox.Show("Your Date '" + dateopen + "' not Close");
            }

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            CreateUser create = new CreateUser();
            create.ShowDialog();
        }

        private void txtusername_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtpassword.Focus();

            }
        }

        private void txtpassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                LoginUser();
            }
        }

   
    }
}

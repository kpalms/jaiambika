﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;

using System.IO;
using System.Drawing.Drawing2D;


namespace Restaurant.PL
{
    public partial class ShowTable : Form
    {
        BL_Field bl_field = new BL_Field();
        BL_Table bl_table = new BL_Table();
        main _main;
        public ShowTable(main main)
        {
            _main = main;
            InitializeComponent();
            this.KeyPreview = true;
        }

        private void btnAddTable_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to Add Table?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {

                    string tbl = bl_table.SelectMaxTable();
                    if (tbl == "")
                    {
                        bl_field.TableNo = "1";
                        bl_table.InsertTable(bl_field);
                       
                        flowLayoutPanel1.Controls.Clear();
                       BindTable();
                        
                    
                    }
                    else
                    {
                        int tblno = Convert.ToInt32(tbl) + 1;
                        bl_field.TableNo = Convert.ToString(tblno);
                        bl_table.InsertTable(bl_field);
                       
                        flowLayoutPanel1.Controls.Clear();
                        BindTable();
                       
                    }
                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }




        }


        private void BindTable()
        {
           
            try
            {

                DataTable dt = bl_table.CheckParcel();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Button bb = new Button();
                    bb.Location = new Point(160, 100 * i + 10);


                    string n = dt.Rows[i]["TableNo"].ToString();
                    bb.Name = dt.Rows[i]["TableNo"].ToString();
                    bb.Size = new Size(100, 81);
                    string a = (dt.Rows[i]["TableNo"].ToString());
                    bb.Text = dt.Rows[i]["TableNo"].ToString();
                    bb.ForeColor = Color.Black;
                    bb.TextAlign = ContentAlignment.BottomCenter;
                    bb.Font = new Font("Lucida Console", 16);
                    if (dt.Rows[i]["Status"].ToString().Trim() == "Open")
                    {

                        bb.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.tableF));
                    }


                    else
                    {
                        bb.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.tableB));

                    }
                    if (dt.Rows[i]["Status"].ToString() == "P")
                    {
                        bb.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.red));
                    }
                    this.flowLayoutPanel1.Controls.Add(bb);
                    bb.Click += new EventHandler(bb_Click);



                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }




        }


        private void ShowTable_Load(object sender, EventArgs e)
        {

            BindTable();
        }

        void bb_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string i = btn.Name;
            this.Hide();
            _main.GetTableId(i);
        }

        private void ShowTable_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

    

        private void lnkRemove_LinkClicked(object sender, EventArgs e)
        {
            DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Catagory?", "Delete", MessageBoxButtons.YesNo);
            if (msgg == DialogResult.Yes)
            {
                bl_table.RemoveTable(bl_field);
                flowLayoutPanel1.Controls.Clear();
                BindTable();
            }
        }

   



















    }
}

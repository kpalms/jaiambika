﻿namespace Restaurant.PL
{
    partial class SendSmsEmail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SendSmsEmail));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.chksms = new System.Windows.Forms.CheckBox();
            this.chkemail = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtsubject = new System.Windows.Forms.TextBox();
            this.txtbody = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnsend = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtsearchMobile = new System.Windows.Forms.TextBox();
            this.txtsearchName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.grdcustomer = new System.Windows.Forms.DataGridView();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grdcustomer)).BeginInit();
            this.SuspendLayout();
            // 
            // chksms
            // 
            this.chksms.AutoSize = true;
            this.chksms.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chksms.Location = new System.Drawing.Point(179, 14);
            this.chksms.Name = "chksms";
            this.chksms.Size = new System.Drawing.Size(52, 17);
            this.chksms.TabIndex = 0;
            this.chksms.Text = "SMS";
            this.chksms.UseVisualStyleBackColor = true;
            // 
            // chkemail
            // 
            this.chkemail.AutoSize = true;
            this.chkemail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkemail.Location = new System.Drawing.Point(297, 14);
            this.chkemail.Name = "chkemail";
            this.chkemail.Size = new System.Drawing.Size(56, 17);
            this.chkemail.TabIndex = 1;
            this.chkemail.Text = "Email";
            this.chkemail.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(68, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 45;
            this.label2.Text = "Emai Subject";
            // 
            // txtsubject
            // 
            this.txtsubject.Location = new System.Drawing.Point(178, 43);
            this.txtsubject.Name = "txtsubject";
            this.txtsubject.Size = new System.Drawing.Size(364, 20);
            this.txtsubject.TabIndex = 46;
            // 
            // txtbody
            // 
            this.txtbody.Location = new System.Drawing.Point(179, 83);
            this.txtbody.Multiline = true;
            this.txtbody.Name = "txtbody";
            this.txtbody.Size = new System.Drawing.Size(825, 115);
            this.txtbody.TabIndex = 47;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(68, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 48;
            this.label1.Text = "Message";
            // 
            // btnsend
            // 
            this.btnsend.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnsend.BackgroundImage")));
            this.btnsend.Location = new System.Drawing.Point(179, 620);
            this.btnsend.Name = "btnsend";
            this.btnsend.Size = new System.Drawing.Size(62, 26);
            this.btnsend.TabIndex = 51;
            this.btnsend.Text = "Send";
            this.btnsend.UseVisualStyleBackColor = true;
            this.btnsend.Click += new System.EventHandler(this.btnsend_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(421, 218);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 56;
            this.label3.Text = "Name";
            // 
            // txtsearchMobile
            // 
            this.txtsearchMobile.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsearchMobile.Location = new System.Drawing.Point(177, 250);
            this.txtsearchMobile.MaxLength = 12;
            this.txtsearchMobile.Multiline = true;
            this.txtsearchMobile.Name = "txtsearchMobile";
            this.txtsearchMobile.Size = new System.Drawing.Size(239, 41);
            this.txtsearchMobile.TabIndex = 52;
            this.txtsearchMobile.TextChanged += new System.EventHandler(this.txtsearchMobile_TextChanged);
            // 
            // txtsearchName
            // 
            this.txtsearchName.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsearchName.Location = new System.Drawing.Point(424, 250);
            this.txtsearchName.Multiline = true;
            this.txtsearchName.Name = "txtsearchName";
            this.txtsearchName.Size = new System.Drawing.Size(165, 41);
            this.txtsearchName.TabIndex = 53;
            this.txtsearchName.TextChanged += new System.EventHandler(this.txtsearchName_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(181, 218);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 55;
            this.label4.Text = "Mobile No";
            // 
            // grdcustomer
            // 
            this.grdcustomer.AllowUserToAddRows = false;
            this.grdcustomer.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            this.grdcustomer.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.grdcustomer.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdcustomer.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(253)))), ((int)(((byte)(244)))));
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.PeachPuff;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdcustomer.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.grdcustomer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.AntiqueWhite;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.PeachPuff;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdcustomer.DefaultCellStyle = dataGridViewCellStyle6;
            this.grdcustomer.EnableHeadersVisualStyles = false;
            this.grdcustomer.Location = new System.Drawing.Point(178, 297);
            this.grdcustomer.MultiSelect = false;
            this.grdcustomer.Name = "grdcustomer";
            this.grdcustomer.RowHeadersVisible = false;
            this.grdcustomer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdcustomer.Size = new System.Drawing.Size(824, 301);
            this.grdcustomer.TabIndex = 206;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(68, 250);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 207;
            this.label5.Text = "Search";
            // 
            // SendSmsEmail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(253)))), ((int)(((byte)(244)))));
            this.ClientSize = new System.Drawing.Size(1016, 734);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.grdcustomer);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtsearchMobile);
            this.Controls.Add(this.txtsearchName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnsend);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtbody);
            this.Controls.Add(this.txtsubject);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.chkemail);
            this.Controls.Add(this.chksms);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SendSmsEmail";
            this.Text = "SendSmsEmail";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.SendSmsEmail_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SendSmsEmail_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.grdcustomer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chksms;
        private System.Windows.Forms.CheckBox chkemail;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtsubject;
        private System.Windows.Forms.TextBox txtbody;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnsend;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtsearchMobile;
        private System.Windows.Forms.TextBox txtsearchName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView grdcustomer;
        private System.Windows.Forms.Label label5;
    }
}
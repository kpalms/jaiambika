﻿namespace Restaurant.PL
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saleStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hotelSettingToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.kotReprintToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.billReprintToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shiftingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageAccessLevelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kotDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addCatagoryToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.addItemToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.addTableToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.addSupplierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.workerToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.messageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printerSettingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adminPartToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuAccessToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemAccessToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sMSSettingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emailSettingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addKotRemarkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transactionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchasingToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.wastageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.expencessToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerPaymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerBalenceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addCustomerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salebyPaymentModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saleSummeryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supplierAccountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createUnitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recipieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewStockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewStockToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.windowsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calculatorToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.notePadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sendSmsEmailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.feedBackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.restaurantToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.periodWiseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.billWiseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cashierWiseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saleByItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.billSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.billDetailsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.customerHistoryToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.discountReportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.parsalReportsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelItemListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelBillReportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.barItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.periodWiseBarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kotAuditBotAuditToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kotItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.todayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yesturdayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saleByTableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemPriceListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchasingReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockEntryReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wastageReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recipieReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.counterReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.securityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dayCloseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dayOpenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.securityToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.logOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.workerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addWorkerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.workerAttendancToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salleryCalculateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salaryHiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.workderAttendaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rateManageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rateCategaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.departmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rateAssignToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.demoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reprintKotToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.securityToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.btnsupplier = new System.Windows.Forms.Button();
            this.btnSaleByPaymentmode = new System.Windows.Forms.Button();
            this.btnsalesummery = new System.Windows.Forms.Button();
            this.btnAddProduct = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btncustomer = new System.Windows.Forms.Button();
            this.btnbillreprint = new System.Windows.Forms.Button();
            this.btnstock = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnpurchase = new System.Windows.Forms.Button();
            this.btncustome = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnAttendance = new System.Windows.Forms.Button();
            this.btnReprintKot = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.menuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip2
            // 
            this.menuStrip2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.menuStrip2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolToolStripMenuItem,
            this.setupToolStripMenuItem,
            this.transactionToolStripMenuItem,
            this.inventoryToolStripMenuItem,
            this.windowsToolStripMenuItem,
            this.reportsToolStripMenuItem1,
            this.helpToolStripMenuItem,
            this.securityToolStripMenuItem,
            this.logOutToolStripMenuItem,
            this.workerToolStripMenuItem,
            this.rateManageToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(1008, 24);
            this.menuStrip2.TabIndex = 3;
            this.menuStrip2.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saleStatusToolStripMenuItem});
            this.fileToolStripMenuItem.Image = global::Restaurant.Properties.Resources.file__4_;
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saleStatusToolStripMenuItem
            // 
            this.saleStatusToolStripMenuItem.Image = global::Restaurant.Properties.Resources._16__9_;
            this.saleStatusToolStripMenuItem.Name = "saleStatusToolStripMenuItem";
            this.saleStatusToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.saleStatusToolStripMenuItem.Text = "SaleStatus";
            this.saleStatusToolStripMenuItem.Click += new System.EventHandler(this.saleStatusToolStripMenuItem_Click);
            // 
            // toolToolStripMenuItem
            // 
            this.toolToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hotelSettingToolStripMenuItem1,
            this.kotReprintToolStripMenuItem,
            this.billReprintToolStripMenuItem,
            this.shiftingToolStripMenuItem,
            this.manageAccessLevelToolStripMenuItem,
            this.kotDetailsToolStripMenuItem});
            this.toolToolStripMenuItem.Image = global::Restaurant.Properties.Resources.toolbox_32x32;
            this.toolToolStripMenuItem.Name = "toolToolStripMenuItem";
            this.toolToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.toolToolStripMenuItem.Text = "Tool";
            // 
            // hotelSettingToolStripMenuItem1
            // 
            this.hotelSettingToolStripMenuItem1.Image = global::Restaurant.Properties.Resources.digg1;
            this.hotelSettingToolStripMenuItem1.Name = "hotelSettingToolStripMenuItem1";
            this.hotelSettingToolStripMenuItem1.Size = new System.Drawing.Size(191, 22);
            this.hotelSettingToolStripMenuItem1.Text = "HotelSetting";
            this.hotelSettingToolStripMenuItem1.Click += new System.EventHandler(this.hotelSettingToolStripMenuItem1_Click);
            // 
            // kotReprintToolStripMenuItem
            // 
            this.kotReprintToolStripMenuItem.Image = global::Restaurant.Properties.Resources.address_books;
            this.kotReprintToolStripMenuItem.Name = "kotReprintToolStripMenuItem";
            this.kotReprintToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.kotReprintToolStripMenuItem.Text = "KotReprint";
            this.kotReprintToolStripMenuItem.Click += new System.EventHandler(this.kotReprintToolStripMenuItem_Click);
            // 
            // billReprintToolStripMenuItem
            // 
            this.billReprintToolStripMenuItem.Image = global::Restaurant.Properties.Resources.address_book_32x32;
            this.billReprintToolStripMenuItem.Name = "billReprintToolStripMenuItem";
            this.billReprintToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.billReprintToolStripMenuItem.Text = "BillReprint";
            this.billReprintToolStripMenuItem.Click += new System.EventHandler(this.billReprintToolStripMenuItem_Click);
            // 
            // shiftingToolStripMenuItem
            // 
            this.shiftingToolStripMenuItem.Image = global::Restaurant.Properties.Resources.table_go;
            this.shiftingToolStripMenuItem.Name = "shiftingToolStripMenuItem";
            this.shiftingToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.shiftingToolStripMenuItem.Text = "Shifting";
            this.shiftingToolStripMenuItem.Click += new System.EventHandler(this.shiftingToolStripMenuItem_Click);
            // 
            // manageAccessLevelToolStripMenuItem
            // 
            this.manageAccessLevelToolStripMenuItem.Image = global::Restaurant.Properties.Resources.Settings_48x48;
            this.manageAccessLevelToolStripMenuItem.Name = "manageAccessLevelToolStripMenuItem";
            this.manageAccessLevelToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.manageAccessLevelToolStripMenuItem.Text = "ManageAccessLevel";
            this.manageAccessLevelToolStripMenuItem.Click += new System.EventHandler(this.manageAccessLevelToolStripMenuItem_Click);
            // 
            // kotDetailsToolStripMenuItem
            // 
            this.kotDetailsToolStripMenuItem.Image = global::Restaurant.Properties.Resources.book_edit;
            this.kotDetailsToolStripMenuItem.Name = "kotDetailsToolStripMenuItem";
            this.kotDetailsToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.kotDetailsToolStripMenuItem.Text = "Kot Details";
            this.kotDetailsToolStripMenuItem.Click += new System.EventHandler(this.kotDetailsToolStripMenuItem_Click);
            // 
            // setupToolStripMenuItem
            // 
            this.setupToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addCatagoryToolStripMenuItem3,
            this.addItemToolStripMenuItem1,
            this.addTableToolStripMenuItem1,
            this.addSupplierToolStripMenuItem,
            this.workerToolStripMenuItem2,
            this.messageToolStripMenuItem,
            this.printerSettingToolStripMenuItem,
            this.adminPartToolStripMenuItem,
            this.menuAccessToolStripMenuItem,
            this.itemAccessToolStripMenuItem,
            this.sMSSettingToolStripMenuItem,
            this.emailSettingToolStripMenuItem,
            this.paymentModeToolStripMenuItem,
            this.addKotRemarkToolStripMenuItem});
            this.setupToolStripMenuItem.Image = global::Restaurant.Properties.Resources.Settings_48x48;
            this.setupToolStripMenuItem.Name = "setupToolStripMenuItem";
            this.setupToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.setupToolStripMenuItem.Text = "Setup";
            // 
            // addCatagoryToolStripMenuItem3
            // 
            this.addCatagoryToolStripMenuItem3.Image = global::Restaurant.Properties.Resources._0__4_;
            this.addCatagoryToolStripMenuItem3.Name = "addCatagoryToolStripMenuItem3";
            this.addCatagoryToolStripMenuItem3.Size = new System.Drawing.Size(158, 22);
            this.addCatagoryToolStripMenuItem3.Text = "AddCatagory";
            this.addCatagoryToolStripMenuItem3.Click += new System.EventHandler(this.addCatagoryToolStripMenuItem3_Click);
            // 
            // addItemToolStripMenuItem1
            // 
            this.addItemToolStripMenuItem1.Image = global::Restaurant.Properties.Resources._0__4_;
            this.addItemToolStripMenuItem1.Name = "addItemToolStripMenuItem1";
            this.addItemToolStripMenuItem1.Size = new System.Drawing.Size(158, 22);
            this.addItemToolStripMenuItem1.Text = "AddItem";
            this.addItemToolStripMenuItem1.Click += new System.EventHandler(this.addItemToolStripMenuItem1_Click);
            // 
            // addTableToolStripMenuItem1
            // 
            this.addTableToolStripMenuItem1.Image = global::Restaurant.Properties.Resources._0__4_;
            this.addTableToolStripMenuItem1.Name = "addTableToolStripMenuItem1";
            this.addTableToolStripMenuItem1.Size = new System.Drawing.Size(158, 22);
            this.addTableToolStripMenuItem1.Text = "AddTable";
            this.addTableToolStripMenuItem1.Click += new System.EventHandler(this.addTableToolStripMenuItem1_Click);
            // 
            // addSupplierToolStripMenuItem
            // 
            this.addSupplierToolStripMenuItem.Image = global::Restaurant.Properties.Resources._0__4_;
            this.addSupplierToolStripMenuItem.Name = "addSupplierToolStripMenuItem";
            this.addSupplierToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.addSupplierToolStripMenuItem.Text = "AddSupplier";
            this.addSupplierToolStripMenuItem.Click += new System.EventHandler(this.addSupplierToolStripMenuItem_Click);
            // 
            // workerToolStripMenuItem2
            // 
            this.workerToolStripMenuItem2.Image = global::Restaurant.Properties.Resources._0__4_;
            this.workerToolStripMenuItem2.Name = "workerToolStripMenuItem2";
            this.workerToolStripMenuItem2.Size = new System.Drawing.Size(158, 22);
            this.workerToolStripMenuItem2.Text = "Worker";
            this.workerToolStripMenuItem2.Click += new System.EventHandler(this.workerToolStripMenuItem2_Click);
            // 
            // messageToolStripMenuItem
            // 
            this.messageToolStripMenuItem.Image = global::Restaurant.Properties.Resources._0__4_;
            this.messageToolStripMenuItem.Name = "messageToolStripMenuItem";
            this.messageToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.messageToolStripMenuItem.Text = "Message";
            this.messageToolStripMenuItem.Click += new System.EventHandler(this.messageToolStripMenuItem_Click);
            // 
            // printerSettingToolStripMenuItem
            // 
            this.printerSettingToolStripMenuItem.Image = global::Restaurant.Properties.Resources.Settings_48x481;
            this.printerSettingToolStripMenuItem.Name = "printerSettingToolStripMenuItem";
            this.printerSettingToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.printerSettingToolStripMenuItem.Text = "PrinterSetting";
            this.printerSettingToolStripMenuItem.Click += new System.EventHandler(this.printerSettingToolStripMenuItem_Click);
            // 
            // adminPartToolStripMenuItem
            // 
            this.adminPartToolStripMenuItem.Image = global::Restaurant.Properties.Resources._18__6_;
            this.adminPartToolStripMenuItem.Name = "adminPartToolStripMenuItem";
            this.adminPartToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.adminPartToolStripMenuItem.Text = "AdminPart";
            this.adminPartToolStripMenuItem.Click += new System.EventHandler(this.adminPartToolStripMenuItem_Click);
            // 
            // menuAccessToolStripMenuItem
            // 
            this.menuAccessToolStripMenuItem.Image = global::Restaurant.Properties.Resources.lock__4_;
            this.menuAccessToolStripMenuItem.Name = "menuAccessToolStripMenuItem";
            this.menuAccessToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.menuAccessToolStripMenuItem.Text = "MenuAccess";
            this.menuAccessToolStripMenuItem.Click += new System.EventHandler(this.menuAccessToolStripMenuItem_Click);
            // 
            // itemAccessToolStripMenuItem
            // 
            this.itemAccessToolStripMenuItem.Image = global::Restaurant.Properties.Resources.lock__4_;
            this.itemAccessToolStripMenuItem.Name = "itemAccessToolStripMenuItem";
            this.itemAccessToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.itemAccessToolStripMenuItem.Text = "ItemAccess";
            this.itemAccessToolStripMenuItem.Click += new System.EventHandler(this.itemAccessToolStripMenuItem_Click);
            // 
            // sMSSettingToolStripMenuItem
            // 
            this.sMSSettingToolStripMenuItem.Image = global::Restaurant.Properties.Resources.Settings_48x48;
            this.sMSSettingToolStripMenuItem.Name = "sMSSettingToolStripMenuItem";
            this.sMSSettingToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.sMSSettingToolStripMenuItem.Text = "SMSSetting";
            this.sMSSettingToolStripMenuItem.Click += new System.EventHandler(this.sMSSettingToolStripMenuItem_Click);
            // 
            // emailSettingToolStripMenuItem
            // 
            this.emailSettingToolStripMenuItem.Image = global::Restaurant.Properties.Resources.Settings_48x48;
            this.emailSettingToolStripMenuItem.Name = "emailSettingToolStripMenuItem";
            this.emailSettingToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.emailSettingToolStripMenuItem.Text = "EmailSetting";
            this.emailSettingToolStripMenuItem.Click += new System.EventHandler(this.emailSettingToolStripMenuItem_Click);
            // 
            // paymentModeToolStripMenuItem
            // 
            this.paymentModeToolStripMenuItem.Image = global::Restaurant.Properties.Resources._15__9_;
            this.paymentModeToolStripMenuItem.Name = "paymentModeToolStripMenuItem";
            this.paymentModeToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.paymentModeToolStripMenuItem.Text = "PaymentMode";
            this.paymentModeToolStripMenuItem.Click += new System.EventHandler(this.paymentModeToolStripMenuItem_Click);
            // 
            // addKotRemarkToolStripMenuItem
            // 
            this.addKotRemarkToolStripMenuItem.Image = global::Restaurant.Properties.Resources.address;
            this.addKotRemarkToolStripMenuItem.Name = "addKotRemarkToolStripMenuItem";
            this.addKotRemarkToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.addKotRemarkToolStripMenuItem.Text = "AddKotRemark";
            this.addKotRemarkToolStripMenuItem.Click += new System.EventHandler(this.addKotRemarkToolStripMenuItem_Click);
            // 
            // transactionToolStripMenuItem
            // 
            this.transactionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.purchasingToolStripMenuItem1,
            this.wastageToolStripMenuItem,
            this.expencessToolStripMenuItem,
            this.customerPaymentToolStripMenuItem,
            this.customerBalenceToolStripMenuItem,
            this.addCustomerToolStripMenuItem,
            this.salebyPaymentModeToolStripMenuItem,
            this.saleSummeryToolStripMenuItem,
            this.supplierAccountToolStripMenuItem});
            this.transactionToolStripMenuItem.Image = global::Restaurant.Properties.Resources._5__9_;
            this.transactionToolStripMenuItem.Name = "transactionToolStripMenuItem";
            this.transactionToolStripMenuItem.Size = new System.Drawing.Size(102, 20);
            this.transactionToolStripMenuItem.Text = "Transaction";
            // 
            // purchasingToolStripMenuItem1
            // 
            this.purchasingToolStripMenuItem1.Image = global::Restaurant.Properties.Resources._66__2_;
            this.purchasingToolStripMenuItem1.Name = "purchasingToolStripMenuItem1";
            this.purchasingToolStripMenuItem1.Size = new System.Drawing.Size(203, 22);
            this.purchasingToolStripMenuItem1.Text = "Purchasing";
            this.purchasingToolStripMenuItem1.Click += new System.EventHandler(this.purchasingToolStripMenuItem1_Click);
            // 
            // wastageToolStripMenuItem
            // 
            this.wastageToolStripMenuItem.Image = global::Restaurant.Properties.Resources._6__6_;
            this.wastageToolStripMenuItem.Name = "wastageToolStripMenuItem";
            this.wastageToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.wastageToolStripMenuItem.Text = "Wastage";
            this.wastageToolStripMenuItem.Click += new System.EventHandler(this.wastageToolStripMenuItem_Click);
            // 
            // expencessToolStripMenuItem
            // 
            this.expencessToolStripMenuItem.Image = global::Restaurant.Properties.Resources._15__9_;
            this.expencessToolStripMenuItem.Name = "expencessToolStripMenuItem";
            this.expencessToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.expencessToolStripMenuItem.Text = "Expencess";
            this.expencessToolStripMenuItem.Click += new System.EventHandler(this.expencessToolStripMenuItem_Click);
            // 
            // customerPaymentToolStripMenuItem
            // 
            this.customerPaymentToolStripMenuItem.Image = global::Restaurant.Properties.Resources._15__9_;
            this.customerPaymentToolStripMenuItem.Name = "customerPaymentToolStripMenuItem";
            this.customerPaymentToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.customerPaymentToolStripMenuItem.Text = "CustomerPayment";
            this.customerPaymentToolStripMenuItem.Click += new System.EventHandler(this.customerPaymentToolStripMenuItem_Click);
            // 
            // customerBalenceToolStripMenuItem
            // 
            this.customerBalenceToolStripMenuItem.Image = global::Restaurant.Properties.Resources._3__12_;
            this.customerBalenceToolStripMenuItem.Name = "customerBalenceToolStripMenuItem";
            this.customerBalenceToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.customerBalenceToolStripMenuItem.Text = "CustomerBalence";
            this.customerBalenceToolStripMenuItem.Click += new System.EventHandler(this.customerBalenceToolStripMenuItem_Click);
            // 
            // addCustomerToolStripMenuItem
            // 
            this.addCustomerToolStripMenuItem.Image = global::Restaurant.Properties.Resources._0__4_;
            this.addCustomerToolStripMenuItem.Name = "addCustomerToolStripMenuItem";
            this.addCustomerToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.addCustomerToolStripMenuItem.Text = "Add Customer";
            this.addCustomerToolStripMenuItem.Click += new System.EventHandler(this.addCustomerToolStripMenuItem_Click);
            // 
            // salebyPaymentModeToolStripMenuItem
            // 
            this.salebyPaymentModeToolStripMenuItem.Image = global::Restaurant.Properties.Resources._15__9_;
            this.salebyPaymentModeToolStripMenuItem.Name = "salebyPaymentModeToolStripMenuItem";
            this.salebyPaymentModeToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.salebyPaymentModeToolStripMenuItem.Text = "Sale by Payment Mode";
            this.salebyPaymentModeToolStripMenuItem.Click += new System.EventHandler(this.salebyPaymentModeToolStripMenuItem_Click);
            // 
            // saleSummeryToolStripMenuItem
            // 
            this.saleSummeryToolStripMenuItem.Image = global::Restaurant.Properties.Resources._15__9_;
            this.saleSummeryToolStripMenuItem.Name = "saleSummeryToolStripMenuItem";
            this.saleSummeryToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.saleSummeryToolStripMenuItem.Text = "Sale Summery";
            this.saleSummeryToolStripMenuItem.Click += new System.EventHandler(this.saleSummeryToolStripMenuItem_Click);
            // 
            // supplierAccountToolStripMenuItem
            // 
            this.supplierAccountToolStripMenuItem.Image = global::Restaurant.Properties.Resources._15__9_;
            this.supplierAccountToolStripMenuItem.Name = "supplierAccountToolStripMenuItem";
            this.supplierAccountToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.supplierAccountToolStripMenuItem.Text = "Supplier Account";
            this.supplierAccountToolStripMenuItem.Click += new System.EventHandler(this.supplierAccountToolStripMenuItem_Click);
            // 
            // inventoryToolStripMenuItem
            // 
            this.inventoryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createUnitToolStripMenuItem,
            this.createItemToolStripMenuItem,
            this.recipieToolStripMenuItem,
            this.viewStockToolStripMenuItem,
            this.viewStockToolStripMenuItem1});
            this.inventoryToolStripMenuItem.Image = global::Restaurant.Properties.Resources.Balance;
            this.inventoryToolStripMenuItem.Name = "inventoryToolStripMenuItem";
            this.inventoryToolStripMenuItem.Size = new System.Drawing.Size(88, 20);
            this.inventoryToolStripMenuItem.Text = "Inventory";
            // 
            // createUnitToolStripMenuItem
            // 
            this.createUnitToolStripMenuItem.Image = global::Restaurant.Properties.Resources._0__4_;
            this.createUnitToolStripMenuItem.Name = "createUnitToolStripMenuItem";
            this.createUnitToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.createUnitToolStripMenuItem.Text = "CreateUnit";
            this.createUnitToolStripMenuItem.Click += new System.EventHandler(this.createUnitToolStripMenuItem_Click);
            // 
            // createItemToolStripMenuItem
            // 
            this.createItemToolStripMenuItem.Image = global::Restaurant.Properties.Resources._0__4_;
            this.createItemToolStripMenuItem.Name = "createItemToolStripMenuItem";
            this.createItemToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.createItemToolStripMenuItem.Text = "ItemName/Ingradient";
            this.createItemToolStripMenuItem.Click += new System.EventHandler(this.createItemToolStripMenuItem_Click);
            // 
            // recipieToolStripMenuItem
            // 
            this.recipieToolStripMenuItem.Image = global::Restaurant.Properties.Resources._0__4_;
            this.recipieToolStripMenuItem.Name = "recipieToolStripMenuItem";
            this.recipieToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.recipieToolStripMenuItem.Text = "Recipie";
            this.recipieToolStripMenuItem.Click += new System.EventHandler(this.recipieToolStripMenuItem_Click);
            // 
            // viewStockToolStripMenuItem
            // 
            this.viewStockToolStripMenuItem.Image = global::Restaurant.Properties.Resources._0__4_;
            this.viewStockToolStripMenuItem.Name = "viewStockToolStripMenuItem";
            this.viewStockToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.viewStockToolStripMenuItem.Text = "Stock Entry";
            this.viewStockToolStripMenuItem.Click += new System.EventHandler(this.viewStockToolStripMenuItem_Click);
            // 
            // viewStockToolStripMenuItem1
            // 
            this.viewStockToolStripMenuItem1.Image = global::Restaurant.Properties.Resources._76__2_;
            this.viewStockToolStripMenuItem1.Name = "viewStockToolStripMenuItem1";
            this.viewStockToolStripMenuItem1.Size = new System.Drawing.Size(193, 22);
            this.viewStockToolStripMenuItem1.Text = "View Stock";
            this.viewStockToolStripMenuItem1.Click += new System.EventHandler(this.viewStockToolStripMenuItem1_Click);
            // 
            // windowsToolStripMenuItem
            // 
            this.windowsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.calculatorToolStripMenuItem1,
            this.notePadToolStripMenuItem,
            this.sendSmsEmailToolStripMenuItem,
            this.feedBackToolStripMenuItem});
            this.windowsToolStripMenuItem.Image = global::Restaurant.Properties.Resources.digg;
            this.windowsToolStripMenuItem.Name = "windowsToolStripMenuItem";
            this.windowsToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.windowsToolStripMenuItem.Text = "Utility";
            // 
            // calculatorToolStripMenuItem1
            // 
            this.calculatorToolStripMenuItem1.Image = global::Restaurant.Properties.Resources.calculator_add;
            this.calculatorToolStripMenuItem1.Name = "calculatorToolStripMenuItem1";
            this.calculatorToolStripMenuItem1.Size = new System.Drawing.Size(156, 22);
            this.calculatorToolStripMenuItem1.Text = "Calculator";
            this.calculatorToolStripMenuItem1.Click += new System.EventHandler(this.calculatorToolStripMenuItem1_Click);
            // 
            // notePadToolStripMenuItem
            // 
            this.notePadToolStripMenuItem.Image = global::Restaurant.Properties.Resources.note_add;
            this.notePadToolStripMenuItem.Name = "notePadToolStripMenuItem";
            this.notePadToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.notePadToolStripMenuItem.Text = "NotePad";
            this.notePadToolStripMenuItem.Click += new System.EventHandler(this.notePadToolStripMenuItem_Click);
            // 
            // sendSmsEmailToolStripMenuItem
            // 
            this.sendSmsEmailToolStripMenuItem.Image = global::Restaurant.Properties.Resources.email_add;
            this.sendSmsEmailToolStripMenuItem.Name = "sendSmsEmailToolStripMenuItem";
            this.sendSmsEmailToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.sendSmsEmailToolStripMenuItem.Text = "SendSmsEmail";
            this.sendSmsEmailToolStripMenuItem.Click += new System.EventHandler(this.sendSmsEmailToolStripMenuItem_Click);
            // 
            // feedBackToolStripMenuItem
            // 
            this.feedBackToolStripMenuItem.Image = global::Restaurant.Properties.Resources._3__11_;
            this.feedBackToolStripMenuItem.Name = "feedBackToolStripMenuItem";
            this.feedBackToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.feedBackToolStripMenuItem.Text = "FeedBack";
            this.feedBackToolStripMenuItem.Click += new System.EventHandler(this.feedBackToolStripMenuItem_Click);
            // 
            // reportsToolStripMenuItem1
            // 
            this.reportsToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.restaurantToolStripMenuItem,
            this.kotAuditBotAuditToolStripMenuItem,
            this.todayToolStripMenuItem,
            this.yesturdayToolStripMenuItem,
            this.saleByTableToolStripMenuItem,
            this.itemPriceListToolStripMenuItem,
            this.customerListToolStripMenuItem,
            this.purchasingReportsToolStripMenuItem,
            this.stockEntryReportToolStripMenuItem,
            this.wastageReportToolStripMenuItem,
            this.stockReportToolStripMenuItem,
            this.recipieReportToolStripMenuItem,
            this.counterReportToolStripMenuItem});
            this.reportsToolStripMenuItem1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.reportsToolStripMenuItem1.Image = global::Restaurant.Properties.Resources.book_go;
            this.reportsToolStripMenuItem1.Name = "reportsToolStripMenuItem1";
            this.reportsToolStripMenuItem1.Size = new System.Drawing.Size(91, 20);
            this.reportsToolStripMenuItem1.Text = "Reports";
            // 
            // restaurantToolStripMenuItem
            // 
            this.restaurantToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.periodWiseToolStripMenuItem,
            this.billWiseToolStripMenuItem,
            this.cashierWiseToolStripMenuItem,
            this.saleByItemToolStripMenuItem,
            this.billSummaryToolStripMenuItem,
            this.billDetailsToolStripMenuItem1,
            this.customerHistoryToolStripMenuItem1,
            this.discountReportToolStripMenuItem1,
            this.parsalReportsToolStripMenuItem1,
            this.cancelItemListToolStripMenuItem,
            this.cancelBillReportToolStripMenuItem1,
            this.barItemToolStripMenuItem,
            this.periodWiseBarToolStripMenuItem});
            this.restaurantToolStripMenuItem.Image = global::Restaurant.Properties.Resources.address_books;
            this.restaurantToolStripMenuItem.Name = "restaurantToolStripMenuItem";
            this.restaurantToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.restaurantToolStripMenuItem.Text = "Restaurant/Bar";
            // 
            // periodWiseToolStripMenuItem
            // 
            this.periodWiseToolStripMenuItem.Image = global::Restaurant.Properties.Resources.address;
            this.periodWiseToolStripMenuItem.Name = "periodWiseToolStripMenuItem";
            this.periodWiseToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.periodWiseToolStripMenuItem.Text = "PeriodWise";
            this.periodWiseToolStripMenuItem.Click += new System.EventHandler(this.periodWiseToolStripMenuItem_Click);
            // 
            // billWiseToolStripMenuItem
            // 
            this.billWiseToolStripMenuItem.Image = global::Restaurant.Properties.Resources.address_book_32x32;
            this.billWiseToolStripMenuItem.Name = "billWiseToolStripMenuItem";
            this.billWiseToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.billWiseToolStripMenuItem.Text = "BillWise";
            this.billWiseToolStripMenuItem.Click += new System.EventHandler(this.billWiseToolStripMenuItem_Click);
            // 
            // cashierWiseToolStripMenuItem
            // 
            this.cashierWiseToolStripMenuItem.Image = global::Restaurant.Properties.Resources.book;
            this.cashierWiseToolStripMenuItem.Name = "cashierWiseToolStripMenuItem";
            this.cashierWiseToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.cashierWiseToolStripMenuItem.Text = "CashierWise";
            this.cashierWiseToolStripMenuItem.Click += new System.EventHandler(this.cashierWiseToolStripMenuItem_Click);
            // 
            // saleByItemToolStripMenuItem
            // 
            this.saleByItemToolStripMenuItem.Image = global::Restaurant.Properties.Resources.book_addresses;
            this.saleByItemToolStripMenuItem.Name = "saleByItemToolStripMenuItem";
            this.saleByItemToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.saleByItemToolStripMenuItem.Text = "ItemWise";
            this.saleByItemToolStripMenuItem.Click += new System.EventHandler(this.saleByItemToolStripMenuItem_Click);
            // 
            // billSummaryToolStripMenuItem
            // 
            this.billSummaryToolStripMenuItem.Image = global::Restaurant.Properties.Resources.book_edit;
            this.billSummaryToolStripMenuItem.Name = "billSummaryToolStripMenuItem";
            this.billSummaryToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.billSummaryToolStripMenuItem.Text = "BillSummary";
            this.billSummaryToolStripMenuItem.Click += new System.EventHandler(this.billSummaryToolStripMenuItem_Click);
            // 
            // billDetailsToolStripMenuItem1
            // 
            this.billDetailsToolStripMenuItem1.Image = global::Restaurant.Properties.Resources.book_go;
            this.billDetailsToolStripMenuItem1.Name = "billDetailsToolStripMenuItem1";
            this.billDetailsToolStripMenuItem1.Size = new System.Drawing.Size(243, 22);
            this.billDetailsToolStripMenuItem1.Text = "BillDetails";
            this.billDetailsToolStripMenuItem1.Click += new System.EventHandler(this.billDetailsToolStripMenuItem1_Click);
            // 
            // customerHistoryToolStripMenuItem1
            // 
            this.customerHistoryToolStripMenuItem1.Image = global::Restaurant.Properties.Resources.book_link;
            this.customerHistoryToolStripMenuItem1.Name = "customerHistoryToolStripMenuItem1";
            this.customerHistoryToolStripMenuItem1.Size = new System.Drawing.Size(243, 22);
            this.customerHistoryToolStripMenuItem1.Text = "Customer Parcel History";
            this.customerHistoryToolStripMenuItem1.Click += new System.EventHandler(this.customerHistoryToolStripMenuItem1_Click);
            // 
            // discountReportToolStripMenuItem1
            // 
            this.discountReportToolStripMenuItem1.Image = global::Restaurant.Properties.Resources.book_addresses;
            this.discountReportToolStripMenuItem1.Name = "discountReportToolStripMenuItem1";
            this.discountReportToolStripMenuItem1.Size = new System.Drawing.Size(243, 22);
            this.discountReportToolStripMenuItem1.Text = "DiscountReport";
            this.discountReportToolStripMenuItem1.Click += new System.EventHandler(this.discountReportToolStripMenuItem1_Click);
            // 
            // parsalReportsToolStripMenuItem1
            // 
            this.parsalReportsToolStripMenuItem1.Image = global::Restaurant.Properties.Resources.address;
            this.parsalReportsToolStripMenuItem1.Name = "parsalReportsToolStripMenuItem1";
            this.parsalReportsToolStripMenuItem1.Size = new System.Drawing.Size(243, 22);
            this.parsalReportsToolStripMenuItem1.Text = "ParsalReports";
            this.parsalReportsToolStripMenuItem1.Click += new System.EventHandler(this.parsalReportsToolStripMenuItem1_Click);
            // 
            // cancelItemListToolStripMenuItem
            // 
            this.cancelItemListToolStripMenuItem.Image = global::Restaurant.Properties.Resources.book_edit;
            this.cancelItemListToolStripMenuItem.Name = "cancelItemListToolStripMenuItem";
            this.cancelItemListToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.cancelItemListToolStripMenuItem.Text = "CancelItemList";
            this.cancelItemListToolStripMenuItem.Click += new System.EventHandler(this.cancelItemListToolStripMenuItem_Click);
            // 
            // cancelBillReportToolStripMenuItem1
            // 
            this.cancelBillReportToolStripMenuItem1.Image = global::Restaurant.Properties.Resources.book_go;
            this.cancelBillReportToolStripMenuItem1.Name = "cancelBillReportToolStripMenuItem1";
            this.cancelBillReportToolStripMenuItem1.Size = new System.Drawing.Size(243, 22);
            this.cancelBillReportToolStripMenuItem1.Text = "CancelBillReport";
            this.cancelBillReportToolStripMenuItem1.Click += new System.EventHandler(this.cancelBillReportToolStripMenuItem1_Click);
            // 
            // barItemToolStripMenuItem
            // 
            this.barItemToolStripMenuItem.Image = global::Restaurant.Properties.Resources.address_books;
            this.barItemToolStripMenuItem.Name = "barItemToolStripMenuItem";
            this.barItemToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.barItemToolStripMenuItem.Text = "BarItemWise";
            this.barItemToolStripMenuItem.Click += new System.EventHandler(this.barItemToolStripMenuItem_Click);
            // 
            // periodWiseBarToolStripMenuItem
            // 
            this.periodWiseBarToolStripMenuItem.Image = global::Restaurant.Properties.Resources.address;
            this.periodWiseBarToolStripMenuItem.Name = "periodWiseBarToolStripMenuItem";
            this.periodWiseBarToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.periodWiseBarToolStripMenuItem.Text = "PeriodWiseBar";
            this.periodWiseBarToolStripMenuItem.Click += new System.EventHandler(this.periodWiseBarToolStripMenuItem_Click);
            // 
            // kotAuditBotAuditToolStripMenuItem
            // 
            this.kotAuditBotAuditToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kotItemToolStripMenuItem});
            this.kotAuditBotAuditToolStripMenuItem.Image = global::Restaurant.Properties.Resources.address;
            this.kotAuditBotAuditToolStripMenuItem.Name = "kotAuditBotAuditToolStripMenuItem";
            this.kotAuditBotAuditToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.kotAuditBotAuditToolStripMenuItem.Text = "KotReports";
            // 
            // kotItemToolStripMenuItem
            // 
            this.kotItemToolStripMenuItem.Image = global::Restaurant.Properties.Resources.book;
            this.kotItemToolStripMenuItem.Name = "kotItemToolStripMenuItem";
            this.kotItemToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.kotItemToolStripMenuItem.Text = "KotItem";
            this.kotItemToolStripMenuItem.Click += new System.EventHandler(this.kotItemToolStripMenuItem_Click);
            // 
            // todayToolStripMenuItem
            // 
            this.todayToolStripMenuItem.Image = global::Restaurant.Properties.Resources.address_book_32x32;
            this.todayToolStripMenuItem.Name = "todayToolStripMenuItem";
            this.todayToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.todayToolStripMenuItem.Text = "Today";
            this.todayToolStripMenuItem.Click += new System.EventHandler(this.todayToolStripMenuItem_Click);
            // 
            // yesturdayToolStripMenuItem
            // 
            this.yesturdayToolStripMenuItem.Image = global::Restaurant.Properties.Resources.book;
            this.yesturdayToolStripMenuItem.Name = "yesturdayToolStripMenuItem";
            this.yesturdayToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.yesturdayToolStripMenuItem.Text = "Yesturday";
            this.yesturdayToolStripMenuItem.Click += new System.EventHandler(this.yesturdayToolStripMenuItem_Click);
            // 
            // saleByTableToolStripMenuItem
            // 
            this.saleByTableToolStripMenuItem.Image = global::Restaurant.Properties.Resources.book_addresses;
            this.saleByTableToolStripMenuItem.Name = "saleByTableToolStripMenuItem";
            this.saleByTableToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.saleByTableToolStripMenuItem.Text = "SalebyTable";
            this.saleByTableToolStripMenuItem.Click += new System.EventHandler(this.saleByTableToolStripMenuItem_Click);
            // 
            // itemPriceListToolStripMenuItem
            // 
            this.itemPriceListToolStripMenuItem.Image = global::Restaurant.Properties.Resources.book_go;
            this.itemPriceListToolStripMenuItem.Name = "itemPriceListToolStripMenuItem";
            this.itemPriceListToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.itemPriceListToolStripMenuItem.Text = "ItemPriceList";
            this.itemPriceListToolStripMenuItem.Click += new System.EventHandler(this.itemPriceListToolStripMenuItem_Click);
            // 
            // customerListToolStripMenuItem
            // 
            this.customerListToolStripMenuItem.Image = global::Restaurant.Properties.Resources.book_link;
            this.customerListToolStripMenuItem.Name = "customerListToolStripMenuItem";
            this.customerListToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.customerListToolStripMenuItem.Text = "CustomerList";
            this.customerListToolStripMenuItem.Click += new System.EventHandler(this.customerListToolStripMenuItem_Click);
            // 
            // purchasingReportsToolStripMenuItem
            // 
            this.purchasingReportsToolStripMenuItem.Image = global::Restaurant.Properties.Resources.address_books;
            this.purchasingReportsToolStripMenuItem.Name = "purchasingReportsToolStripMenuItem";
            this.purchasingReportsToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.purchasingReportsToolStripMenuItem.Text = "ExpReports";
            this.purchasingReportsToolStripMenuItem.Click += new System.EventHandler(this.purchasingReportsToolStripMenuItem_Click);
            // 
            // stockEntryReportToolStripMenuItem
            // 
            this.stockEntryReportToolStripMenuItem.Image = global::Restaurant.Properties.Resources.address_book_32x32;
            this.stockEntryReportToolStripMenuItem.Name = "stockEntryReportToolStripMenuItem";
            this.stockEntryReportToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.stockEntryReportToolStripMenuItem.Text = "Purchasing Report";
            this.stockEntryReportToolStripMenuItem.Click += new System.EventHandler(this.stockEntryReportToolStripMenuItem_Click);
            // 
            // wastageReportToolStripMenuItem
            // 
            this.wastageReportToolStripMenuItem.Image = global::Restaurant.Properties.Resources.book;
            this.wastageReportToolStripMenuItem.Name = "wastageReportToolStripMenuItem";
            this.wastageReportToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.wastageReportToolStripMenuItem.Text = "WastageReport";
            this.wastageReportToolStripMenuItem.Click += new System.EventHandler(this.wastageReportToolStripMenuItem_Click);
            // 
            // stockReportToolStripMenuItem
            // 
            this.stockReportToolStripMenuItem.Image = global::Restaurant.Properties.Resources.book_addresses;
            this.stockReportToolStripMenuItem.Name = "stockReportToolStripMenuItem";
            this.stockReportToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.stockReportToolStripMenuItem.Text = "StockReport";
            this.stockReportToolStripMenuItem.Click += new System.EventHandler(this.stockReportToolStripMenuItem_Click);
            // 
            // recipieReportToolStripMenuItem
            // 
            this.recipieReportToolStripMenuItem.Image = global::Restaurant.Properties.Resources.book_edit;
            this.recipieReportToolStripMenuItem.Name = "recipieReportToolStripMenuItem";
            this.recipieReportToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.recipieReportToolStripMenuItem.Text = "RecipieReport";
            this.recipieReportToolStripMenuItem.Click += new System.EventHandler(this.recipieReportToolStripMenuItem_Click);
            // 
            // counterReportToolStripMenuItem
            // 
            this.counterReportToolStripMenuItem.Name = "counterReportToolStripMenuItem";
            this.counterReportToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.counterReportToolStripMenuItem.Text = "Counter Report";
            this.counterReportToolStripMenuItem.Click += new System.EventHandler(this.counterReportToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Image = global::Restaurant.Properties.Resources.hh;
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // securityToolStripMenuItem
            // 
            this.securityToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.purchaseToolStripMenuItem,
            this.dayCloseToolStripMenuItem,
            this.dayOpenToolStripMenuItem,
            this.securityToolStripMenuItem2});
            this.securityToolStripMenuItem.Image = global::Restaurant.Properties.Resources.Settings_48x481;
            this.securityToolStripMenuItem.Name = "securityToolStripMenuItem";
            this.securityToolStripMenuItem.Size = new System.Drawing.Size(81, 20);
            this.securityToolStripMenuItem.Text = "Security";
            // 
            // purchaseToolStripMenuItem
            // 
            this.purchaseToolStripMenuItem.Image = global::Restaurant.Properties.Resources._66__2_;
            this.purchaseToolStripMenuItem.Name = "purchaseToolStripMenuItem";
            this.purchaseToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.purchaseToolStripMenuItem.Text = "Purchase";
            this.purchaseToolStripMenuItem.Click += new System.EventHandler(this.purchaseToolStripMenuItem_Click);
            // 
            // dayCloseToolStripMenuItem
            // 
            this.dayCloseToolStripMenuItem.Image = global::Restaurant.Properties.Resources.Button_Delete;
            this.dayCloseToolStripMenuItem.Name = "dayCloseToolStripMenuItem";
            this.dayCloseToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.dayCloseToolStripMenuItem.Text = "DayClose";
            this.dayCloseToolStripMenuItem.Click += new System.EventHandler(this.dayCloseToolStripMenuItem_Click);
            // 
            // dayOpenToolStripMenuItem
            // 
            this.dayOpenToolStripMenuItem.Image = global::Restaurant.Properties.Resources.Button_Delete;
            this.dayOpenToolStripMenuItem.Name = "dayOpenToolStripMenuItem";
            this.dayOpenToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.dayOpenToolStripMenuItem.Text = "DayOpen";
            this.dayOpenToolStripMenuItem.Click += new System.EventHandler(this.dayOpenToolStripMenuItem_Click);
            // 
            // securityToolStripMenuItem2
            // 
            this.securityToolStripMenuItem2.Name = "securityToolStripMenuItem2";
            this.securityToolStripMenuItem2.Size = new System.Drawing.Size(127, 22);
            this.securityToolStripMenuItem2.Text = "Security";
            this.securityToolStripMenuItem2.Click += new System.EventHandler(this.securityToolStripMenuItem2_Click);
            // 
            // logOutToolStripMenuItem
            // 
            this.logOutToolStripMenuItem.Image = global::Restaurant.Properties.Resources.lock__4_;
            this.logOutToolStripMenuItem.Name = "logOutToolStripMenuItem";
            this.logOutToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.logOutToolStripMenuItem.Text = "LogOut";
            this.logOutToolStripMenuItem.Click += new System.EventHandler(this.logOutToolStripMenuItem_Click);
            // 
            // workerToolStripMenuItem
            // 
            this.workerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addWorkerToolStripMenuItem,
            this.workerAttendancToolStripMenuItem,
            this.salleryCalculateToolStripMenuItem,
            this.paymentToolStripMenuItem,
            this.salaryHiToolStripMenuItem,
            this.workderAttendaceToolStripMenuItem});
            this.workerToolStripMenuItem.Image = global::Restaurant.Properties.Resources._18__6_;
            this.workerToolStripMenuItem.Name = "workerToolStripMenuItem";
            this.workerToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.workerToolStripMenuItem.Text = "Worker";
            // 
            // addWorkerToolStripMenuItem
            // 
            this.addWorkerToolStripMenuItem.Image = global::Restaurant.Properties.Resources._0__4_;
            this.addWorkerToolStripMenuItem.Name = "addWorkerToolStripMenuItem";
            this.addWorkerToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.addWorkerToolStripMenuItem.Text = "AddWorker";
            this.addWorkerToolStripMenuItem.Click += new System.EventHandler(this.addWorkerToolStripMenuItem_Click);
            // 
            // workerAttendancToolStripMenuItem
            // 
            this.workerAttendancToolStripMenuItem.Image = global::Restaurant.Properties.Resources._0__5_;
            this.workerAttendancToolStripMenuItem.Name = "workerAttendancToolStripMenuItem";
            this.workerAttendancToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.workerAttendancToolStripMenuItem.Text = "Worker Attendanc";
            this.workerAttendancToolStripMenuItem.Click += new System.EventHandler(this.workerAttendancToolStripMenuItem_Click);
            // 
            // salleryCalculateToolStripMenuItem
            // 
            this.salleryCalculateToolStripMenuItem.Image = global::Restaurant.Properties.Resources._15__9_;
            this.salleryCalculateToolStripMenuItem.Name = "salleryCalculateToolStripMenuItem";
            this.salleryCalculateToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.salleryCalculateToolStripMenuItem.Text = "Sallery Calculate";
            this.salleryCalculateToolStripMenuItem.Click += new System.EventHandler(this.salleryCalculateToolStripMenuItem_Click);
            // 
            // paymentToolStripMenuItem
            // 
            this.paymentToolStripMenuItem.Image = global::Restaurant.Properties.Resources._15__9_;
            this.paymentToolStripMenuItem.Name = "paymentToolStripMenuItem";
            this.paymentToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.paymentToolStripMenuItem.Text = "Payment";
            this.paymentToolStripMenuItem.Click += new System.EventHandler(this.paymentToolStripMenuItem_Click);
            // 
            // salaryHiToolStripMenuItem
            // 
            this.salaryHiToolStripMenuItem.Image = global::Restaurant.Properties.Resources._15__9_;
            this.salaryHiToolStripMenuItem.Name = "salaryHiToolStripMenuItem";
            this.salaryHiToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.salaryHiToolStripMenuItem.Text = "Salary History";
            this.salaryHiToolStripMenuItem.Click += new System.EventHandler(this.salaryHiToolStripMenuItem_Click);
            // 
            // workderAttendaceToolStripMenuItem
            // 
            this.workderAttendaceToolStripMenuItem.Image = global::Restaurant.Properties.Resources.atten;
            this.workderAttendaceToolStripMenuItem.Name = "workderAttendaceToolStripMenuItem";
            this.workderAttendaceToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.workderAttendaceToolStripMenuItem.Text = "WorkderAttendace";
            this.workderAttendaceToolStripMenuItem.Click += new System.EventHandler(this.workderAttendaceToolStripMenuItem_Click);
            // 
            // rateManageToolStripMenuItem
            // 
            this.rateManageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rateCategaryToolStripMenuItem,
            this.departmentToolStripMenuItem,
            this.rateAssignToolStripMenuItem,
            this.demoToolStripMenuItem});
            this.rateManageToolStripMenuItem.Image = global::Restaurant.Properties.Resources._15__9_;
            this.rateManageToolStripMenuItem.Name = "rateManageToolStripMenuItem";
            this.rateManageToolStripMenuItem.Size = new System.Drawing.Size(111, 20);
            this.rateManageToolStripMenuItem.Text = "Rate Manage";
            // 
            // rateCategaryToolStripMenuItem
            // 
            this.rateCategaryToolStripMenuItem.Image = global::Restaurant.Properties.Resources._0__4_;
            this.rateCategaryToolStripMenuItem.Name = "rateCategaryToolStripMenuItem";
            this.rateCategaryToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.rateCategaryToolStripMenuItem.Text = "Rate Categary";
            this.rateCategaryToolStripMenuItem.Click += new System.EventHandler(this.rateCategaryToolStripMenuItem_Click);
            // 
            // departmentToolStripMenuItem
            // 
            this.departmentToolStripMenuItem.Image = global::Restaurant.Properties.Resources._0__4_;
            this.departmentToolStripMenuItem.Name = "departmentToolStripMenuItem";
            this.departmentToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.departmentToolStripMenuItem.Text = "Department";
            this.departmentToolStripMenuItem.Click += new System.EventHandler(this.departmentToolStripMenuItem_Click);
            // 
            // rateAssignToolStripMenuItem
            // 
            this.rateAssignToolStripMenuItem.Image = global::Restaurant.Properties.Resources._0__4_;
            this.rateAssignToolStripMenuItem.Name = "rateAssignToolStripMenuItem";
            this.rateAssignToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.rateAssignToolStripMenuItem.Text = "RateAssign";
            this.rateAssignToolStripMenuItem.Click += new System.EventHandler(this.rateAssignToolStripMenuItem_Click);
            // 
            // demoToolStripMenuItem
            // 
            this.demoToolStripMenuItem.Image = global::Restaurant.Properties.Resources._0__4_;
            this.demoToolStripMenuItem.Name = "demoToolStripMenuItem";
            this.demoToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.demoToolStripMenuItem.Text = "RateSlap";
            this.demoToolStripMenuItem.Click += new System.EventHandler(this.demoToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Image = global::Restaurant.Properties.Resources.Button_Delete;
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // reprintKotToolStripMenuItem
            // 
            this.reprintKotToolStripMenuItem.Name = "reprintKotToolStripMenuItem";
            this.reprintKotToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.reprintKotToolStripMenuItem.Text = "ReprintKot";
            this.reprintKotToolStripMenuItem.Click += new System.EventHandler(this.reprintKotToolStripMenuItem_Click);
            // 
            // securityToolStripMenuItem1
            // 
            this.securityToolStripMenuItem1.Name = "securityToolStripMenuItem1";
            this.securityToolStripMenuItem1.Size = new System.Drawing.Size(127, 22);
            this.securityToolStripMenuItem1.Text = "Security";
            this.securityToolStripMenuItem1.Click += new System.EventHandler(this.securityToolStripMenuItem1_Click_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Green;
            this.label1.Location = new System.Drawing.Point(340, 628);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(333, 38);
            this.label1.TabIndex = 7;
            this.label1.Text = "K-Palms IT Services";
            // 
            // btnsupplier
            // 
            this.btnsupplier.BackColor = System.Drawing.Color.Crimson;
            this.btnsupplier.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnsupplier.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsupplier.Image = global::Restaurant.Properties.Resources.people_delivery;
            this.btnsupplier.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsupplier.Location = new System.Drawing.Point(823, 432);
            this.btnsupplier.Name = "btnsupplier";
            this.btnsupplier.Size = new System.Drawing.Size(150, 136);
            this.btnsupplier.TabIndex = 88;
            this.btnsupplier.Text = "Suppliers";
            this.btnsupplier.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnsupplier.UseVisualStyleBackColor = false;
            this.btnsupplier.Click += new System.EventHandler(this.btnsupplier_Click);
            // 
            // btnSaleByPaymentmode
            // 
            this.btnSaleByPaymentmode.BackColor = System.Drawing.Color.SlateBlue;
            this.btnSaleByPaymentmode.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSaleByPaymentmode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaleByPaymentmode.Image = global::Restaurant.Properties.Resources._5__9_;
            this.btnSaleByPaymentmode.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSaleByPaymentmode.Location = new System.Drawing.Point(617, 432);
            this.btnSaleByPaymentmode.Name = "btnSaleByPaymentmode";
            this.btnSaleByPaymentmode.Size = new System.Drawing.Size(150, 136);
            this.btnSaleByPaymentmode.TabIndex = 87;
            this.btnSaleByPaymentmode.Text = "Sale By PaymentMode";
            this.btnSaleByPaymentmode.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnSaleByPaymentmode.UseVisualStyleBackColor = false;
            this.btnSaleByPaymentmode.Click += new System.EventHandler(this.btnSaleByPaymentmode_Click);
            // 
            // btnsalesummery
            // 
            this.btnsalesummery.BackColor = System.Drawing.Color.MediumAquamarine;
            this.btnsalesummery.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnsalesummery.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsalesummery.Image = global::Restaurant.Properties.Resources._3__12_;
            this.btnsalesummery.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsalesummery.Location = new System.Drawing.Point(424, 432);
            this.btnsalesummery.Name = "btnsalesummery";
            this.btnsalesummery.Size = new System.Drawing.Size(150, 136);
            this.btnsalesummery.TabIndex = 86;
            this.btnsalesummery.Text = "Sale Summery";
            this.btnsalesummery.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnsalesummery.UseVisualStyleBackColor = false;
            this.btnsalesummery.Click += new System.EventHandler(this.btnsalesummery_Click);
            // 
            // btnAddProduct
            // 
            this.btnAddProduct.BackColor = System.Drawing.Color.Gold;
            this.btnAddProduct.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAddProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddProduct.Image = global::Restaurant.Properties.Resources.ingra;
            this.btnAddProduct.Location = new System.Drawing.Point(28, 432);
            this.btnAddProduct.Name = "btnAddProduct";
            this.btnAddProduct.Size = new System.Drawing.Size(150, 136);
            this.btnAddProduct.TabIndex = 5;
            this.btnAddProduct.Text = "Ingredients\r\n(F3)\r\n";
            this.btnAddProduct.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAddProduct.UseVisualStyleBackColor = false;
            this.btnAddProduct.Click += new System.EventHandler(this.btnAddProduct_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Image = global::Restaurant.Properties.Resources.add_to_basket_512;
            this.button3.Location = new System.Drawing.Point(221, 71);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(150, 136);
            this.button3.TabIndex = 7;
            this.button3.Text = "ItemName\r\n(ctrl+i)";
            this.button3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btncustomer
            // 
            this.btncustomer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btncustomer.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btncustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncustomer.Image = global::Restaurant.Properties.Resources._0__4_;
            this.btncustomer.Location = new System.Drawing.Point(823, 71);
            this.btncustomer.Name = "btncustomer";
            this.btncustomer.Size = new System.Drawing.Size(150, 136);
            this.btncustomer.TabIndex = 4;
            this.btncustomer.Text = "Add Table";
            this.btncustomer.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btncustomer.UseVisualStyleBackColor = false;
            this.btncustomer.Click += new System.EventHandler(this.btncustomer_Click);
            // 
            // btnbillreprint
            // 
            this.btnbillreprint.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnbillreprint.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnbillreprint.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnbillreprint.Image = global::Restaurant.Properties.Resources.inkjet_printer;
            this.btnbillreprint.Location = new System.Drawing.Point(617, 71);
            this.btnbillreprint.Name = "btnbillreprint";
            this.btnbillreprint.Size = new System.Drawing.Size(150, 136);
            this.btnbillreprint.TabIndex = 85;
            this.btnbillreprint.Text = "Bill Reprint";
            this.btnbillreprint.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnbillreprint.UseVisualStyleBackColor = false;
            this.btnbillreprint.Click += new System.EventHandler(this.btnbillreprint_Click);
            // 
            // btnstock
            // 
            this.btnstock.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnstock.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnstock.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnstock.Image = global::Restaurant.Properties.Resources.chart;
            this.btnstock.Location = new System.Drawing.Point(823, 247);
            this.btnstock.Name = "btnstock";
            this.btnstock.Size = new System.Drawing.Size(150, 136);
            this.btnstock.TabIndex = 3;
            this.btnstock.Text = "Stock\r\n(F1)";
            this.btnstock.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnstock.UseVisualStyleBackColor = false;
            this.btnstock.Click += new System.EventHandler(this.btnstock_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Image = global::Restaurant.Properties.Resources.Switchr_Large_Icon;
            this.button2.Location = new System.Drawing.Point(28, 71);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(150, 136);
            this.button2.TabIndex = 6;
            this.button2.Text = "Category\r\n(ctrl+c)";
            this.button2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnpurchase
            // 
            this.btnpurchase.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnpurchase.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnpurchase.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnpurchase.Image = global::Restaurant.Properties.Resources.lorry_add;
            this.btnpurchase.Location = new System.Drawing.Point(617, 247);
            this.btnpurchase.Name = "btnpurchase";
            this.btnpurchase.Size = new System.Drawing.Size(150, 136);
            this.btnpurchase.TabIndex = 1;
            this.btnpurchase.Text = "Purchase\r\n(ctrl+p)";
            this.btnpurchase.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnpurchase.UseVisualStyleBackColor = false;
            this.btnpurchase.Click += new System.EventHandler(this.btnpurchase_Click);
            // 
            // btncustome
            // 
            this.btncustome.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btncustome.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btncustome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncustome.Image = global::Restaurant.Properties.Resources.customer__2_;
            this.btncustome.Location = new System.Drawing.Point(424, 71);
            this.btncustome.Name = "btncustome";
            this.btncustome.Size = new System.Drawing.Size(150, 136);
            this.btncustome.TabIndex = 84;
            this.btncustome.Text = "Customer";
            this.btncustome.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btncustome.UseVisualStyleBackColor = false;
            this.btncustome.Click += new System.EventHandler(this.btncustome_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = global::Restaurant.Properties.Resources.url;
            this.button1.Location = new System.Drawing.Point(424, 247);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(150, 136);
            this.button1.TabIndex = 0;
            this.button1.Text = "Sale\r\n(ctrl+s)";
            this.button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnAttendance
            // 
            this.btnAttendance.BackColor = System.Drawing.Color.YellowGreen;
            this.btnAttendance.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAttendance.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAttendance.Image = global::Restaurant.Properties.Resources.atten;
            this.btnAttendance.Location = new System.Drawing.Point(221, 432);
            this.btnAttendance.Name = "btnAttendance";
            this.btnAttendance.Size = new System.Drawing.Size(150, 136);
            this.btnAttendance.TabIndex = 83;
            this.btnAttendance.Text = "Attendance";
            this.btnAttendance.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAttendance.UseVisualStyleBackColor = false;
            this.btnAttendance.Click += new System.EventHandler(this.btnAttendance_Click);
            // 
            // btnReprintKot
            // 
            this.btnReprintKot.BackColor = System.Drawing.Color.OrangeRed;
            this.btnReprintKot.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnReprintKot.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReprintKot.Image = global::Restaurant.Properties.Resources.inkjet_printer;
            this.btnReprintKot.Location = new System.Drawing.Point(28, 247);
            this.btnReprintKot.Name = "btnReprintKot";
            this.btnReprintKot.Size = new System.Drawing.Size(150, 136);
            this.btnReprintKot.TabIndex = 82;
            this.btnReprintKot.Text = "Kot Reprint";
            this.btnReprintKot.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnReprintKot.UseVisualStyleBackColor = false;
            this.btnReprintKot.Click += new System.EventHandler(this.btnReprintKot_Click);
            this.btnReprintKot.MouseEnter += new System.EventHandler(this.btnReprintKot_MouseEnter);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Image = global::Restaurant.Properties.Resources.DOC;
            this.button4.Location = new System.Drawing.Point(221, 247);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(150, 136);
            this.button4.TabIndex = 8;
            this.button4.Text = "Kot Details";
            this.button4.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.btnsupplier);
            this.Controls.Add(this.btnSaleByPaymentmode);
            this.Controls.Add(this.btnsalesummery);
            this.Controls.Add(this.btnAddProduct);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btncustomer);
            this.Controls.Add(this.btnbillreprint);
            this.Controls.Add(this.btnstock);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnpurchase);
            this.Controls.Add(this.btncustome);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnAttendance);
            this.Controls.Add(this.btnReprintKot);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip2);
            this.Name = "Home";
            this.Text = "K-Palms Help Line 9096234202,9595163594/www.kpalms.net";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Home_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Home_KeyDown);
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saleStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hotelSettingToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem kotReprintToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem billReprintToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem shiftingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageAccessLevelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addCatagoryToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem addItemToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem addTableToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem addSupplierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem workerToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem messageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printerSettingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem adminPartToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuAccessToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem itemAccessToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sMSSettingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem emailSettingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymentModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transactionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchasingToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem wastageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem expencessToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inventoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createUnitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createItemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recipieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewStockToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewStockToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem windowsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calculatorToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem notePadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sendSmsEmailToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem restaurantToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem periodWiseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem billWiseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cashierWiseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saleByItemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem billSummaryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem billDetailsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem customerHistoryToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem discountReportToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem parsalReportsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cancelItemListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cancelBillReportToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem kotAuditBotAuditToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kotItemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem todayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yesturdayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saleByTableToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem itemPriceListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchasingReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stockEntryReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wastageReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stockReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recipieReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.Button btnAddProduct;
        private System.Windows.Forms.Button btncustomer;
        private System.Windows.Forms.Button btnstock;
        private System.Windows.Forms.Button btnpurchase;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;

        private System.Windows.Forms.ToolStripMenuItem addKotRemarkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem securityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerPaymentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerBalenceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addCustomerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem barItemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem periodWiseBarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salebyPaymentModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saleSummeryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logOutToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem workerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addWorkerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem workerAttendancToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salleryCalculateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salaryHiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem supplierAccountToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kotDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rateManageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rateCategaryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem departmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rateAssignToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem demoToolStripMenuItem;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dayCloseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dayOpenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem feedBackToolStripMenuItem;

        private System.Windows.Forms.ToolStripMenuItem workderAttendaceToolStripMenuItem;

        private System.Windows.Forms.ToolStripMenuItem reprintKotToolStripMenuItem;
        private System.Windows.Forms.Button btnReprintKot;
        private System.Windows.Forms.ToolStripMenuItem securityToolStripMenuItem1;
        private System.Windows.Forms.Button btnAttendance;
        private System.Windows.Forms.Button btncustome;
        private System.Windows.Forms.Button btnbillreprint;
        private System.Windows.Forms.Button btnsalesummery;
        private System.Windows.Forms.Button btnSaleByPaymentmode;
        private System.Windows.Forms.ToolStripMenuItem securityToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem counterReportToolStripMenuItem;
        private System.Windows.Forms.Button btnsupplier;


    }
}
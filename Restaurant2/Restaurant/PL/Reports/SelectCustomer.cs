﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;



using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using Restaurant.PL.Reports;


namespace Restaurant.PL.Reports
{
    public partial class SelectCustomer : Form
    {
        BL_Field bl_field = new BL_Field();
        BL_Wastage bl_wastage = new BL_Wastage();
        BL_Vendor bl_vendor = new BL_Vendor();
        BL_NewItem bl_newitem = new BL_NewItem();
        Common common = new Common();

        BL_Customer bl_customer = new BL_Customer();


        public SelectCustomer()
        {
            InitializeComponent();
        }

        private void txtitemname_TextChanged(object sender, EventArgs e)
        {
            //DataTable dt = null;
            //if (txtcustomerName.Text != "")
            //{
            //    bl_field.TextSearch = txtcustomerName.Text;
            //    dt = bl_customer.SelectCustomerBySearch(bl_field);
            //    if (dt.Rows.Count > 0)
            //    {
            //        lstitem.Visible = true;
            //        lstitem.DataSource = dt;
            //        lstitem.Columns["CustId"].Visible=false;
            //        lstitem.Columns["Email"].Visible = false;
            //        lstitem.Columns["Status"].Visible = false;
            //    }

            //}
            //else {
            //    if (txtcustomerName.Focused)
            //    {
            //        lstitem.Visible = true;
            //        dt = bl_customer.SelectCustomer();
            //        lstitem.DataSource = dt;
            //    }
             

            //}
        }

        private void txtitemname_KeyDown(object sender, KeyEventArgs e)
        {

            //if (e.KeyCode == Keys.Enter)
            //{
            //    if (lstitem.Rows.Count > 0)
            //    {
            //        txtcustomerName.Text = lstitem.SelectedRows[0].Cells[1].Value.ToString();
            //        bl_field.CustIdStatic = lstitem.SelectedRows[0].Cells[0].Value.ToString();
            //        lstitem.Hide();

            //        btnOk.Focus();
            //    }
            //    else
            //    {
            //        lstitem.Visible = false;
            //        txtcustomerName.Focus();
            //    }

            //}

            //if (e.KeyCode == Keys.Up)
            //{
            //    DataTable dtTemp = lstitem.DataSource as DataTable;

            //    object[] arr = dtTemp.Rows[dtTemp.Rows.Count - 1].ItemArray;
            //    for (int i = dtTemp.Rows.Count - 2; i >= 0; i--)
            //    {
            //        dtTemp.Rows[i + 1].ItemArray = dtTemp.Rows[i].ItemArray;
            //    }
            //    dtTemp.Rows[0].ItemArray = arr;



            //}

            //if (e.KeyCode == Keys.Down)
            //{

            //    DataTable dtTemp = lstitem.DataSource as DataTable;

            //    object[] arr = dtTemp.Rows[0].ItemArray;
            //    for (int i = 1; i < dtTemp.Rows.Count; i++)
            //    {
            //        dtTemp.Rows[i - 1].ItemArray = dtTemp.Rows[i].ItemArray;
            //    }
            //    dtTemp.Rows[dtTemp.Rows.Count - 1].ItemArray = arr;



            //}
        }

        private void SelectCustomer_Load(object sender, EventArgs e)
        {
           // lstitem.Visible = false;
            common.BindCustomerCombo(cbocustomer);

        }

        private void SelectCustomer_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void txtcustomerName_Leave(object sender, EventArgs e)
        {
           // lstitem.Visible = false;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (cbocustomer.Text != "")
            {
                DateTime dd1 = Convert.ToDateTime(dtpFBill.SelectionStart.ToLongDateString());
                DateTime dd2 = Convert.ToDateTime(dtpTBill.SelectionStart.ToLongDateString());
                bl_field.CustIdStatic = cbocustomer.SelectedValue.ToString();
                bl_field.FromDate = dd1.ToString("dd/MM/yyyy");
                bl_field.Todate = dd2.ToString("dd/MM/yyyy");

                this.Close();
                ReportsViewer reportviewer = new ReportsViewer("CustomerHistory");
                reportviewer.Show();
            }
            else {
                MessageBox.Show("Please Select Customer Name?");
            }
        }
    }
}

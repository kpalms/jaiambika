﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using Restaurant.PL.Reports;
using System.Drawing.Printing;
using CrystalDecisions.CrystalReports.Engine;




namespace Restaurant.PL.Reports
{
    public partial class ReportsViewer : Form
    {
        BL_Sale bl_sale = new BL_Sale();

        BL_Field bl_field = new BL_Field();
        public string searchname = "";
        BL_Purchase bl_purchase = new BL_Purchase();

        BL_Reports bl_reports = new BL_Reports();
        Common common = new Common();


        public ReportsViewer(string search)
        {
            InitializeComponent();
            searchname = search;
            
        }

        public Int32 GetPaperSize(String sPrinterName, String sPaperSizeName)
        {
            PrintDocument docPrintDoc = new PrintDocument();
            docPrintDoc.PrinterSettings.PrinterName = sPrinterName;
            for (int i = 0; i < docPrintDoc.PrinterSettings.PaperSizes.Count; i++)
            {
                int raw = docPrintDoc.PrinterSettings.PaperSizes[i].RawKind;
                if (docPrintDoc.PrinterSettings.PaperSizes[i].PaperName == sPaperSizeName)
                {
                    return raw;
                }
            }
            return 0;
        }

        private void ReportsViewer_Load(object sender, EventArgs e)
         {


 

            try
                                   {
                string role = bl_field.RoleLogin;
                DataSet ds = null;
                if (searchname == "salebyitem")
                {
                    bl_field.Status = "Close";
                    ds = bl_reports.SearchSaleByItem(bl_field);
                    rptsalebyitem rptsale = new rptsalebyitem();
                    rptsale.SetDataSource(ds.Tables[0]);
                    rptsale.SetParameterValue("FromDate", bl_field.FromDate);
                    rptsale.SetParameterValue("ToDate", bl_field.Todate);
                    rptsale.SetParameterValue("HeaderName", bl_field.HeaderName);
                    crystalReportViewer1.ReportSource = rptsale;
                    crystalReportViewer1.Refresh();
                }

                if (searchname == "salebyitemBar")
                {
                    bl_field.Status = "Close";
                    ds = bl_reports.SearchSaleByItemBar(bl_field);
                    rptsalebyitem rptsale = new rptsalebyitem();
                    rptsale.SetDataSource(ds.Tables[0]);
                    rptsale.SetParameterValue("FromDate", bl_field.FromDate);
                    rptsale.SetParameterValue("ToDate", bl_field.Todate);
                    rptsale.SetParameterValue("HeaderName", bl_field.HeaderName);
                    crystalReportViewer1.ReportSource = rptsale;
                    crystalReportViewer1.Refresh();
                }


                else if (searchname == "today")
                {

                    bl_field.Status = "Close";
                    bl_field.FromDate = DateTime.Now.ToString("dd/MM/yyyy");
                    bl_field.Todate = DateTime.Now.ToString("dd/MM/yyyy");
                    ds = bl_reports.SearchSaleByBillNo(bl_field);
                    rptBillWise rptsale = new rptBillWise();
                    rptsale.SetDataSource(ds.Tables[0]);
                    rptsale.SetParameterValue("FromDate", bl_field.FromDate);
                    rptsale.SetParameterValue("ToDate", bl_field.Todate);
                    rptsale.SetParameterValue("HeaderName", "Today Report");
                    crystalReportViewer1.ReportSource = rptsale;
                    crystalReportViewer1.Refresh();

                }
                else if (searchname == "yesturday")
                {

                    bl_field.Status = "Close";
                    DateTime date = DateTime.Now;
                    DateTime yesdate = date.AddDays(-1);
                    string fdate = yesdate.ToString("dd/MM/yyyy");


                    bl_field.FromDate = fdate;
                    bl_field.Todate = fdate;
                     ds = bl_reports.SearchSaleByBillNo(bl_field);
                    rptBillWise rptsale = new rptBillWise();
                    rptsale.SetDataSource(ds.Tables[0]);
                    rptsale.SetParameterValue("FromDate", fdate);
                    rptsale.SetParameterValue("ToDate", fdate);
                    rptsale.SetParameterValue("HeaderName", "yesturday Report");
                    crystalReportViewer1.ReportSource = rptsale;
                    crystalReportViewer1.Refresh();
                }
                else if (searchname == "salebyTable")
                    {
                        bl_field.Status = "Close";
                        ds = bl_reports.SearchSaleByTable(bl_field);
                        rptsalebyTable rptsale = new rptsalebyTable();
                        rptsale.SetDataSource(ds.Tables[0]);
                        rptsale.SetParameterValue("FromDate", bl_field.FromDate);
                        rptsale.SetParameterValue("ToDate", bl_field.Todate);
                        crystalReportViewer1.ReportSource = rptsale;
                        crystalReportViewer1.Refresh();
                    }
                    else if (searchname == "PeriodWise")
                    {

                        bl_field.Status = "Close";
                         ds = bl_reports.SearchSaleByBillNo(bl_field);
                        rptBillWise rptsale = new rptBillWise();
                        rptsale.SetDataSource(ds.Tables[0]);
                        rptsale.SetParameterValue("FromDate", bl_field.FromDate);
                        rptsale.SetParameterValue("ToDate", bl_field.Todate);
                        rptsale.SetParameterValue("HeaderName", bl_field.HeaderName);
                        crystalReportViewer1.ReportSource = rptsale;
                        crystalReportViewer1.Refresh();

                    }
                else if (searchname == "PeriodWiseBar")
                {

                    bl_field.Status = "Close";
                    ds = bl_reports.SearchSaleByBillNoBar(bl_field);
                    rptBillWise rptsale = new rptBillWise();
                    rptsale.SetDataSource(ds.Tables[0]);
                    rptsale.SetParameterValue("FromDate", bl_field.FromDate);
                    rptsale.SetParameterValue("ToDate", bl_field.Todate);
                    rptsale.SetParameterValue("HeaderName", bl_field.HeaderName);
                    crystalReportViewer1.ReportSource = rptsale;
                    crystalReportViewer1.Refresh();

                }
                else if (searchname == "BillWise")
                {

                    bl_field.Status = "Close";
                    ds = bl_reports.SearchBillNo(bl_field);
                    rptBillWise rptsale = new rptBillWise();
                    rptsale.SetDataSource(ds.Tables[0]);
                    rptsale.SetParameterValue("FromDate", bl_field.FromDate);
                    rptsale.SetParameterValue("ToDate", bl_field.Todate);
                    rptsale.SetParameterValue("HeaderName", bl_field.HeaderName);
                    crystalReportViewer1.ReportSource = rptsale;
                    crystalReportViewer1.Refresh();
                }
                else if (searchname == "CashierWise")
                {

                    bl_field.Status = "Close";
                    ds = bl_reports.SearchCashierWise(bl_field);
                    if (ds.Tables[0].Rows.Count > 0)
                    {

                       rptCashier rptsale = new rptCashier();
                       rptsale.SetDataSource(ds.Tables[0]);
                        string CashierName = ds.Tables[0].Rows[0][3].ToString();
                        rptsale.SetParameterValue("FromDate", bl_field.FromDate);
                        rptsale.SetParameterValue("ToDate", bl_field.Todate);
                        crystalReportViewer1.ReportSource = rptsale;
                        crystalReportViewer1.Refresh();
                    }
                    else {
                        MessageBox.Show("No Records Found");
                    }

                }
                    else if (searchname == "ItemPriceList")
                    {
                        ds = bl_reports.SearchByItemList(bl_field);
                        rptItemPriceList rptItem = new rptItemPriceList();
                         rptItem.SetDataSource(ds.Tables[0]);
                        crystalReportViewer1.ReportSource = rptItem;
                        crystalReportViewer1.Refresh();
                    }

                   else if (searchname == "PurchasingExp")
                    {

                        ds = bl_reports.SelectPurchaseExp(bl_field);
                        rptExpences rptpurchase = new rptExpences();
                        rptpurchase.SetDataSource(ds.Tables[0]);

                        rptpurchase.SetParameterValue("FromDate", bl_field.FromDate);
                        rptpurchase.SetParameterValue("ToDate", bl_field.Todate);
                        crystalReportViewer1.ReportSource = rptpurchase;
                        crystalReportViewer1.Refresh();
                    }

                        else if (searchname == "CancelItem")
                             {
                        ds = bl_reports.CancelBillAllDays(bl_field);
                        rptcancel rptcan= new rptcancel ();
                        rptcan.SetDataSource(ds.Tables[0]);
                        rptcan.SetParameterValue("FromDate", bl_field.FromDate);
                        rptcan.SetParameterValue("ToDate", bl_field.Todate);
                        crystalReportViewer1.ReportSource = rptcan;
                        crystalReportViewer1.Refresh();
                    }
                    else if (searchname == "ParsalReports")
                    {
                    
                        ds = bl_reports.ParsalReports(bl_field);
                        rptparsalReports rptsale = new rptparsalReports();
                         rptsale.SetDataSource(ds.Tables[0]);
                        rptsale.SetParameterValue("FromDate", bl_field.FromDate);
                        rptsale.SetParameterValue("ToDate", bl_field.Todate);
                        crystalReportViewer1.ReportSource = rptsale;
                        crystalReportViewer1.Refresh();
                    }
                    else if (searchname == "CustomerList")
                    {
                        ds = bl_reports.CustomerList(bl_field);
                        rptcustomerList rptcan = new rptcustomerList();
                        rptcan.SetDataSource(ds.Tables[0]);
                        crystalReportViewer1.ReportSource = rptcan;
                        crystalReportViewer1.Refresh();
                    }

                else if (searchname == "KotWise")
                {


                    ds = bl_reports.KotWise(bl_field);
                    rptKotWise rptsale = new rptKotWise();
                    rptsale.SetDataSource(ds.Tables[0]);
                    rptsale.SetParameterValue("FromDate", bl_field.FromDate);
                    rptsale.SetParameterValue("ToDate", bl_field.Todate);
                    rptsale.SetParameterValue("HeaderName", bl_field.HeaderName);
                    crystalReportViewer1.ReportSource = rptsale;
                    crystalReportViewer1.Refresh();
                }
                else if (searchname == "KotWiseItem")
                {

                    ds = bl_reports.KotWiseItem(bl_field);
                    rptKotWiseItem rptsale = new rptKotWiseItem();
                    rptsale.SetDataSource(ds.Tables[0]);
                    rptsale.SetParameterValue("FromDate", bl_field.FromDate);
                    rptsale.SetParameterValue("ToDate", bl_field.Todate);
                    rptsale.SetParameterValue("HeaderName", bl_field.HeaderName);
                    crystalReportViewer1.ReportSource = rptsale;
                    crystalReportViewer1.Refresh();
                }
                else if (searchname == "BillSummary")
                {
                    ds = bl_reports.BillSummary(bl_field);
                    rptSaleSummery rptcan = new rptSaleSummery();
                    rptcan.SetDataSource(ds.Tables[0]);
                    rptcan.SetParameterValue("FromDate", bl_field.FromDate);
                    rptcan.SetParameterValue("ToDate", bl_field.Todate);
                    rptcan.SetParameterValue("HeaderName", bl_field.HeaderName);
                    crystalReportViewer1.ReportSource = rptcan;
                    crystalReportViewer1.Refresh();
                }
                else if (searchname == "PurchasingReport")
                 {

                     ds = bl_reports.PurchasingReport(bl_field);
                    rptPurchaseReport rptpurchase = new rptPurchaseReport();
                    rptpurchase.SetDataSource(ds.Tables[0]);
                    rptpurchase.SetParameterValue("FromDate", bl_field.FromDate);
                    rptpurchase.SetParameterValue("ToDate", bl_field.Todate);
                    crystalReportViewer1.ReportSource = rptpurchase;
                    crystalReportViewer1.Refresh();
                }
                else if (searchname == "WastageReport")
                {

                    ds = bl_reports.WastageReport(bl_field);
                    rptWastageReport rptpurchase = new rptWastageReport();
                    rptpurchase.SetDataSource(ds.Tables[0]);
                    rptpurchase.SetParameterValue("FromDate", bl_field.FromDate);
                    rptpurchase.SetParameterValue("ToDate", bl_field.Todate);
                    crystalReportViewer1.ReportSource = rptpurchase;
                    crystalReportViewer1.Refresh();
                }
                else if (searchname == "BillDetailsReport")
                {

                    ds = bl_reports.BillDetailsReport(bl_field);
                    rptBillDetails rptpurchase = new rptBillDetails();
                    rptpurchase.SetDataSource(ds.Tables[0]);
                    rptpurchase.SetParameterValue("FromDate", bl_field.FromDate);
                    rptpurchase.SetParameterValue("ToDate", bl_field.Todate);
                    crystalReportViewer1.ReportSource = rptpurchase;
                    crystalReportViewer1.Refresh();
                }
                else if (searchname == "CounterReport")
                {

                    ds = bl_reports.CounterReport(bl_field);
                    rptBillDetails rptpurchase = new rptBillDetails();
                    rptpurchase.SetDataSource(ds.Tables[0]);
                    rptpurchase.SetParameterValue("FromDate", bl_field.FromDate);
                    rptpurchase.SetParameterValue("ToDate", bl_field.Todate);
                    crystalReportViewer1.ReportSource = rptpurchase;
                    crystalReportViewer1.Refresh();
                }
                else if (searchname == "StockReport")
                {

                    ds = bl_reports.StockReport(bl_field);
                    rptStockReport rptpurchase = new rptStockReport();
                    rptpurchase.SetDataSource(ds.Tables[0]);
                    crystalReportViewer1.ReportSource = rptpurchase;
                    crystalReportViewer1.Refresh();
                }
                else if (searchname == "RecipieReport")
                {

                    ds = bl_reports.RecipieReport(bl_field);
                    rptRecipieReport rptpurchase = new rptRecipieReport();
                    rptpurchase.SetDataSource(ds.Tables[0]);
                    crystalReportViewer1.ReportSource = rptpurchase;
                    crystalReportViewer1.Refresh();
                }
                else if (searchname == "DisCountReports")
                {
                    ds = bl_reports.DisCountReports(bl_field);
                    rptdiscount rptcan = new rptdiscount();
                    rptcan.SetDataSource(ds.Tables[0]);
                    rptcan.SetParameterValue("FromDate", bl_field.FromDate);
                    rptcan.SetParameterValue("ToDate", bl_field.Todate);
                    crystalReportViewer1.ReportSource = rptcan;
                    crystalReportViewer1.Refresh();
                }
                else if (searchname == "CancelBill")
                {
                    ds = bl_reports.CancelBill(bl_field);
                    rptCancelBill rptpurchase = new rptCancelBill();
                    rptpurchase.SetDataSource(ds.Tables[0]);
                    rptpurchase.SetParameterValue("FromDate", bl_field.FromDate);
                    rptpurchase.SetParameterValue("ToDate", bl_field.Todate);
                    crystalReportViewer1.ReportSource = rptpurchase;
                    crystalReportViewer1.Refresh();
                }
                else if (searchname == "CustomerHistory")
                {
                    ds = bl_reports.CustomerHistory(bl_field);
                    rptCustomerHistory rptpurchase = new rptCustomerHistory();
                    rptpurchase.SetDataSource(ds.Tables[0]);
                    rptpurchase.SetParameterValue("FromDate", bl_field.FromDate);
                    rptpurchase.SetParameterValue("ToDate", bl_field.Todate);
                    crystalReportViewer1.ReportSource = rptpurchase;
                    crystalReportViewer1.Refresh();
                }
                    else
                    {

                    }
            }
            catch 
            {

                MessageBox.Show("error occured!Please contact Administration?");             
            }


        }


        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (printDialog1.ShowDialog() == DialogResult.OK)
            {
                crystalReportViewer1.PrintReport();
            }
           
        }

  


    }
}

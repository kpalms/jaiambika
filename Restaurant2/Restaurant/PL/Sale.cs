﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;

using Restaurant.PL.Reports;

namespace Restaurant.PL
{
    public partial class Sale : Form
    {
        BL_Sale bl_sale = new BL_Sale();

        BL_Field bl_field = new BL_Field();
        BL_Customer bl_customer = new BL_Customer();
        Common common = new Common();

        BL_Worker bl_worker = new BL_Worker();
        string btnName = "";
        public Sale()
        {
            InitializeComponent();
            this.KeyPreview = true;
        }

        private void rdoalldays_CheckedChanged(object sender, EventArgs e)
        {


        }

        private void Sale_Load(object sender, EventArgs e)
        {
            dtpfrom.CustomFormat = "dd/MM/yyyy";
            dtpto.CustomFormat = "dd/MM/yyyy";
            pnlpassword.Visible = false;
            //RefreshData();
            BindBillNo();
         
          

        }


        public void BindDataByDate()
        {
            bl_field.FromDate = dtpfrom.Text;
            bl_field.Todate = dtpto.Text;
            DataTable dt = bl_sale.SelectDataByDate(bl_field);
            grdsale.DataSource = dt;
            grdsale.Columns["OrderDetailId"].Visible = false;
            grdsale.Columns["ParsalNo"].Visible = false;
            //int Closetot = 0;
            //for (int i = 0; i < dt.Rows.Count; i++)
            //{

            //    Closetot += Convert.ToInt32(dt.Rows[i][7]);

            //}

        }

        private void BindBillNo()
        {
            bl_field.FromDate = dtpfrom.Text;
            bl_field.Todate = dtpto.Text;
            DataTable dt = bl_sale.SelectBillNo(bl_field );
            ddlbillno.DataSource = dt;
            ddlbillno.DisplayMember = "BillNo";
            ddlbillno.ValueMember = "BillNo";

        }
        //public void RefreshData()
        //{

        //    bl_field.FromDate = dtpfrom.Text;
        //    bl_field.Todate = dtpto.Text;
        //    DataSet ds = bl_sale.SearchToday(bl_field);
        //    bl_field.Status ="Close";
 
        //    int Closetot = 0;
        //    for (int i = 0; i < ds.Tables[0].Rows.Count;i++ )
        //    {
             
        //            Closetot += Convert.ToInt32(ds.Tables[0].Rows[i][7]);
              
        //    }

          
        //    lblclosetotal.Text = Convert.ToString(Closetot);
        //    grdsale.DataSource = ds.Tables[0];
        //    grdsale.Columns["OrderDetailId"].Visible = false;
        //    ddlbillno.Text = "";
           
        //}


        private void Calculation()
        {

            //decimal closeTotal = 0;
            for (int i = 0; i < grdsale.Rows.Count;i++ )
            {
            //decimal closeTotal = 0;
            //for (int i = 0; i < grdsale.Rows.Count;i++ )
            //{
            //    //closeTotal+=grdsale.Rows[i][7].


            }
        }



        private void rdotoday_CheckedChanged(object sender, EventArgs e)
        {

            bl_field.Status = "Close";
            bl_field.Date = DateTime.Now.ToString("dd/MM/yyyy");
            DataSet ds = bl_sale.SearchTodayStatus(bl_field);
            grdsale.DataSource = ds.Tables[0];
            grdsale.Columns["OrderDetailId"].Visible = false;
            grdsale.Columns["ReceiptNo"].Visible = false;
            grdsale.Columns["Qt"].Visible = false;
            grdsale.Columns["ParsalNo"].Visible = false;
            grdsale.Columns["CustId"].Visible = false;
            grdsale.Columns["KotQty"].Visible = false;
        }

        //private void rdoOpen_CheckedChanged(object sender, EventArgs e)
        //{
        //    bl_field.Status = "Open";
        //    bl_field.Date = DateTime.Now.ToString("dd/MM/yyyy");
        //    DataSet ds = bl_sale.SearchTodayStatus(bl_field);
        //    grdsale.DataSource = ds.Tables[0];
        //    grdsale.Columns["OrderDetailId"].Visible = false;
        //    grdsale.Columns["ReceiptNo"].Visible = false;
        //    grdsale.Columns["Qt"].Visible = false;
        //    grdsale.Columns["ParsalNo"].Visible = false;
        //    grdsale.Columns["CustId"].Visible = false;
        //    grdsale.Columns["KotQty"].Visible = false;
        //}
 

 

        private void Sale_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }



        private void btnrefresh_Click(object sender, EventArgs e)
        {
            //RefreshData();
            BindBillNo();
        }

        //private void rdosalebyitem_CheckedChanged(object sender, EventArgs e)
        //{
     
        //}

        private void ddlbillno_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlbillno.Text != "System.Data.DataRowView")
            {
                if (ddlbillno.Text != "")
                {

                    BindBillWise();
                }
                else
                {
                   // RefreshData();
                }
            }
            
        }
        private void BindBillWise()
        {
            bl_field.Status = "Close";
            bl_field.Date = DateTime.Now.ToString("dd/MM/yyyy");
            bl_field.ReceiptNo = Convert.ToInt32(ddlbillno.Text);
            DataSet ds = bl_sale.SearchByBillNo(bl_field);
            grdsale.DataSource = ds.Tables[0];
            grdsale.Columns["OrderDetailId"].Visible = false;
            grdsale.Columns["ParsalNo"].Visible = false;
       }

        private void btncancelBill_Click(object sender, EventArgs e)
        {
            btnName = "CancelBill";
            pnlpassword.Visible = true;
            txtpassword.Focus();
            
        }

        private void btncancelItem_Click(object sender, EventArgs e)
        {
            if (grdsale.Rows.Count > 0)
            {
                if (ddlbillno.Text != "")
                {
                    DialogResult msgg = MessageBox.Show("Are you sure you want to Cancel Item Item?", "Delete", MessageBoxButtons.YesNo);
                    if (msgg == DialogResult.Yes)
                    {

                        bl_field.ReceiptNo = Convert.ToInt32(grdsale.SelectedRows[0].Cells[10].Value.ToString());
                        bl_field.Price = grdsale.SelectedRows[0].Cells[10].Value.ToString();
                        bl_field.Qty = grdsale.SelectedRows[0].Cells[10].Value.ToString();
                        bl_field.Date = DateTime.Now.ToString("dd/MM/yyyy");
                        bl_field.SaleId = grdsale.SelectedRows[0].Cells[0].Value.ToString();
                        bl_sale.CloseCancelBillByItem(bl_field);
                        BindBillWise();
                    }
                }
                else
                {
                    MessageBox.Show("Please select Bill no");
                }
            }
            else
            {
                MessageBox.Show("No Records Found");
            }
        }

        

        private void btnreprint_Click(object sender, EventArgs e)
        {
            try
            {
                string pp = common.ReturnOneValue("select BillPrinter from PrinterSetup");
                if (pp != "0")
                {

                 string msg = common.ReturnOneValue("select Message from Message where Status='YES'");

                if (ddlbillno.Text != "")
                {

                 DialogResult msgg = MessageBox.Show("Are you sure you want to Reprint Bill?", "Reprint", MessageBoxButtons.YesNo);
                 if (msgg == DialogResult.Yes)
                 {
          
                    bl_field.BillNo = ddlbillno.Text;
                    DataSet ds = bl_sale.RePrint(bl_field);
                   rptPrintBill rptItem = new rptPrintBill();
            rptItem.SetDataSource(ds.Tables[0]);
            decimal tot = 0;

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                tot = tot + Convert.ToDecimal(dr[5].ToString());

            }
          

            string word = common.changeToWords(Convert.ToString(tot), true);
            rptItem.SetParameterValue("Word", word);
            rptItem.SetParameterValue("WaiterName", ds.Tables[0].Rows[0][10]);
            rptItem.SetParameterValue("Message", msg);
            rptItem.SetParameterValue("HotelName", bl_field.HotelName);
            rptItem.SetParameterValue("HotelAddress", bl_field.HotelAddress);
            bl_field.Date = DateTime.Now.ToString("dd/MM/yyyy");
            bl_field.Total = Convert.ToDecimal(tot);

            rptItem.SetParameterValue("ReceiptNo", ds.Tables[0].Rows[0][7]);
            rptItem.SetParameterValue("Date", ds.Tables[0].Rows[0][8]);
            rptItem.SetParameterValue("BillTime", ds.Tables[0].Rows[0][9]);
            string discheck = ds.Tables[0].Rows[0][13].ToString();

            if (discheck != "0.00")
            {
                rptItem.SetParameterValue("discount", ds.Tables[0].Rows[0][12]);
                rptItem.SetParameterValue("lbldiscount", "DisCount");
                rptItem.SetParameterValue("lblsubtotal", "SubTotal");
                rptItem.SetParameterValue("PrintTotal", Convert.ToDecimal(tot) - Convert.ToDecimal(ds.Tables[0].Rows[0][13]));
                rptItem.SetParameterValue("SubTotal",tot);
            }
            else
            {
                rptItem.SetParameterValue("discount", "");
                rptItem.SetParameterValue("lbldiscount", "");
                rptItem.SetParameterValue("lblsubtotal", "");
                rptItem.SetParameterValue("SubTotal", "");
                rptItem.SetParameterValue("PrintTotal", tot);

            }

            rptItem.SetParameterValue("lblName", "");
            rptItem.SetParameterValue("lblMobileNo", "");
            rptItem.SetParameterValue("lblAddress", "");
            rptItem.SetParameterValue("Name", "");
            rptItem.SetParameterValue("MobileNo", "");
            rptItem.SetParameterValue("Address", "");

            if (ds.Tables[0].Rows[0]["DeliveryCharge"].ToString() != "0.00")
            {
                rptItem.SetParameterValue("lblDeliveryCharge", "DeleveryCharge");
                rptItem.SetParameterValue("DeliveryCharge", ds.Tables[0].Rows[0]["DeliveryCharge"].ToString());
            }
            else
            {
                rptItem.SetParameterValue("lblDeliveryCharge", "");
                rptItem.SetParameterValue("DeliveryCharge", "");
            }





            rptItem.PrintOptions.PrinterName = pp;
            rptItem.PrintToPrinter(1, false, 0, 0);

         
        }//if end
    }
    else
    {
        MessageBox.Show("Please select Bill no");
    }
}
else {
    MessageBox.Show("Printer not found");
}
            }
            catch
            {


            }
        
        }

        //private void rdoRestaurant_CheckedChanged(object sender, EventArgs e)
        //{
        //    ddlbillno.SelectedIndex = -1;
        //    RefreshData();
        //    BindBillNo();
        //}

        //private void rdoBar_CheckedChanged(object sender, EventArgs e)
        //{
        //    ddlbillno.SelectedIndex = -1;
        //    RefreshData();
        //    BindBillNo();
        //}

        private void btnok_Click(object sender, EventArgs e)
        {
            CheckPassword();
        }
        private void CheckPassword()
        { 
            bl_field.Password = txtpassword.Text;
            SqlDataReader dr = bl_worker.CheckSecurity(bl_field);
            if (dr.Read())
            {
                txtpassword.Text = "";
                pnlpassword.Visible = false;
                string role = dr[0].ToString();
                if (role == "Admin"||role=="Manager")
                {
                    if (btnName == "CancelBill")
                    {
                        CancelBill();
                    }
                    if (btnName == "CancelItem")
                    {
                        CancelItem();
                    }
                }
                    else
                {

                    MessageBox.Show("Please check password?");
                    txtpassword.Text = "";
                }
            }
            else
            {
                MessageBox.Show("Please check password?");
                txtpassword.Text = "";
            }
        }
        private void CancelBill()
        {
            if (grdsale.Rows.Count > 0)
            {
                if (ddlbillno.Text != "")
                {
                    DialogResult msgg = MessageBox.Show("Are you sure you want to Cancel Bill?", "Delete", MessageBoxButtons.YesNo);
                    if (msgg == DialogResult.Yes)
                    {

                        bl_field.Date = DateTime.Now.ToString("dd/MM/yyyy");
                        bl_field.BillNo = ddlbillno.Text;
                        bl_sale.CloseCancelBill(bl_field);
                        //RefreshData();
                    }
                }
                else
                {
                    MessageBox.Show("Please select Bill no");
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtpassword.Text = "";
            pnlpassword.Visible = false;
        }

        private void txtpassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (txtpassword.Text != "")
            {
                if (e.KeyCode == Keys.Enter)
                {
                    CheckPassword();
                }
            }
        }

        private void btnbillshow_Click(object sender, EventArgs e)
        {
            BindBillNo();
            BindDataByDate();
        }





        private void cancelItem_Click(object sender, EventArgs e)
        {
            btnName = "CancelItem";
            pnlpassword.Visible = true;
            txtpassword.Focus();
        }

        private void CancelItem()
        {
            DialogResult msgg = MessageBox.Show("Are you sure you want to Cancel Item?", "Delete", MessageBoxButtons.YesNo);
            if (msgg == DialogResult.Yes)
            {

                try
                {

                    string SaleId = grdsale.SelectedRows[0].Cells["OrderDetailId"].Value.ToString();
                    string TableNo = grdsale.SelectedRows[0].Cells["TableNo"].Value.ToString();
                    string orderno = common.ReturnOneValue("select KotOrderNo from OrderDetail where OrderDetailId='" + SaleId + "'");
                    string CashierName = grdsale.SelectedRows[0].Cells["CashierName"].Value.ToString();
                    string ItemName = grdsale.SelectedRows[0].Cells["ItemName"].Value.ToString();
                    string rate = grdsale.SelectedRows[0].Cells["Price"].Value.ToString();
                    string curqty = grdsale.SelectedRows[0].Cells["Qty"].Value.ToString();

                    string Amount = grdsale.SelectedRows[0].Cells["Price"].Value.ToString();

                    CancelItemSaleStatus quanti = new CancelItemSaleStatus(SaleId, TableNo, orderno, CashierName, ItemName, curqty, rate, Amount, this);
                    quanti.ShowDialog();


                }
                catch
                {
                }


            }
        }

        private void ddlbillno_KeyPress(object sender, KeyPressEventArgs e)
        {
            common.ComboBoxCheckValue(ddlbillno, e);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;

using Restaurant.PL.Reports;

namespace Restaurant.PL
{
    public partial class KotReprint : Form
    {
        BL_Sale bl_sale = new BL_Sale();
        BL_Field bl_field = new BL_Field();
        DataSet ds;
        Common common = new Common();

        public KotReprint()
        {
            InitializeComponent();
        }

        private void KotReprint_Load(object sender, EventArgs e)
        {
            BindKot();
            BindOrderNo();

        }
        private void BindOrderNo()
        {
            bl_field.FromDate = dtpfrom.Text;
            bl_field.Todate = dtpto.Text;
            DataTable dt = bl_sale.BindorderNo(bl_field);
            ddlbillno.DataSource = dt;
            ddlbillno.DisplayMember = "KotOrderNo";
            ddlbillno.ValueMember = "KotOrderNo";
        }
        private void BindKot()
        {
            bl_field.FromDate = dtpfrom.Text;
            bl_field.Todate = dtpto.Text;
            DataTable dt = bl_sale.BindKotReprint(bl_field);
            grdsale.DataSource = dt;
     
        }

        private void ddlbillno_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlbillno.Text != "System.Data.DataRowView")
            {
                if (ddlbillno.Text != "")
                {
                    bl_field.TextSearch = ddlbillno.Text;
                    ds = bl_sale.BindKotReprintSearch(bl_field);
                    grdsale.DataSource = ds.Tables[0];
                }
            }

        }

        private void btnall_Click(object sender, EventArgs e)
        {
            BindKot();
        }

        private void PrintKOT(DataSet ds)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to Print KOT?", "Print KOT", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    rptKotPrint rptItem = new rptKotPrint();
                    rptItem.SetDataSource(ds.Tables[0]);
                    rptItem.SetParameterValue("lblname", "TableNo");
                    rptItem.SetParameterValue("no", grdsale.SelectedRows[0].Cells["TableNo"].Value.ToString());
                    if (bl_field.KOTPRINT == "yes")
                    {
                        rptItem.SetParameterValue("KotOrderNo", "RePrint");
                    }
                    else
                    {
                        rptItem.SetParameterValue("KotOrderNo", ddlbillno.Text);
                    }
                    string pp = common.ReturnOneValue("select KotPrinter from PrinterSetup");
                    rptItem.PrintOptions.PrinterName = pp;

                    rptItem.PrintToPrinter(1, false, 0, 0);

                }

                
            }
            catch (Exception ex)
            {
                MessageBox.Show("this error" + ex);
            }



        }

        private void btnreprint_Click(object sender, EventArgs e)
        {
            PrintKOT(ds);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using Restaurant.BL;

namespace Restaurant.PL
{
    public partial class Unit : Form
    {
        BL_Field bl_field = new BL_Field();
        BL_NewItem bl_newitem = new BL_NewItem();

        public Unit()
        {
            InitializeComponent();
            this.KeyPreview = true;

        }

        private void btnadd_Click(object sender, EventArgs e)
        {
        try
        {
            if (txtname.Text != "")
            {
                bl_field.UnitName = txtname.Text;
                bl_newitem.InsertUnit(bl_field);
                txtname.Text = "";
                MessageBox.Show("Record inserted Successfully");
                BindUnitName();

            }
            else
            {
                MessageBox.Show("Please enter Name?");
            }
       }

            catch
            {
                MessageBox.Show("error occured!Please contact Administration?");

            }
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            txtname.Text = "";
            btndelete.Enabled = false;
            btnUpdate.Enabled = false;
            btnadd.Enabled = true;
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Unit?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_newitem.DeleteUnit(bl_field);
                    txtname.Text = "";
                    MessageBox.Show("Record Delete Successfully");
                    BindUnitName();
                    btndelete.Enabled = false;
                    btnUpdate.Enabled = false;
                    btnadd.Enabled = true;

                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
        }

        private void Unit_Load(object sender, EventArgs e)
        {
            BindUnitName();
            btndelete.Enabled = false;
            btnUpdate.Enabled = false;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            bl_field.UnitId = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            txtname.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            btndelete.Enabled = true;
            btnUpdate.Enabled = true;
            btnadd.Enabled = false;
        }
        private void BindUnitName()
        {
            DataTable dt = bl_newitem.SelectUnit();
            if (dt.Rows.Count > 0)
            {
                dataGridView1.DataSource = dt;
                dataGridView1.Columns["UnitId"].Visible = false;
                dataGridView1.Columns["Status"].Visible = false;
            }
            else
            {
                dataGridView1.DataSource = null;
            }
        }

        private void Unit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void txtname_TextChanged(object sender, EventArgs e)
        {
            txtname.CharacterCasing = CharacterCasing.Upper;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                bl_field.UnitName = txtname.Text;
                bl_newitem.UpdateUnit(bl_field);
                txtname.Text = "";
                MessageBox.Show("Record Updated Successfully");
                BindUnitName();
                btndelete.Enabled = false;
                btnUpdate.Enabled = false;
                btnadd.Enabled = true;
           
            }
            catch
            {
                MessageBox.Show("error occured!Please contact Administration?");
            }
        }
    }
}

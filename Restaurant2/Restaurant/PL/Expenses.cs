﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using Restaurant.PL.Reports;

namespace Restaurant.PL
{
    public partial class Expenses : Form
    {
        BL_Field bl_field = new BL_Field();
        BL_Purchase bl_purchase = new BL_Purchase();
        BL_NewItem bl_newitem = new BL_NewItem();
        Common commmon = new Common();

        public Expenses()
        {
            InitializeComponent();
            this.KeyPreview = true;
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtitemname.Text != "" && txtcost.Text != "")
                {
                    SetField();
                    bl_purchase.InsertExpenses(bl_field);
                    BindPurchase();
                    MessageBox.Show("Recods Inserted Successfully");
                    Clear();
                    txtitemname.Focus();
                }
                else
                {
                    MessageBox.Show("Please enter Itemname and cost");
                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }


        }


        private void SetField()
        {
            bl_field.Status = "Open";
            bl_field.Date = ddldate.Text;
            bl_field.ItemName = txtitemname.Text;
          
            bl_field.Cost = txtcost.Text;
            bl_field.Notes = txtnotes.Text;

        }
        private void Clear()
        {
            ddldate.Text = "";
            txtitemname.Text = "";
            //string date = bl_changeofday.SelectDate();
            //ddldate.Text = date;
            txtcost.Text = "";
            txtnotes.Text = "";
        }
        private void Purchasing_Load(object sender, EventArgs e)
        {
            //string date = bl_changeofday.SelectDate();
            //ddldate.Text=date;
            ddldate.CustomFormat = "dd/MM/yyyy";
            ddldate.Text = DateTime.Now.ToString();
             bl_field .FromDate =ddldate.Text ;
           
            BindPurchase();
            btndelete.Enabled = false;
            btnUpdate.Enabled = false;
        }

        private void BindPurchase()
        {
            bl_field.FromDate=ddldate.Text;
            DataTable dt = bl_purchase.SelectExpenses(bl_field);
            grdpurchase.DataSource = dt;
            grdpurchase.Columns["ExpensesId"].Visible = false;
            grdpurchase.Columns["Qty"].Visible=false;
            //grdpurchase.Columns["PurchaseId"].Visible = false;
            decimal tot = 0;
            lbltotal.Text = "";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                decimal price = Convert .ToDecimal (dt.Rows[i][4].ToString());
                tot = tot + price;
                lbltotal.Text = Convert .ToString (tot);

            }
        }

        private void grdpurchase_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (grdpurchase.Rows.Count>0)
                {
                bl_field.PurchaseId = Convert.ToInt32(grdpurchase.SelectedRows[0].Cells[0].Value.ToString());
                string d = grdpurchase.SelectedRows[0].Cells[1].Value.ToString();
                DateTime date = DateTime.ParseExact(d, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                ddldate.Value = date;
                //ddldate.Text = grdpurchase.SelectedRows[0].Cells[1].Value.ToString();
                
                txtitemname.Text = grdpurchase.SelectedRows[0].Cells[2].Value.ToString();
              
                txtcost.Text = grdpurchase.SelectedRows[0].Cells[4].Value.ToString();
                txtnotes.Text = grdpurchase.SelectedRows[0].Cells[5].Value.ToString();

                btnadd.Enabled = false;
                btndelete.Enabled = true;
                btnUpdate.Enabled = true;
                }
            }
            catch 
            {
                MessageBox.Show("error occured ?please contact administrator");
             
            }

        
        
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure to delete selected Record?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_field.ExpensesId = Convert.ToInt32(grdpurchase.SelectedRows[0].Cells[0].Value.ToString());
                    bl_purchase.DeleteExpenses(bl_field);
                    BindPurchase();
                    Clear();
                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration");
            }






        }

        private void txtqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            commmon.NumberOnly(e);
        }

        private void txtcost_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            //{
            //    e.Handled = true;
            //    MessageBox.Show("Please enter number only");
            //}

            commmon.OneDecimalPoint(txtcost, e);
        }

        private void Purchasing_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            Clear();
            btndelete.Enabled = false;
            btnUpdate.Enabled = false;
            btnadd.Enabled = true;
        }

        private void btnshow_Click(object sender, EventArgs e)
        {
            bl_field.FromDate = ddldate.Text;
            DataTable dt = bl_purchase.SelectExpensesByDate(bl_field);
            grdpurchase.DataSource = dt;
            grdpurchase.Columns["PurchaseId"].Visible = false;
            grdpurchase.Columns["Status"].Visible = false;

            decimal tot = 0;
            lbltotal.Text = "";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                decimal price = Convert.ToDecimal(dt.Rows[i][4].ToString());
                tot = tot + price;
                lbltotal.Text = Convert.ToString(tot);

            }

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                {
                    SetField();
                    bl_field.ExpensesId = Convert.ToInt32(grdpurchase.SelectedRows[0].Cells[0].Value.ToString());
                    bl_purchase.UpdateExpenses(bl_field);
                    Clear();
                    BindPurchase();

                }
            }
            catch
            {
                MessageBox.Show("error occured!Please contact Administration");
            }
        }

        private void ddldate_ValueChanged(object sender, EventArgs e)
        {
            BindPurchase();
        }



        

       

       

    }
}

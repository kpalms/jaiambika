﻿namespace Restaurant.PL
{
    partial class Sale
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlpassword = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.txtpassword = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnok = new System.Windows.Forms.Button();
            this.pnlheader = new System.Windows.Forms.Panel();
            this.cancelItem = new System.Windows.Forms.Button();
            this.btnbillshow = new System.Windows.Forms.Button();
            this.dtpto = new System.Windows.Forms.DateTimePicker();
            this.dtpfrom = new System.Windows.Forms.DateTimePicker();
            this.btncancelBill = new System.Windows.Forms.Button();
            this.btnreprint = new System.Windows.Forms.Button();
            this.ddlbillno = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.grdsale = new System.Windows.Forms.DataGridView();
            this.label8 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.pnlpassword.SuspendLayout();
            this.pnlheader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdsale)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pnlpassword);
            this.panel1.Controls.Add(this.pnlheader);
            this.panel1.Controls.Add(this.grdsale);
            this.panel1.Location = new System.Drawing.Point(12, 31);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(948, 679);
            this.panel1.TabIndex = 0;
            // 
            // pnlpassword
            // 
            this.pnlpassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlpassword.Controls.Add(this.button1);
            this.pnlpassword.Controls.Add(this.txtpassword);
            this.pnlpassword.Controls.Add(this.label6);
            this.pnlpassword.Controls.Add(this.btnok);
            this.pnlpassword.Location = new System.Drawing.Point(525, 42);
            this.pnlpassword.Name = "pnlpassword";
            this.pnlpassword.Size = new System.Drawing.Size(161, 115);
            this.pnlpassword.TabIndex = 85;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(133, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(27, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "x";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtpassword
            // 
            this.txtpassword.Location = new System.Drawing.Point(27, 45);
            this.txtpassword.Name = "txtpassword";
            this.txtpassword.PasswordChar = '*';
            this.txtpassword.Size = new System.Drawing.Size(100, 20);
            this.txtpassword.TabIndex = 4;
            this.txtpassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtpassword_KeyDown);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(24, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Enter Password";
            // 
            // btnok
            // 
            this.btnok.Location = new System.Drawing.Point(44, 84);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(75, 23);
            this.btnok.TabIndex = 5;
            this.btnok.Text = "OK";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // pnlheader
            // 
            this.pnlheader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlheader.Controls.Add(this.cancelItem);
            this.pnlheader.Controls.Add(this.btnbillshow);
            this.pnlheader.Controls.Add(this.dtpto);
            this.pnlheader.Controls.Add(this.dtpfrom);
            this.pnlheader.Controls.Add(this.btncancelBill);
            this.pnlheader.Controls.Add(this.btnreprint);
            this.pnlheader.Controls.Add(this.ddlbillno);
            this.pnlheader.Controls.Add(this.label4);
            this.pnlheader.Location = new System.Drawing.Point(55, 45);
            this.pnlheader.Name = "pnlheader";
            this.pnlheader.Size = new System.Drawing.Size(819, 116);
            this.pnlheader.TabIndex = 86;
            // 
            // cancelItem
            // 
            this.cancelItem.BackColor = System.Drawing.Color.Red;
            this.cancelItem.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cancelItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelItem.Location = new System.Drawing.Point(454, 14);
            this.cancelItem.Name = "cancelItem";
            this.cancelItem.Size = new System.Drawing.Size(100, 23);
            this.cancelItem.TabIndex = 89;
            this.cancelItem.Text = "Cancel Item";
            this.cancelItem.UseVisualStyleBackColor = false;
            this.cancelItem.Click += new System.EventHandler(this.cancelItem_Click);
            // 
            // btnbillshow
            // 
            this.btnbillshow.BackgroundImage = global::Restaurant.Properties.Resources.button;
            this.btnbillshow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnbillshow.Location = new System.Drawing.Point(311, 15);
            this.btnbillshow.Name = "btnbillshow";
            this.btnbillshow.Size = new System.Drawing.Size(75, 23);
            this.btnbillshow.TabIndex = 88;
            this.btnbillshow.Text = "ShowBill";
            this.btnbillshow.UseVisualStyleBackColor = true;
            this.btnbillshow.Click += new System.EventHandler(this.btnbillshow_Click);
            // 
            // dtpto
            // 
            this.dtpto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpto.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpto.Location = new System.Drawing.Point(170, 15);
            this.dtpto.Name = "dtpto";
            this.dtpto.Size = new System.Drawing.Size(132, 20);
            this.dtpto.TabIndex = 87;
            // 
            // dtpfrom
            // 
            this.dtpfrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpfrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpfrom.Location = new System.Drawing.Point(25, 15);
            this.dtpfrom.Name = "dtpfrom";
            this.dtpfrom.Size = new System.Drawing.Size(132, 20);
            this.dtpfrom.TabIndex = 86;
            // 
            // btncancelBill
            // 
            this.btncancelBill.BackColor = System.Drawing.Color.Red;
            this.btncancelBill.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btncancelBill.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncancelBill.Location = new System.Drawing.Point(560, 14);
            this.btncancelBill.Name = "btncancelBill";
            this.btncancelBill.Size = new System.Drawing.Size(75, 23);
            this.btncancelBill.TabIndex = 58;
            this.btncancelBill.Text = "Cancel Bill";
            this.btncancelBill.UseVisualStyleBackColor = false;
            this.btncancelBill.Click += new System.EventHandler(this.btncancelBill_Click);
            // 
            // btnreprint
            // 
            this.btnreprint.BackgroundImage = global::Restaurant.Properties.Resources.button;
            this.btnreprint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnreprint.Location = new System.Drawing.Point(641, 14);
            this.btnreprint.Name = "btnreprint";
            this.btnreprint.Size = new System.Drawing.Size(75, 23);
            this.btnreprint.TabIndex = 75;
            this.btnreprint.Text = "RePrint Bill";
            this.btnreprint.UseVisualStyleBackColor = true;
            this.btnreprint.Click += new System.EventHandler(this.btnreprint_Click);
            // 
            // ddlbillno
            // 
            this.ddlbillno.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ddlbillno.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlbillno.FormattingEnabled = true;
            this.ddlbillno.Location = new System.Drawing.Point(526, 66);
            this.ddlbillno.Name = "ddlbillno";
            this.ddlbillno.Size = new System.Drawing.Size(142, 39);
            this.ddlbillno.TabIndex = 57;
            this.ddlbillno.SelectedIndexChanged += new System.EventHandler(this.ddlbillno_SelectedIndexChanged);
            this.ddlbillno.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ddlbillno_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(480, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 56;
            this.label4.Text = "BillNo";
            // 
            // grdsale
            // 
            this.grdsale.AllowUserToAddRows = false;
            this.grdsale.AllowUserToDeleteRows = false;
            this.grdsale.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdsale.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(253)))), ((int)(((byte)(244)))));
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.PeachPuff;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdsale.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.grdsale.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.Orange;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdsale.DefaultCellStyle = dataGridViewCellStyle10;
            this.grdsale.EnableHeadersVisualStyles = false;
            this.grdsale.Location = new System.Drawing.Point(55, 190);
            this.grdsale.MultiSelect = false;
            this.grdsale.Name = "grdsale";
            this.grdsale.ReadOnly = true;
            this.grdsale.RowHeadersVisible = false;
            this.grdsale.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdsale.Size = new System.Drawing.Size(818, 320);
            this.grdsale.TabIndex = 28;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(478, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(113, 24);
            this.label8.TabIndex = 87;
            this.label8.Text = "Sale Status";
            // 
            // Sale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(253)))), ((int)(((byte)(244)))));
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Sale";
            this.ShowInTaskbar = false;
            this.Text = "x";
            this.Load += new System.EventHandler(this.Sale_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Sale_KeyDown);
            this.panel1.ResumeLayout(false);
            this.pnlpassword.ResumeLayout(false);
            this.pnlpassword.PerformLayout();
            this.pnlheader.ResumeLayout(false);
            this.pnlheader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdsale)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView grdsale;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox ddlbillno;
        private System.Windows.Forms.Button btncancelBill;
        private System.Windows.Forms.Button btnreprint;
        private System.Windows.Forms.Panel pnlpassword;
        private System.Windows.Forms.TextBox txtpassword;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnok;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel pnlheader;
        private System.Windows.Forms.Button btnbillshow;
        private System.Windows.Forms.DateTimePicker dtpto;
        private System.Windows.Forms.DateTimePicker dtpfrom;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button cancelItem;

    }
}
﻿namespace Restaurant.PL
{
    partial class ChangeItemName
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btncancel = new System.Windows.Forms.Button();
            this.btnchage = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtchagep = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btncancel
            // 
            this.btncancel.BackgroundImage = global::Restaurant.Properties.Resources.button21;
            this.btncancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btncancel.Location = new System.Drawing.Point(157, 148);
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(75, 35);
            this.btncancel.TabIndex = 7;
            this.btncancel.Text = "Cancel";
            this.btncancel.UseVisualStyleBackColor = true;
            this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // btnchage
            // 
            this.btnchage.BackgroundImage = global::Restaurant.Properties.Resources.button21;
            this.btnchage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnchage.Location = new System.Drawing.Point(62, 148);
            this.btnchage.Name = "btnchage";
            this.btnchage.Size = new System.Drawing.Size(75, 35);
            this.btnchage.TabIndex = 6;
            this.btnchage.Text = "Change";
            this.btnchage.UseVisualStyleBackColor = true;
            this.btnchage.Click += new System.EventHandler(this.btnchage_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(59, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Change ItemName";
            // 
            // txtchagep
            // 
            this.txtchagep.BackColor = System.Drawing.Color.AntiqueWhite;
            this.txtchagep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtchagep.Location = new System.Drawing.Point(62, 92);
            this.txtchagep.Name = "txtchagep";
            this.txtchagep.Size = new System.Drawing.Size(170, 20);
            this.txtchagep.TabIndex = 4;
            this.txtchagep.TextChanged += new System.EventHandler(this.txtchagep_TextChanged);
            this.txtchagep.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtchagep_KeyDown);
            // 
            // ChangeItemName
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(253)))), ((int)(((byte)(244)))));
            this.ClientSize = new System.Drawing.Size(292, 266);
            this.Controls.Add(this.btncancel);
            this.Controls.Add(this.btnchage);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtchagep);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ChangeItemName";
            this.Text = "ChangeItemName";
            this.Load += new System.EventHandler(this.ChangeItemName_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btncancel;
        private System.Windows.Forms.Button btnchage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtchagep;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Restaurant.BL;
namespace Restaurant.PL
{
    public partial class RateAssign : Form
    {
        public RateAssign()
        {
            InitializeComponent();
        }
        BL_Worker bl_worker = new BL_Worker();
        int id;
        private void RateAssign_Load(object sender, EventArgs e)
        {
            BindRateCat();
            BindDepartment();
            Bind();
            btnupdate.Enabled = false;
            btndelete.Enabled = false;

        }

        private void BindRateCat()
        {
            DataTable dt = bl_worker.SelectRateCategary();
            DataRow row = dt.NewRow();
            row["RateCatName"] = "SELECT";
            dt.Rows.InsertAt(row, 0);
            comboratecat.DataSource = dt;
            comboratecat.ValueMember = "RateCatId";
            comboratecat.DisplayMember = "RateCatName";
        }
        private void BindDepartment()
        {
            DataTable dt = bl_worker.SelectDepartment();
            DataRow row = dt.NewRow();
            row["Department"] = "SELECT";
            dt.Rows.InsertAt(row, 0);
            combodepartment.DataSource = dt;
            combodepartment.DisplayMember = "Department";
            combodepartment.ValueMember = "DpId";


        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            if (combodepartment.Text == "SELECT")
            {
                MessageBox.Show("Please Select Department");
                combodepartment.Focus();

            }
            else if (comboratecat.Text == "SELECT")
             {
                 MessageBox.Show("Please Select RateCategary");
                 comboratecat.Focus();

             }
             else
             {
                 SetFill();
                 string returnvalue = bl_worker.CheckRateAssign(bl_worker);
                 if (returnvalue != "")
                 {
                     MessageBox.Show("This record already exists!");

                 }
                 else
                 {
                     bl_worker.RateAssignInsert(bl_worker);
                     Bind();
                 }
             }

        }

        private void Bind()
    {
        DataTable dt = bl_worker.SelectRateAssign();
        dgvrateassign.DataSource = dt;
        dgvrateassign.Columns["RateAId"].Visible = false;

    }
        private void Clear()
        {
            combodepartment.Text = "SELECT";
            comboratecat.Text = "SELECT";
        }

        private void SetFill()
        {
            bl_worker.DpId = Convert.ToInt32(combodepartment.SelectedValue.ToString());
            bl_worker.RateCatId = Convert.ToInt32(comboratecat.SelectedValue.ToString());
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            Clear();
            btnupdate.Enabled = false;
            btndelete.Enabled = false;
            btnsave.Enabled = true;
        }

        private void dgvrateassign_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
            if (dgvrateassign.Rows.Count > 0)
            {
                id = Convert.ToInt32(dgvrateassign.SelectedRows[0].Cells[0].Value.ToString());
                combodepartment.Text = dgvrateassign.SelectedRows[0].Cells[1].Value.ToString();
                comboratecat.Text = dgvrateassign.SelectedRows[0].Cells[2].Value.ToString();
                btnsave.Enabled = false;
                btnupdate.Enabled = true;
                btndelete.Enabled = true;
            }
            }
                catch( Exception ex)
            {
                MessageBox.Show("error occured!Please contact Administration?"+ ex);
            }

        }
      

        private void btnupdate_Click(object sender, EventArgs e)
        {
            if (combodepartment.Text == "SELECT")
            {
                MessageBox.Show("Please SELECT Department");
                combodepartment.Focus();

            }
            else if (comboratecat.Text == "SELECT")
            {
                MessageBox.Show("Please SELECT RateCategory");
                comboratecat.Focus();

            }
            else
            {
                SetFill();
                //string returnvalue = bl_worker.CheckRateAssign(bl_worker);
                //if (returnvalue != "")
                //{
                //    MessageBox.Show("This record already exists!");

                //}
                //else
                //{
                   
                    bl_worker.RateAId = id;
                    bl_worker.RateAssignUpdate(bl_worker);
                    Bind();
                    Clear();
                    btnsave.Enabled = true;
                    btnupdate.Enabled = false;
                    btndelete.Enabled = false;
                //}
            }
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {

                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Department?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_worker.RateAId = id;
                    bl_worker.RateAssignDelete(bl_worker);
                    Clear();
                    Bind();
                    btnsave.Enabled = true;
                    btnupdate.Enabled = false;
                    btndelete.Enabled = false;
                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
        }
    }
}

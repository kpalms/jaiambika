﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Restaurant.BL;
using Restaurant.DL;

namespace Restaurant.PL
{
    public partial class SupplierAccount : Form
    {
        public SupplierAccount()
        {
            InitializeComponent();
        }

        BL_Worker bl_worker = new BL_Worker();
        BL_Field bl_field = new BL_Field();
        int id;
        private void SupplierAccount_Load(object sender, EventArgs e)
        {
            BindSupplier();
            BindSupplierAccount();
            btndelete.Enabled = false;
            btnupdate.Enabled = false;
        }

        private void BindSupplier()
        {
            DataTable dt = bl_worker.SelectSuppliderName();
            DataRow row = dt.NewRow();

            row["SupplierName"] = "Select";
            dt.Rows.InsertAt(row, 0);
            combosupplier.DataSource = dt;
            combosupplier.DisplayMember = "SupplierName";
            combosupplier.ValueMember = "SupplierId";
        }

        private void BindSupplierAccount()
        {
            DataTable dt = bl_worker.SelectSupplierAccount();
            dgvsupplier.DataSource = dt;
            dgvsupplier.Columns["SupplierAId"].Visible = false;
             decimal debitamt = 0;
            decimal creditamt = 0;
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["Debit"].ToString() != "")
                    {
                        debitamt = debitamt + Convert.ToDecimal(dt.Rows[i]["Debit"].ToString());
                    }
                    if (dt.Rows[i]["Credit"].ToString() != "")
                    {
                        creditamt = creditamt + Convert.ToDecimal(dt.Rows[i]["Credit"].ToString());
                    }
                }
                lbldebit.Text = Convert.ToString(debitamt);
                lblcredit.Text = Convert.ToString(creditamt);
                lblremaningamount.Text = Convert.ToString(Convert.ToDecimal(lbldebit.Text) - Convert.ToDecimal(lblcredit.Text));

            }
        }


        private void BindSupplierAccountNamewise()
        {
            txtamount.Text = "";
            txtbillno.Text = "";
            if (combosupplier.Text == "Select")
            {
                lblcredit.Text = "00";
                lbldebit.Text = "00";
                lblremaningamount.Text = "00";
            }
            bl_field.SupplierId = combosupplier.SelectedValue.ToString();
            DataTable dt = bl_worker.SelectSupplierAccountNamewise(bl_field);
            dgvsupplier.DataSource = dt;
            dgvsupplier.Columns["SupplierAId"].Visible = false;
            decimal debitamt = 0;
            decimal creditamt = 0;
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["Debit"].ToString() != "")
                    {
                        debitamt = debitamt + Convert.ToDecimal(dt.Rows[i]["Debit"].ToString());
                    }
                    if (dt.Rows[i]["Credit"].ToString() != "")
                    {
                        creditamt = creditamt + Convert.ToDecimal(dt.Rows[i]["Credit"].ToString());
                    }
                }
                lbldebit.Text = Convert.ToString(debitamt);
                lblcredit.Text = Convert.ToString(creditamt);
                lblremaningamount.Text = Convert.ToString(Convert.ToDecimal(lbldebit.Text) - Convert.ToDecimal(lblcredit.Text));

            }
        }

        private void dgvsupplier_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            id = Convert.ToInt32(dgvsupplier.SelectedRows[0].Cells["SupplierAId"].Value.ToString());
            DTP.Text = dgvsupplier.SelectedRows[0].Cells["Date"].Value.ToString();
            combosupplier.Text = dgvsupplier.SelectedRows[0].Cells["SupplierAId"].Value.ToString();
           txtbillno.Text = dgvsupplier.SelectedRows[0].Cells["ReciptNo"].Value.ToString();
           txtamount.Text = dgvsupplier.SelectedRows[0].Cells["Credit"].Value.ToString();
          btndelete.Enabled = true;
          btnupdate.Enabled = true;
          btnsave.Enabled = false;


        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
            if (combosupplier.Text == "Select")
            {
                MessageBox.Show("Please Select Supplier");
            }
            //else if (txtbillno.Text == "")
            //{
            //    MessageBox.Show("Please Enter Bill No");
            //}
            else if (txtamount.Text == "")
            {
                MessageBox.Show("Please Enter Amount");
            }
            else
            {

                SetFill();
                bl_worker.SupplierAccount(bl_field);
                Clear();
                BindSupplierAccount();
            }
             }
            catch
            {
                MessageBox.Show("error occured!Please contact Administration?");
            }
        }

        private void SetFill()
        {
            bl_field.Date = DTP.Text;
            bl_field.SupplierId =  combosupplier.SelectedValue.ToString();
            bl_field.BillNo = "0";
            bl_field.ReciptNos = txtbillno.Text;
            bl_field.Debit = 0;
            bl_field.Credit = Convert.ToDecimal( txtamount.Text);

        }

        private void Clear()
        {
            combosupplier.Text = "Select";
            txtamount.Text = "";
            txtbillno.Text = "";
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            Clear();
            btndelete.Enabled = false;
            btnupdate.Enabled = false;
            btnsave.Enabled = true;


        }

        private void btnupdate_Click(object sender, EventArgs e)
        {
             try
            {
            if (combosupplier.Text == "Select")
            {
                MessageBox.Show("Please Select Supplier");
            }
            else if (txtbillno.Text == "")
            {
                MessageBox.Show("Please Enter Bill No");
            }
            else if (txtamount.Text == "")
            {
                MessageBox.Show("Please Enter Amount");
            }
            else
            {

                SetFill();
                bl_field.SupplierAId = id;
                bl_worker.SupplierAccountUpdate(bl_field);
                Clear();
                BindSupplierAccount();
                btnsave.Enabled = true;
            }
            }
             catch
             {

                 MessageBox.Show("error occured!Please contact Administration?");
             }
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
             try
            {
                
                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Record?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
            bl_field.SupplierAId = id;
            bl_worker.SupplierAccountDelete(bl_field);
            Clear();
            BindSupplierAccount();
            btnsave.Enabled = true;
                }
            }
             catch
             {

                 MessageBox.Show("error occured!Please contact Administration?");
             }
        }

        private void dgvsupplier_Click(object sender, EventArgs e)
        {

        }

        private void combosupplier_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (combosupplier.Text != "System.Data.DataRowView")
                {
                    if (combosupplier.ValueMember == "SupplierId")
                    {

                        BindSupplierAccountNamewise();
                        



                    }

                }

            }
            catch
            {

            }
        }

        private void txtbillno_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            //{
            //    e.Handled = true;
            //    MessageBox.Show("Please enter number only");
            //}
        }

        private void txtamount_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b' && e.KeyChar !='.')
            //{
            //    e.Handled = true;
            //    MessageBox.Show("Please enter number only");
            //}
            Common common = new Common();
            common.OneDecimalPoint(txtamount, e);
        }
    }
}

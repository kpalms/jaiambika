﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;


namespace Restaurant.PL
{
    public partial class Qty : Form
    {
        BL_Field bl_field = new BL_Field();
        BL_Sale bl_sale = new BL_Sale();
        main _mainForm;
        string _price = "";

        public Qty(string saleId, string que, string  price, main mainfrm)
        {
            _mainForm = mainfrm;
            InitializeComponent();
            this.KeyPreview = true;
            bl_field.SaleId = saleId;
            _price = price;
            txtqty.Text = que;
        }

        private void Qty_Load(object sender, EventArgs e)
        {
            
        }

        private void btnchangeqty_Click(object sender, EventArgs e)
        {
            try
            {
               
                    ChangeQty();
        

            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }


        }



        private void ChangeQty()
        {

            if (txtqty.Text == "" || Convert.ToInt32(txtqty.Text)<1)
            {
                MessageBox.Show("Please enter valid quantity?");
                txtqty.Focus();

            }
            else
            {
            bl_field.QtyS = Convert.ToInt32(txtqty.Text);
            decimal total = Convert.ToDecimal(_price) * Convert.ToDecimal(txtqty.Text);
            bl_field.Total = total;

            bl_sale.UpdateQty(bl_field);
            this.Hide();
            _mainForm.PageRefreshEvent();
            }
          
        }



        private void txtqty_KeyPress(object sender, KeyPressEventArgs e)
        {
         
            if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            {
                e.Handled = true;
            }
        }

        private void Qty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void txtqty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ChangeQty();
            }
        }
    }
}

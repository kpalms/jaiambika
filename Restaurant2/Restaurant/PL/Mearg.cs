﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;


namespace Restaurant.PL
{
    public partial class Mearg : Form
    {
        main main;
        public Mearg(main _main)
        {
            InitializeComponent();
            main = _main;
        }
        BL_Merge bl_merge = new BL_Merge();
        private void btnok_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text == "SELECT")
            {
                MessageBox.Show("Please Select Table No");
            }
            else if (comboBox2.Text == "SELECT")
            {
                MessageBox.Show("Please Select Table No");
            }
            else
            {
                SetFill();
                bl_merge.UpdateTableName(bl_merge);
                main.BindTable();
                MessageBox.Show("Merge Successfully");
                this.Close();
            }
        }

        private void Mearg_Load(object sender, EventArgs e)
        {
            SelectTable();
            SelectTable1();
        }

        private void SelectTable()
        {
            DataTable dt = bl_merge.SelectTableName();
            DataRow row = dt.NewRow();
            row["TableName"] = "SELECT";
            dt.Rows.InsertAt(row, 0);
            comboBox1.DataSource = dt;
            comboBox1.DisplayMember = "TableName";
            if (dt.Rows.Count == 2)
            {
                MessageBox.Show("Not Minimum Records to Merge");
                this.Close();
            }
        }

        private void SelectTable1()
        {
            DataTable dt = bl_merge.SelectTableName();
            DataRow row = dt.NewRow();
            row["TableName"] = "SELECT";
            dt.Rows.InsertAt(row, 0);
            comboBox2.DataSource = dt;
            comboBox2.DisplayMember = "TableName";
        }

        private void SetFill()
        {
           string s = comboBox1.Text;
            string[] words = s.Split(' ');
            bl_merge.TableNo =Convert.ToInt32( words[0]);
           bl_merge.Seat = words[1];


            string s1 = comboBox2.Text;
            string[] words1 = s1.Split(' ');
            bl_merge.TableNo1= Convert.ToInt32( words1[0]);
            bl_merge.Seat1 = words1[1];
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

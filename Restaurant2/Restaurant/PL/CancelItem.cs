﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;

namespace Restaurant.PL
{
    public partial class CancelItem : Form
    {
        main _mainForm;
        BL_Field bl_field = new BL_Field();
        BL_Sale bl_sale = new BL_Sale();

        string _saleid;

        public CancelItem(string SaleId,string TableNo, string kotnobot, string CashierName, string ItemName, string curqty, string rate, string amount,main mainfrm)
        {
            InitializeComponent();

            _mainForm = mainfrm;
            this.KeyPreview = true;
            lbltableno.Text = TableNo;
            lblkotbot.Text = kotnobot;
            lblcashier.Text = CashierName;
            lblitemname.Text = ItemName;
            lblcurrentqty.Text = curqty;
            lblrate.Text = rate;
            lblamount.Text = amount;
            _saleid = SaleId;

        }

        private void CancelItem_Load(object sender, EventArgs e)
        {
            lblmsg.Visible = false;

            

        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            if (txtresion.Text == "")
            {
                MessageBox.Show("Please enter Resion?");
                txtresion.Focus();
            }
            else
            {
                SetField();
                bl_sale.InsertCancelBill(bl_field);
                if (Convert.ToInt32(lblcurrentqty.Text) > 1)
                {
                    bl_field.SaleId = _saleid;
                    int qt = Convert.ToInt32(lblcurrentqty.Text) - 1;
                    bl_field.QtyS = Convert.ToInt32(qt);
                    bl_sale.UpdateQty(bl_field);
                    _mainForm.PageRefreshEvent();
                }
                else
                {
                    bl_field.SaleId = _saleid;
                    bl_sale.CancelSaleItem(bl_field);
                    _mainForm.PageRefreshEvent();
                }
                panel1.Visible = false;
                lblmsg.Visible = true;
                lblmsg.Text = "Item Canceled Successfully";

            }
        }

        private void SetField()
        {
            bl_field.Date = bl_field.GlobalDate;
            bl_field.TableNo = lbltableno.Text;
            bl_field.KotOrderNo = lblkotbot.Text;
            bl_field.CashierName = lblcashier.Text;
            bl_field.ItemName = lblitemname.Text;
            bl_field.Price = lblrate.Text;
            bl_field.Qty = lblqty.Text;
            bl_field.Total = Convert.ToDecimal(lblrate.Text);
            bl_field.Resion = txtresion.Text;
       
         
        }

        private void CancelItem_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

       
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Restaurant.BL;

namespace Restaurant.PL
{



    public partial class EmailSetting : Form
    {


        BL_Field bl_field = new BL_Field();
        BL_SmsEmail bl_smsemail = new BL_SmsEmail();

        public EmailSetting()
        {
            InitializeComponent();
        }

        private void EmailSetting_Load(object sender, EventArgs e)
        {
            BindHotelSetting();
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                if (btnsave.Text == "Save")
                {
                    if (txtemailid.Text == "")
                    {

                        MessageBox.Show("Please enter emailid?");

                    }
                    else if (txtpass.Text == "")
                    {
                        MessageBox.Show("Please enter password?");
                    }
                    else if (txthost.Text == "")
                    {
                        MessageBox.Show("Please enter host?");
                    }
                    else if (txtport.Text == "")
                    {
                        MessageBox.Show("Please enter port?");
                    }
                    else
                    {
                        SetField();
                        bl_smsemail.Insertemail(bl_field);
                        Clear();
                        MessageBox.Show("Record inserted Successfully");
                        BindHotelSetting();
                    }
                }
           else
                {
                    SetField();
                    bl_smsemail.Updateemail(bl_field);

                    MessageBox.Show("Record Updated Successfully");
                    BindHotelSetting();
                }
            }
            catch
            {
                MessageBox.Show("error occured!Please contact Administration?");

            }
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Email Id?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_smsemail.Deleteemail(bl_field);
                    Clear();
                    MessageBox.Show("Record Delete Successfully");
                    BindHotelSetting();

                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
        }

        private void btnclose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SetField()
        {
            bl_field.Email = txtemailid.Text;
            bl_field.Password = txtpass.Text;
            bl_field.Host = txthost.Text;
            bl_field.Port = txtport.Text;


        }
        private void Clear()
        {
            txtemailid.Text = "";
            txtpass.Text = "";
            txthost.Text = "";
            txtport.Text="";
        }

        private void BindHotelSetting()
        {
            DataTable dt = bl_smsemail.Selectemail();
            if (dt.Rows.Count > 0)
            {
                bl_field.EmailId =dt.Rows[0][0].ToString();
                txtemailid.Text = dt.Rows[0][1].ToString();
                txtpass.Text = dt.Rows[0][2].ToString();
                txthost.Text = dt.Rows[0][3].ToString();
                txtport.Text = dt.Rows[0][4].ToString();
                btnsave.Text = "Update";
            }
            else
            {
                btnsave.Text = "Save";
                Clear();
            }

        }

        private void EmailSetting_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }


    }
}

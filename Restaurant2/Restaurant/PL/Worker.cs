﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;



namespace Restaurant.PL
{
    public partial class Worker : Form
    {
        BL_Worker bl_worker = new BL_Worker();
        BL_Field bl_field = new BL_Field();
        public Worker()
        {
            InitializeComponent();
            this.KeyPreview = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                
                    string email = txtemail.Text;
                    if (txtname.Text == "")
                    {
                         MessageBox.Show("Please enter Name");

                    }
                    else if (txtphone.Text != "" && txtphone.Text.Length < 10)
                    {
                        MessageBox.Show("Invalid Phone No.");
                        txtphone.Focus();

                    }
                    else if (txtmobileno.Text != "" && txtmobileno.Text.Length < 10)
                    {
                        MessageBox.Show("Invalid Mobile No.");
                        txtmobileno.Focus();

                    }
                    else if (txtemail.Text != "" && (email.IndexOf('@') == -1 || email.IndexOf('.') == -1))
                    {
                        MessageBox.Show("Invalid Email Id");
                        txtemail.Focus();

                    }
                    else if (txtsalary.Text == "")
                    {
                        MessageBox.Show("Please Enter Salary");
                        txtsalary.Focus();

                    }
                    else if (txtusername.Text == "")
                    {
                        MessageBox.Show("Please Enter UserName");
                        txtusername.Focus();
                    }
                    else if (txtpassword.Text == "")
                    {
                        MessageBox.Show("Please Enter Password");
                        txtpassword.Focus();
                    }
                    else if (ddlsecurty.Text == "Select")
                    {
                        MessageBox.Show("Please Select Security Role");
                        ddlsecurty.Focus();
                    }



                    else
                    {
                        SetField();
                        bl_worker.InsertWorker(bl_field);
                        MessageBox.Show("Records Added successfully");
                        ClearField();
                        BindWorker();
                    }
                                
            }
            catch
            {
              
                MessageBox.Show("error occured!Please contact Administration?");
            }

        }
        private void SetField()
        {
            bl_field.Name = txtname.Text;
            bl_field.MiddleName = txtmiddlename.Text;
            bl_field.LastName = txtlastname.Text;
            bl_field.PhoneNo = txtphone.Text;
            bl_field.Mobile = txtmobileno.Text;
            bl_field.Email = txtemail.Text;
            bl_field.Salary = txtsalary.Text;
            bl_field.Country = txtcountry.Text;
            bl_field.State = txtstate.Text;
            bl_field.City = txtcity.Text;
            bl_field.Address = txtaddress.Text;
            bl_field.Role = ddlsecurty.Text;
            bl_field.Username = txtusername.Text;
            bl_field.Password = txtpassword.Text;

        }
        private void ClearField()
        {
                txtname.Text="";
                txtmiddlename.Text="";
                txtlastname.Text="";
                txtphone.Text  ="";
                txtmobileno.Text="";
                txtemail.Text="";
                txtcountry.Text="";
                txtstate.Text="";
                txtcity.Text="";
                txtaddress.Text="";
                ddlsecurty.Text = "";
                txtusername.Text = "";
                txtpassword.Text = "";
                txtsalary.Text = "";

                ddlsecurty.Text = "Select";

                btndelete.Enabled = false;
                btnUpdate.Enabled = false;
                button1.Enabled = true;

        }
        private void Worker_Load(object sender, EventArgs e)
        {
            BindWorker();
            ddlsecurty.Items.Insert(0, "Select");
            ddlsecurty.SelectedIndex = 0;
            btndelete.Enabled = false;
            btnUpdate.Enabled=false;
                    

        }

        private void grdworker_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                bl_field.WorkerId = grdworker.SelectedRows[0].Cells[0].Value.ToString();
                txtname.Text = grdworker.SelectedRows[0].Cells[1].Value.ToString();
                txtmiddlename.Text = grdworker.SelectedRows[0].Cells[2].Value.ToString();
                txtlastname.Text = grdworker.SelectedRows[0].Cells[3].Value.ToString();
                txtphone.Text = grdworker.SelectedRows[0].Cells[4].Value.ToString();
                txtmobileno.Text = grdworker.SelectedRows[0].Cells[5].Value.ToString();
                txtemail.Text = grdworker.SelectedRows[0].Cells[6].Value.ToString();
                txtsalary.Text = grdworker.SelectedRows[0].Cells[7].Value.ToString();
                txtcountry.Text = grdworker.SelectedRows[0].Cells[8].Value.ToString();
                txtstate.Text = grdworker.SelectedRows[0].Cells[9].Value.ToString();
                txtcity.Text = grdworker.SelectedRows[0].Cells[10].Value.ToString();
                txtaddress.Text = grdworker.SelectedRows[0].Cells[11].Value.ToString();
                ddlsecurty.Text = grdworker.SelectedRows[0].Cells[12].Value.ToString();
                txtusername.Text = grdworker.SelectedRows[0].Cells[13].Value.ToString();
                txtpassword.Text = grdworker.SelectedRows[0].Cells[14].Value.ToString();
                btnUpdate.Enabled=true;
                btndelete.Enabled = true;
                button1.Enabled = false;
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }


        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Worker?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_worker.DeleteWorker(bl_field);
                    ClearField();
                    MessageBox.Show("Record Delete Successfully");
                    BindWorker();
                    txtname.Focus();
                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }


        }
        private void BindWorker()
        {
            DataTable dt = bl_worker.SelectWorker();
            grdworker.DataSource = dt;
            grdworker.Columns["WorkerId"].Visible = false;
            grdworker.Columns["Password"].Visible = false;
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            ClearField();
            txtname.Focus();

            btndelete.Enabled = false;
            btnUpdate.Enabled = false;
            button1.Enabled = true;
   

        }

        private void txtphone_KeyPress(object sender, KeyPressEventArgs e)
        {
            Common common = new Common();
            common.NumberOnly(e);
            txtphone.MaxLength = 10;
        }

        private void Worker_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void txtmobileno_KeyPress(object sender, KeyPressEventArgs e)
        {
            Common common = new Common();
            common.NumberOnly(e);
            txtmobileno.MaxLength = 10;

        }


        private void txtsalary_KeyPress(object sender, KeyPressEventArgs e)
        {
            Common common = new Common();
            common.OneDecimalPoint(txtsalary, e);
        }

      private void btnUpdate_Click(object sender, EventArgs e)
        {
           
                string email = txtemail.Text;
                if (txtname.Text == "")
                {
                    MessageBox.Show("Please enter name");
                }
                else if (txtphone.Text != "" && txtphone.Text.Length < 10)
                {
                    MessageBox.Show("Invalid Phone No.");
                    txtphone.Focus();


                }
                else if (txtmobileno.Text != "" && txtmobileno.Text.Length < 10)
                {
                    MessageBox.Show("Invalid Mobile No.");
                    txtmobileno.Focus();

                }
                else if (txtemail.Text != "" && (email.IndexOf('@') == -1 || email.IndexOf('.') == -1))
                {
                    MessageBox.Show("Invalid Email Id");
                    txtemail.Focus();

                }
                else if (txtsalary.Text == "")
                {
                    MessageBox.Show("Please Enter Salary");
                    txtsalary.Focus();

                }
                else
                {

                    SetField();
                    bl_worker.UpdateWorker(bl_field);
                    MessageBox.Show("Records Updated successfully");
                    ClearField();
                    BindWorker();
                    txtname.Focus();
                }
            }
        }
}

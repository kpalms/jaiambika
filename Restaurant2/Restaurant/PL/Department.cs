﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using Restaurant.BL;
namespace Restaurant.PL
{
    public partial class Department : Form
    {
        public Department()
        {
            InitializeComponent();
        }
        BL_Worker bl_worker = new BL_Worker();
        int id;
        private void btnsave_Click(object sender, EventArgs e)
        {
            if (txtdepartment.Text == "")
            {
                MessageBox.Show("Please Enter Department");
                txtdepartment.Focus();

            }
            else
            {
                bl_worker.Department = txtdepartment.Text;
                bl_worker.Sequence = Convert.ToInt32(txtSequence.Text);
                  string returnvalue = bl_worker.CheckDepartment(bl_worker);

                    if (returnvalue == txtdepartment.Text)
                    {
                        MessageBox.Show("This record already exists!");
                        txtdepartment.Text = "";
                        txtdepartment.Focus();
                    }
                    else
                    {
                        
                        bl_worker.DepartmentInsert(bl_worker);
                        txtdepartment.Text = "";
                        Bind();
                    }
            }
        }

        private void Bind()
        {
            DataTable dt = bl_worker.SelectDepartment();
            dgvdepartment.DataSource = dt;
            dgvdepartment.Columns["DpId"].Visible = false;

        }

        private void Department_Load(object sender, EventArgs e)
        {
            Bind();
            btnupdate.Enabled = false;
            btndelete.Enabled = false;
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            txtdepartment.Text = "";
            btnupdate.Enabled = false;
            btndelete.Enabled = false;
            btnsave.Enabled = true;
        }

        private void dgvdepartment_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvdepartment.Rows.Count > 0)
            {
                id = Convert.ToInt32(dgvdepartment.SelectedRows[0].Cells[0].Value.ToString());
                txtdepartment.Text = dgvdepartment.SelectedRows[0].Cells[1].Value.ToString();
                txtSequence.Text = dgvdepartment.SelectedRows[0].Cells[2].Value.ToString();
                btnsave.Enabled = false;
                btnupdate.Enabled = true;
                btndelete.Enabled = true;
            }
        }

        private void btnupdate_Click(object sender, EventArgs e)
        {
            if (txtdepartment.Text == "")
            {
                MessageBox.Show("Please Enter Department");
                txtdepartment.Focus();

            }
            else
            {
                bl_worker.Department = txtdepartment.Text;
                  //string returnvalue = bl_worker.CheckDepartment(bl_worker);

                  //if (returnvalue == txtdepartment.Text)
                  //{
                  //    MessageBox.Show("This record already exists!");
                  //    txtdepartment.Text = "";
                  //    txtdepartment.Focus();
                  //}
                  //else
                  //{
                      bl_worker.DpId = id;
                      bl_worker.Sequence = Convert.ToInt32(txtSequence.Text);
                      bl_worker.DepartmentUpdate(bl_worker);
                      txtdepartment.Text = "";
                      txtSequence.Text = "";
                      Bind();
                      btnsave.Enabled = true;
                      btnupdate.Enabled = false;
                      btndelete.Enabled = false;
                  //}
            }
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {

                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Department?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_worker.DpId = id;
                    bl_worker.DepartmentDelete(bl_worker);
                    txtdepartment.Text = "";
                    txtSequence.Text = "";
                    Bind();
                    btnsave.Enabled = true;
                    btnupdate.Enabled = false;
                    btndelete.Enabled = false;
                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
        }

        private void txtdepartment_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = Char.ToUpper(e.KeyChar);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using Restaurant.PL.Reports;
using System.Collections;



namespace Restaurant.PL
{
    
    public partial class Recipie : Form
    {
        Common common = new Common();
        BL_Catagory bl_catagory = new BL_Catagory();
        BL_NewItem bl_newitem = new BL_NewItem();
        BL_Field bl_field = new BL_Field();
        string id = "";
        public Recipie()
        {
            InitializeComponent();
            this.KeyPreview = true;
        }

        private void Recipie_Load(object sender, EventArgs e)
        {
            btndelete.Enabled = false;
            btnUpdate.Enabled = false;
            common.BindCatagory(ddlcatagory);
            common.BindUnitName(ddlunit);
            BindItemName();
            //BindNewItem();
           // BindUnitName();
            BindRecipeName();
           
       
        }



        private void ddlcatagory_SelectedIndexChanged(object sender, EventArgs e)
        {
            string catid = ddlcatagory.SelectedValue.ToString();
            BindItemName(catid);
        }


        private void BindItemName(string catid)
        {
            if (ddlcatagory.Text != "System.Data.DataRowView")
            {
                if (catid != "System.Data.DataRowView")
                {
                    DataTable dt = bl_catagory.SelectSubCatagory(catid);
                    DataRow row = dt.NewRow();
                    row["ItemName"] = "Select";
                    dt.Rows.InsertAt(row, 0);
                    ddlsubcat.DataSource = dt;
                    ddlsubcat.ValueMember = "SubCatId";
                    ddlsubcat.DisplayMember = "ItemName";
                }
            }
 
        }



        private void btnadd_Click(object sender, EventArgs e)
        {
            try
            {
                    if (ddlcatagory.Text == "Select")
                    {
                        MessageBox.Show("Please select Catagory?");
                    }
                    else if (ddlsubcat.Text == "Select")
                    {
                        MessageBox.Show("Please select SubCategory?");
                    }
                    else if(cboitemname.Text=="Select")
                    {
                        MessageBox.Show("Please select Item Name?");
                    }
                    else if (ddlunit.Text == "Select")
                    {
                        MessageBox.Show("Please select Unit?");
                    }
                    else if (txtqty.Text == "" || Convert.ToInt32(txtqty.Text)<=0)
                    {
                        MessageBox.Show("Please Enter Qty?");
                    }
                    else
                    {
                        SetField();
                        string name = bl_newitem.CheckIngradient(bl_field);
                        if (name == "")
                        {
                            bl_newitem.InsertRecipe(bl_field);
                        }
                        else 

                        {
                            MessageBox.Show("ingredients already exist"); 
                        }
                        BindRecipeName();
                        BindRecipeList();
                        
                        Clear();
                        MessageBox.Show("Record inserted Successfully");
                    }
                }

            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
 
        }


        private void SetField()
        {
            bl_field.CatId = ddlcatagory.SelectedValue.ToString();
            bl_field.SubcatId = ddlsubcat.SelectedValue.ToString();
            bl_field.ItemId = cboitemname.SelectedValue.ToString();
            bl_field.UnitId = ddlunit.SelectedValue.ToString();
            bl_field.Qty = txtqty.Text;

        }

        private void Clear()
        {
            cboitemname.Text = "Select";
            txtqty.Text = "";
            ddlunit.Text = "Select";

            btndelete.Enabled = false;
            btnUpdate.Enabled = false;
            btnadd.Enabled=true;

        }


        private void btncancel_Click(object sender, EventArgs e)
        {
            Clear();
            btndelete.Enabled = false;
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Ingredient?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_field.RecipeId = Convert .ToInt32 (dataGridView1.SelectedRows[0].Cells[0].Value.ToString());
                    bl_newitem .DeleteRecipe (bl_field);                    
                    BindRecipeList();
                    BindRecipeName();
                    btnadd.Text = "";
                    Clear();
                    MessageBox.Show("Record Delete Successfully");
                   

                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }

        }

        private void Recipie_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void BindRecipeName()
        {
            DataTable dt = bl_newitem.SelectRecipeName();
            if (dt.Rows.Count > 0)
            {
                lstrecipiename.DataSource = dt;
                lstrecipiename.DisplayMember = "SubCatName";
                lstrecipiename.ValueMember ="SubCatId";
            }
            else
            {
                lstrecipiename.DataSource = null;
            } 
        }

        private void lstrecipiename_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lstrecipiename_Click(object sender, EventArgs e)
        {
            BindRecipeList();
            BindRecord();
        }
        private void BindRecord()
        {
            DataTable dt = bl_newitem.SelectRecipeById(bl_field);
            if (dt.Rows.Count > 0)
            {
                ddlcatagory.Text = "";
                ddlsubcat.Text = "";
                cboitemname.Text = "Select";
                ddlunit.Text = "Select";
                txtqty.Text = "";


                ddlcatagory.SelectedText = dataGridView1.SelectedRows[0].Cells["CatagoryName"].Value.ToString();
               ddlsubcat.SelectedText = dataGridView1.SelectedRows[0].Cells["Dish"].Value.ToString();
               //cboitemname.SelectedText = dataGridView1.SelectedRows[0].Cells["ItemNames"].Value.ToString();
               //ddlunit.SelectedText = dataGridView1.SelectedRows[0].Cells["UnitName"].Value.ToString();
               //txtqty.Text = dataGridView1.SelectedRows[0].Cells["Qty"].Value.ToString();
            }
        }

        private void BindRecipeList()
        {

            if (lstrecipiename.Text != "System.Data.DataRowView")
            {
                if (lstrecipiename.SelectedIndex!=-1)
                {
                    bl_field.SubcatId = lstrecipiename.SelectedValue.ToString();
                    DataTable dt = bl_newitem.SelectRecipeById(bl_field);
                    if (dt.Rows.Count > 0)
                    {
                        dataGridView1.DataSource = dt;
                        dataGridView1.Columns["RecipeId"].Visible = false;
                       dataGridView1.Columns["CatagoryName"].Visible = false;
                    }
                    else
                    {
                        dataGridView1.DataSource = null;
                    }
                }


            }
          
        }

        //private void txtitemname_TextChanged(object sender, EventArgs e)
        //{
        //    if (txtitemname.Focus())
        //    {
        //        if (txtitemname.Text != "")
        //        {
        //            bl_field.TextSearch = txtitemname.Text;
        //            DataTable dt = bl_newitem.SearchNewItemBySearch(bl_field);
        //            if (dt.Rows.Count > 0)
        //            {
        //                grdItem.Visible = true;
        //                grdItem.DataSource = dt;
        //                grdItem.Columns["ItemId"].Visible = false;
        //                grdItem.Columns["Rate"].Visible = false;
   
        //            }
        //        }
        //        else
        //        {
        //            DataTable dt = bl_newitem.SearchNewItemAll();
        //            if (dt.Rows.Count > 0)
        //            {
        //                grdItem.Visible = true;
        //                grdItem.DataSource = dt;
        //                grdItem.Columns["ItemId"].Visible = false;
        //                grdItem.Columns["Rate"].Visible = false;
        //                //grdItem.Columns["Inventory"].Visible = false;
        //                //grdItem.Columns["PrimeryId"].Visible = false;
        //                //grdItem.Columns["SeconderyId"].Visible = false;
        //                //grdItem.Columns["ConversionUnit"].Visible = false;

        //            }
        //        }
        //    }
        //    else 
        //    {
        //        grdItem.Visible = false;
        //    }
        //}

        private void BindItemName()
        {
            DataTable dt = bl_newitem.SearchNewItemAll();
            DataRow row = dt.NewRow();
            row["ItemNames"] = "Select";
            dt.Rows.InsertAt(row, 0);
            cboitemname.DataSource = dt;
            cboitemname.DisplayMember = "ItemNames";
            cboitemname.ValueMember = "ItemId";

        }

        //private void txtitemname_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyCode == Keys.Enter)
        //    {

        //        if (grdItem.Rows.Count > 0)
        //        {

        //            txtitemname.Text = grdItem.SelectedRows[0].Cells[1].Value.ToString();
        //            bl_field.ItemId = grdItem.SelectedRows[0].Cells["ItemId"].Value.ToString();

        //            grdItem.Hide();
        //            ddlunit.Focus();
                   
        //        }
        //        else
        //        {

        //            grdItem.Visible = false;
        //            txtitemname.Focus();
        //        }

        //    }

        //    if (e.KeyCode == Keys.Up)
        //    {
        //        DataTable dtTemp = grdItem.DataSource as DataTable;


        //        object[] arr = dtTemp.Rows[dtTemp.Rows.Count - 1].ItemArray;
        //        for (int i = dtTemp.Rows.Count - 2; i >= 0; i--)
        //        {
        //            dtTemp.Rows[i + 1].ItemArray = dtTemp.Rows[i].ItemArray;
        //        }
        //        dtTemp.Rows[0].ItemArray = arr;



        //    }

        //    if (e.KeyCode == Keys.Down)
        //    {

        //        DataTable dtTemp = grdItem.DataSource as DataTable;

        //        object[] arr = dtTemp.Rows[0].ItemArray;
        //        for (int i = 1; i < dtTemp.Rows.Count; i++)
        //        {
        //            dtTemp.Rows[i - 1].ItemArray = dtTemp.Rows[i].ItemArray;
        //        }
        //        dtTemp.Rows[dtTemp.Rows.Count - 1].ItemArray = arr;



        //    }

        //}

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Rows.Count>0)
            {
                id = dataGridView1.SelectedRows[0].Cells["RecipeId"].Value.ToString();
                ddlcatagory.Text = dataGridView1.SelectedRows[0].Cells["CatagoryName"].Value.ToString();
                ddlsubcat.Text = dataGridView1.SelectedRows[0].Cells["Dish"].Value.ToString();
                cboitemname.Text = dataGridView1.SelectedRows[0].Cells["ItemNames"].Value.ToString();
                //grdItem.Visible = false;
                ddlunit.Text = dataGridView1.SelectedRows[0].Cells["UnitName"].Value.ToString();
                txtqty.Text = dataGridView1.SelectedRows[0].Cells["Qty"].Value.ToString();
                btndelete.Enabled = true;
                btnUpdate.Enabled=true;
                btnadd.Enabled = false;
              
               

            }
        }

        private void ddlcatagory_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ddlsubcat.Focus();                

            }

        }

        private void ddlsubcat_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                cboitemname.Focus();

            }
        }

        private void ddlunit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
              txtqty.Focus();

            }
        }

        private void txtqty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnadd.Focus();

            }
        }

        private void txtitemname_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = Char.ToUpper(e.KeyChar);
        }

        private void txtqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.' && (e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            //{
            //    e.Handled = true;
            //}

            //if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            //{
            //    e.Handled = true;
            //}
            common.OneDecimalPoint(txtqty,e);



        }

        private void cboitemname_KeyPress(object sender, KeyPressEventArgs e)
        {
            common.ComboBoxCheckValue(cboitemname, e);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlcatagory.Text == "Select")
                {
                    MessageBox.Show("Please select Catagory?");
                }
                else if (ddlsubcat.Text == "Select")
                {
                    MessageBox.Show("Please select SubCategory?");
                }
                else if (cboitemname.Text == "Select")
                {
                    MessageBox.Show("Please select Item Name?");
                }
                else if (ddlunit.Text == "Select")
                {
                    MessageBox.Show("Please select Unit?");
                }
                else if (txtqty.Text == "")
                {
                    MessageBox.Show("Please Enter Qty?");
                }
                else
                {
                    SetField();
                    bl_field.RecipeId = Convert.ToInt32(id);
                    bl_newitem.UpdateRecipe(bl_field);
                    BindRecipeName();
                    BindRecipeList();
                    Clear();
                    MessageBox.Show("Record Updated Successfully");
                }

            }

                catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void cboitemname_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ddlunit.Focus();

            }
        }
       

        private void ddlcatagory_KeyPress(object sender, KeyPressEventArgs e)
        {
            common.ComboBoxCheckValue(ddlcatagory, e);
        }

        private void ddlsubcat_KeyPress(object sender, KeyPressEventArgs e)
        {
            common.ComboBoxCheckValue(ddlsubcat, e);
        }

        private void ddlunit_KeyPress(object sender, KeyPressEventArgs e)
        {
            common.ComboBoxCheckValue(ddlunit, e);
        }
    }
}

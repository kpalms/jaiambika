﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using Restaurant.BL;
using Restaurant.DL;

namespace Restaurant.PL
{
    public partial class CreateItem : Form
    {
        BL_Field bl_field = new BL_Field();
        BL_NewItem bl_newitem = new BL_NewItem();

        Common commmon = new Common();


        public CreateItem()
        {
            InitializeComponent();
            this.KeyPreview = true;

        }

        private void CreateItem_Load(object sender, EventArgs e)
        {
            BindNewItem();
            btndelete.Enabled = false;
            btnUpdate.Enabled = false;

        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            try
            {
                    bl_field.ItemName = txtname.Text;
                    string name = bl_newitem.CheckItemName(bl_field);
                    if (name == "")
                    {
                        SetField();
                        bl_newitem.InsertNewItem(bl_field);
                        BindNewItem();
                        Clear();
                        MessageBox.Show("Record inserted Successfully");
                        txtname.Focus();
                    }

                    else
                    {
                        MessageBox.Show("This Item already exist!");
                    }

                }
            catch
            {
                MessageBox.Show("error occured!Please contact Administration?");
            }
        }

        private void Clear()
        {
            txtname.Text = "";
            txtRate.Text = "";
            btndelete.Enabled = false;
            btnUpdate.Enabled = false;
            btnadd.Enabled = true;

        }

        private void SetField()
        {
            bl_field.ItemName = txtname.Text;
            if (txtRate.Text != "")
            {
                bl_field.PurchasingRate = txtRate.Text;
            }
            else
            {
                bl_field.PurchasingRate = "0";
            }

        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            Clear();
            //commmon.BindUnitName(ddlPunit);
            //commmon.BindUnitName(ddlSUnit);


        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Item?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_newitem.DeleteNewItem(bl_field);
                    BindNewItem();
                    //commmon.BindUnitName(ddlPunit);
                    //commmon.BindUnitName(ddlSUnit);
                    Clear();
                    MessageBox.Show("Record Delete Successfully");

                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            bl_field.ItemId = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            txtname.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            txtRate.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();

            //ddlPunit.Text = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
            //ddlSUnit.Text = dataGridView1.SelectedRows[0].Cells[4].Value.ToString();
            //txtconversion.Text = dataGridView1.SelectedRows[0].Cells[5].Value.ToString();
            //int inventory = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[6].Value.ToString());
            //if (inventory == 1)
            //{
            //    checkBox1.Checked = true;
            //}
            //else
            //{
            //    checkBox1.Checked = false;
            //}
            btndelete.Enabled = true;
            btnUpdate.Enabled=true;
            btnadd.Enabled = false;
        }
        private void BindNewItem()
        {
            DataTable dt = bl_newitem.SelectNewItem();
            if (dt.Rows.Count > 0)
            {
                dataGridView1.DataSource = dt;
                dataGridView1.Columns["ItemId"].Visible = false;
                //dataGridView1.Columns["Inventory"].Visible = false;
            }
            else
            {
                dataGridView1.DataSource = null;
            }
        }

        private void CreateItem_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //bl_field.ItemIdStatic = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            // bl_field.ItemNamesStatic = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            // bl_field.Inventory = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
            //bl_field.ClientClick = "Purchase";
            // this.Close();
            //  _purchasing.CheckData();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            //if (checkBox1.Checked == true)
            //{
            //    pnlinventory.Visible = true;
            //}
            //else {
            //    pnlinventory.Visible = false;
            //}
        }

        private void txtname_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = Char.ToUpper(e.KeyChar);
        }

        private void txtname_TextChanged(object sender, EventArgs e)
        {
            txtname.CharacterCasing = CharacterCasing.Upper;
        }

        private void txtconversion_TextChanged(object sender, EventArgs e)
        {
            //commmon.NumberOnly(e);
        }

        private void txtconversion_KeyPress(object sender, KeyPressEventArgs e)
        {
            commmon.NumberOnly(e);
        }

        private void txtRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.' && (e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            //{
            //    e.Handled = true;
            //}

            //// only allow one decimal point

            //if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            //{
            //    e.Handled = true;
            //}
            Common common = new Common();
            common.OneDecimalPoint(txtRate, e);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                {

                    SetField();
                    bl_newitem.UpdateNewItem(bl_field);
                    BindNewItem();
                    Clear();
                    MessageBox.Show("Record Updated Successfully");
                }
            }
            catch
            { 

            }
        }
    }
}

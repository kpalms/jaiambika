﻿namespace Restaurant.PL
{
    partial class SalaryCalculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SalaryCalculator));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.DTP = new System.Windows.Forms.DateTimePicker();
            this.comboworkars = new System.Windows.Forms.ComboBox();
            this.txtpresentdays = new System.Windows.Forms.TextBox();
            this.txtabsentdays = new System.Windows.Forms.TextBox();
            this.txthalfdays = new System.Windows.Forms.TextBox();
            this.txtovertime = new System.Windows.Forms.TextBox();
            this.txtdeduction = new System.Windows.Forms.TextBox();
            this.txtpaid = new System.Windows.Forms.TextBox();
            this.btnsave = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.txtsalary = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblapsentdays = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblhalfdays = new System.Windows.Forms.Label();
            this.lblpresentdays = new System.Windows.Forms.Label();
            this.dgvsalarycalculater = new System.Windows.Forms.DataGridView();
            this.btnupdate = new System.Windows.Forms.Button();
            this.btndelete = new System.Windows.Forms.Button();
            this.Rtxtremarks = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvsalarycalculater)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(32, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Date :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(32, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Worker Name :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(32, 160);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "PresentDays :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(32, 198);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "AbsentDays :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(418, 45);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Halfdays :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(418, 88);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Extra In Rs :  +";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(418, 134);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Deduction In Rs :  -";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(418, 167);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Paid :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(418, 205);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Remark :";
            // 
            // DTP
            // 
            this.DTP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTP.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTP.Location = new System.Drawing.Point(149, 47);
            this.DTP.Name = "DTP";
            this.DTP.Size = new System.Drawing.Size(200, 20);
            this.DTP.TabIndex = 9;
            this.DTP.ValueChanged += new System.EventHandler(this.DTP_ValueChanged);
            // 
            // comboworkars
            // 
            this.comboworkars.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboworkars.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboworkars.FormattingEnabled = true;
            this.comboworkars.Location = new System.Drawing.Point(149, 83);
            this.comboworkars.Name = "comboworkars";
            this.comboworkars.Size = new System.Drawing.Size(200, 21);
            this.comboworkars.TabIndex = 10;
            this.comboworkars.SelectedIndexChanged += new System.EventHandler(this.comboworkars_SelectedIndexChanged);
            // 
            // txtpresentdays
            // 
            this.txtpresentdays.BackColor = System.Drawing.Color.AntiqueWhite;
            this.txtpresentdays.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtpresentdays.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpresentdays.Location = new System.Drawing.Point(149, 160);
            this.txtpresentdays.Name = "txtpresentdays";
            this.txtpresentdays.Size = new System.Drawing.Size(200, 20);
            this.txtpresentdays.TabIndex = 11;
            this.txtpresentdays.TextChanged += new System.EventHandler(this.txtpresentdays_TextChanged);
            this.txtpresentdays.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtpresentdays_KeyPress);
            // 
            // txtabsentdays
            // 
            this.txtabsentdays.BackColor = System.Drawing.Color.AntiqueWhite;
            this.txtabsentdays.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtabsentdays.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtabsentdays.Location = new System.Drawing.Point(149, 198);
            this.txtabsentdays.Name = "txtabsentdays";
            this.txtabsentdays.Size = new System.Drawing.Size(200, 20);
            this.txtabsentdays.TabIndex = 12;
            this.txtabsentdays.TextChanged += new System.EventHandler(this.txtabsentdays_TextChanged);
            this.txtabsentdays.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtabsentdays_KeyPress);
            // 
            // txthalfdays
            // 
            this.txthalfdays.BackColor = System.Drawing.Color.AntiqueWhite;
            this.txthalfdays.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txthalfdays.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txthalfdays.Location = new System.Drawing.Point(567, 45);
            this.txthalfdays.Name = "txthalfdays";
            this.txthalfdays.Size = new System.Drawing.Size(200, 20);
            this.txthalfdays.TabIndex = 13;
            this.txthalfdays.TextChanged += new System.EventHandler(this.txthalfdays_TextChanged);
            this.txthalfdays.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txthalfdays_KeyPress);
            // 
            // txtovertime
            // 
            this.txtovertime.BackColor = System.Drawing.Color.AntiqueWhite;
            this.txtovertime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtovertime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtovertime.Location = new System.Drawing.Point(567, 88);
            this.txtovertime.Name = "txtovertime";
            this.txtovertime.Size = new System.Drawing.Size(200, 20);
            this.txtovertime.TabIndex = 14;
            this.txtovertime.TextChanged += new System.EventHandler(this.txtovertime_TextChanged);
            this.txtovertime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtovertime_KeyPress);
            // 
            // txtdeduction
            // 
            this.txtdeduction.BackColor = System.Drawing.Color.AntiqueWhite;
            this.txtdeduction.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtdeduction.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdeduction.Location = new System.Drawing.Point(567, 132);
            this.txtdeduction.Name = "txtdeduction";
            this.txtdeduction.Size = new System.Drawing.Size(200, 20);
            this.txtdeduction.TabIndex = 15;
            this.txtdeduction.TextChanged += new System.EventHandler(this.txtdeduction_TextChanged);
            this.txtdeduction.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtdeduction_KeyPress);
            // 
            // txtpaid
            // 
            this.txtpaid.BackColor = System.Drawing.Color.AntiqueWhite;
            this.txtpaid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtpaid.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpaid.Location = new System.Drawing.Point(567, 171);
            this.txtpaid.Name = "txtpaid";
            this.txtpaid.Size = new System.Drawing.Size(200, 20);
            this.txtpaid.TabIndex = 16;
            this.txtpaid.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtpaid_KeyPress);
            // 
            // btnsave
            // 
            this.btnsave.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnsave.BackgroundImage")));
            this.btnsave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Location = new System.Drawing.Point(35, 290);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(79, 37);
            this.btnsave.TabIndex = 18;
            this.btnsave.Text = "Save";
            this.btnsave.UseVisualStyleBackColor = true;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(32, 124);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(50, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Salary :";
            // 
            // txtsalary
            // 
            this.txtsalary.BackColor = System.Drawing.Color.AntiqueWhite;
            this.txtsalary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsalary.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsalary.Location = new System.Drawing.Point(149, 124);
            this.txtsalary.Name = "txtsalary";
            this.txtsalary.Size = new System.Drawing.Size(200, 20);
            this.txtsalary.TabIndex = 20;
            this.txtsalary.TextChanged += new System.EventHandler(this.txtsalary_TextChanged);
            this.txtsalary.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtsalary_KeyDown);
            this.txtsalary.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtsalary_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(601, 290);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(90, 13);
            this.label13.TabIndex = 23;
            this.label13.Text = "Present Days :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(601, 329);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(86, 13);
            this.label14.TabIndex = 24;
            this.label14.Text = "Apsent Days :";
            // 
            // lblapsentdays
            // 
            this.lblapsentdays.AutoSize = true;
            this.lblapsentdays.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblapsentdays.Location = new System.Drawing.Point(697, 329);
            this.lblapsentdays.Name = "lblapsentdays";
            this.lblapsentdays.Size = new System.Drawing.Size(19, 13);
            this.lblapsentdays.TabIndex = 25;
            this.lblapsentdays.Text = "00";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(601, 365);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(70, 13);
            this.label16.TabIndex = 26;
            this.label16.Text = "Half Days :";
            // 
            // lblhalfdays
            // 
            this.lblhalfdays.AutoSize = true;
            this.lblhalfdays.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblhalfdays.Location = new System.Drawing.Point(697, 365);
            this.lblhalfdays.Name = "lblhalfdays";
            this.lblhalfdays.Size = new System.Drawing.Size(19, 13);
            this.lblhalfdays.TabIndex = 27;
            this.lblhalfdays.Text = "00";
            // 
            // lblpresentdays
            // 
            this.lblpresentdays.AutoSize = true;
            this.lblpresentdays.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblpresentdays.Location = new System.Drawing.Point(697, 290);
            this.lblpresentdays.Name = "lblpresentdays";
            this.lblpresentdays.Size = new System.Drawing.Size(19, 13);
            this.lblpresentdays.TabIndex = 28;
            this.lblpresentdays.Text = "00";
            // 
            // dgvsalarycalculater
            // 
            this.dgvsalarycalculater.AllowUserToAddRows = false;
            this.dgvsalarycalculater.AllowUserToDeleteRows = false;
            this.dgvsalarycalculater.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvsalarycalculater.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(253)))), ((int)(((byte)(244)))));
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.PeachPuff;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvsalarycalculater.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvsalarycalculater.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.Orange;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvsalarycalculater.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgvsalarycalculater.EnableHeadersVisualStyles = false;
            this.dgvsalarycalculater.Location = new System.Drawing.Point(22, 416);
            this.dgvsalarycalculater.MultiSelect = false;
            this.dgvsalarycalculater.Name = "dgvsalarycalculater";
            this.dgvsalarycalculater.ReadOnly = true;
            this.dgvsalarycalculater.RowHeadersVisible = false;
            this.dgvsalarycalculater.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvsalarycalculater.Size = new System.Drawing.Size(745, 171);
            this.dgvsalarycalculater.TabIndex = 32;
            this.dgvsalarycalculater.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvsalarycalculater_CellClick);
            // 
            // btnupdate
            // 
            this.btnupdate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnupdate.BackgroundImage")));
            this.btnupdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnupdate.Location = new System.Drawing.Point(149, 290);
            this.btnupdate.Name = "btnupdate";
            this.btnupdate.Size = new System.Drawing.Size(79, 37);
            this.btnupdate.TabIndex = 33;
            this.btnupdate.Text = "Update";
            this.btnupdate.UseVisualStyleBackColor = true;
            this.btnupdate.Click += new System.EventHandler(this.btnupdate_Click);
            // 
            // btndelete
            // 
            this.btndelete.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btndelete.BackgroundImage")));
            this.btndelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btndelete.Location = new System.Drawing.Point(270, 290);
            this.btndelete.Name = "btndelete";
            this.btndelete.Size = new System.Drawing.Size(79, 37);
            this.btndelete.TabIndex = 35;
            this.btndelete.Text = "Delete";
            this.btndelete.UseVisualStyleBackColor = true;
            this.btndelete.Click += new System.EventHandler(this.btndelete_Click);
            // 
            // Rtxtremarks
            // 
            this.Rtxtremarks.BackColor = System.Drawing.Color.AntiqueWhite;
            this.Rtxtremarks.Location = new System.Drawing.Point(567, 198);
            this.Rtxtremarks.Multiline = true;
            this.Rtxtremarks.Name = "Rtxtremarks";
            this.Rtxtremarks.Size = new System.Drawing.Size(200, 62);
            this.Rtxtremarks.TabIndex = 36;
            // 
            // SalaryCalculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(253)))), ((int)(((byte)(244)))));
            this.ClientSize = new System.Drawing.Size(798, 610);
            this.Controls.Add(this.Rtxtremarks);
            this.Controls.Add(this.btndelete);
            this.Controls.Add(this.btnupdate);
            this.Controls.Add(this.dgvsalarycalculater);
            this.Controls.Add(this.comboworkars);
            this.Controls.Add(this.lblpresentdays);
            this.Controls.Add(this.lblhalfdays);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.lblapsentdays);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtsalary);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.txtpaid);
            this.Controls.Add(this.txtdeduction);
            this.Controls.Add(this.txtovertime);
            this.Controls.Add(this.txthalfdays);
            this.Controls.Add(this.txtabsentdays);
            this.Controls.Add(this.txtpresentdays);
            this.Controls.Add(this.DTP);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SalaryCalculator";
            this.Text = "SalaryCalculator";
            this.Load += new System.EventHandler(this.SalaryCalculator_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvsalarycalculater)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker DTP;
        private System.Windows.Forms.ComboBox comboworkars;
        private System.Windows.Forms.TextBox txtpresentdays;
        private System.Windows.Forms.TextBox txtabsentdays;
        private System.Windows.Forms.TextBox txthalfdays;
        private System.Windows.Forms.TextBox txtovertime;
        private System.Windows.Forms.TextBox txtdeduction;
        private System.Windows.Forms.TextBox txtpaid;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtsalary;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblapsentdays;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lblhalfdays;
        private System.Windows.Forms.Label lblpresentdays;
        private System.Windows.Forms.DataGridView dgvsalarycalculater;
        private System.Windows.Forms.Button btnupdate;
        private System.Windows.Forms.Button btndelete;
        private System.Windows.Forms.TextBox Rtxtremarks;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Collections;
using System.Collections.Generic;


namespace Restaurant.PL
{
    public partial class WorkerAttendanc : Form
    {

        BL_Field bl_field = new BL_Field();
        BL_Worker bl_worker = new BL_Worker();
        Common common = new Common();

        public WorkerAttendanc()
        {
            InitializeComponent();
        }

        private void WorkerAttendanc_Load(object sender, EventArgs e)
        {
            CheckRoomStatus();
        }


        private void CheckRoomStatus()
        {
            try
            {


                dgvattendance.Rows.Clear();
                DateTime slcdt = Convert.ToDateTime(monthCalendar1.SelectionStart.ToLongDateString());

                List<DateTime> week = GetWeek(slcdt);
                int h = 0;
                foreach (DateTime dt in week)
                {
                    dgvattendance.Columns[h].HeaderText = dt.ToString("dddd") + "\n" + dt.Day.ToString();
                    //dgvattendance.Columns[h].HeaderText = dt.Day.ToString();
                    h++;
                }
                string catid = "";
         
                catid = "1";
           

                DataTable dtworker = bl_worker.SelectWorker();
                ArrayList arrroom = new ArrayList();
                Dictionary<string, string> dict = new Dictionary<string, string>();
                if (dtworker.Rows.Count > 0)
                {
                    for (int i = 0; i < dtworker.Rows.Count; i++)
                    {
                        Button bb = new Button();
                        string name = dtworker.Rows[i]["WorkerId"].ToString();
                        arrroom.Add(name);
                        dict.Add(dtworker.Rows[i]["WorkerId"].ToString(), dtworker.Rows[i]["Name"].ToString());
                    }
                }


                dgvattendance.TopLeftHeaderCell.Value = slcdt.ToString("MMMM") + " " + slcdt.Year;//"  Time  ";

                int r = 0;
                //foreach (string nn in arrroom)
                //{
                //    dgvattendance.Rows.Add();
                //    string ht = nn.ToString();
                //    dgvattendance.Rows[r].HeaderCell.Value = ht;
                //    r++;
                //}
                foreach (KeyValuePair<string, string> d in dict)
                {
                    dgvattendance.Rows.Add();
                    string id = d.Key;
                    string name = d.Value;
                    dgvattendance.Rows[r].HeaderCell.Value = name.ToString() ;
                    r++;
                }


                DateTime aa = Convert.ToDateTime(week[0].Date.ToLongDateString());


                int m = slcdt.Month;
                int y = slcdt.Year;
                string month = common.Get2Digit(m);
                string year = common.Get2Digit(y);
  
                string fromDate = "01" + "/" + month + "/" + year;
                int daysInMonth= System.DateTime.DaysInMonth(Convert.ToInt32(year), Convert.ToInt32(month));
                string ToDate = daysInMonth + "/" + month + "/" + year;

                bl_worker.FromDate = fromDate;
                //DateTime kk = Convert.ToDateTime(week[6].Date.ToLongDateString());
                bl_worker.Todate = ToDate;
                DataSet ds_TDay = bl_worker.SelectAllAttendance(bl_worker);

                if (ds_TDay.Tables[0].Rows.Count > 0)
                {
                    int cou = 0;

                    foreach (DataRow dr in ds_TDay.Tables[0].Rows)
                    {
               

                        string Colindex = "0";
                        DateTime dtt = Convert.ToDateTime(dr["Date"].ToString());
                        string ColheaderText = dtt.ToString("dddd") + "\n" + dtt.Day.ToString();//"Monday";


                        foreach (DataGridViewColumn column in dgvattendance.Columns)
                            if (column.HeaderText.Equals(ColheaderText, StringComparison.InvariantCultureIgnoreCase))
                            {
                                Colindex = column.Index.ToString();
                                break;
                            }

                        string RoheaderText = dr["Name"].ToString();//"03:00:00 PM";
                        string Roindex = "0";
                        foreach (DataGridViewRow row in dgvattendance.Rows)
                            if (row.HeaderCell.Value.Equals(RoheaderText) == true)//, StringComparison.InvariantCultureIgnoreCase))
                            {
                                Roindex = row.Index.ToString();
                                break;
                            }
                        int c = Convert.ToInt32(Colindex);
                        int rr = Convert.ToInt32(Roindex);
                        cou += 1;
                        string AttendanceType = "";
                        if (dr["AttendanceType"].ToString() == "0")
                        {
                            AttendanceType = "Absent";
                        }
                        else
                        {
                            AttendanceType = "Half Day";
                        }
                    
                        dgvattendance.Rows[rr].Cells[c].Value = dgvattendance.Rows[rr].Cells[c].Value + "" + AttendanceType + " " + dr["Reasion"].ToString();
                        if (dr["AttendanceType"].ToString() == "0")
                        {
                            dgvattendance.Rows[rr].Cells[c].Style.BackColor = Color.Red;
                        }
                        if (dr["AttendanceType"].ToString() == "1")
                        {
                            dgvattendance.Rows[rr].Cells[c].Style.BackColor = Color.Yellow;
                        }

                    }


                }
            }
            catch
            {


            }

        }


        public  List<DateTime> GetWeek(DateTime initDt)
        {

            List<DateTime> week = new List<DateTime>();

            int m = initDt.Month ;
            int y = initDt.Year;
            string month = common.Get2Digit(m);
            string year = common.Get2Digit(y);
            string day = "01";


           string strDate = day + "/" + month + "/" + year;
           DateTime date = Convert.ToDateTime(strDate);
           int daysInJuly = System.DateTime.DaysInMonth(Convert.ToInt32(year), Convert.ToInt32(month));

           for (int i = 0; i < daysInJuly; i++)
            {
                week.Add(date);
                date = date.AddDays(1.0);
            }

            return week;
        }





    }
}

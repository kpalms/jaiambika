﻿namespace Restaurant.PL
{
    partial class Qty
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtqty = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnchangeqty = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtqty
            // 
            this.txtqty.Location = new System.Drawing.Point(49, 57);
            this.txtqty.Name = "txtqty";
            this.txtqty.Size = new System.Drawing.Size(112, 20);
            this.txtqty.TabIndex = 0;
            this.txtqty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtqty_KeyDown);
            this.txtqty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtqty_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Enter Quantity";
            // 
            // btnchangeqty
            // 
            this.btnchangeqty.BackgroundImage = global::Restaurant.Properties.Resources.button;
            this.btnchangeqty.Location = new System.Drawing.Point(49, 100);
            this.btnchangeqty.Name = "btnchangeqty";
            this.btnchangeqty.Size = new System.Drawing.Size(112, 23);
            this.btnchangeqty.TabIndex = 2;
            this.btnchangeqty.Text = "Change Quantity";
            this.btnchangeqty.UseVisualStyleBackColor = true;
            this.btnchangeqty.Click += new System.EventHandler(this.btnchangeqty_Click);
            // 
            // Qty
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(253)))), ((int)(((byte)(244)))));
            this.ClientSize = new System.Drawing.Size(206, 158);
            this.Controls.Add(this.btnchangeqty);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtqty);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Qty";
            this.ShowInTaskbar = false;
            this.Text = "Change Qty";
            this.Load += new System.EventHandler(this.Qty_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Qty_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtqty;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnchangeqty;
    }
}
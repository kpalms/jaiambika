﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;


namespace Restaurant.PL
{
    public partial class AddSubCatagory : Form
    {
        BL_Catagory bl_catagory = new BL_Catagory();
        BL_Field bl_field = new BL_Field();
        Common common = new Common();

        public AddSubCatagory()
        {
            InitializeComponent();
            this.KeyPreview = true;
        }

        private void AddSubCatagory_Load(object sender, EventArgs e)
        {
           // BindSearchCatagory();
           
            BindCatagory();
            btndelete.Enabled = false;
            btnUpdate.Enabled = false;
     
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            if (ddlcatagory.SelectedIndex >0)
            {

                try
                {
                    
                    if (txtitem.Text == "")
                    {
                        MessageBox.Show("Please enter Name?");
                    }

                    else
                    {
                        Insert();
                    }
                                        
                }
                catch
                {

                    MessageBox.Show("error occured!Please contact Administration?");
                }
            }
            else 
            {
                MessageBox.Show("Select Catagory");
            }


        }


        private void Insert()
        {
            bl_field.No = txtno.Text;
            string no = bl_catagory.GetNo(bl_field);
            if (no == "")
            {
                SetField();
                bl_catagory.InsertSubCatagory(bl_field);

                MessageBox.Show("Record inserted Successfully");
                string catid = ddlcatagory.SelectedValue.ToString();

                BindGrid(catid);
                
                clear();
                txtitem.Focus();
            }
            else
            {
                MessageBox.Show("This No. already exit. Please Enter another No.?");
                txtno.Focus();
            }
        }
        private void SetField()
        {
            bl_field.CatId = ddlcatagory.SelectedValue.ToString();
            bl_field.SubcatagoryName = txtitem.Text;
          
            bl_field.No = txtno.Text;
            bl_field.ShortName = txtshortname.Text;

        }
        private void BindCatagory()
        {
            DataTable dt = bl_catagory.Catagory(bl_field);
            DataRow row = dt.NewRow();
            row["CatagoryName"] = "SELECT";
            dt.Rows.InsertAt(row, 0);
            ddlcatagory.DataSource = dt;
            ddlcatagory.ValueMember = "CatId";
            ddlcatagory.DisplayMember = "CatagoryName";
            
        }

        //private void BindSearchCatagory()
        //{
        //    bl_field.TextSearch = ddlcatagory.Text;
        //    DataTable dt = bl_catagory.SearchCatagory(bl_field);
        //    ddlcatagory.DataSource = dt;
        //    ddlcatagory.ValueMember = "CatId";
        //    ddlcatagory.DisplayMember = "CatagoryName";
        //}


        private void BindGrid(string catid)
        {
            DataTable dt = bl_catagory.SelectSubCatagory(catid);
            dataGridView1.DataSource = dt;
            dataGridView1.Columns["CatId"].Visible = false;
            dataGridView1.Columns["SubCatId"].Visible = false;
            dataGridView1.Columns["Type"].Visible = false;

        }
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            bl_field.SubcatId = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            txtitem.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            txtno.Text = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
            txtshortname.Text = dataGridView1.SelectedRows[0].Cells[4].Value.ToString();
            btnUpdate.Enabled = true;
            btndelete.Enabled = true;
            btnadd.Enabled = false;
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            BindCatagory();
            clear();
            btndelete.Enabled = false;
            btnUpdate.Enabled = false;
            btnadd.Enabled = true;
        }
        private void clear()
        {
            txtitem.Text = "";
            txtno.Text = "";
            txtshortname.Text = "";
        }
        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Item?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_field.SubcatId = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                    bl_catagory.DeleteSubCatagory(bl_field);
                    txtitem.Text = "";
                    MessageBox.Show("Record Deleted Successfully");
                    string catid = ddlcatagory.SelectedValue.ToString();

                    BindGrid(catid);
                    clear();
                    txtitem.Focus();
                    btnUpdate.Enabled = false;
                    btndelete.Enabled = false;
                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }


        }

        private void ddlcatagory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlcatagory.Text != "System.Data.DataRowView")
            {
                if (ddlcatagory.ValueMember == "CatId")
                {
                    string catid = ddlcatagory.SelectedValue.ToString();
                    BindGrid(catid);
                }
            }
        }

      

        private void AddSubCatagory_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void txtitem_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = Char.ToUpper(e.KeyChar);
        }

        private void txtshortname_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = Char.ToUpper(e.KeyChar);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Types tp = new Types(this);
            tp.ShowDialog();

        }


        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtshortname_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void txtpurchaseunit_KeyPress(object sender, KeyPressEventArgs e)
        {
            common.NumberOnly(e);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
                SetField();
                bl_catagory.UpdateSubCatagory(bl_field);
                clear();
                MessageBox.Show("Record Updated Successfully");
                string catid = ddlcatagory.SelectedValue.ToString();

                BindGrid(catid);
                clear();
                txtitem.Focus();

        }

    }
}

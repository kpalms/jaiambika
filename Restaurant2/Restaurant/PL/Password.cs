﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Restaurant.BL;
using System.Data.SqlClient;
using Restaurant.PL;
using Restaurant.DL;


namespace Restaurant.PL
{
    public partial class Password : Form
    {

        BL_Security bl_security = new BL_Security();
        BL_Sale bl_sale = new BL_Sale();
        BL_Field bl_field = new BL_Field();
        BL_Worker bl_worker = new BL_Worker();
        BL_HotelSetting blhotelseting = new BL_HotelSetting();
        Common common = new Common();

        string formname;
        //main _mainForm;
        public Password(string name)
        {

            InitializeComponent();
            //_mainForm = mainfrm;
            formname = name;
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            CheckPassword();
        }

            private void CheckPassword()
            {
            bl_field.Password = txtpassword.Text;
            SqlDataReader dr = bl_worker.CheckSecurity(bl_field);
            if (dr.Read())
            {
                string role = dr[0].ToString();
                if (role == "Admin")
                {
                    this.Close();
                    if (formname == "Security")
                    {
                        //main mm = new main();
                        Security sec = new Security();
                        sec.ShowDialog();
                    }
                    if (formname == "Worker")
                    {
                        Worker wr = new Worker();
                        wr.ShowDialog();
                    }
                    if (formname == "AdminPart")
                    {

                        ExecuteQuery eq = new ExecuteQuery();
                        eq.ShowDialog();
                     
                    }
    
                    if (formname == "ManageAccess")
                    {
                        
                        //AccessLevel al = new AccessLevel(_mainForm);
                        //al.ShowDialog();
                    }
                    if (formname == "ChangePrice")
                    {

                        //ChangePrice al = new ChangePrice(_mainForm);
                        //al.ShowDialog();
                    }
                    if (formname == "DayClose")
                    {

                        DialogResult msgg = MessageBox.Show("Are you sure you want to Close Day?", "Delete", MessageBoxButtons.YesNo);
                        if (msgg == DialogResult.Yes)
                        {
                            DateTime date = Convert.ToDateTime(bl_field.GlobalDate);
                            //dtptodaydate.Value = date;

                            //DateTime date = Convert.ToDateTime(bl_field.GlobalDate);

                            DateTime newdates = date.AddDays(1);

                            bl_field.Status = "1";
                            bl_field.Date = newdates.ToString("dd/MM/yyyy");
                            blhotelseting.CloseDate(bl_field);
                            blhotelseting.InsertDate(bl_field);
                            string dateopen = common.ReturnOneValue("select Date from Date where  Status=1");
                            bl_field.GlobalDate = dateopen;

                        }


                    }
                    if (formname == "DayOpen")
                    {
                        DialogResult msgg = MessageBox.Show("Are you sure you want to Open Previous Date?", "Delete", MessageBoxButtons.YesNo);
                        if (msgg == DialogResult.Yes)
                        {
                            //DateTime date = Convert.ToDateTime(bl_field.GlobalDate);

                            DateTime date = Convert.ToDateTime(bl_field.GlobalDate);
                            DateTime newdates = date.AddDays(-1);

                            bl_field.Status = "1";
                            bl_field.Date = newdates.ToString("dd/MM/yyyy");
                            blhotelseting.CloseDate(bl_field);
                            blhotelseting.InsertDate(bl_field);
                            string dateopen = common.ReturnOneValue("select Date from Date where  Status=1");
                            bl_field.GlobalDate = dateopen;
                        }


                    }


                    if (formname == "CancelBill")
                    {
                        DialogResult msgg = MessageBox.Show("Are you sure you want to CancelBill?", "Delete", MessageBoxButtons.YesNo);
                        if (msgg == DialogResult.Yes)
                        {
                            CancelBill();
                        }


                    }

                    if (formname == "ModifyBill")
                    {
                        Modify mm = new Modify();
                        mm.ShowDialog();
                    }
                    
 
                }
                else
                {
                    MessageBox.Show("Please check password?");
                }
            }
            else
            {
                MessageBox.Show("Please check password?");
            }
        }


            private void CancelBill()
            {
                bl_field.BillNo = bl_field.BillNoStatic;

                bl_field.PayModeId = 3;
                bl_field.Memo = 0;
                bl_field.CustomerId = 0;
                bl_sale.UpdateBill(bl_field);


                bl_sale.CloseCancelBill(bl_field);
                MessageBox.Show("Bill Cancel successfully");
                this.Close();
            }


        private void Password_Load(object sender, EventArgs e)
        {

        }

        private void txtpassword_KeyDown(object sender, KeyEventArgs e)
        {

            if (txtpassword.Text != "")
            {
                if (e.KeyCode == Keys.Enter)
                {
                    CheckPassword();
                }
            }
            ////kkk

        }

        private void Password_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}

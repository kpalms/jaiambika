﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;


namespace Restaurant.PL
{
    public partial class AmountPay : Form
    {
        BL_Field bl_field = new BL_Field();
        BL_Sale bl_sale = new BL_Sale();
        main _mainForm;
        string no;

        public AmountPay(string amountpay, string billno,string tableno,string seat, main mainfrm)
        {

            InitializeComponent();
            _mainForm = mainfrm;
            this.KeyPreview = true;
           txtamountpay.Text = amountpay;
           bl_field.BillNo = billno;
           bl_field.BillNoStatic = billno;
           bl_field.TableNo = tableno;
           lblbillno.Text = billno;
           bl_field.Seat = seat;
           lblSeat.Text = tableno+" "+seat;
        }

        private void AmountPay_Load(object sender, EventArgs e)
        {
            //comboBox1.SelectedIndex =0;
            BindPayMode();

            pnlcustomer.Visible = false;
            ddlcustomer.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            ddlcustomer.AutoCompleteSource = AutoCompleteSource.ListItems;

            comboBox1.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            comboBox1.AutoCompleteSource = AutoCompleteSource.ListItems;


        }
        private void BindCustomer()
        {

            Common common = new Common();
            common.BindCustomerCombo(ddlcustomer);
            //BL_Customer bl_customer = new BL_Customer();
            //DataTable dt = bl_customer.SelectCustomer();

            //DataRow row = dt.NewRow();
            //row["name"] = "SELECT";
            //dt.Rows.InsertAt(row, 0);

            //Dictionary<int, string> dist = new Dictionary<int, string>();
            //if (dt.Rows.Count > 0)
            //{

            //        for (int i = 0; i < dt.Rows.Count; i++)
            //        {
            //            if (dt.Rows[i]["Name"].ToString() != "" && dt.Rows[i]["CustId"].ToString() != "")
            //            {
            //                dist.Add(Convert.ToInt32(dt.Rows[i]["CustId"].ToString()), dt.Rows[i]["Name"].ToString());
            //            }
            //        }
                
            //}

            //ddlcustomer.DataSource = dt;
            //ddlcustomer.ValueMember = "CustId";
            //ddlcustomer.DisplayMember = "Name";
        }
        private void BindPayMode()
        {
            BL_PaymentMode bl_paymentmode = new BL_PaymentMode();
            DataTable dt = bl_paymentmode.SelectPaymentMode();

            DataRow row = dt.NewRow();
            row["PaymentMode"] = "SELECT";
            dt.Rows.InsertAt(row, 0);


            comboBox1.DataSource = dt;
            comboBox1.DisplayMember = "PaymentMode";
            comboBox1.ValueMember = "PayModeId";
 
        }



        private void AmountPay_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void txttender_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            {
                e.Handled = true;
               
            }
        }

        private void txttender_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                comboBox1.Focus();
            }
        }

        private void txttender_TextChanged(object sender, EventArgs e)
        {
            if (txttender.Text != "")
            {
                txtchange.Text = Convert.ToString(Convert.ToDecimal(txttender.Text) - Convert.ToDecimal(txtamountpay.Text));
            }
            else 
            {
                txtchange.Text = "";
            }
        }

        private void comboBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnok.Focus();
            }
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked!=true && txttender.Text == "")
            {
                MessageBox.Show("Please enter Tenderd Amount");
            }
            else if (checkBox1.Checked != true && comboBox1.Text == "SELECT")
            {
                MessageBox.Show("Please select Payment Mode");
            }
            else if (checkBox1.Checked == true && ddlcustomer.Text == "Select")
            {
                MessageBox.Show("Please Select Customer Name");
            }
            else if (checkBox1.Checked == true && txtbalence.Text == "")
            {
                MessageBox.Show("Please Enter balence Amount");
            }
            else
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to Paid selected Bill?", "Paid", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_field.PayModeId = Convert.ToInt32(comboBox1.SelectedValue.ToString());
                    if (checkBox1.Checked == true)
                    {
                        bl_field.Memo = 1;
                        bl_field.CustomerId = Convert.ToInt32(ddlcustomer.SelectedValue.ToString());
                    }
                    else {
                        bl_field.Memo = 0;
                        bl_field.CustomerId = 0;
                    }

                    bl_sale.UpdateBill(bl_field);

                    if (checkBox1.Checked == true)
                    {
                        //insert debit amount

                        bl_field.Debit = Convert.ToDecimal(txtamountpay.Text);
                        if (txtcredit.Text != "")
                        {
                            bl_field.Credit = Convert.ToDecimal(txtcredit.Text);
                        }
                        else
                        {
                            bl_field.Credit = 0;
                        }
                        bl_field.Date = bl_field.GlobalDate;
                       
                        bl_sale.InsertDebitAmt(bl_field);
                        //insert debit amount
                    }


                    _mainForm.BindTable();
                    this.Close();
                }
            }
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {


            if (checkBox1.Checked == true)
            {
                BindCustomer();
                comboBox1.Text = "CREDIT";
                pnlcustomer.Visible = true;
            }
            else {
                comboBox1.Text = "SELECT";
                pnlcustomer.Visible = false;
            }

        }

        private void txtcredit_TextChanged(object sender, EventArgs e)
        {
            if (txtcredit.Text != "")
            {
                decimal amounpay = Convert.ToDecimal(txtamountpay.Text);
                decimal credit = Convert.ToDecimal(txtcredit.Text);

                txtbalence.Text = Convert.ToString(amounpay - credit);
            }
            else
            {
                txtbalence.Text = "";
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text == "CREDIT")
            {
                checkBox1.Checked = true;
            }
            else 
            {
                checkBox1.Checked = false;
            }
        }

        private void btncancelBill_Click(object sender, EventArgs e)
        {
            DialogResult msgg = MessageBox.Show("Are you sure you want to CancelBill?", "Delete", MessageBoxButtons.YesNo);
            if (msgg == DialogResult.Yes)
            {

                Password pw = new Password("CancelBill");
                pw.ShowDialog();

                _mainForm.BindTable();
                this.Close();

            }
        }

        private void btnaddcustomer_Click(object sender, EventArgs e)
        {
            main m = new main();
            Customer cm = new Customer(m);
            cm.ShowDialog();
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            txttender.Text += btn0.Text;
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            txttender.Text += btn1.Text;
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            txttender.Text += btn2.Text;
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            txttender.Text += btn3.Text;
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            txttender.Text += btn4.Text;
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            txttender.Text += btn5.Text;
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            txttender.Text += btn6.Text;
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            txttender.Text += btn7.Text;
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            txttender.Text += btn8.Text;
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            txttender.Text += btn9.Text;
        }

        private void btnclear_Click(object sender, EventArgs e)
        {
            txttender.Text ="";
        }

        private void btnbackspace_Click(object sender, EventArgs e)
        {
            if (txttender.Text == string.Empty)
            {
              //  errorProvider1.SetError(txttender, "Must enter the value to remove");
            }
            else
            {
               // errorProvider1.Clear();
                no = txttender.Text;
                int no1 = no.Length;
                txttender.Text = (no.Substring(0, no1 - 1));
            }
        }

        private void txtcredit_KeyPress(object sender, KeyPressEventArgs e)
        {
            Common common = new Common();
            common.OneDecimalPoint(txtcredit, e);
        }






    }
}

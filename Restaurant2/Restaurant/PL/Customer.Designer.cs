﻿namespace Restaurant.PL
{
    partial class Customer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Customer));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btndelete = new System.Windows.Forms.Button();
            this.btnadd = new System.Windows.Forms.Button();
            this.grdcustomer = new System.Windows.Forms.DataGridView();
            this.txtaddress = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtsearchName = new System.Windows.Forms.TextBox();
            this.txtsearchMobile = new System.Windows.Forms.TextBox();
            this.btnclear = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbosociety = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtemail = new System.Windows.Forms.TextBox();
            this.Birtday = new System.Windows.Forms.Label();
            this.Anniversary = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dtpBirthday = new System.Windows.Forms.MaskedTextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.txtflatname = new System.Windows.Forms.ComboBox();
            this.txtmobile2 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtmobile3 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.dtpAnniversary = new System.Windows.Forms.MaskedTextBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grdcustomer)).BeginInit();
            this.SuspendLayout();
            // 
            // btndelete
            // 
            this.btndelete.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btndelete.BackgroundImage")));
            this.btndelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btndelete.Location = new System.Drawing.Point(325, 237);
            this.btndelete.Name = "btndelete";
            this.btndelete.Size = new System.Drawing.Size(77, 30);
            this.btndelete.TabIndex = 12;
            this.btndelete.Text = "Delete";
            this.btndelete.UseVisualStyleBackColor = true;
            this.btndelete.Click += new System.EventHandler(this.btndelete_Click);
            // 
            // btnadd
            // 
            this.btnadd.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnadd.BackgroundImage")));
            this.btnadd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnadd.Location = new System.Drawing.Point(158, 237);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(77, 30);
            this.btnadd.TabIndex = 6;
            this.btnadd.Text = "Save";
            this.btnadd.UseVisualStyleBackColor = true;
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click);
            // 
            // grdcustomer
            // 
            this.grdcustomer.AllowUserToAddRows = false;
            this.grdcustomer.AllowUserToDeleteRows = false;
            this.grdcustomer.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdcustomer.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(253)))), ((int)(((byte)(244)))));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.PeachPuff;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdcustomer.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grdcustomer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Orange;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdcustomer.DefaultCellStyle = dataGridViewCellStyle2;
            this.grdcustomer.EnableHeadersVisualStyles = false;
            this.grdcustomer.Location = new System.Drawing.Point(12, 288);
            this.grdcustomer.MultiSelect = false;
            this.grdcustomer.Name = "grdcustomer";
            this.grdcustomer.ReadOnly = true;
            this.grdcustomer.RowHeadersVisible = false;
            this.grdcustomer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdcustomer.Size = new System.Drawing.Size(790, 367);
            this.grdcustomer.TabIndex = 9;
            this.grdcustomer.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdcustomer_RowEnter);
            this.grdcustomer.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdcustomer_CellDoubleClick);
            this.grdcustomer.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdcustomer_CellClick);
            this.grdcustomer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grdcustomer_KeyDown);
            // 
            // txtaddress
            // 
            this.txtaddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtaddress.Location = new System.Drawing.Point(490, 33);
            this.txtaddress.Multiline = true;
            this.txtaddress.Name = "txtaddress";
            this.txtaddress.Size = new System.Drawing.Size(161, 31);
            this.txtaddress.TabIndex = 3;
            this.txtaddress.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtaddress_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(155, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 34;
            this.label2.Text = "Mobile No";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(475, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 35;
            this.label3.Text = "Discription";
            // 
            // txtsearchName
            // 
            this.txtsearchName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsearchName.Location = new System.Drawing.Point(158, 174);
            this.txtsearchName.Multiline = true;
            this.txtsearchName.Name = "txtsearchName";
            this.txtsearchName.Size = new System.Drawing.Size(161, 31);
            this.txtsearchName.TabIndex = 7;
            this.txtsearchName.TextChanged += new System.EventHandler(this.txtsearchName_TextChanged);
            this.txtsearchName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtsearchName_KeyDown);
            // 
            // txtsearchMobile
            // 
            this.txtsearchMobile.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsearchMobile.Location = new System.Drawing.Point(158, 106);
            this.txtsearchMobile.MaxLength = 10;
            this.txtsearchMobile.Multiline = true;
            this.txtsearchMobile.Name = "txtsearchMobile";
            this.txtsearchMobile.Size = new System.Drawing.Size(161, 31);
            this.txtsearchMobile.TabIndex = 4;
            this.txtsearchMobile.TextChanged += new System.EventHandler(this.txtsearchMobile_TextChanged);
            this.txtsearchMobile.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtsearchMobile_KeyDown);
            this.txtsearchMobile.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtsearchMobile_KeyPress);
            // 
            // btnclear
            // 
            this.btnclear.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnclear.BackgroundImage")));
            this.btnclear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnclear.Location = new System.Drawing.Point(409, 237);
            this.btnclear.Name = "btnclear";
            this.btnclear.Size = new System.Drawing.Size(77, 30);
            this.btnclear.TabIndex = 13;
            this.btnclear.Text = "Clear";
            this.btnclear.UseVisualStyleBackColor = true;
            this.btnclear.Click += new System.EventHandler(this.btnclear_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(155, 158);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 42;
            this.label1.Text = "Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(158, 221);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 13);
            this.label4.TabIndex = 43;
            this.label4.Text = "F10";
            // 
            // cbosociety
            // 
            this.cbosociety.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbosociety.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbosociety.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbosociety.FormattingEnabled = true;
            this.cbosociety.Location = new System.Drawing.Point(158, 33);
            this.cbosociety.Name = "cbosociety";
            this.cbosociety.Size = new System.Drawing.Size(161, 33);
            this.cbosociety.TabIndex = 1;
            this.cbosociety.SelectedIndexChanged += new System.EventHandler(this.cbosociety_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(155, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 45;
            this.label5.Text = "Society";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(322, 158);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 47;
            this.label6.Text = "Email";
            // 
            // txtemail
            // 
            this.txtemail.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtemail.Location = new System.Drawing.Point(325, 174);
            this.txtemail.Multiline = true;
            this.txtemail.Name = "txtemail";
            this.txtemail.Size = new System.Drawing.Size(161, 31);
            this.txtemail.TabIndex = 8;
            // 
            // Birtday
            // 
            this.Birtday.AutoSize = true;
            this.Birtday.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Birtday.Location = new System.Drawing.Point(489, 159);
            this.Birtday.Name = "Birtday";
            this.Birtday.Size = new System.Drawing.Size(53, 13);
            this.Birtday.TabIndex = 50;
            this.Birtday.Text = "Birthday";
            // 
            // Anniversary
            // 
            this.Anniversary.AutoSize = true;
            this.Anniversary.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Anniversary.Location = new System.Drawing.Point(660, 158);
            this.Anniversary.Name = "Anniversary";
            this.Anniversary.Size = new System.Drawing.Size(73, 13);
            this.Anniversary.TabIndex = 51;
            this.Anniversary.Text = "Anniversary";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(322, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(115, 13);
            this.label7.TabIndex = 55;
            this.label7.Text = "Flat/Building Name";
            // 
            // dtpBirthday
            // 
            this.dtpBirthday.BeepOnError = true;
            this.dtpBirthday.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpBirthday.Location = new System.Drawing.Point(492, 175);
            this.dtpBirthday.Mask = "00/00/0000";
            this.dtpBirthday.Name = "dtpBirthday";
            this.dtpBirthday.Size = new System.Drawing.Size(161, 30);
            this.dtpBirthday.TabIndex = 57;
            this.dtpBirthday.ValidatingType = typeof(System.DateTime);
            this.dtpBirthday.TypeValidationCompleted += new System.Windows.Forms.TypeValidationEventHandler(this.dtpBirthday_TypeValidationCompleted);
            // 
            // txtflatname
            // 
            this.txtflatname.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtflatname.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.txtflatname.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtflatname.FormattingEnabled = true;
            this.txtflatname.Location = new System.Drawing.Point(323, 34);
            this.txtflatname.Name = "txtflatname";
            this.txtflatname.Size = new System.Drawing.Size(161, 33);
            this.txtflatname.TabIndex = 2;
            this.txtflatname.SelectedIndexChanged += new System.EventHandler(this.txtflatname_SelectedIndexChanged);
            // 
            // txtmobile2
            // 
            this.txtmobile2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmobile2.Location = new System.Drawing.Point(325, 106);
            this.txtmobile2.MaxLength = 10;
            this.txtmobile2.Multiline = true;
            this.txtmobile2.Name = "txtmobile2";
            this.txtmobile2.Size = new System.Drawing.Size(161, 31);
            this.txtmobile2.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(322, 90);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 13);
            this.label8.TabIndex = 61;
            this.label8.Text = "Mobile No";
            // 
            // txtmobile3
            // 
            this.txtmobile3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmobile3.Location = new System.Drawing.Point(490, 106);
            this.txtmobile3.MaxLength = 10;
            this.txtmobile3.Multiline = true;
            this.txtmobile3.Name = "txtmobile3";
            this.txtmobile3.Size = new System.Drawing.Size(161, 31);
            this.txtmobile3.TabIndex = 6;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(487, 90);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 13);
            this.label9.TabIndex = 63;
            this.label9.Text = "Mobile No";
            // 
            // dtpAnniversary
            // 
            this.dtpAnniversary.BeepOnError = true;
            this.dtpAnniversary.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpAnniversary.Location = new System.Drawing.Point(659, 175);
            this.dtpAnniversary.Mask = "00/00/0000";
            this.dtpAnniversary.Name = "dtpAnniversary";
            this.dtpAnniversary.Size = new System.Drawing.Size(161, 30);
            this.dtpAnniversary.TabIndex = 10;
            this.dtpAnniversary.ValidatingType = typeof(System.DateTime);
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnUpdate.BackgroundImage")));
            this.btnUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdate.Location = new System.Drawing.Point(242, 237);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(77, 30);
            this.btnUpdate.TabIndex = 60;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // Customer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(253)))), ((int)(((byte)(244)))));
            this.ClientSize = new System.Drawing.Size(834, 724);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.dtpAnniversary);
            this.Controls.Add(this.txtmobile3);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtmobile2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtflatname);
            this.Controls.Add(this.dtpBirthday);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Anniversary);
            this.Controls.Add(this.Birtday);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtemail);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cbosociety);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnclear);
            this.Controls.Add(this.txtsearchMobile);
            this.Controls.Add(this.txtsearchName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtaddress);
            this.Controls.Add(this.grdcustomer);
            this.Controls.Add(this.btndelete);
            this.Controls.Add(this.btnadd);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Customer";
            this.ShowInTaskbar = false;
            this.Text = "Customer";
            this.Load += new System.EventHandler(this.Customer_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Customer_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.grdcustomer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btndelete;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.DataGridView grdcustomer;
        private System.Windows.Forms.TextBox txtaddress;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtsearchName;
        private System.Windows.Forms.TextBox txtsearchMobile;
        private System.Windows.Forms.Button btnclear;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbosociety;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtemail;
        private System.Windows.Forms.Label Birtday;
        private System.Windows.Forms.Label Anniversary;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MaskedTextBox dtpBirthday;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ComboBox txtflatname;

        private System.Windows.Forms.TextBox txtmobile2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtmobile3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.MaskedTextBox dtpAnniversary;

        private System.Windows.Forms.Button btnUpdate;

    }
}
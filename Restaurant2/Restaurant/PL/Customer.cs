﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using Restaurant.PL.Reports;
using System.Collections;

namespace Restaurant.PL
{
    public partial class Customer : Form
    {
        BL_Field bl_field = new BL_Field();
        BL_Customer bl_customer = new BL_Customer();
        Common comm = new Common();

        main  _main;
        public Customer(main  man)
        {
            InitializeComponent();
            this.KeyPreview = true;
            _main = man;
        }

        private void Customer_Load(object sender, EventArgs e)
        {
            cbosociety.Focus();
            BindCustomer();
            BindSociety();
            BindFlatBuilding();
            btndelete.Enabled = false;
            btnUpdate.Enabled = false;
        }

        private void Customer_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
            if (e.KeyCode == Keys.F10)
            {
                SaveRecords();
            }
            if (e.KeyCode == Keys.Home)
            {
               grdcustomer.Focus();
            }
            
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            SaveRecords();
        }

        private void SaveRecords()
        {
            try
            {  
                string email = txtemail.Text;

                if (cbosociety.Text == "" && txtflatname.Text == "")
                {
                    MessageBox.Show("Please enter Society or flat name?");
                }

                else if (txtemail.Text != "" && (email.IndexOf('@') == -1 || email.IndexOf('.') == -1))
                {
                    MessageBox.Show("Invalid Email Id");
                    txtemail.Focus();

                }
                else
                {
                    SetField();
                    bl_customer.InsertCustomer(bl_field);
                    ClearField();
                    MessageBox.Show("Record inserted Successfully");
                    BindCustomer();
                    BindSociety();
                    BindFlatBuilding();
                    grdcustomer.Focus();
                }   
                
            }
            catch
            {
                MessageBox.Show("error occured!Please contact Administration?");

            }
        }
        private void SetField()
        {
            bl_field.Name = txtsearchName.Text;
            bl_field.FlatName = txtflatname.Text;
            bl_field.Mobile = txtsearchMobile.Text;
            bl_field.Address = txtaddress.Text;
            bl_field.Society = cbosociety.Text;
            bl_field.Email = txtemail.Text;
            bl_field.Mobile2 = txtmobile2.Text;
            bl_field.Mobile3 = txtmobile3.Text;
            if (dtpBirthday.MaskFull)
            {
                bl_field.Birthday = dtpBirthday.Text;
            }
            else {
                bl_field.Birthday = "";
            }
            if (dtpAnniversary.MaskFull)
            {
                bl_field.Anniversary = dtpAnniversary.Text;
            }
            else
            {
                bl_field.Anniversary = "";
            }
           

        }
        private void ClearField()
        {
            txtsearchName.Text = "";
            txtsearchMobile.Text = "";
            txtaddress.Text = "";
            cbosociety.Text = "";
            txtemail.Text = "";
            dtpAnniversary.Text = "";
            dtpBirthday.Text = "";
            txtflatname.Text = "";
            txtmobile2.Text = "";
            txtmobile3.Text = "";
        }
        private void grdcustomer_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (grdcustomer.Rows.Count > 0)
            {
                btndelete.Enabled = true;
                bl_field.CustId = grdcustomer.SelectedRows[0].Cells["CustId"].Value.ToString();
                txtsearchName.Text = grdcustomer.SelectedRows[0].Cells["Name"].Value.ToString();
                txtsearchMobile.Text = grdcustomer.SelectedRows[0].Cells["MobileNo"].Value.ToString();
                txtaddress.Text = grdcustomer.SelectedRows[0].Cells["Address"].Value.ToString();
                txtemail.Text = grdcustomer.SelectedRows[0].Cells["Email"].Value.ToString();
                cbosociety.Text = grdcustomer.SelectedRows[0].Cells["Society"].Value.ToString();
                dtpBirthday.Text = grdcustomer.SelectedRows[0].Cells["Birthday"].Value.ToString();
                dtpAnniversary.Text = grdcustomer.SelectedRows[0].Cells["Anniversary"].Value.ToString();
                txtflatname.Text = grdcustomer.SelectedRows[0].Cells["FlatName"].Value.ToString();

                txtmobile2.Text = grdcustomer.SelectedRows[0].Cells["MobileNo2"].Value.ToString();
                txtmobile3.Text = grdcustomer.SelectedRows[0].Cells["MobileNo3"].Value.ToString();



                btndelete.Enabled = true;
                btnUpdate.Enabled = true;
                btnadd.Enabled = false;


            }
        }

        private void BindCustomer()
        {
            DataTable dt = bl_customer.SelectCustomer();
            grdcustomer.DataSource = dt;
            grdcustomer.Columns["CustId"].Visible = false;
            grdcustomer.Columns["Status"].Visible = false;
        }
        private void BindSociety()
        {
            DataTable dt = bl_customer.SelectSociety();
            DataRow row = dt.NewRow();
            row["Society"] = "";
            dt.Rows.InsertAt(row, 0);
            cbosociety.DataSource = dt;
            cbosociety.DisplayMember = "Society";
            cbosociety.ValueMember = "Society";

        }
        private void BindFlatBuilding()
        {

            DataTable dt = bl_customer.SelectFlat();
            DataRow row = dt.NewRow();
            row["FlatName"] = "";
            dt.Rows.InsertAt(row, 0);
            txtflatname.DataSource = dt;
            txtflatname.DisplayMember = "FlatName";
            txtflatname.ValueMember = "FlatName";

        }
        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Customer?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_customer.DeleteCustomer(bl_field);
                    txtsearchName.Text = "";
                    MessageBox.Show("Record Delete Successfully");
                    BindCustomer();
                    BindSociety();
                    BindFlatBuilding();
                    btndelete.Enabled = false;
                    btnUpdate.Enabled = false;
                    btnadd.Enabled = true;
                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
        }

        private void txtsearchName_TextChanged(object sender, EventArgs e)
        {
            if (txtsearchName.Text != "")
            {
                if (btnadd.Text != "Save")
                {
                    bl_field.TextSearch = txtsearchName.Text;
                    DataTable dt = bl_customer.SelectCustomerName(bl_field);
                    if (dt.Rows.Count > 0)
                    {
                        grdcustomer.DataSource = dt;

                    }
                    else
                    {
                        grdcustomer.DataSource = null;
                    }
                }

            }
            else
            {
                BindCustomer();
            }
        }

        private void txtsearchName_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter)
            //{
            //    SelectCustomer();

            //}
 
        }

        private void txtsearchMobile_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter)
            //{
            //    SelectCustomer();
            //}
    
 
        }
        private void txtaddress_KeyDown(object sender, KeyEventArgs e)
        {
      
        }
        private void txtsearchMobile_TextChanged(object sender, EventArgs e)
        {
            if (txtsearchMobile.Text != "")
            {
                if (btnUpdate.Text != "Update")
                {
                    bl_field.TextSearch = txtsearchMobile.Text;
                    DataTable dt = bl_customer.SelectCustomerMobileNo(bl_field);
                    if (dt.Rows.Count > 0)
                    {
                        grdcustomer.DataSource = dt;

                    }
                    else
                    {
                        grdcustomer.DataSource = null;
                    }
                }

            }
            else
            {
                BindCustomer();
            }
        }

        private void btnclear_Click(object sender, EventArgs e)
        {
            ClearField();
            btndelete.Enabled = false;
            btnUpdate.Enabled = false;
            btnadd.Enabled = true;
        }

        private void grdcustomer_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            SelectCustomer();
        }

        private void SelectCustomer()
        {
            bl_field.CustIdStatic = grdcustomer.SelectedRows[0].Cells[0].Value.ToString();
            this.Close();
            _main.InsertCustomerById();
    
        }

        private void grdcustomer_RowEnter(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void grdcustomer_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SelectCustomer();
            }

    
        }

        private void txtsearchMobile_KeyPress(object sender, KeyPressEventArgs e)
        {
            comm.NumberOnly(e);

        }



        private void dtpBirthday_TypeValidationCompleted(object sender, TypeValidationEventArgs e)
        {
            if (dtpBirthday.Text != "")
            {
                if (!e.IsValidInput)
                {
                    toolTip1.ToolTipTitle = "Invalid Date Value";
                    toolTip1.Show("We're sorry, but the value you entered is not a valid date. Please change the value.", dtpBirthday, 5000);
                    e.Cancel = true;
                }
            }
        }

        private void cbosociety_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbosociety.Text != "System.Data.DataRowView")
            {
                if (cbosociety.ValueMember == "Society")
                {
                    bl_field.TextSearch = cbosociety.Text;
                    DataTable dt = bl_customer.SelectCustomerBySociety(bl_field);
                    grdcustomer.DataSource = dt;
                    grdcustomer.Columns["CustId"].Visible = false;
                    grdcustomer.Columns["Status"].Visible = false;
                }
            }
        }

        private void txtflatname_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (txtflatname.Text != "System.Data.DataRowView")
            {
                if (txtflatname.ValueMember == "FlatName")
                {
                    bl_field.TextSearch = txtflatname.Text;
                    DataTable dt = bl_customer.SelectCustomerByFlat(bl_field);
                    grdcustomer.DataSource = dt;
                    grdcustomer.Columns["CustId"].Visible = false;
                    grdcustomer.Columns["Status"].Visible = false;
                }
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                SetField();
                bl_customer.UpdateCustomer(bl_field);
                ClearField();
                MessageBox.Show("Record Updated Successfully");
                BindCustomer();
                btndelete.Enabled = false;
                btnUpdate.Enabled = false;
                btnadd.Enabled = true;
            }
            catch 
            {
                MessageBox.Show("error occured!Please contact Administration");
            }
        }




    }
}

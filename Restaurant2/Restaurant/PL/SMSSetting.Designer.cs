﻿namespace Restaurant.PL
{
    partial class SMSSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SMSSetting));
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txturl = new System.Windows.Forms.TextBox();
            this.txtuserid = new System.Windows.Forms.TextBox();
            this.txtpassword = new System.Windows.Forms.TextBox();
            this.btnsave = new System.Windows.Forms.Button();
            this.btnclose = new System.Windows.Forms.Button();
            this.btndelete = new System.Windows.Forms.Button();
            this.rdoon = new System.Windows.Forms.RadioButton();
            this.rdooff = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rdobOn = new System.Windows.Forms.RadioButton();
            this.rdobOFF = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(85, 210);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 35;
            this.label2.Text = "URL";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(85, 251);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 36;
            this.label1.Text = "UserId";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(85, 299);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 37;
            this.label3.Text = "Password";
            // 
            // txturl
            // 
            this.txturl.BackColor = System.Drawing.Color.AntiqueWhite;
            this.txturl.Location = new System.Drawing.Point(185, 207);
            this.txturl.Name = "txturl";
            this.txturl.Size = new System.Drawing.Size(440, 20);
            this.txturl.TabIndex = 38;
            // 
            // txtuserid
            // 
            this.txtuserid.BackColor = System.Drawing.Color.AntiqueWhite;
            this.txtuserid.Location = new System.Drawing.Point(185, 248);
            this.txtuserid.Name = "txtuserid";
            this.txtuserid.Size = new System.Drawing.Size(440, 20);
            this.txtuserid.TabIndex = 39;
            // 
            // txtpassword
            // 
            this.txtpassword.BackColor = System.Drawing.Color.AntiqueWhite;
            this.txtpassword.Location = new System.Drawing.Point(185, 292);
            this.txtpassword.Name = "txtpassword";
            this.txtpassword.PasswordChar = '*';
            this.txtpassword.Size = new System.Drawing.Size(440, 20);
            this.txtpassword.TabIndex = 40;
            // 
            // btnsave
            // 
            this.btnsave.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnsave.BackgroundImage")));
            this.btnsave.Location = new System.Drawing.Point(222, 380);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(75, 39);
            this.btnsave.TabIndex = 41;
            this.btnsave.Text = "Save";
            this.btnsave.UseVisualStyleBackColor = true;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // btnclose
            // 
            this.btnclose.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnclose.BackgroundImage")));
            this.btnclose.Location = new System.Drawing.Point(382, 380);
            this.btnclose.Name = "btnclose";
            this.btnclose.Size = new System.Drawing.Size(75, 39);
            this.btnclose.TabIndex = 43;
            this.btnclose.Text = "Close";
            this.btnclose.UseVisualStyleBackColor = true;
            this.btnclose.Click += new System.EventHandler(this.btnclose_Click);
            // 
            // btndelete
            // 
            this.btndelete.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btndelete.BackgroundImage")));
            this.btndelete.Location = new System.Drawing.Point(301, 380);
            this.btndelete.Name = "btndelete";
            this.btndelete.Size = new System.Drawing.Size(75, 39);
            this.btndelete.TabIndex = 42;
            this.btndelete.Text = "Delete";
            this.btndelete.UseVisualStyleBackColor = true;
            this.btndelete.Click += new System.EventHandler(this.btndelete_Click);
            // 
            // rdoon
            // 
            this.rdoon.AutoSize = true;
            this.rdoon.Location = new System.Drawing.Point(6, 11);
            this.rdoon.Name = "rdoon";
            this.rdoon.Size = new System.Drawing.Size(41, 17);
            this.rdoon.TabIndex = 44;
            this.rdoon.TabStop = true;
            this.rdoon.Text = "ON";
            this.rdoon.UseVisualStyleBackColor = true;
            this.rdoon.CheckedChanged += new System.EventHandler(this.rdoon_CheckedChanged);
            // 
            // rdooff
            // 
            this.rdooff.AutoSize = true;
            this.rdooff.Location = new System.Drawing.Point(6, 44);
            this.rdooff.Name = "rdooff";
            this.rdooff.Size = new System.Drawing.Size(45, 17);
            this.rdooff.TabIndex = 45;
            this.rdooff.TabStop = true;
            this.rdooff.Text = "OFF";
            this.rdooff.UseVisualStyleBackColor = true;
            this.rdooff.CheckedChanged += new System.EventHandler(this.rdooff_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.rdoon);
            this.panel1.Controls.Add(this.rdooff);
            this.panel1.Location = new System.Drawing.Point(82, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(57, 69);
            this.panel1.TabIndex = 46;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.rdobOn);
            this.panel2.Controls.Add(this.rdobOFF);
            this.panel2.Location = new System.Drawing.Point(224, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(57, 69);
            this.panel2.TabIndex = 47;
            // 
            // rdobOn
            // 
            this.rdobOn.AutoSize = true;
            this.rdobOn.Location = new System.Drawing.Point(6, 11);
            this.rdobOn.Name = "rdobOn";
            this.rdobOn.Size = new System.Drawing.Size(41, 17);
            this.rdobOn.TabIndex = 44;
            this.rdobOn.TabStop = true;
            this.rdobOn.Text = "ON";
            this.rdobOn.UseVisualStyleBackColor = true;
            this.rdobOn.CheckedChanged += new System.EventHandler(this.rdobOn_CheckedChanged);
            // 
            // rdobOFF
            // 
            this.rdobOFF.AutoSize = true;
            this.rdobOFF.Location = new System.Drawing.Point(6, 44);
            this.rdobOFF.Name = "rdobOFF";
            this.rdobOFF.Size = new System.Drawing.Size(45, 17);
            this.rdobOFF.TabIndex = 45;
            this.rdobOFF.TabStop = true;
            this.rdobOFF.Text = "OFF";
            this.rdobOFF.UseVisualStyleBackColor = true;
            this.rdobOFF.CheckedChanged += new System.EventHandler(this.rdobOFF_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(30, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 48;
            this.label4.Text = "Switch";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(173, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 49;
            this.label5.Text = "Bill Sms";
            // 
            // SMSSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(253)))), ((int)(((byte)(244)))));
            this.ClientSize = new System.Drawing.Size(658, 537);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.btnclose);
            this.Controls.Add(this.btndelete);
            this.Controls.Add(this.txtpassword);
            this.Controls.Add(this.txtuserid);
            this.Controls.Add(this.txturl);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SMSSetting";
            this.Text = "SMSSetting";
            this.Load += new System.EventHandler(this.SMSSetting_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SMSSetting_KeyDown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txturl;
        private System.Windows.Forms.TextBox txtuserid;
        private System.Windows.Forms.TextBox txtpassword;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Button btnclose;
        private System.Windows.Forms.Button btndelete;
        private System.Windows.Forms.RadioButton rdoon;
        private System.Windows.Forms.RadioButton rdooff;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton rdobOn;
        private System.Windows.Forms.RadioButton rdobOFF;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}
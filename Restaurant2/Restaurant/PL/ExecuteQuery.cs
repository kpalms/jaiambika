﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using Restaurant.BL;
using System.Data.SqlClient;
using Restaurant.PL;


namespace Restaurant.PL
{
    public partial class ExecuteQuery : Form
    {
        public ExecuteQuery()
        {
            InitializeComponent();
        }

        private void btnsubmit_Click(object sender, EventArgs e)
        {
            try
            {
                BL_Security bl_security = new BL_Security();

                bl_security.ExcuteQuery(txtquery.Text);
                MessageBox.Show("Query Excuted? Please Close This Form");
                txtquery.Text = "";
            }
            catch (Exception ex)
            {

                MessageBox.Show("error" + ex);
            }

        }

        private void ExecuteQuery_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using Restaurant.PL.Reports;
using System.Globalization;


using System.Data;
namespace Restaurant
{
    public partial class SaleSummery : Form
    {

        BL_Sale bl_sale = new BL_Sale();
        BL_Field bl_field = new BL_Field();
        Common common = new Common();


        public SaleSummery()
        {
            InitializeComponent();
        }

        private void btnbillshow_Click(object sender, EventArgs e)
        {
            Sale();
        }


        private void Sale()
        {
            bl_field.FromDate = dtpfrom.Text;
            bl_field.Todate = dtpto.Text;

            DataTable dt = bl_sale.SaleSummery(bl_field);
            grdsale.DataSource = dt;

            if (dt.Rows.Count > 0)
            {
                decimal food = 0;
                decimal Liquor = 0;
                decimal Total = 0;
                decimal Dis = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    food = food + Convert.ToDecimal(dt.Rows[i]["Food"].ToString());
                    Liquor = Liquor + Convert.ToDecimal(dt.Rows[i]["Liquor"].ToString());
                    Total = Total + Convert.ToDecimal(dt.Rows[i]["Total"].ToString());
                    Dis = Dis + Convert.ToDecimal(dt.Rows[i]["DiscountAmount"].ToString());
                }

                lblfood.Text = Convert.ToString(food);
                lblliquor.Text = Convert.ToString(Liquor);
                lbltotal.Text = Convert.ToString(Total);
                lblDiscount.Text = Convert.ToString(Dis);

            }
            else
            {
                lblfood.Text = Convert.ToString(0.00);
                lblliquor.Text = Convert.ToString(0.00);
                lbltotal.Text = Convert.ToString(0.00);
                lblDiscount.Text = Convert.ToString(0.00);
            }

            lblcreditamt.Text = common.ReturnOneValue("select ((Debit)-(Credit)) as CreditAmount from Installment where convert(datetime, Date, 103) between convert(datetime, '" + dtpfrom.Text + "', 103) and convert(datetime, '" + dtpto.Text + "', 103) ");
            lblrunning.Text = common.ReturnOneValue("select sum(Price*Qty) as Total from OrderDetail where Status='Open'");
            lblpending.Text = common.ReturnOneValue("select sum(od.Price*od.Qty) -(o.DiscountAmount) as Total from Orders o inner join OrderDetail od on o.OrderId=od.OrderId where od.Status='P' group by o.DiscountAmount");

        }

        private void SaleSummery_Load(object sender, EventArgs e)
        {
            dtpfrom.CustomFormat = "dd/MM/yyyy";
            dtpto.CustomFormat = "dd/MM/yyyy";

            //dtpfrom.Text = DateTime.Now.ToString("dd/MM/yyyy");
            //dtpto.Text = DateTime.Now.ToString("dd/MM/yyyy");

            Sale();

        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using Restaurant.BL;
namespace Restaurant
{
    public partial class MenuHead : Form
    {
        BL_Field bl_field = new BL_Field();
        BL_TreeView bl_treeview = new BL_TreeView();
  

        public MenuHead()
        {
            InitializeComponent();
            this.KeyPreview = true;
        }

        private void MenuHead_Load(object sender, EventArgs e)
        {
            BindMenu();
            btnUpdate.Enabled = false;
            btndelete.Enabled = false;
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            try
            {
                 if (txtname.Text == "")
                    {
                        MessageBox.Show("Please enter Name?");
                    }
                    else
                    {

                        SetField();
                        bl_treeview.InsertTreeViewMenu(bl_field);
                        Clear();
                        MessageBox.Show("Record inserted Successfully");
                        BindMenu();
                        txtname.Focus();
                    }
            }
            catch
            {
                MessageBox.Show("error occured!Please contact Administration?");
            }
        }

        private void SetField()
        {
            bl_field.MenuName = txtname.Text;

        }
        private void Clear()
        {
            txtname.Text = "";
            btnUpdate.Enabled = false;
            btndelete.Enabled = false;
            btnadd.Enabled = true;

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            bl_field.MenuId = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[0].Value.ToString());
            txtname.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            btnUpdate.Enabled = true;
            btndelete.Enabled = true;
            btnadd.Enabled = false;
        }
        private void BindMenu()
        {
            DataTable dt = bl_treeview.SelectTreeViewMenu(bl_field);
            dataGridView1.DataSource = dt;
            dataGridView1.Columns["MenuId"].Visible = false;
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            Clear();
            txtname.Text = "";
            txtname.Focus();
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult msgg = MessageBox.Show("Are you sure you want to delete selected Menu Name?", "Delete", MessageBoxButtons.YesNo);
                if (msgg == DialogResult.Yes)
                {
                    bl_treeview.DeleteTreeViewMenu(bl_field);
                    MessageBox.Show("Record Delete Successfully");
                    BindMenu();
                    Clear();
                    txtname.Focus();
                }
            }
            catch
            {

                MessageBox.Show("error occured!Please contact Administration?");
            }
        }

        private void MenuHead_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtname.Text == "")
                {
                    MessageBox.Show("Please enter Name?");
                }
                else
                {
                    SetField();
                    bl_treeview.UpdateTreeViewMenu(bl_field);
                    Clear();
                    MessageBox.Show("Record Updated Successfully");
                    BindMenu();
                    txtname.Focus();
                }
            }
            catch
            {
                MessageBox.Show("Error Occured! Please Contact Administration");
            }
        }







    }
}

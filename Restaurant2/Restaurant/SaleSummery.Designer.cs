﻿namespace Restaurant
{
    partial class SaleSummery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnbillshow = new System.Windows.Forms.Button();
            this.dtpto = new System.Windows.Forms.DateTimePicker();
            this.dtpfrom = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.grdsale = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblfood = new System.Windows.Forms.Label();
            this.lblliquor = new System.Windows.Forms.Label();
            this.lbltotal = new System.Windows.Forms.Label();
            this.lblDiscount = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblcreditamt = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblrunning = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblpending = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grdsale)).BeginInit();
            this.SuspendLayout();
            // 
            // btnbillshow
            // 
            this.btnbillshow.BackgroundImage = global::Restaurant.Properties.Resources.button;
            this.btnbillshow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnbillshow.Location = new System.Drawing.Point(348, 79);
            this.btnbillshow.Name = "btnbillshow";
            this.btnbillshow.Size = new System.Drawing.Size(75, 23);
            this.btnbillshow.TabIndex = 96;
            this.btnbillshow.Text = "ShowBill";
            this.btnbillshow.UseVisualStyleBackColor = true;
            this.btnbillshow.Click += new System.EventHandler(this.btnbillshow_Click);
            // 
            // dtpto
            // 
            this.dtpto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpto.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpto.Location = new System.Drawing.Point(207, 79);
            this.dtpto.Name = "dtpto";
            this.dtpto.Size = new System.Drawing.Size(132, 20);
            this.dtpto.TabIndex = 95;
            // 
            // dtpfrom
            // 
            this.dtpfrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpfrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpfrom.Location = new System.Drawing.Point(62, 79);
            this.dtpfrom.Name = "dtpfrom";
            this.dtpfrom.Size = new System.Drawing.Size(132, 20);
            this.dtpfrom.TabIndex = 94;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Maroon;
            this.label5.Location = new System.Drawing.Point(224, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(153, 25);
            this.label5.TabIndex = 93;
            this.label5.Text = "Sale Summery";
            // 
            // grdsale
            // 
            this.grdsale.AllowUserToAddRows = false;
            this.grdsale.AllowUserToDeleteRows = false;
            this.grdsale.AllowUserToResizeColumns = false;
            this.grdsale.AllowUserToResizeRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            this.grdsale.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.grdsale.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdsale.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.OldLace;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Maroon;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdsale.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.grdsale.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Maroon;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.Maroon;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdsale.DefaultCellStyle = dataGridViewCellStyle9;
            this.grdsale.EnableHeadersVisualStyles = false;
            this.grdsale.Location = new System.Drawing.Point(61, 117);
            this.grdsale.MultiSelect = false;
            this.grdsale.Name = "grdsale";
            this.grdsale.ReadOnly = true;
            this.grdsale.RowHeadersVisible = false;
            this.grdsale.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdsale.Size = new System.Drawing.Size(543, 388);
            this.grdsale.TabIndex = 92;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Maroon;
            this.label1.Location = new System.Drawing.Point(61, 525);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 25);
            this.label1.TabIndex = 97;
            this.label1.Text = "Food";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Maroon;
            this.label2.Location = new System.Drawing.Point(155, 525);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 25);
            this.label2.TabIndex = 98;
            this.label2.Text = "Liquor";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Maroon;
            this.label3.Location = new System.Drawing.Point(372, 525);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 25);
            this.label3.TabIndex = 99;
            this.label3.Text = "Total";
            // 
            // lblfood
            // 
            this.lblfood.AutoSize = true;
            this.lblfood.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblfood.ForeColor = System.Drawing.Color.Maroon;
            this.lblfood.Location = new System.Drawing.Point(61, 560);
            this.lblfood.Name = "lblfood";
            this.lblfood.Size = new System.Drawing.Size(61, 25);
            this.lblfood.TabIndex = 100;
            this.lblfood.Text = "Food";
            // 
            // lblliquor
            // 
            this.lblliquor.AutoSize = true;
            this.lblliquor.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblliquor.ForeColor = System.Drawing.Color.Maroon;
            this.lblliquor.Location = new System.Drawing.Point(156, 560);
            this.lblliquor.Name = "lblliquor";
            this.lblliquor.Size = new System.Drawing.Size(54, 25);
            this.lblliquor.TabIndex = 101;
            this.lblliquor.Text = "0.00";
            // 
            // lbltotal
            // 
            this.lbltotal.AutoSize = true;
            this.lbltotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltotal.ForeColor = System.Drawing.Color.Maroon;
            this.lbltotal.Location = new System.Drawing.Point(377, 560);
            this.lbltotal.Name = "lbltotal";
            this.lbltotal.Size = new System.Drawing.Size(54, 25);
            this.lbltotal.TabIndex = 102;
            this.lbltotal.Text = "0.00";
            // 
            // lblDiscount
            // 
            this.lblDiscount.AutoSize = true;
            this.lblDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiscount.ForeColor = System.Drawing.Color.Maroon;
            this.lblDiscount.Location = new System.Drawing.Point(251, 560);
            this.lblDiscount.Name = "lblDiscount";
            this.lblDiscount.Size = new System.Drawing.Size(54, 25);
            this.lblDiscount.TabIndex = 104;
            this.lblDiscount.Text = "0.00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Maroon;
            this.label6.Location = new System.Drawing.Point(250, 525);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 25);
            this.label6.TabIndex = 103;
            this.label6.Text = "Discount";
            // 
            // lblcreditamt
            // 
            this.lblcreditamt.AutoSize = true;
            this.lblcreditamt.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcreditamt.ForeColor = System.Drawing.Color.Maroon;
            this.lblcreditamt.Location = new System.Drawing.Point(490, 560);
            this.lblcreditamt.Name = "lblcreditamt";
            this.lblcreditamt.Size = new System.Drawing.Size(54, 25);
            this.lblcreditamt.TabIndex = 106;
            this.lblcreditamt.Text = "0.00";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Maroon;
            this.label7.Location = new System.Drawing.Point(490, 525);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(114, 25);
            this.label7.TabIndex = 105;
            this.label7.Text = "Credit Amt";
            // 
            // lblrunning
            // 
            this.lblrunning.AutoSize = true;
            this.lblrunning.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblrunning.ForeColor = System.Drawing.Color.Maroon;
            this.lblrunning.Location = new System.Drawing.Point(63, 651);
            this.lblrunning.Name = "lblrunning";
            this.lblrunning.Size = new System.Drawing.Size(54, 25);
            this.lblrunning.TabIndex = 108;
            this.lblrunning.Text = "0.00";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Maroon;
            this.label8.Location = new System.Drawing.Point(61, 612);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(146, 25);
            this.label8.TabIndex = 107;
            this.label8.Text = "Running Total";
            // 
            // lblpending
            // 
            this.lblpending.AutoSize = true;
            this.lblpending.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblpending.ForeColor = System.Drawing.Color.Maroon;
            this.lblpending.Location = new System.Drawing.Point(252, 651);
            this.lblpending.Name = "lblpending";
            this.lblpending.Size = new System.Drawing.Size(54, 25);
            this.lblpending.TabIndex = 110;
            this.lblpending.Text = "0.00";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Maroon;
            this.label9.Location = new System.Drawing.Point(250, 612);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(146, 25);
            this.label9.TabIndex = 109;
            this.label9.Text = "Pending Total";
            // 
            // SaleSummery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Ivory;
            this.ClientSize = new System.Drawing.Size(664, 734);
            this.Controls.Add(this.lblpending);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lblrunning);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblcreditamt);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblDiscount);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lbltotal);
            this.Controls.Add(this.lblliquor);
            this.Controls.Add(this.lblfood);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnbillshow);
            this.Controls.Add(this.dtpto);
            this.Controls.Add(this.dtpfrom);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.grdsale);
            this.Name = "SaleSummery";
            this.Text = "SaleSummery";
            this.Load += new System.EventHandler(this.SaleSummery_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdsale)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnbillshow;
        private System.Windows.Forms.DateTimePicker dtpto;
        private System.Windows.Forms.DateTimePicker dtpfrom;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView grdsale;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblfood;
        private System.Windows.Forms.Label lblliquor;
        private System.Windows.Forms.Label lbltotal;
        private System.Windows.Forms.Label lblDiscount;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblcreditamt;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblrunning;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblpending;
        private System.Windows.Forms.Label label9;
    }
}
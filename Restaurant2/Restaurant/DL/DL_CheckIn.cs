﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;

namespace Restaurant.DL
{
    class DL_CheckIn
    {
        DL_Connection dl_connection = new DL_Connection();




        public string InsertCheckIn(BL_Field bl_field)
        {
            string str = "insert into CheckIn(Billno,CustomerName, MobileNo,RoomNo,Rate,Persons,ExtraPersons,ExtraRate,Advance,Dateofarr,ArrTime,cashier,Status)values('" + bl_field.ReceiptNo + "','" + bl_field.CustomerName + "','" + bl_field.Mobile + "','" + bl_field.RoomNo + "','" + bl_field.Rate + "','" + bl_field.Person + "','" + bl_field.ExtraPersons + "','" + bl_field.ExtraRate + "','" + bl_field.Advance + "','" + bl_field.Dateofarr + "','" + bl_field.ArrTime + "','"+bl_field.Cashier+"','"+bl_field.Status+"');SELECT SCOPE_IDENTITY()";
            string id=dl_connection.UseExcuteScaler(str);
            return id;

        }

        public void UpdateCheckIn(BL_Field bl_field)
        {
            string str = "update CheckIn set CustomerName='" + bl_field.CustomerName + "',MobileNo='" + bl_field.Mobile + "',RoomNo='" + bl_field.RoomNo + "',Rate='" + bl_field.Rate + "',Persons='" + bl_field.Person + "',ExtraPersons='" + bl_field.ExtraPersons + "',ExtraRate='" + bl_field.ExtraRate + "',Advance='" + bl_field.Advance + "',Dateofarr='" + bl_field.Dateofarr + "',ArrTime='" + bl_field.ArrTime + "',cashier='"+bl_field.Cashier+"' where CheckInId='" + bl_field.CheckInId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void DeleteCheckIn(BL_Field bl_field)
        {
            string str = "delete from  CheckIn where CheckInId='" + bl_field.CheckInId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }

        public DataTable SelectCheckIn(BL_Field bl_field)
        {
            string str = "select * from CheckIn where Status='"+bl_field.Status+"'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }

        public DataTable SelectRoomTotal(BL_Field bl_field)
        {
            string str = "select sum(Price) from AddRoom where RoomNo='" + bl_field .RoomNo+ "'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }


        public DataTable SelectCheckInById(BL_Field bl_field)
        {
            string str = "select * from CheckIn where CheckInId='" + bl_field.CheckInId + "'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }

        public void InsertCheckOut(BL_Field bl_field)
        {
            string str = "insert into CheckOut(CheckInId,CheckOutBillNo,DateofDep,DepTime,NoOfDays,Charges,LTaxRate,LTaxAmount,DiscountAmount,Telephone,Misc,Total,Cashier,Status)values('" + bl_field.CheckInId + "','" + bl_field.ReceiptNo + "','" + bl_field.DateofDep + "','" + bl_field.DepTime + "','" + bl_field.NoOfDays + "','" + bl_field.Charges + "','" + bl_field.LTaxRate + "','" + bl_field.LTax + "','" + bl_field.Discount + "','" + bl_field.Telephone + "','" + bl_field.Misc + "','" + bl_field.Total + "','" + bl_field.Cashier + "','" + bl_field.Status + "')";
            string cout="update Checkin set Status='"+bl_field.Status+"' where CheckInId='"+bl_field.CheckInId+"'";
            dl_connection.UseExcuteScaler(str);
            dl_connection.UseExcuteScaler(cout);
        }


        public void DeleteCheckOut(BL_Field bl_field)
        {
            string str = "delete from  CheckIn where CheckInId='" + bl_field.CheckInId + "'";
            string str2 = "delete from  CheckOut where CheckInId='" + bl_field.CheckInId + "'";
            dl_connection.UseExcuteNonQuery(str);
            dl_connection.UseExcuteNonQuery(str2);

            string upp1 = "update CheckIn set Billno=Billno-1 where CheckInId > '" + bl_field.CheckInId + "'";
            string upp2 = "update CheckOut set CheckOutBillNo=CheckOutBillNo-1 where CheckInId > '" + bl_field.CheckInId + "'";
            dl_connection.UseExcuteNonQuery(upp1);
            dl_connection.UseExcuteNonQuery(upp2);

        }


        public DataTable SelectCheckInOut(BL_Field bl_field)
        {
            string str = "select inn.*,out.* from CheckIn inn inner join CheckOut out on inn.CheckInId=out.CheckInId where inn.Status='" + bl_field.Status + "' order by  convert(datetime, DateofDep, 103) desc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }
    }
}

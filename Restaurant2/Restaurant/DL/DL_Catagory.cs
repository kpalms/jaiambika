﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;

namespace Restaurant.DL
{
    class DL_Catagory
    {
        DL_Connection dl_connection = new DL_Connection();

        private const string CatId = "CatId";
        private const string CatagoryName = "CatagoryName";
        private const string tblCatagory = "Catagory";

        private const string SubCatId = "SubCatId";
        private const string SubCatName = "SubCatName";
        private const string tblSubCatagory = "SubCatagory";
        private const string Price = "Price";
        private const string No = "No";
        private const string ShortName = "ShortName";
        //catagory
        public void InsertCatagory(BL_Field bl_field)
        {
            string str = "insert into " + tblCatagory + "(" + CatagoryName + ",Type,Sequence)values('" + bl_field.CatagoryName + "','" + bl_field.Type + "','"+bl_field.Sequence+"')";
            dl_connection.UseExcuteNonQuery(str);
        }
        public DataTable SelectCatagory(BL_Field bl_field)
        {
            string str = "select * from " + tblCatagory + " order by Sequence asc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        public DataTable Catagory(BL_Field bl_field)
        {
           // string str = "select * from " + tblCatagory + "  order by Sequence asc";
            string str = "select CatagoryName, case when Type=0 then 'Reastaurant' else 'Bar' end as Type, CatId,Sequence  from " + tblCatagory + " order by Sequence asc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }


        //public DataTable SearchCatagory(BL_Field bl_field)
        //{
        //    string str = "select * from " + tblCatagory + " where " + CatagoryName + " like '" + bl_field.TextSearch + "%'  order by CatagoryName asc";
        //    DataTable dt = dl_connection.UseDatatable(str);
        //    return dt;
        //}

        public DataTable SelectBottal(BL_Field bl_field)
        {
            string str = "select * from " + tblSubCatagory + " where SaleByML='Yes' order by SubCatName asc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }
        public DataTable SelectItemBySearch(BL_Field bl_field)
        {
            //string str = "select sc.SubCatId,sc.SubCatName as ItemName,sc.CatId,sc.Price,sc.No,sc.ShortName,c.Type from " + tblSubCatagory + " sc inner join Catagory c on sc.CatId=c.CatId  where " + SubCatName + " like '" + bl_field.TextSearch + "%'";
            string str = "SELECT CA.SubCatId,CA.SubCatName as ItemName,CA.CatId,RS.Rate AS Price,CA.No,CA.ShortName,C.Type FROM RateSlap RS INNER JOIN  SubCatagory CA  ON CA.SubCatId=RS.SubCatId INNER JOIN  RateAssign RA  ON RA.RateCatId=RS.RateCatId AND RA.DpId='" + bl_field.DpId + "' INNER JOIN  Catagory C ON C.CatId=CA.CatId where CA.SubCatName like '" + bl_field.TextSearch + "%' order by SubCatName asc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        public DataTable SelectItemBySearchNo(BL_Field bl_field)
        {
            //string str = "select sc.SubCatId,sc.SubCatName as ItemName,sc.CatId,sc.Price,sc.No,sc.ShortName,c.Type   from " + tblSubCatagory + " sc inner join Catagory c on sc.CatId=c.CatId where sc." + No + " ='" + bl_field.TextSearch + "'";
            string str = "SELECT CA.SubCatId,CA.SubCatName as ItemName,CA.CatId,RS.Rate AS Price,CA.No,CA.ShortName,C.Type FROM RateSlap RS INNER JOIN  SubCatagory CA  ON CA.SubCatId=RS.SubCatId INNER JOIN  RateAssign RA  ON RA.RateCatId=RS.RateCatId AND RA.DpId='" + bl_field.DpId + "' INNER JOIN  Catagory C ON C.CatId=CA.CatId where CA.No like '" + bl_field.TextSearch + "%' order by SubCatName asc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }

        public void UpdateCatagory(BL_Field bl_field)
        {
            string str = "update " + tblCatagory + " set " + CatagoryName + "='" + bl_field.CatagoryName + "',Type='" + bl_field.Type + "',Sequence='" + bl_field.Sequence + "' where " + CatId + "='" + bl_field.CatId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void DeleteCatagory(BL_Field bl_field)
        {
            string str = "delete from "+tblCatagory +" where "+CatId +"='"+bl_field .CatId +"'";
            dl_connection.UseExcuteNonQuery(str);
        }
       
        //subcatagory
        public void InsertSubCatagory(BL_Field bl_field)
        {
            string str = "insert into " + tblSubCatagory + "(" + SubCatName + "," + CatId + "," + No + "," + ShortName + ")values('" + bl_field.SubcatagoryName + "','" + bl_field.CatId + "','" + bl_field.No + "','" + bl_field.ShortName + "')";
            dl_connection.UseExcuteNonQuery(str);
        }
        public DataTable SelectSubCatagory(string cataid)
        {
            string str = "select sc.SubCatId,sc.SubCatName as ItemName,sc.CatId,sc.No,sc.ShortName,c.Type from " + tblSubCatagory + " sc inner join Catagory c on sc.CatId=c.CatId where sc." + CatId + "='" + cataid + "' order by SubCatName asc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }

        public DataTable SelectSubCatagorydp(BL_Field bl_field)
        {
            string str = "SELECT CA.SubCatId,CA.SubCatName as ItemName,CA.CatId,RS.Rate AS Price,CA.No,CA.ShortName,C.Type FROM RateSlap RS INNER JOIN  SubCatagory CA  ON CA.SubCatId=RS.SubCatId AND RS.CatId='" + bl_field.SaticCatId + "' INNER JOIN  RateAssign RA  ON RA.RateCatId=RS.RateCatId AND RA.DpId='" + bl_field.DpId + "'INNER JOIN  Catagory C ON C.CatId=CA.CatId order by SubCatName asc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }

        public DataTable GetSubCatagory(string cataid)
        {
            string str = "select * from " + tblSubCatagory + " where " + CatId + "='" + cataid + "' order by SubCatName asc";
            //string str = "select s.*,t.Type from SubCatagory s inner join tblType t on s.TypeId=t.TypeId where " + CatId + "='" + cataid + "' order by SubCatName asc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }
        public void UpdateSubCatagory(BL_Field bl_field)
        {
            string str = "update " + tblSubCatagory + " set " + SubCatName + "='" + bl_field.SubcatagoryName + "'," + No + "='" + bl_field.No + "'," + ShortName + "='" + bl_field.ShortName + "' where " + SubCatId + "='" + bl_field.SubcatId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void DeleteSubCatagory(BL_Field bl_field)
        {
            string str = "delete from " + tblSubCatagory + " where " + SubCatId + "='" + bl_field.SubcatId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }

        public string GetIdCatagory(BL_Field bl_field)
        {
          string str = "select "+CatId +" from "+tblCatagory +" where "+CatagoryName +" ='"+bl_field .CatagoryName +"'";
          string id=  dl_connection.UseExcuteScaler(str);
          return id;
        }

        public string GetNo(BL_Field bl_field)
        {
            string str = "select " + No + " from " + tblSubCatagory + " where " + No  + " ='" + bl_field.No + "'";
            string id = dl_connection.UseExcuteScaler(str);
            return id;
        }


        public DataTable SearchShortName(BL_Field bl_field)
        {
            //string str = "select sc.SubCatId,sc.SubCatName as ItemName,sc.CatId,sc.Price,sc.No,sc.ShortName,c.Type  from " + tblSubCatagory + " sc inner join Catagory c on sc.CatId=c.CatId where sc." + ShortName + " like '" + bl_field.TextSearch + "%'";
            string str = "SELECT CA.SubCatId,CA.SubCatName as ItemName,CA.CatId,RS.Rate AS Price,CA.No,CA.ShortName,C.Type FROM RateSlap RS INNER JOIN  SubCatagory CA  ON CA.SubCatId=RS.SubCatId INNER JOIN  RateAssign RA  ON RA.RateCatId=RS.RateCatId AND RA.DpId='" + bl_field.DpId + "' INNER JOIN  Catagory C ON C.CatId=CA.CatId where CA.ShortName like '" + bl_field.TextSearch + "%' or CA.No like '" + bl_field.TextSearch + "%' order by SubCatName asc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }



        //group
        public void InsertType(BL_Field bl_field)
        {
            string str = "insert into tblType(Type)values('" + bl_field.Type + "')";
            dl_connection.UseExcuteNonQuery(str);
        }

        public void UpdateType(BL_Field bl_field)
        {
            string str = "update tblType set Type='" + bl_field.Type + "' where TypeId='" + bl_field.TypeId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void DeleteType(BL_Field bl_field)
        {
            string str = "delete from tblType where TypeId='" + bl_field.TypeId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }

        public DataTable SelectType()
        {
            string str = "select * from tblType";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }




        //kot remark
        public void InsertKotRemark(BL_Field bl_field)
        {
            string str = "insert into KotRemark(KotName)values('" + bl_field.AddKotRemark + "')";
            dl_connection.UseExcuteNonQuery(str);
        }

        public void UpdateKotRemark(BL_Field bl_field)
        {
            string str = "update KotRemark set KotName='" + bl_field.AddKotRemark + "' where KotId='" + bl_field.AddKotId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void DeleteKotRemark(BL_Field bl_field)
        {
            string str = "delete from KotRemark where KotId='" + bl_field.AddKotId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }

        public DataTable SelectKotRemark()
        {
            string str = "select * from KotRemark";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        public DataTable SearchKotRemark(BL_Field bl_field)
        {
            string str = "select * from KotRemark where KotName like'" + bl_field.TextSearch + "%'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }


    }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;


namespace Restaurant.DL
{
    class DL_Serial
    {
        DL_Connection dl_connection = new DL_Connection();
        private const string key = "Skey";
        private const string SerialKey = "SerialKey";
        private const string Status = "Status";
        private const string No = "NO";
        private const string tblTrail = "Trail";
        public void CreateTable()
        {
            string str = "CREATE TABLE [dbo].[SerialKey]([Skey] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,[Status] [char](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL) ON [PRIMARY]";
            dl_connection.UseExcuteNonQuery(str );

        }
        
        public void InsertKey(BL_Field bl_field)
        {
            string str = "insert into SerialKey("+ key +","+ Status +")values('"+bl_field .SerialKey +"','F')";
            dl_connection.UseExcuteNonQuery(str );

        }
        public void EnterdKey(BL_Field bl_field)
        {
            string str = "update SerialKey set Status='T'";
            dl_connection.UseExcuteNonQuery(str );

        }
        public SqlDataReader SelectKey(BL_Field bl_field)
        {
            string str = "select * from " + SerialKey + "";
           SqlDataReader dr= dl_connection.UseExcuteDataReader(str );
           return dr;

        }



        public void Try(BL_Field bl_field)
        {
            string str = "insert into Trail(" + No + ")values(0)";
            dl_connection.UseExcuteNonQuery(str);

        }

        public string SelectMaxNo()
        {
            string str = "select Count(No) from " + tblTrail + "";
            string tbl = dl_connection.UseExcuteScaler(str);
            return tbl;

        }








    }
}

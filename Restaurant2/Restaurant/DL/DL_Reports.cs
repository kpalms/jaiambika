﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;


namespace Restaurant.DL
{
    class DL_Reports
    {

        DL_Connection dl_connection = new DL_Connection();

        private const string SaleId = "OrderDetailId";
        private const string TableNo = "TableNo";
        private const string Person = "Person";
        private const string ItemName = "ItemName";
        private const string Price = "Price";
        private const string Qty = "Qty";
        private const string Dis = "Dis";
        private const string Total = "Total";
        private const string Date = "Date";
        private const string Status = "Status";
        private const string ReceiptNo = "ReceiptNo";
        private const string Qt = "Qt";
        private const string tblSale = "OrderDetail";

        private const string CatagoryName = "CatagoryName";
        private const string SubCatName = "SubCatName";
        private const string CatId = "CatId";

        private const string tblCatagory = "Catagory";
        private const string tblSubCatagory = "SubCatagory";
        private const string tblPurchase = "Purchase";
        private const string ParsalNo = "ParsalNo";
        private const string CustId = "CustId";
        private const string SubTotal = "SubTotal";

        private const string GrandTotal = "GrandTotal";
        private const string DisAmount = "DisAmount";



        
        public DataSet SearchTodayClose(BL_Field bl_field)
        {
            string str = "select  tblOne.Date,tblOne.RestoTotal,tblTwo.BarTotal,tblTrhee.CheckInAdvance,tblfour.CheckOutTotal from (select Date,sum(total)RestoTotal from Orders where type='Restaurant' and Status='Close' and Date='" + bl_field.Date + "' group by Date) as tblOne,(select sum(total)BarTotal from Orders where type='Bar' and Status='Close' and Date='" + bl_field.Date + "' ) as tblTwo,(select sum(Advance)CheckInAdvance from CheckIn where Status='CheckIn' and Dateofarr='" + bl_field.Date + "' )as tblTrhee,(select sum(Total)CheckOutTotal from CheckOut where Status='CheckOut' and DateofDep='" + bl_field.Date + "')as tblfour";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }

        public DataSet SearchSaleByItem(BL_Field bl_field)
        {
           // string str = "select s.ItemName+' '+s.Unit+' '+s.UnitName  as ItemName ,sum(s." + Qty + ")as Qty,s.Price,sum(Total)as Total,c.CatagoryName from " + tblSale + " s inner join SubCatagory sb on s.SubCatId=sb.SubCatId inner join Catagory c on s.CatId=c.CatId where convert(datetime, s." + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103)  and s." + Status + "='" + bl_field.Status + "' and s.Type='" + bl_field.TypeStatic + "' and s.PendingStatus='Paid' group by s.ItemName,s.Price,s.Unit,s.UnitName,c.CatagoryName order by s.ItemName asc";
            string str = "select s.ItemName,sum(s." + Qty + ")as Qty,s.Price,sum(s.Price*s.Qty)as Total,c.CatagoryName,b.DiscountAmount as DiscountAmount from " + tblSale + " s inner join SubCatagory sb on s.subCatId  = sb.subCatId inner join Catagory c on c.catid= sb.catid inner join Orders b on s.OrderId=b.OrderId where convert(datetime, s." + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103)  and b.PendingStatus='1' group by s.ItemName,s.Price,c.CatagoryName,b.DiscountAmount order by s.ItemName asc";
 
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }

        public DataSet SearchSaleByItemBar(BL_Field bl_field)
        {
            // string str = "select s.ItemName+' '+s.Unit+' '+s.UnitName  as ItemName ,sum(s." + Qty + ")as Qty,s.Price,sum(Total)as Total,c.CatagoryName from " + tblSale + " s inner join SubCatagory sb on s.SubCatId=sb.SubCatId inner join Catagory c on s.CatId=c.CatId where convert(datetime, s." + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103)  and s." + Status + "='" + bl_field.Status + "' and s.Type='" + bl_field.TypeStatic + "' and s.PendingStatus='Paid' group by s.ItemName,s.Price,s.Unit,s.UnitName,c.CatagoryName order by s.ItemName asc";
            string str = "select s.ItemName,sum(s." + Qty + ")as Qty,s.Price,sum(s.Price*s.Qty)as Total,c.CatagoryName from " + tblSale + " s inner join SubCatagory sb on s.subCatId  = sb.subCatId inner join Catagory c on c.catid= sb.catid inner join Orders b on s.OrderId=b.OrderId where convert(datetime, s." + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103)  and b.PendingStatus='1' and s.Type=1 group by s.ItemName,s.Price,c.CatagoryName order by s.ItemName asc";

            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }

        public DataSet Yesturday(BL_Field bl_field)
        {
           string str = "select Date, " + ReceiptNo + " ," + Total + " as Total from Orders where Date ='" + bl_field.Date + "' and "+Status+"='"+bl_field.Status+"'";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }

        public DataSet SearchSaleByTable(BL_Field bl_field)
        {
            string str = "select s.Date,s.TableNo,sum(s.Price*s.Qty)-(b.DiscountAmount)  as Total from Orders b inner join OrderDetail s on s.OrderId=b.OrderId where convert(datetime, s." + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) and PendingStatus='1'  group by s.TableNo,s.Date,b.DiscountAmount order by Date asc,TableNo asc";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }

        public DataSet SearchSaleByBillNo(BL_Field bl_field)
        {
            string str = "select b.Date, b.BillNo as ReceiptNo ,sum(s.Price*s.Qty)-(b.DiscountAmount) as Total from OrderDetail s inner join Orders b on s.OrderId=b.OrderId where convert(datetime, b." + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) and PendingStatus='1' and b.CancelBill=0  group by b.Date, b.BillNo,b.DiscountAmount order by b.BillNo asc";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        public DataSet SearchSaleByBillNoBar(BL_Field bl_field)
        {
            string str = "select b.Date, b.BillNo as ReceiptNo ,sum(s.Price*s.Qty)-(b.DiscountAmount) as Total from OrderDetail s inner join Orders b on s.OrderId=b.OrderId where convert(datetime, b." + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) and b.PendingStatus='1' and s.Type=1 and b.CancelBill=0  group by b.Date, b.BillNo,b.DiscountAmount order by b.BillNo asc";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        public DataSet SearchCashierWise(BL_Field bl_field)
        {
            string str = "select b.Date,b.BillNo ,sum(s.Price*s.Qty)-(b.DiscountAmount) as Total,b.CashierName from Orders b inner join OrderDetail s on b.OrderId=s.OrderId where convert(datetime, b." + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) and CashierName='" + bl_field.CashierNameStatic + "' and b.PendingStatus='1' and b.CancelBill=0  group by b.Date,b.BillNo,b.CashierName,b.DiscountAmount order by b.BillNo asc";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        public DataSet SearchBillNo(BL_Field bl_field)
        {
            string str = "select b.Date,b.BillNo as ReceiptNo,sum(s.Price*s.Qty)-(b.DiscountAmount) as Total from Orders b inner join OrderDetail s on b.OrderId=s.OrderId where b.BillNo between '" + bl_field.FromDate + "' and '" + bl_field.Todate + "' and PendingStatus='1' and b.CancelBill=0 group by b.Date,b.BillNo,b.DiscountAmount order by BillNo asc";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        public DataSet SearchSaleByBillViewItem(BL_Field bl_field)
        {
            string str = "select s.Date, s." + ReceiptNo + " as BillNo,sb.SubCatName ,sum(s." + Qty + ")as Qty from OrderDetail s inner join SubCatagory sb on s.SubCatId=sb.SubCatId where convert(datetime, s." + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) and s." + Status + "='" + bl_field.Status + "'  group by s.Date,sb.SubCatName,s." + ReceiptNo + " order by s.Date asc, s." + ReceiptNo + " asc";
           DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }

        public DataSet SearchByItemList(BL_Field bl_field)
        {
            //string str = "select c.CatagoryName ,s.SubCatName,s.Price,s.No,s.ShortName from Catagory c inner join SubCatagory s on c.CatId=s.CatId order by c." + CatagoryName + " asc";
            string str = "SELECT R.RateSlapId, C.CatagoryName AS CatagoryName,S.SubCatName AS SubCatName, T.RateCatName AS RateCatName ,R.Rate as price,s.ShortName,s.No From  RateSlap R  INNER JOIN Catagory C ON C.CatId=R.CatId INNER JOIN   SubCatagory S ON S.SubCatId = R.SubCatId INNER JOIN   RateCategary T  ON T.RateCatId=R.RateCatId ORDER BY SubCatName ";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        public DataSet SearchByItemListBar(BL_Field bl_field)
        {
            string str = "select c." + CatagoryName + " ,s." + SubCatName + ",cp.Unit,s.price as SubCatId,cp.Price  from " + tblCatagory + " c inner join " + tblSubCatagory + " s on c." + CatId + "=s." + CatId + " left join CreatPack cp on s.SubCatId=cp.SubCatId where s.Type='" + bl_field.TypeStatic + "' order by c." + CatagoryName + " asc";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        public DataSet SelectPurchaseExp(BL_Field bl_field)
        {
            string str = "select * from Expenses where convert(datetime,  Date , 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) ";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }

        public DataSet CancelBillAllDays(BL_Field bl_field)
        {
            string str = "select Date,TableNo,KotOrderNo as OrderNo,CashierName,ItemName,Qty,Amount,Resion from CancelItem where convert(datetime, " + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) order by ItemName asc";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }

        public DataSet ParsalReports(BL_Field bl_field)
        {
            //string str = "select s.Date, s.ParsalNo as ParsalNo ,b.BillNo as BillNo,sum(s.Price*s.Qty) as Total,b.DiscountAmount,c.Name,c.MobileNo,c.Address  from Orders b left join Customer c on b.CustId=c.CustId inner join OrderDetail s on s.OrderId=b.OrderId where convert(datetime, b." + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) and s.TableNo=0 and PendingStatus='1' and b.CancelBill=0 group by s.Date, s.ParsalNo ,b.BillNo  ,c.Name,c.MobileNo,c.Address,b.DiscountAmount";
            string str = "select b.BillNo,s.TableNo,s.Person,s.ItemName,s.Price,s.Qty,sum(s.Price*s.Qty) as Total,s.Date,b.BillTime,b.WaitorName,s.KotOrderNo as OrderNo,b.Discount,b.DiscountAmount from OrderDetail s inner join Orders b on s.OrderId=b.OrderId where convert(datetime, s." + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) and s.KotStatus='2' and PendingStatus='1' and b.CancelBill=0 and TableNo=0 group by s.TableNo,s.Person,s.ItemName,s.Price,s.Qty,s.Date,b.WaitorName,s.KotOrderNo ,b.BillNo,b.BillTime,b.Discount,b.DiscountAmount order by b.BillNo asc";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        public DataSet GetParsal(BL_Field bl_field)
        {
            string str = "select distinct ParsalNo from  OrderDetail where convert(datetime, " + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) and ParsalNo>0 and Status='Close' ";
            //string str = "select " + ParsalNo + " as ParsalNo ,count(" + ItemName + ")as OrderCount,sum(" + Total + ")as Total,c.Name,c.MobileNo,c.Address  from Sale s inner join Customer c on s.CustId=c.CustId where Date between '" + bl_field.FromDate + "' and '" + bl_field.Todate + "' and " + ParsalNo + ">0 and s."+Status+"='"+bl_field.Status+"' group by " + ParsalNo + ",Name,MobileNo,Address";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        public DataSet GetParsalDetails(BL_Field bl_field)
        {
            string str = "select ParsalNo,Date,ItemName,Price,Qty,Dis,Total from  OrderDetail where convert(datetime, " + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) and ParsalNo>0 and Status='Close' ";
            //string str = "select " + ParsalNo + " as ParsalNo ,count(" + ItemName + ")as OrderCount,sum(" + Total + ")as Total,c.Name,c.MobileNo,c.Address  from Sale s inner join Customer c on s.CustId=c.CustId where Date between '" + bl_field.FromDate + "' and '" + bl_field.Todate + "' and " + ParsalNo + ">0 and s."+Status+"='"+bl_field.Status+"' group by " + ParsalNo + ",Name,MobileNo,Address";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        public DataSet GetBillDetails(BL_Field bl_field)
        {
            string str = "select ParsalNo,DisAmount,SubTotal,Total as GrandTotal from  Orders where convert(datetime, " + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) and Status='Close' ";
            //string str = "select " + ParsalNo + " as ParsalNo ,count(" + ItemName + ")as OrderCount,sum(" + Total + ")as Total,c.Name,c.MobileNo,c.Address  from Sale s inner join Customer c on s.CustId=c.CustId where Date between '" + bl_field.FromDate + "' and '" + bl_field.Todate + "' and " + ParsalNo + ">0 and s."+Status+"='"+bl_field.Status+"' group by " + ParsalNo + ",Name,MobileNo,Address";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        
        public DataSet CustomerList(BL_Field bl_field)
        {
            string str = "select * from Customer";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        public DataSet DisCountReports(BL_Field bl_field)
        {
            //string str = "select * from Bill where DiscountAmount>0 and convert(datetime, " + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) ";
            string str = "select b.Date,s.TableNo,s.ParsalNo,b.BillNo,b.CashierName,b.Discount,b.DiscountAmount from OrderDetail s inner join Orders b on s.OrderId=b.OrderId where convert(datetime, b." + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) and DiscountAmount>0 and PendingStatus='1' and b.CancelBill=0 group by b.OrderId, b.Date,s.TableNo,s.ParsalNo,b.BillNo,b.CashierName,b.Discount,b.DiscountAmount";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        public DataSet CashBookReports(BL_Field bl_field)
        {
            string str = "select * from CashBook where convert(datetime, " + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) ";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }

        public DataSet SelectCheckIn(BL_Field bl_field)
        {
            string str = "select * from CheckIn where CheckInId='"+bl_field.AdvanceIdStatic+"'";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        public DataSet CheckInReports(BL_Field bl_field)
        {
            string str = "select * from CheckIn where Status='CheckIn' and convert(datetime, Dateofarr , 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) ";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        public DataSet CheckOutReports(BL_Field bl_field)
        {
            string str = "select inn.*,out.* from CheckIn inn inner join CheckOut out on inn.CheckInId=out.CheckInId where inn.Status='CheckOut' and convert(datetime, out.DateofDep , 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) ";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        public DataSet BillWiseCheckOut(BL_Field bl_field)
        {
            string str = "select inn.*,out.* from CheckIn inn inner join CheckOut out on inn.CheckInId=out.CheckInId where inn.Status='CheckOut' and CheckOutBillNo between  '" + bl_field.FromDate + "' and  '" + bl_field.Todate + "' ";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        public DataSet CheckOutCashierWise(BL_Field bl_field)
        {
            string str = "select inn.*,out.* from CheckIn inn inner join CheckOut out on inn.CheckInId=out.CheckInId where inn.Status='CheckOut' and out.Cashier='"+bl_field.CashierNameStatic+"' ";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        public DataSet RoomWise(BL_Field bl_field)
        {
            string str = "select ci.RoomNo, sum(ci.Advance+co.Total) as Total  from CheckIn ci inner join CheckOut co on ci.CheckInId=co.CheckInId where co.Status='CheckOut' and convert(datetime, DateofDep , 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) group by ci.RoomNo";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        public DataSet SelectCheckOut(BL_Field bl_field)
        {
            string str = "select inn.*,out.* from CheckIn inn inner join CheckOut out on inn.CheckInId=out.CheckInId where inn.CheckInId='" + bl_field.AdvanceIdStatic + "'";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }

        public DataSet KotWise(BL_Field bl_field)
        {
            string str = "select s.Date,s.OrderId,s.TableNo,sum(s.Price*Qty) as Total,b.WaitorName from OrderDetail s inner join Orders b on s.OrderId=b.OrderId where convert(datetime, s." + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) and KotStatus='2' group by s.Date, s.TableNo,b.WaitorName,s.OrderId order by OrderId asc";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        public DataSet KotWiseItem(BL_Field bl_field)
         {
            string str = "select s.TableNo,s.Person,s.ItemName,s.Price,s.Qty,sum(s.Price*s.Qty) as Total,s.Date,b.WaitorName,s.KotOrderNo as OrderNo,b.DiscountAmount as Dis from OrderDetail s inner join Orders b on s.OrderId=b.OrderId where convert(datetime, s." + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) and s.KotStatus='2'  group by s.TableNo,s.Person,s.ItemName,s.Price,s.Qty,s.Date,b.WaitorName,s.KotOrderNo,b.DiscountAmount order by s.KotOrderNo asc";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }

        public DataSet BillSummary(BL_Field bl_field)
        {
            //string str = "select b.Date,min(b.BillNo)as FromBillNo,Max(b.BillNo)as ToBillNo,Count(b.BillNo)as NoOfBill,Sum(s.Price*s.Qty)-(b.DiscountAmount)as BillAmount from Orders b inner join OrderDetail s on b.OrderId=s.OrderId where convert(datetime, b." + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) and b.PendingStatus='1' and b.CancelBill=0 group by b.Date,b.DiscountAmount";
            string str = "select b.Date,min(b.BillNo)as FromBillNo,Max(b.BillNo)as ToBillNo,Sum(s.Price*s.Qty)as BillAmount,(select sum(DiscountAmount) from Orders)as Discount ,Sum(s.Price*s.Qty)-(select sum(DiscountAmount) from Orders)as Total from Orders b inner join OrderDetail s on b.OrderId=s.OrderId where convert(datetime, b.Date, 103) between convert(datetime, '"+bl_field.FromDate +"', 103) and convert(datetime, '"+bl_field.Todate+"', 103) and b.PendingStatus='1' and b.CancelBill=0 group by b.Date";
           
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }

        public DataSet PurchasingReport(BL_Field bl_field)
        {
            //string str = "select se.StockEntryId,se.Date,se.BillNo,sp.SupplierName,im.ItemNames,se.Qty,u.UnitName,se.Rate,se.Amount,se.Notes from StockEntry se inner join Supplier sp on se.SupplierId=sp.SupplierId inner join ItemName im on se.ItemId=im.ItemId inner join Unit u on se.UnitId=u.UnitId where convert(datetime,  se.Date , 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) ";
            string str = "select pd.PurchaseDetailsId,pd.ItemId,i.ItemNames,pd.Qty,pd.UnitId,u.UnitName,pd.Rate,pd.Notes,p.PurchaseId,p.Date,p.BillNo,p.SupplierId,s.SupplierName,p.Discount,p.DiscountAmount,(pd.Rate*pd.Qty)as Total from Purchase p inner join PurchaseDetails pd on p.PurchaseId=pd.PurchaseId inner join ItemName i on pd.ItemId=i.ItemId inner join Unit u on pd.UnitId=u.UnitId inner join Supplier s on p.SupplierId=s.SupplierId where convert(datetime,  p.Date , 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) ";

            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        public DataSet WastageReport(BL_Field bl_field)
        {
            string str = "select w.WastageId,w.Date,w.ItemName,w.Qty,w.UnitId,u.UnitName,w.Resion from Wastage w inner join Unit u on w.UnitId=u.UnitId where convert(datetime,  Date , 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) ";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        public DataSet BillDetailsReport(BL_Field bl_field)
        {
            string str = "select b.BillNo,s.TableNo,s.Person,s.ItemName,s.Price,s.Qty,sum(s.Price*s.Qty) as Total,s.Date,b.BillTime,b.WaitorName,s.KotOrderNo as OrderNo,b.Discount,b.DiscountAmount from OrderDetail s inner join Orders b on s.OrderId=b.OrderId where convert(datetime, s." + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) and s.KotStatus='2' and PendingStatus='1' and b.CancelBill=0 group by s.TableNo,s.Person,s.ItemName,s.Price,s.Qty,s.Date,b.WaitorName,s.KotOrderNo ,b.BillNo,b.BillTime,b.Discount,b.DiscountAmount order by b.BillNo asc";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        public DataSet CounterReport(BL_Field bl_field)
        {
            string str = "select b.BillNo,s.TableNo,s.Person,s.ItemName,s.Price,s.Qty,sum(s.Price*s.Qty) as Total,s.Date,b.BillTime,b.WaitorName,s.KotOrderNo as OrderNo,b.Discount,b.DiscountAmount from OrderDetail s inner join Orders b on s.OrderId=b.OrderId where convert(datetime, s." + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) and s.KotStatus='2' and PendingStatus='1' and b.CancelBill=0 and TableNo=0 and Seat='0' group by s.TableNo,s.Person,s.ItemName,s.Price,s.Qty,s.Date,b.WaitorName,s.KotOrderNo ,b.BillNo,b.BillTime,b.Discount,b.DiscountAmount order by b.BillNo asc";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }

        public DataSet StockReport(BL_Field bl_field)
        {
            string str = "select s.StockId,i.ItemNames,u.UnitName,s.Stock,s.MinQty from Stock s inner join ItemName i on s.ItemId=i.ItemId inner join Unit u on s.UnitId=u.UnitId ";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        public DataSet RecipieReport(BL_Field bl_field)
        {
            string str = "select sc.SubCatName,i.ItemNames,u.UnitName,r.Qty from Recipe r inner join SubCatagory sc on r.SubCatId=sc.SubCatId inner join ItemName i on r.ItemId =i.ItemId inner join Unit u on r.UnitId=u.UnitId";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        public DataSet CancelBill(BL_Field bl_field)
        {
            string str = "select b.BillNo,s.TableNo,s.Person,s.ItemName,s.Price,s.Qty,sum(s.Price*s.Qty) as Total,s.Date,b.BillTime,b.WaitorName,s.KotOrderNo as OrderNo,b.Discount,b.DiscountAmount from OrderDetail s inner join Orders b on s.OrderId=b.OrderId where convert(datetime, s." + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) and s.KotStatus='2' and b.CancelBill=1 group by s.TableNo,s.Person,s.ItemName,s.Price,s.Qty,s.Date,b.WaitorName,s.KotOrderNo ,b.BillNo,b.BillTime,b.Discount,b.DiscountAmount order by b.BillNo asc";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        public DataSet CustomerHistory(BL_Field bl_field)
        {
            string str = "select b.BillNo,s.TableNo,s.Person,s.ItemName,s.Price,s.Qty,sum(s.Price*s.Qty) as Total,s.Date,b.BillTime,b.WaitorName,s.KotOrderNo as OrderNo,b.Discount,b.DiscountAmount,c.Name,c.MobileNo,c.Address from OrderDetail s inner join Orders b on s.OrderId=b.OrderId inner join Customer c on s.CustId=c.CustId where convert(datetime, s." + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) and b.PendingStatus=1 and b.CancelBill=0 and s.CustId='" + bl_field.CustIdStatic + "' and s.CustId>0  group by s.TableNo,s.Person,s.ItemName,s.Price,s.Qty,s.Date,b.WaitorName,s.KotOrderNo ,b.BillNo,b.BillTime,b.Discount,b.DiscountAmount,c.Name,c.MobileNo,c.Address order by b.BillNo asc";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;

namespace Restaurant.DL
{
    class DL_Room
    {
        DL_Connection dl_connection = new DL_Connection();

        private const string RoomId = "RoomId";
        private const string Price = "Price";
        private const string Roomnum = "RoomNo";
        private const string tblRoomNo = "AddRoom";


        public void InsertRoom(BL_Field bl_field)
        {
            string str = "insert into " + tblRoomNo + "(" + Roomnum + "," + Price + ",Floors)values('" + bl_field.RoomNo + "'," + bl_field.Price + ",'"+bl_field.Floor+"')";
            dl_connection.UseExcuteNonQuery(str);
        }

        public void UpdateRoom(BL_Field bl_field)
        {
            string str = "update " + tblRoomNo + " set " + Roomnum + "='" + bl_field.RoomNo + "'," + Price + "='" + bl_field.Price + "',Floors='" + bl_field.Floor + "' where " + RoomId + "='" + bl_field.RoomId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void DeleteRoom(BL_Field bl_field)
        {
            string str = "delete from " + tblRoomNo + " where " + RoomId + "='" + bl_field.RoomId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }

        public DataTable SelectRoom()
        {
            string str = "select * from " + tblRoomNo + " ";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }

        public string CheckRoom(BL_Field bl_field)
        {
            string str = "select RoomNo from AddRoom where RoomNo='" + bl_field.RoomNo + "' ";
            string roomno = dl_connection.UseExcuteScaler(str);
            return roomno;
        }


    }
}

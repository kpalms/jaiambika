﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using Restaurant.BL;
using Restaurant.PL;
using Restaurant.DL;

namespace Restaurant.DL
{
    
    class DL_ReprintKOT
    {
        private const string TblId = "TblId";
        private const string TableNo = "TableNo";
        private const string tblTableCount = "TableCount";
        private const string tblSale = "OrderDetail";
        private const string Status = "Status";


        DL_Connection dl_connection = new DL_Connection();
        public DataTable SelectKotDetails(String KotOrderNo)
        {
            string str = "select ItemName,Qty from OrderDetail where KotOrderNo='" + KotOrderNo + "'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        public DataTable SelectKotTable(BL_ReprintKOT bl_reprintkot)
        {

            string str = "select distinct TableNo,KotOrderNo from OrderDetail where Status='Open' and KotStatus=2";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }


        public DataSet ReprintKot(string kotorderno)
        {
            string str = "select OrderDetailId,TableNo,ItemName,Qty,KotRemark,ParsalNo,KotOrderNo,StartTime from OrderDetail where KotOrderNo='" + kotorderno + "'";
            DataSet ds = dl_connection.UseDataSet(str);
            return ds;

        }

    }
}

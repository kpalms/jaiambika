﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;


namespace Restaurant.DL
{
    class DL_Installment
    {


        DL_Connection dlConnection = new DL_Connection();


        public DataTable SelectBill(BL_Customer bl_Customer)
        {
            string str = "select convert(varchar(50),InvoiceNo) as InvoiceNo from Orders where CustomerId='" + bl_Customer.CustomerIdStatic + "' and Memo='Credit'";
            DataTable dt = dlConnection.UseDatatable(str);
            return dt;
        }


        public DataTable SelectOrderDetailsByBillno(BL_Installment blInstallment)
        {
            string str = "Select od.OrderDetailId,p.ProductName,od.Rate,od.Qty,u.UnitName,od.Discount,od.Vat,od.ProductNameId  from OrderDetails od inner join Orders o on od.OrderId=o.OrderId inner join AddProduct p on od.ProductNameId=p.ProductNameId inner join Unit u on od.UnitId=u.UnitId where o.InvoiceNo='" + blInstallment.InvoiceNo + "'  ";
            DataTable dt = dlConnection.UseDatatable(str);
            return dt;
        }

        public void InsertInstallment(BL_Installment blInstallment)
        {
            string str = "insert into Installment(Credit,Date,ReceiptNo,CustId)values('" + blInstallment.Credit + "','" + blInstallment.PayDate + "','" + blInstallment.ReceiptNo + "','" + blInstallment.CustomerIdStatic + "')";
            dlConnection.UseExcuteNonQuery(str);

        }


        public void InstallmentUpdate(BL_Installment blInstallment)
        {
            string str = "Update Installment set credit='"+blInstallment.Credit+"', Date='" + blInstallment.PayDate + "',ReceiptNo='" + blInstallment.ReceiptNo + "' where InstId='" + blInstallment.InstId + "'";
            dlConnection.UseExcuteNonQuery(str);

        }


        public void InstallmentDelete(BL_Installment blInstallment)
        {
            string str = "delete from Installment where InstId='" + blInstallment.InstId + "'";
            dlConnection.UseExcuteNonQuery(str);

        }

        public DataTable GetCustomerBalence(BL_Installment blInstallment)
        {
            string str = "select distinct  c.FirstName+''+c.MiddleName+''+LastName as Name, (select sum(od.Rate*od.Qty) from OrderDetails od)as Amount,(select sum(i.Amount) from Installment i) as PaidAmount ,(select sum(od.Rate*od.Qty) from OrderDetails od)-(select sum(i.Amount) from Installment i)as Balence from  Orders o left join Customer c on o.CustomerId=c.CustomerId left join OrderDetails od on o.OrderId=od.OrderId inner join Installment i on i.CustomerId=o.CustomerId where o.CustomerId=35 group by o.Date, c.FirstName, c.MiddleName,c.LastName,i.Amount";
            DataTable dt = dlConnection.UseDatatable(str);
            return dt;
        }

        public DataTable GetSaleDetails(BL_Customer bl_Customer)
        {
            string str = "select o.BillNo,sum(od.Price*od.Qty)as Total from orders o inner join OrderDetail od on o.OrderId=od.OrderId where o.CustId='" + bl_Customer.CustomerIdStatic + "' group by o.BillNo";
            DataTable dt = dlConnection.UseDatatable(str);
            return dt;
        }
        public DataTable SelectInstallment(BL_Customer bl_Customer)
        {
            string str = "Select * from Installment where CustId='" + bl_Customer.CustomerIdStatic + "'";
            DataTable dt = dlConnection.UseDatatable(str);
            return dt;
        }
        public DataTable SelectBalence()
        {
            string str = "select c.Name as Name ,i.CustId,sum(i.Debit)TotalAmount,sum(i.Credit)PaidAmount,sum(i.Debit)-sum(i.Credit)as BalenceAmount from Installment i inner join Customer c on i.CustId=c.CustId group by i.CustId,c.Name";
            DataTable dt = dlConnection.UseDatatable(str);
            return dt;
        }



    }
}

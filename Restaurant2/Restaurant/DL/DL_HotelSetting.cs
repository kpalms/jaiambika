﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;


namespace Restaurant.DL
{
    class DL_HotelSetting
    {
        private const string HotelId = "HotelId";
        private const string Name = "Name";
        private const string Address = "Address";
        private const string No = "No";
        private const string tblHotelSetting = "HotelSetting";
        DL_Connection dl_connection = new DL_Connection();


        //catagory
        public void InsertHotelSetting(BL_Field bl_field)
        {
            string str = "insert into " + tblHotelSetting + "(" + Name + "," + Address + "," + No + ")values('" + bl_field.Name + "','" + bl_field.Address + "','" + bl_field.No + "')";
            dl_connection.UseExcuteNonQuery(str);
        }
        public DataTable SelectHotelSetting()
        {
            string str = "select * from " + tblHotelSetting + "";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }


        public void UpdateHotelSetting(BL_Field bl_field)
        {
            string str = "update " + tblHotelSetting + " set " + Name + "='" + bl_field.Name + "'," + Address + "='" + bl_field.Address + "'," + No + "='" + bl_field.No + "' where " + HotelId + "='" + bl_field.HotelId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void DeleteHotelSetting(BL_Field bl_field)
        {
            string str = "delete from " + tblHotelSetting + " where " + HotelId + "='" + bl_field.HotelId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }

        public void InsertDate(BL_Field bl_field)
        {
            string str = "insert into Date (Date,Status)values('" + bl_field.Date + "','" + bl_field.Status + "')";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void CloseDate(BL_Field bl_field)
        {
            string str = "update Date set Status=0 where Status=1";
            dl_connection.UseExcuteNonQuery(str);
        }

    }
}

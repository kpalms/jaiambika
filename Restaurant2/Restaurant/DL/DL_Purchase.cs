﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;

namespace Restaurant.DL
{
    class DL_Purchase
    {

        DL_Connection dl_connection = new DL_Connection();

        private const string PurchaseId = "PurchaseId";
        private const string Date = "Date";
        private const string ItemName = "ItemName";
        private const string BillNo = "BillNo";
        private const string SupplierId = "SupplierId";
        private const string ItemId = "ItemId";
        private const string UnitId = "UnitId";
        private const string Rate = "Rate";
        private const string Amount = "Amount";
        private const string Qty = "Qty";
        private const string Cost = "Cost";
        private const string Notes = "Notes";
        private const string Status = "Status";
        private const string tblPurchase = "Purchase";

        private const string tblSupplier = "Supplier";
        private const string tblUnit = "Unit";
        private const string tblItemName = "ItemName";


        //catagory

        //=====PurchaseDetails======================================================================================================
       
        public void InsertPurchaseDetail(BL_Field bl_field)
        {
            string str =" insert into PurchaseDetails(ItemId,Qty,UnitId,Rate,Status,Notes)values('"+bl_field.ItemId+"','"+bl_field.Qty+"','"+bl_field.UnitId+"','"+bl_field.Rate+"','"+bl_field.Status+"','"+bl_field.Notes+"')";
            dl_connection.UseExcuteNonQuery(str);


        }
        public DataTable SelectPurchaseDetail(BL_Field bl_field)
        {
            string str = "select pd.PurchaseDetailsId, i.ItemNames,pd.Qty,u.UnitName,pd.Rate,pd.Notes,pd.Status,pd.ItemId,pd.UnitId  from PurchaseDetails pd inner join ItemName i on pd.ItemId=i.ItemId inner join Unit u on pd.UnitId=u.UnitId  where pd.Status ='Open'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        public void UpdatePurchaseDetail(BL_Field bl_field)
        {
            string str = "update PurchaseDetails set ItemId='" + bl_field.ItemId + "'," + Qty + "='" + bl_field.Qty + "'," + Notes + "='" + bl_field.Notes + "', Rate='" + bl_field.Rate + "' where PurchaseDetailsId='" + bl_field.PurchaseDetailsId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void DeletePurchaseDetail(BL_Field bl_field)
        {
            string str = "delete from PurchaseDetails where PurchaseDetailsId='" + bl_field.PurchaseDetailsId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }

        public void UpdatePurchaseId(BL_Field bl_field)
        {
            string str = "update PurchaseDetails set PurchaseId='" + bl_field.PurchaseId + "',Status='Close' where Status='" + bl_field.Status + "'";
            dl_connection.UseExcuteNonQuery(str);
        }



        //================Purhase================================================================================


        public string InsertPurchase(BL_Field bl_field)
        {
            string str = " insert into Purchase(Date,BillNo,SupplierId,Discount)values('" + bl_field.Date + "','" + bl_field.BillNo + "','" + bl_field.SupplierId + "','" + bl_field.Discount + "');SELECT SCOPE_IDENTITY()";
            string id = dl_connection.UseExcuteScaler(str);
            return id;


        }



        //===========================Stock=====================================================================

        public string SelectItemId(BL_Field bl_field)
        {
            string str = "select ItemId from Stock where ItemId='" + bl_field.ItemId + "'";
            string id = dl_connection.UseExcuteScaler(str);
            return id;
        }


        public string SelectStockByID(BL_Field bl_field)
        {
            string str = "select Stock from Stock where ItemId='" + bl_field.ItemId + "'";
            string qty = dl_connection.UseExcuteScaler(str);
            return qty;
        }

        public void AddStock(BL_Field bl_field)
        {
            string str = "update Stock set Stock='" + bl_field.Stock + "' where ItemId='" + bl_field.ItemId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }

                
        //=====================Purchase Security===========================================================================

        
        public DataTable SelectPurchaeByDate(BL_Field bl_field)
        {
            string str = "select pr.PurchaseDetailsId,i.ItemNames,p.BillNo,p.Date,s.SupplierName,pr.Qty,u.UnitName,pr.Rate,p.Discount from PurchaseDetails pr inner join Purchase p on pr.PurchaseId=p.PurchaseId inner join ItemName i on pr.ItemId=i.ItemId inner join Unit u on pr.UnitId=u.UnitId inner join Supplier s on p.SupplierId=s.SupplierId  where convert(datetime,Date,103) between convert(datetime, '" + bl_field.FromDate + "', 103)and convert(datetime, '" + bl_field.Todate + "', 103)";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }


        public DataTable SelectInvoiceNo(BL_Field bl_field)
        {
            string str = "select convert(varchar(50),BillNo)as BillNo from Purchase where convert(datetime,Date,103) between convert(datetime, '" + bl_field.FromDate + "', 103)and convert(datetime, '" + bl_field.Todate + "', 103)";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }

        public DataTable SelectDataByInvoice(BL_Field bl_field)
        {
            string str = "select pr.PurchaseDetailsId,i.ItemNames,p.BillNo,p.Date,s.SupplierName,pr.Qty,u.UnitName,pr.Rate,p.Discount from PurchaseDetails pr inner join Purchase p on pr.PurchaseId=p.PurchaseId inner join ItemName i on pr.ItemId=i.ItemId  inner join Unit u on pr.UnitId=u.UnitId inner join Supplier s on p.SupplierId=s.SupplierId where convert(varchar(50),p.BillNo)='" + bl_field.BillNo + "'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }


        public void UpdatePurchase(BL_Field bl_field)
        {
            string str = "update Purchase set SupplierId='" + bl_field.SupplierId + "',Date='" + bl_field.Date + "',BillNo ='" + bl_field.BillNo + "' where BillNo ='" + bl_field.BillNo + "'";
            dl_connection.UseExcuteNonQuery(str);
        }









        //================================================================================================


        public DataTable SelectExpensesByDate(BL_Field bl_field)
        {
            string str = "select * from Expenses where Date between '" + bl_field.FromDate + "' and '" + bl_field.Todate + "'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        public DataSet SelectExpensesByStatus(BL_Field bl_field)
        {
            string str = "select * from Expenses where " + Status + "='Open'";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;

        }

        public void CloseExpenses()
        {
            string str = "update Expenses set Status='Close'";
            dl_connection.UseExcuteNonQuery(str);
        }


        public void InsertExpenses(BL_Field bl_field)
        {
            string str = "insert into Expenses (" + Date + "," + ItemName + "," + Qty + "," + Cost + "," + Notes + ")values('" + bl_field.Date + "','" + bl_field.ItemName + "','" + bl_field.Qty + "','" + bl_field.Cost + "','" + bl_field.Notes + "')";
            dl_connection.UseExcuteNonQuery(str);
        }
        public DataTable SelectExpenses(BL_Field bl_field)
        {
            //string str = "select * from " + tblPurchase + " where Status='Open'";
            string str = "select * from Expenses where Date= '" + bl_field.FromDate + "'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        public void UpdateExpenses(BL_Field bl_field)
        {
            string str = "update Expenses set " + Date + "='" + bl_field.Date + "'," + ItemName + "='" + bl_field.ItemName + "'," + Qty + "='" + bl_field.Qty + "'," + Cost + "='" + bl_field.Cost + "'," + Notes + "='" + bl_field.Notes + "' where ExpensesId='" + bl_field.ExpensesId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void DeleteExpenses(BL_Field bl_field)
        {
            string str = "delete from Expenses where ExpensesId='" + bl_field.ExpensesId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }


    }
}

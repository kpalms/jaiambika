﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;

namespace Restaurant.DL
{
    class DL_Vendor
    {
        DL_Connection dl_connection = new DL_Connection();
        private const string VendorId = "SupplierId";
        private const string VendorName = "SupplierName";
        private const string ContactPerson = "ContactPerson";
        private const string Country = "Country";
        private const string State = "State";
        private const string City = "City";
        private const string Address = "Address";
        private const string PhoneNo = "PhoneNo";
        private const string MobileNo = "MobileNo";
        private const string Email = "Email";
        private const string Note = "Note";
        private const string Status = "Status";
        private const string tblVendor = "Supplier";


        public void InsertVendor(BL_Field bl_field)
        {
            string str = "insert into " + tblVendor + "(" + VendorName + "," + ContactPerson + "," + Country + "," + State + "," + City + "," + Address + "," + PhoneNo + "," + MobileNo + "," + Email + "," + Note + ")values('" + bl_field.SupplierName + "','" + bl_field.ContactPerson + "','" + bl_field.Country + "','" + bl_field.State + "','" + bl_field.City + "','" + bl_field.Address + "','" + bl_field.PhoneNo + "','" + bl_field.Mobile + "','" + bl_field.Email + "','" + bl_field.Note + "')";
            dl_connection.UseExcuteNonQuery(str);
        }
        public DataTable SelectVendor()
        {
            string str = "select * from " + tblVendor + " order by SupplierName asc ";
            DataTable dt = dl_connection.UseDatatable(str); 
            return dt;

        }
        public DataTable SearchVendor(BL_Field bl_field)
        {
            string str = "select * from " + tblVendor + " where " + VendorName + " like '" + bl_field.TextSearch + "%'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }

        public DataTable SearchVendorAll(BL_Field bl_field)
        {
            string str = "select * from " + tblVendor + " ";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }

        public void UpdateVendor(BL_Field bl_field)
        {
            string str = "update " + tblVendor + " set " + VendorName + "='" + bl_field.SupplierName + "'," + ContactPerson + "='" + bl_field.ContactPerson + "'," + Country + "='" + bl_field.Country + "'," + State + "='" + bl_field.State + "'," + City + "='" + bl_field.City + "'," + Address + "='" + bl_field.Address + "'," + PhoneNo + "='" + bl_field.PhoneNo + "'," + MobileNo + "='" + bl_field.Mobile + "'," + Email + "='" + bl_field.Email + "'," + Note + "='" + bl_field.Note + "' where " + VendorId + "='" + bl_field.SupplierId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void DeleteVendor(BL_Field bl_field)
        {
            string str = "delete from " + tblVendor + " where " + VendorId + "='" + bl_field.SupplierId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;

namespace Restaurant.DL
{
    class DL_Sale
    {
        DL_Connection dl_connection = new DL_Connection();

        private const string SaleId = "OrderDetailId";
        private const string TableNo = "TableNo";
        private const string Person = "Person";
        private const string ItemName = "ItemName";
        private const string Price = "Price";
        private const string Qty = "Qty";
        private const string Dis = "Dis";
        private const string Total = "Total";
        private const string Date = "Date";
        private const string Status = "Status";
        private const string ReceiptNo = "ReceiptNo";
        private const string Qt = "Qt";
        private const string tblSale = "OrderDetail";

        private const string CatagoryName = "CatagoryName";
        private const string SubCatName = "SubCatName";
        private const string CatId = "CatId";
        private const string Seat = "Seat";
        private const string tblCatagory = "Catagory";
        private const string tblSubCatagory = "SubCatagory";
        private const string SubCatId = "SubCatId";
        private const string ParsalNo = "ParsalNo";
        private const string CustId = "CustId";
        private const string SubTotal = "SubTotal";

        private const string GrandTotal = "GrandTotal";
        private const string DisAmount = "DisAmount";

        private const string tblBill = "Orders";

        private const string BillNo = "BillNo";

        public void InsertSale(BL_Field bl_field)
        {
            string qry = "insert into " + tblSale + "(SubCatId,TableNo,ItemName,Price,Qty,Person,KotStatus,KotRemark,ParsalNo,Status,Date,WaitorName,Type,Seat,DpId)values('" + bl_field.SubcatId + "','" + bl_field.TableNoS + "','" + bl_field.ItemName + "','" + bl_field.PriceS + "','" + bl_field.QtyS + "','" + bl_field.Person + "','1','" + bl_field.KotRemark + "','" + bl_field.ParsalNo + "','" + bl_field.Status + "','" + bl_field.Date + "','" + bl_field.Name + "','" + bl_field.Type + "','" + bl_field.Seat + "','" + bl_field.DpId + "')";
            dl_connection.UseExcuteNonQuery(qry);
       
        }


        public DataTable SelectItemName(BL_Field bl_field)
        {
            string str = "select s.OrderDetailId,s.OrderId,s.SubCatId,s.TableNo,s.ItemName,s.Price,s.Qty,s.Person,s.KotStatus,s.KotRemark,s.ParsalNo,s.KotOrderNo,s.StartTime,s.WaitorName,s.DpId,DeliveryCharge from " + tblSale + " s where " + TableNo + "='" + bl_field.TableNoS + "' and " + Status + "='" + bl_field.Status + "' and Seat='" + bl_field.Seat + "'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        public DataTable SelectParsal(BL_Field bl_field)
        {
           // string str = "select s.SaleId,s.TableNo,s.Person,sc.SubCatName as ItemName,s.Price,s.Qty,s.Unit,s.Dis,s.Total,s.Date,s.Status,s.ReceiptNo,s.Qt,s.ParsalNo,s.CustId,s.KotQty,s.WaitorName,s.CashierName,s.KotOrderNo,s.StartTime,s.Type,s.SubCatId from " + tblSale + " s inner join SubCatagory sc on s.SubCatId=sc.SubCatId where " + ParsalNo + "='" + bl_field.ParsalNo + "' and " + Status + "='" + bl_field.Status + "'";
            string str = "select s.OrderDetailId,s.OrderId,s.SubCatId,s.TableNo,s.ItemName,s.Price,s.Qty,s.Person,s.KotStatus,s.KotRemark,s.ParsalNo,s.KotOrderNo,s.StartTime,s.WaitorName  from " + tblSale + " s where " + ParsalNo + "='" + bl_field.ParsalNo + "' and " + Status + "='" + bl_field.Status + "'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }

        public DataSet PrintQt(BL_Field bl_field)
        {
            //string str = "select s.SaleId,s.TableNo,s.Person,s.ItemName+' '+s.Unit+' '+s.UnitName as ItemName,s.Price,s.Qty,s.Dis,s.Total,s.Date,s.Status,s.ReceiptNo,s.Qt,s.ParsalNo,s.CustId,s.KotQty,s.WaitorName,s.CashierName,s.KotOrderNo,s.StartTime,s.Type,s.SubCatId from " + tblSale + " s inner join SubCatagory sc on s.SubCatId=sc.SubCatId where " + TableNo + "='" + bl_field.TableNoPrintBill + "' and " + Status + "='Open' and " + Qt + "='No'";
            string str = "select OrderDetailId,TableNo,ItemName,Qty,KotRemark,ParsalNo,KotOrderNo,StartTime from OrderDetail where " + TableNo + "='" + bl_field.TableNoPrintBill + "' and Seat='"+bl_field.Seat+"' and " + Status + "='Open' and KotStatus='1'";
           
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;

        }
       public DataSet RePrintQt(BL_Field bl_field)
        {
            // string str = "select s.SaleId,s.TableNo,s.Person,s.ItemName+' '+s.Unit+' '+s.UnitName as ItemName,s.Price,s.Qty,s.Dis,s.Total,s.Date,s.Status,s.ReceiptNo,s.Qt,s.ParsalNo,s.CustId,s.KotQty,s.WaitorName,s.CashierName,s.KotOrderNo,s.StartTime,s.Type,s.SubCatId from " + tblSale + " s inner join SubCatagory sc on s.SubCatId=sc.SubCatId where " + TableNo + "='" + bl_field.TableNoPrintBill + "' and " + Status + "='Open'";
            string str = "select OrderDetailId,TableNo,ItemName,Qty,KotRemark,ParsalNo,KotOrderNo,StartTime from OrderDetail where " + TableNo + "='" + bl_field.TableNoPrintBill + "'and  KotStatus=2 and Status='Open' ";

           DataSet dt = dl_connection.UseDataSet(str);
            return dt;

        }
        public DataSet ParsalPrintQt(BL_Field bl_field)
        {
           // string str = "select * from " + tblSale + " where " + ParsalNo + "='" + bl_field.ParsalNoStatic + "' and " + Status + "='Parsal' and " + Qt + "='No'";
            string str = "select OrderDetailId,TableNo,ItemName,Qty,KotRemark,ParsalNo,KotOrderNo,StartTime from OrderDetail where " + ParsalNo + "='" + bl_field.ParsalNoStatic + "' and " + Status + "='Parsal' and KotStatus='1'";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        public DataSet ParsalRePrintQt(BL_Field bl_field)
        {
            string str = "select * from " + tblSale + " where " + ParsalNo + "='" + bl_field.ParsalNoStatic + "' and " + Status + "='Parsal' ";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }

        public DataSet SelectRestoPrint(BL_Field bl_field)
        {
            //string str = "select s.TableNo,s.Person,s.ItemName as ItemName,s.Price,sum(s.Qty)as Qty,sum(s.Qty*s.Price)as Total,s.ParsalNo,s.SubCatId from OrderDetail s where s." + TableNo + "='" + bl_field.TableNoPrintBill + "' and s." + Status + "='" + bl_field.Status + "' group by s.TableNo,s.Person,s.ItemName,s.Price,s.ParsalNo,s.SubCatId order by min(OrderDetailId)  ";
            string str = "select convert(varchar(50),s.TableNo,103)+' '+s.Seat as TableNo,s.Person,s.ItemName as ItemName,s.Price,sum(s.Qty)as Qty,sum(s.Qty*s.Price)as Total,s.ParsalNo,s.SubCatId,Date from OrderDetail s where s." + TableNo + "='" + bl_field.TableNoPrintBill + "'and s." + Seat + "='" + bl_field.Seat + "' and s." + Status + "='" + bl_field.Status + "' group by s.TableNo,s.Seat,s.Person,s.ItemName,s.Price,s.ParsalNo,s.SubCatId,s.Date order by min(OrderDetailId)  ";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }

        public DataSet SelectParsalPrint(BL_Field bl_field)
        {
            //string str = "select s.TableNo,s.Person,s.ItemName as ItemName,s.Price,sum(s.Qty)as Qty,sum(s.Qty*s.Price)as Total,s.ParsalNo,s.SubCatId from Sale s where s." + ParsalNo + "='" + bl_field.ParsalNoStatic + "' and s." + Status + "='" + bl_field.Status + "' group by s.TableNo,s.Person,s.ItemName,s.Price,s.ParsalNo,s.SubCatId order by min(SaleId)  ";
            string str = "select s.TableNo,s.Person,s.ItemName as ItemName,s.Price,sum(s.Qty)as Qty,sum(s.Qty*s.Price)as Total,s.ParsalNo,s.SubCatId from OrderDetail s where s." + ParsalNo + "='" + bl_field.ParsalNoStatic + "' and s." + Status + "='" + bl_field.Status + "' group by s.TableNo,s.Person,s.ItemName,s.Price,s.ParsalNo,s.SubCatId order by min(OrderDetailId)  ";
      
            DataSet dt = dl_connection.UseDataSet(str);
           return dt;
        }

        public void UpdateQt(BL_Field bl_field)
        {
            string str = "Update " + tblSale + " set KotStatus='2',KotOrderNo=" + bl_field.KotOrderNo + ",StartTime='" + bl_field.Time + "' where " + TableNo + "='" + bl_field.TableNoPrintBill + "' and Seat='" + bl_field.Seat + "' and " + Status + "='Open' and KotStatus='1'";
            dl_connection.UseExcuteNonQuery(str);

        }
        public void UpdateOrderNo(BL_Field bl_field)
        {
            string str = "Update " + tblSale + " set OrderId=" + bl_field.KotOrderNo + ",StartTime='" + bl_field.Time + "' where " + TableNo + "='" + bl_field.TableNoPrintBill + "' and " + Status + "='" + bl_field.Status + "' and " + Qt + "='No'";
            dl_connection.UseExcuteNonQuery(str);

        }
        public void UpdateOrderNoParsal(BL_Field bl_field)
        {
            string str = "Update " + tblSale + " set OrderId=" + bl_field.KotOrderNo + ",StartTime='" + bl_field.Time + "' where " + ParsalNo + "='" + bl_field.ParsalNoStatic + "' and " + Status + "='"+bl_field.Status+"' and " + Qt + "='No'";
            dl_connection.UseExcuteNonQuery(str);
        }

        public void ParsalUpdateQt(BL_Field bl_field)
        {
            //string str = "Update " + tblSale + " set " + Qt + "='Yes' where " + ParsalNo + "='" + bl_field.ParsalNoStatic + "' and " + Status + "='Parsal' and " + Qt + "='No'";
            string str = "Update " + tblSale + " set KotStatus='2',KotOrderNo=" + bl_field.KotOrderNo + ",StartTime='" + bl_field.Time + "' where " + ParsalNo + "='" + bl_field.ParsalNoStatic + "' and " + Status + "='Parsal' and KotStatus='1'";
            dl_connection.UseExcuteNonQuery(str);
        }
        
        public void UpdateQty(BL_Field bl_field)
        {
           string str = "update " + tblSale + " set " + Qty + "='" + bl_field.QtyS + "',KotRemark='"+bl_field.KotRemark+"' where " + SaleId  + "='" + bl_field.SaleId+ "'";
           dl_connection.UseExcuteNonQuery(str);
        }
        public void UpdatePerson(BL_Field bl_field)
        {
            string str = "update " + tblSale + " set " + Person + "='" + bl_field.Person + "' where " + TableNo + "='" + bl_field.TableNoS + "' and Status='Open'";
           dl_connection.UseExcuteNonQuery(str);
        }
        public void UpdateItemName(BL_Field bl_field)
        {
            string str = "update " + tblSale + " set " + ItemName + "='" + bl_field.ItemName + "' where " + SaleId + "='" + bl_field.SaleId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void ChangePrice(BL_Field bl_field)
        {
           string str = "update " + tblSale + " set " + Price   + "=" + bl_field.PriceS  + " where " + SaleId  + "='" + bl_field.SaleId + "'";
           dl_connection.UseExcuteNonQuery(str);
        }
        public void ChangeDis(BL_Field bl_field)
        {
            string str = "update " + tblSale + " set " + Dis + "='" + bl_field.Dis + "' where " + bl_field.ColName + "='" + bl_field.TableNoS + "' and " + Status + "='" + bl_field.Status + "'";
           dl_connection.UseExcuteNonQuery(str);
        }


        public void KotRemark(BL_Field bl_field)
        {
            string str = "update " + tblSale + " set KotRemark ='"+bl_field.KotRemark+"'  where " + SaleId + "='" + bl_field.SaleId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }




        public void DeleteSaleItem(BL_Field bl_field)
        {
            string str = "delete from " + tblSale + " where " +SaleId  + "='" + bl_field.SaleId + "' and "+Status +"='"+bl_field .Status +"'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void CancelSaleItem(BL_Field bl_field)
        {
            string str = "delete from " + tblSale + " where " + SaleId + "='" + bl_field.SaleId + "' ";
            dl_connection.UseExcuteNonQuery(str);
        }

        public void CloseOrderResto(BL_Field bl_field)
        {
            //string str = "update " + tblSale + " set " + Status + "='" + bl_field.Status + "',OrderId='" + bl_field.OrderId + "' where " + TableNo + "='" + bl_field.TableNoPrintBill + "' and " + Status + "='Open' and Seat='B'";
            string str = "update " + tblSale + " set " + Status + "='" + bl_field.Status + "',OrderId='" + bl_field.OrderId + "' where " + TableNo + "='" + bl_field.TableNoPrintBill + "'and " + Seat + "='" + bl_field.Seat + "' and " + Status + "='Open'";
            dl_connection.UseExcuteNonQuery(str);
        }

        public void CloseParsal(BL_Field bl_field)
        {
            //string str = "update "+tblSale +" set "+Status +"='Close',"+ReceiptNo +"='"+bl_field .ReceiptNo +"' where "+ParsalNo  +"='"+bl_field .ParsalNoStatic +"' and "+Status +"='Parsal'";
            string str = "update " + tblSale + " set " + Status + "='Close', OrderId='" + bl_field.OrderId + "' where " + ParsalNo + "='" + bl_field.ParsalNoStatic + "' and " + Status + "='Parsal';update Orders set PendingStatus='1' where OrderId='" + bl_field.OrderId + "' ";
            
            dl_connection.UseExcuteNonQuery(str);
        }
        public void CloseDivideBill(BL_Field bl_field)
        {
            string str = "update " + tblSale + " set " + Status + "='Close',OrderId='" + bl_field.OrderId + "' where OrderDetailId in('" + bl_field.SaleId + "')";
            dl_connection.UseExcuteNonQuery(str);
        }
//search sale

        public DataTable SearchAllDaysLoad(BL_Field bl_field)
        {
            string str = "select * from " + tblSale + " where  " + Status + "='Close' order by Date Asc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }

        public DataSet SearchAllDays(BL_Field bl_field)
        {
            string str = "select * from " + tblSale + " where  " + Status + "='Close' order by Date Asc";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;

        }
        public DataSet SearchToday(BL_Field bl_field)
        {
            //string str = "select s.SaleId,s.TableNo,s.Person,s.ItemName as ItemName,s.Price,s.Qty,s.Dis,s.Total,s.Date,s.Status,s.ReceiptNo,s.Qt,s.ParsalNo,s.CustId,s.KotQty,s.WaitorName,s.CashierName,s.KotOrderNo,s.StartTime from " + tblSale + " s inner join SubCatagory sb on s.SubCatId=sb.SubCatId  where  " + Status + " in ('Close','Open') and " + Date + "='" + bl_field.Date + "'";
            string str = "select s.OrderDetailId,s.Date,b.BillNo,s.TableNo,s.ItemName,s.Price,s.Qty,sum(s.Price*s.Qty) as Total,s.ParsalNo,b.CashierName from OrderDetail s inner join Orders b on s.OrderId=b.OrderId where  s.Status='Close' and b.PendingStatus=1 and  convert(datetime, s." + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103)  group by s.OrderDetailId,s.Date,b.BillNo,s.TableNo,s.ItemName,s.Price,s.Qty,s.ParsalNo,b.CashierName  order by b.BillNo desc";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;

        }
        
       public DataSet SearchTodayOpen(BL_Field bl_field)
        {
            string str = "select * from " + tblSale + " where  " + Status + "='Open' and " + Date + "='" + bl_field.Date + "'";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;

        }
        public DataSet SearchTodayStatus(BL_Field bl_field)
        {
            //string str = "select * from " + tblSale + " where  " + Status + "='"+bl_field .Status +"' and " + Date + "='" + bl_field.Date + "'";
            string str = "select s.OrderDetailId,s.TableNo,s.Person,sb.SubCatName,s.Price,s.Qty,s.Unit,s.Dis,s.Total,s.Date,s.Status,s.ReceiptNo,s.Qt,s.ParsalNo,s.CustId,s.KotQty,s.WaitorName,s.CashierName,s.KotOrderNo,s.StartTime,s.Type from " + tblSale + " s inner join SubCatagory sb on s.SubCatId=sb.SubCatId  where  s." + Status + "='" + bl_field.Status + "' and s." + Date + "='" + bl_field.Date + "'";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;

        }
        public DataSet SelectBillByNo(BL_Field bl_field)
        {
            string str = "select * from Orders where  ReceiptNo='" + bl_field.ReceiptNo + "' and " + Status + "='" + bl_field.Status + "'";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        public DataSet SelectBill(BL_Field bl_field)
        {
            string str = "select * from Orders where " + Date + "='" + bl_field.Date + "' and Status='" + bl_field.Status + "'";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        public DataSet SearchYesturday(BL_Field bl_field)
        {
            string str = "select * from " + tblSale + " where  " + Status + "='Close' and "+Date +"='"+bl_field .Date +"'";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;

        }

        public DataSet SearchFromTo(BL_Field bl_field)
        {
            string str = "select * from " + tblSale + " where  " + Status + "='Close' and "+Date +" between '"+bl_field .FromDate +"' and '"+bl_field .Todate +"' order by date desc";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;

        }

        public DataSet GetAllSale(BL_Field bl_field)
        {
            string str = "select s.Date,b.BillNo,s.TableNo,sum(s.Price*s.Qty) as Total,s.ParsalNo,b.OrderId from OrderDetail s inner join Orders b on s.OrderId=b.OrderId where  s.Status='Close' group by s.Date,b.BillNo,s.TableNo,s.ParsalNo,b.OrderId order by b.BillNo desc";

            //string str = "select * from Bill where Status='Close' and PendingStatus='Paid' order by convert (datetime,Date,103) desc";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        
        public void CancelBill(BL_Field bl_field)
        {
            string str = "update " + tblSale + " set " + Status + "='Cancel' where " + TableNo + "='" + bl_field.TableNoPrintBill + "' and " + Status + "='"+bl_field .Status +"'";
            dl_connection.UseExcuteNonQuery(str);
        }


        public string CheckQt(BL_Field bl_Field)
        {
            string str = "select KotStatus from OrderDetail where " + SaleId + "=" + bl_Field.SaleId + "";
          string chkqt= dl_connection.UseExcuteScaler(str);
          return chkqt;
        }



        public DataTable SelectBillNo(BL_Field bl_field)
        {
           string str = "select  BillNo from " + tblBill + " where PendingStatus='1' and convert(datetime, " + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) order by BillNo Asc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }

        public DataTable SelectDataByDate(BL_Field bl_field)
        {
            string str = "select s.OrderDetailId,s.Date,b.BillNo,s.TableNo,s.ItemName,s.Price,s.Qty,sum(s.Price*s.Qty) as Total,s.ParsalNo,b.CashierName  from OrderDetail s inner join Orders b on s.OrderId=b.OrderId where  s.Status='Close' and  convert(datetime, s." + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103)  group by s.OrderDetailId,s.Date,b.BillNo,s.TableNo,s.ItemName,s.Price,s.Qty,s.ParsalNo,b.CashierName  order by b.BillNo desc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }





        public DataTable SelectParsalNo(BL_Field bl_field)
        {
            string str = "select distinct " + ParsalNo  + " from " + tblSale + " where "+Status +"='Parsal' and " + Date + "='" + bl_field.Date + "'  order by "+ParsalNo +" Asc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        
        public DataSet SearchByBillNo(BL_Field bl_field)
        {
            string str = "select s.OrderDetailId,s.TableNo,s.ItemName,s.Price,s.Qty,s.Date,b.BillNo,s.ParsalNo,b.CashierName from " + tblSale + " s inner join Orders b on s.OrderId=b.OrderId where  s." + Status + " ='Close' and PendingStatus='1' and b.BillNo =" + bl_field.ReceiptNo + "";
           // string str = "select s.SaleId,s.TableNo,s.Person,sb.SubCatName as ItemName,s.Price,s.Qty,s.Dis,s.Total,s.Date,s.Status,s.ReceiptNo,s.Qt,s.ParsalNo,s.CustId,s.KotQty,s.WaitorName,s.CashierName,s.KotOrderNo,s.StartTime from " + tblSale + " s inner join SubCatagory sb on s.SubCatId=sb.SubCatId where  s." + Status + " ='Close' and s." + ReceiptNo + "=" + bl_field.ReceiptNo + "";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        public DataSet RePrint(BL_Field bl_field)
        {
            //string str = "select s.TableNo,s.Person,s.ItemName+' '+s.Unit+''+s.UnitName as ItemName,s.Price,sum(s.Qty)as Qty,s.Dis,sum(s.Total)as Total,s.Date,s.Status,s.ReceiptNo,s.Qt,s.ParsalNo,s.CustId,s.WaitorName,s.CashierName,s.Type from " + tblSale + " s inner join SubCatagory sc on s.SubCatId=sc.SubCatId where s." + ReceiptNo + "=" + bl_field.ReceiptNo + " and s.Type='" + bl_field.Type + "' group by s.TableNo,s.Person,s.ItemName,s.UnitName,s.Price,s.Unit,s.Dis,s.Date,s.Status,s.ReceiptNo,s.Qt,s.ParsalNo,s.CustId,s.WaitorName,s.CashierName,s.StartTime,s.Type order by min(SaleId) ";
            string str = "select convert(varchar(50),s.TableNo,103)+''+Seat as TableNo,s.Person,s.ItemName as ItemName,s.Price,sum(s.Qty)as Qty,sum(s.Qty*s.Price)as Total,s.ParsalNo,b.BillNo,b.Date,b.BillTime,b.WaitorName,b.CashierName,b.Discount,b.DiscountAmount,s.DeliveryCharge from OrderDetail s inner join Orders b on s.OrderId=b.OrderId where b.BillNo='" + bl_field.BillNo + "' and s.Status='Close' group by convert(varchar(50),s.TableNo,103)+''+Seat ,s.Person,s.ItemName,s.Price,s.ParsalNo,s.SubCatId,b.BillNo,b.Date,b.BillTime,b.WaitorName,b.CashierName,b.Discount,b.DiscountAmount,s.DeliveryCharge  order by min(OrderDetailId)";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }
        public DataSet SearchByParsalNo(BL_Field bl_field)
        {
            string str = "select * from " + tblSale + " where  " + Status + " ='"+bl_field .Status +"' and " + Date + "='" + bl_field.Date + "' and " + ParsalNo  + "=" + bl_field.ParsalNo+ "";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;

        }
        public void CloseCancelBill(BL_Field bl_field)
        {
            string str = "update s set s.Status='Cancel' from OrderDetail s inner join Orders b on s.OrderId=b.OrderId where b.BillNo='" + bl_field.BillNo + "';update b set b.CancelBill=1 from OrderDetail s inner join Orders b on s.OrderId=b.OrderId where b.BillNo='" + bl_field.BillNo + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void CloseCancelBillByItem(BL_Field bl_field)
        {
            string str = "update " + tblSale + " set " + Status + "='Cancel' where " + SaleId + "='" + bl_field.SaleId+ "'";
            dl_connection.UseExcuteNonQuery(str);
        }

        public void DeleteSaleLast(BL_Field bl_field)
        {
            string str1 = "delete from " + tblBill + " where OrderId='" + bl_field.OrderId + "';delete from " + tblSale + " where OrderId='" + bl_field.OrderId + "'";
            dl_connection.UseExcuteNonQueryForDelete(str1);
            //string upp1 = "update " + tblBill + " set ReceiptNo=ReceiptNo-1 where ReceiptNo > '" + bl_field.ReceiptNo + "'";
            //string upp2 = "update " + tblSale + " set ReceiptNo=ReceiptNo-1 where ReceiptNo > '" + bl_field.ReceiptNo + "'";
            //dl_connection.UseExcuteNonQueryForDelete(upp1);
            //dl_connection.UseExcuteNonQueryForDelete(upp2);
            //dl_connection.CloseConnection();
        }

        public string GetQty(BL_Field bl_field)
        {
            string str = "select Qty from OrderDetail where OrderDetailId="+bl_field .SaleId +"";
           string qty =dl_connection.UseExcuteScaler(str);
           return qty;
        }

        public void ParsalToCustomer(BL_Field bl_field)
        {
            string str = "Update " + tblSale + " set " + CustId + "='" + bl_field.CustIdStatic + "' where TableNo='" + bl_field.TableNoS + "' and Seat='" + bl_field.Seat + "' and " + Status + "='Open'";
            dl_connection.UseExcuteNonQuery(str);
        }


        public void InsertGrandTotal(BL_Field bl_field)
        {
            string str = "insert into " + tblSale + "(" + TableNo + "," + Person + "," + ItemName + "," + Price + "," + Qty + "," + Dis + "," + Total + "," + Date + "," + Status + "," + ReceiptNo + "," + Qt + "," + ParsalNo + "," + CustId + "," + GrandTotal + "," + DisAmount + ")values('" + bl_field.TableNoS + "','0','Total','0','0','" + bl_field.Dis + "','0','" + bl_field.Date + "','Close','" + bl_field.ReceiptNo + "','0','" + bl_field.ParsalNo + "','" + bl_field.CustId + "'," + bl_field.GrandTotal + "," + bl_field.DisAmount + ") ";
            dl_connection.UseExcuteNonQuery(str);
        }

        public string  InsertBill(BL_Field bl_field)
        {
            string str = "insert into Orders(BillNo,BillTime,CashierName,WaitorName,Discount,DiscountAmount,CustId,PendingStatus,Date)values('" + bl_field.BillNo + "','" + DateTime.Now.ToString("hh:mm:ss") + "','" + bl_field.CashierName + "','" + bl_field.Name + "','" + bl_field.Dis + "','" + bl_field.DisAmount + "','" + bl_field.CustId + "','0','"+bl_field.Date+"');SELECT SCOPE_IDENTITY()";
            string id= dl_connection.UseExcuteScaler(str);
            return id;
        }

        public void InsertCancelBill(BL_Field bl_field)
        {
            string str = "insert into CancelItem(Date,TableNo,KotOrderNo,CashierName,ItemName,Qty,Amount,Resion)values('" + bl_field.Date + "','" + bl_field.TableNo + "','" + bl_field.KotOrderNo + "','" + bl_field.CashierName + "','" + bl_field.ItemName + "','" + bl_field.Qty + "','" + bl_field.Total + "','"+bl_field.Resion+"')";
            dl_connection.UseExcuteNonQuery(str);
        }


        public DataSet GetTableDetails(BL_Field bl_field)
        {
            string str = "select TableNo,sum(price*Qty)as total from OrderDetail where status in ('Parsal','Open') group by TableNo";
           DataSet ds= dl_connection.UseDataSet(str);
           return ds;
        }
        public DataSet GetKotDetails(BL_Field bl_field)
        {
            string str = "select KotOrderNo,TableNo,StartTime from OrderDetail where status in ('Parsal','Open') and KotOrderNo>0 group by KotOrderNo,TableNo,StartTime";
            DataSet ds = dl_connection.UseDataSet(str);
            return ds;
        }
        public DataSet GetPendingBill(BL_Field bl_field)
        {

            string str = "select b.OrderId,b.BillNo,s.TableNo,sum(s.Price*s.Qty) as Total,b.PendingStatus from Orders b inner join OrderDetail s on b.OrderId=s.OrderId where PendingStatus='0' group by b.OrderId,b.BillNo,s.TableNo,b.PendingStatus order by BillNo asc";
            DataSet ds = dl_connection.UseDataSet(str);
             return ds;
        }
        public DataSet GetSettleBills(BL_Field bl_field)
        {

           // string str = "select b.OrderId,b.BillNo,s.TableNo,sum(s.Price*s.Qty)-(b.DiscountAmount) as Total,b.PendingStatus from Orders b inner join OrderDetail s on b.OrderId=s.OrderId where PendingStatus='0' and s.TableNo='" + bl_field.TableNo + "' group by b.OrderId,b.BillNo,s.TableNo,b.PendingStatus,b.DiscountAmount order by BillNo asc";
            string str = "select b.OrderId,b.BillNo,s.TableNo,sum(s.Price*s.Qty)-(b.DiscountAmount) as Total,b.PendingStatus from Orders b inner join OrderDetail s on b.OrderId=s.OrderId where PendingStatus='0' and s.TableNo='" + bl_field.TableNo + "'and s.KotStatus='2' and s.Seat='" + bl_field.Seat + "' group by b.OrderId,b.BillNo,s.TableNo,b.PendingStatus,b.DiscountAmount order by BillNo asc";
            DataSet ds = dl_connection.UseDataSet(str);
            return ds;
        }
        public DataTable GetPendingBills(BL_Field bl_field)
        {

            string str = "select distinct s.TableNo,b.PendingStatus from Orders b inner join OrderDetail s on b.OrderId=s.OrderId where PendingStatus='0' ";
            DataTable ds = dl_connection.UseDatatable(str);
            return ds;
        }

        public DataSet GetBill(BL_Field bl_field)
        {
            string str = "select BillNo from Orders";
            DataSet ds = dl_connection.UseDataSet(str);
            return ds;
        }
        public DataSet GetBillCheckOut(BL_Field bl_field)
        {
            string str = "select CheckOutBillNo from CheckOut where Status='CheckOut'";
            DataSet ds = dl_connection.UseDataSet(str);
            return ds;
        }
        public void UpdateBill(BL_Field bl_field)
        {
            string str = "update Orders set PendingStatus='1',PayModeId='" + bl_field.PayModeId + "',Memo='" + bl_field.Memo + "',CustId='"+bl_field.CustomerId+"' where BillNo='" + bl_field.BillNo + "'";
            dl_connection.UseExcuteNonQuery(str);

            string str2 = "update OrderDetail set Status='Close' where Status='P' and TableNo='"+bl_field.TableNo+"' and Seat='"+bl_field.Seat+"'";
            dl_connection.UseExcuteNonQuery(str2);
        }
        public void InsertDebitAmt(BL_Field bl_field)
        {
            string str = "Insert into Installment (Date,CustId,BillNo,ReceiptNo,Debit,Credit) values ('" + bl_field.Date + "','" + bl_field.CustomerId + "','" + bl_field.BillNo + "','" + bl_field.ReceiptNo + "','" + bl_field.Debit + "','" + bl_field.Credit + "')";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void InsertKot(BL_Field bl_field)
        {
            string str = "insert into Kot(OrderDetailId,No,KotDate,StartTime,ItemName,Price,Qty,Total,TableNo,Status,Qt,Type)values('"+bl_field.SaleId+"','" + bl_field.No + "','" + bl_field.Date + "','" + bl_field.Time + "','" + bl_field.ItemName + "','" + bl_field.PriceS + "','" + bl_field.QtyS + "','" + bl_field.Total + "','" + bl_field.TableNoS + "','" + bl_field.Status + "','No','" + bl_field.Type + "')";
            dl_connection.UseExcuteNonQuery(str);
        }


        public DataSet ShowBill(BL_Field bl_field)
        {
            string str = "select s.OrderDetailId,s.TableNo,s.Person,s.ItemName as ItemName,s.Price,s.Qty,s.Dis,s.Total,s.Date,s.Status,s.ReceiptNo,s.Qt,s.ParsalNo,s.CustId,s.KotQty,s.WaitorName,s.CashierName,s.KotOrderNo,s.StartTime from " + tblSale + " s inner join SubCatagory sb on s.SubCatId=sb.SubCatId  where  " + Status + "='Close' and  convert(datetime, s." + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) and PendingStatus='Paid'";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;
        }

        public void InsertRemoveItem(BL_Field bl_field)
        {
            string str = "insert into RemoveItem(Date,Time,KotOrderNo,CashierName,TableNo,ParsalNo,ItemName,Price,Qty,Reason)values('" + bl_field.Date + "','" + bl_field.Time + "','" + bl_field.KotOrderNo + "','" + bl_field.CashierName + "','" + bl_field.TableNo + "','" + bl_field.ParsalNo + "','" + bl_field.ItemName + "','" + bl_field.Price + "','" + bl_field.Qty + "','')";
            dl_connection.UseExcuteNonQuery(str);
        }

        public DataTable BindorderNo(BL_Field bl_field)
        {
            string str = "select distinct KotOrderNo from OrderDetail where KotStatus=2 and convert(datetime, " + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) order by KotOrderNo Asc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }

        public DataTable BindKotReprint(BL_Field bl_field)
        {
            string str = "select OrderDetailId,Date,TableNo,ItemName,Qty,Person,KotRemark,ParsalNo,KotOrderNo,StartTime from OrderDetail where KotStatus=2 and convert(datetime, " + Date + ", 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) order by KotOrderNo Asc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }

        public DataSet BindKotReprintSearch(BL_Field bl_field)
        {
            string str = "select OrderDetailId,Date,TableNo,ItemName,Qty,Person,KotRemark,ParsalNo,KotOrderNo,StartTime from OrderDetail where KotOrderNo='" + bl_field.TextSearch + "'";
            DataSet dt = dl_connection.UseDataSet(str);
            return dt;

        }


        public DataTable CheckPendingStatus(BL_Field bl_field)
        {
            string str = "select o.PendingStatus,o.BillNo,sum(od.Price*od.Qty) as Total from Orders o inner join OrderDetail od on o.OrderId=od.OrderId where od.TableNo='" + bl_field.TableNo + "' and od.Status='P' and Seat='" + bl_field.Seat + "' group by o.PendingStatus,o.BillNo";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }

        public DataTable SelectCustomer(BL_Sale bl_sale)
        {
            string str = "Select * from Customer where Name like '" + bl_sale.TextSearch + "%'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }

        public DataTable SelectCustomerAll()
        {
            string str = "Select * from Customer";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }


        public DataTable SaleByPaymentMode(BL_Field bl_field)
        {
            string str = "select p.PaymentMode,b.Date,sum(s.Price*s.Qty)-(b.DiscountAmount) as Total from OrderDetail s inner join Orders b on s.OrderId=b.OrderId inner join PaymentMode p on b.PayModeId=p.PayModeId where convert(datetime, b.Date, 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) and b.PendingStatus='1' and b.CancelBill=0  group by b.Date ,b.DiscountAmount,p.PaymentMode";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }
        public DataTable SaleSummery(BL_Field bl_field)
        {
            string str = "select b.Date,sum(case when Type='0' then s.Price*s.Qty else 0 end ) as Food,sum(case when Type='1' then s.Price*s.Qty else 0 end ) as Liquor,(b.DiscountAmount) as DiscountAmount,sum(case when Type='0' then s.Price*s.Qty else 0 end )+sum(case when Type='1' then s.Price*s.Qty else 0 end )-(b.DiscountAmount)as Total from OrderDetail s inner join Orders b on s.OrderId=b.OrderId where convert(datetime, b.Date, 103) between convert(datetime, '" + bl_field.FromDate + "', 103) and convert(datetime, '" + bl_field.Todate + "', 103) and b.PendingStatus='1' and b.CancelBill=0  group by b.Date ,b.DiscountAmount";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }


        public void CloseCounter(BL_Field bl_field)
        {
            string str1 = "update OrderDetail set Status ='Close',OrderId='" + bl_field.OrderId + "' where " + TableNo + "='" + bl_field.TableNoPrintBill + "'and " + Seat + "='" + bl_field.Seat + "' and " + Status + "='Open'";
            dl_connection.UseExcuteNonQuery(str1);

            string str2 = "update OrderDetail set KotCheck=1 where OrderId='" + bl_field.OrderId + "'";
            dl_connection.UseExcuteNonQuery(str2);


            string str3 = "update Orders set PendingStatus='1',PayModeId='" + bl_field.PayModeId + "',Memo='" + bl_field.Memo + "',CustId='" + bl_field.CustomerId + "',CashierName='"+bl_field.CashierName+"' where OrderId='" + bl_field.OrderId + "'";
            dl_connection.UseExcuteNonQuery(str3);



        }



        public void DeleveryCharge(BL_Field bl_field)
        {
            string str = "Update OrderDetail set DeliveryCharge='" + bl_field.DeliveryCharge + "' where TableNo='" + bl_field.TableNoS + "' and Seat='" + bl_field.Seat + "' and " + Status + "='Open'";
            dl_connection.UseExcuteNonQuery(str);

        }


    }
}

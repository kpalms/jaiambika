﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;


namespace Restaurant.DL
{
    class DL_CashBook
    {

        private const string CashBookId = "CashBookId";
        private const string Date = "Date";
        private const string OpeningBal = "OpeningBal";
        private const string Sale = "Sale";
        private const string Total = "Total";

        private const string Expencess = "Expencess";
        private const string Balence = "Balence";
        private const string Given = "Given";
        private const string Status = "Status";
        private const string tblCashBook = "CashBook";

        private const string ReceiptNo = "ReceiptNo";

        DL_Connection dl_connection = new DL_Connection();


        //catagory
        public void InsertCashBook(BL_Field bl_field)
        {
            string str = "insert into " + tblCashBook + "(Date,OpeningBal,Sale,Total,Expencess,Balence,Given,Closing)values('" + bl_field.Date + "','" + bl_field.OpeningBal + "','" + bl_field.Sale + "','" + bl_field.TotalC + "','" + bl_field.Expencess + "','" + bl_field.Balence + "','" + bl_field.Given + "'," + bl_field.Closing + ")";
            dl_connection.UseExcuteNonQuery(str);
        }
        public DataTable SelectCashBook()
        {
            string str = "select * from " + tblCashBook + "";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }


        public void UpdateCashBook(BL_Field bl_field)
        {
            string str = "update " + tblCashBook + " set Date='" + bl_field.Date + "',OpeningBal='" + bl_field.OpeningBal + "',Sale='" + bl_field.Sale + "', Total=" + bl_field.TotalC + ",Expencess=" + bl_field.Expencess + ",Balence=" + bl_field.Balence + ",Given=" + bl_field.Given + ",Closing=" + bl_field.Closing + " where CashBookId='" + bl_field.CashBookId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }

        //public void DeleteHotelSetting(BL_Field bl_field)
        //{
        //    string str = "delete from " + tblHotelSetting + " where " + HotelId + "='" + bl_field.HotelId + "'";
        //    dl_connection.UseExcuteNonQuery(str);
        //}
        public DataTable GetMaxDate(BL_Field bl_field)
        {
            string str = "select top 1 CashBookId ,Date,Closing,Expencess from CashBook order by CashBookId desc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }
        public string TodaySale(BL_Field bl_field)
        {
            string str = "select sum(" + Total + ") as Total from Bill where Date ='" + bl_field.Date + "' and " + Status + "='" + bl_field.Status + "'";
            string dt = dl_connection.UseExcuteScaler(str);
            return dt;
        }
        public DataTable SaleByDate(BL_Field bl_field)
        {
            string str = "select sum(" + Total + ") as Total,Date from Bill where Date ='" + bl_field.Date + "' and " + Status + "='" + bl_field.Status + "' group by Date";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }

        public DataTable PurchaseByDate(BL_Field bl_field)
        {
            string str = "select sum(Cost) as Total,Date from Purchase where Date ='" + bl_field.Date + "' group by Date";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }

        public DataTable TodayParchase(BL_Field bl_field)
        {
            string str = "select sum (Cost) as Total,Date from Purchase where Date ='" + bl_field.Date + "' group by Date";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }

        public string GetOpening(BL_Field bl_field)
        {
            string str = "select Closing from CashBook where Date ='" + bl_field.Date + "'";
            string dt = dl_connection.UseExcuteScaler(str);
            return dt;
        }

        public string CheckDate(BL_Field bl_field)
        {
            string str = "select Date from CashBook where Date ='" + bl_field.Date + "'";
            string dt = dl_connection.UseExcuteScaler(str);
            return dt;
        }

    }
}

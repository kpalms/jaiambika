﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;


namespace Restaurant.DL
{
    class DL_NewItem
    {


        DL_Connection dl_connection = new DL_Connection();

        private const string ItemId = "ItemId";
        private const string ItemNames = "ItemNames";
        private const string tblItemName = "ItemName";

        private const string UnitId = "UnitId";
        private const string UnitName = "UnitName";
        private const string tblUnit = "Unit";

        //group
        private const string GroupId = "GroupId";
        private const string GroupName = "GroupName";
        private const string Status = "Status";
        private const string tblGroup = "Groups";

        //tax
        private const string TaxId = "TaxId";
        private const string TaxDescription = "TaxDescription";
        private const string TaxRate = "TaxRate";
        private const string tblTax = "Tax";


        //store location
        private const string StoreId = "StoreId";
        private const string StoreName = "StoreName";
        private const string tblStoreLocation = "StoreLocation";

        //store recipe
        private const string RecipeId = "RecipeId";
        private const string CatagoryId = "CatId";
        private const string SubCatId = "SubCatId";
        private const string Qty = "Qty";
        private const string tblRecipe = "Recipe";


        //stock
        private const string tblStock = "Stock";
        private const string tblConversionUnit = "ConversionUnit";

        private const string Stock = "Stock";
        private const string Purchase = "Purchase";
        private const string PReturn = "PReturn";
        private const string Sale = "Sale";
        private const string Wastage = "Wastage";
        private const string Closing = "Closing";
        private const string MinQty = "MinQty";
        private const string StockId = "StockId";
        
        //conversion
        private const string ConversionId = "ConversionId";
        private const string PrimeryId = "PrimeryId";
        private const string SeconderyId = "SeconderyId";
        private const string ConversionUnit = "ConversionUnit";
        private const string Units = "Units";

        
        //private const string ItemId = "ItemId";
        //private const string ItemNames = "ItemNames";
        //private const string UnitId = "UnitId";
        //private const string UnitName = "UnitName";
        //private const string kkkk = "kkkk";
        //private const string kkkk = "kkkk";

        public void InsertNewItem(BL_Field bl_field)
        {
            string str = "insert into " + tblItemName + "(" + ItemNames + ",Rate)values('" + bl_field.ItemName + "','" + bl_field.PurchasingRate + "')";
            dl_connection.UseExcuteNonQuery(str);
        }

        public string CheckItemName(BL_Field bl_field)
        {
            string str = "select ItemNames from " + tblItemName + " where ItemNames='" + bl_field.ItemName + "'";
            string name= dl_connection.UseExcuteScaler(str);
            return name;

        }


        public DataTable SelectNewItem()
        {
            string str = "select i." + ItemId + ", i." + ItemNames + ",i.Rate from " + tblItemName + " i ";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        public DataTable SelectIdNewItem(BL_Field bl_field)
        {
            string str = "select * from "+tblItemName+" where "+ItemId+"='"+bl_field.ItemId+"'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        
        public DataTable SelectNewItemById(BL_Field bl_field)
        {
            string str = "select * from "+tblItemName+" where "+ItemId +"="+bl_field .ItemId +"";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        public DataTable SearchNewItemBySearch(BL_Field bl_field)
        {
            string str = "select * from " + tblItemName + " where " + ItemNames + " like '" + bl_field.TextSearch + "%' ";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        public DataTable SearchNewItemAll()
        {
            string str = "select * from " + tblItemName + " ";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        
        //public DataTable SelectNewItemById(BL_Field bl_field)
        //{
        //    string str = "select * from " + tblItemName + " where " + ItemId + "=" + bl_field.ItemId + "";
        //    DataTable dt = dl_connection.UseDatatable(str);
        //    return dt;

        //}

        public void UpdateNewItem(BL_Field bl_field)
        {
            string str = "update ItemName set ItemNames ='" + bl_field.ItemName + "',Rate='" + bl_field.PurchasingRate + "' where " + ItemId + "='" + bl_field.ItemId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void DeleteNewItem(BL_Field bl_field)
        {
            string str = "delete from " + tblItemName + " where " + ItemId + "='" + bl_field.ItemId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }


        public void InsertUnit(BL_Field bl_field)
        {
            string str = "insert into " + tblUnit + "(" + UnitName + ")values('" + bl_field.UnitName + "')";
            dl_connection.UseExcuteNonQuery(str);
        }
        public DataTable SelectUnit()
        {
            string str = "select * from " + tblUnit + "";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        public DataTable SelectUnitById(string unitid,string secondery)
        {
            string str = "select * from " + tblUnit + " where UnitId in (" + unitid + ","+secondery+")";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        
        public void UpdateUnit(BL_Field bl_field)
        {
            string str = "update " + tblUnit + " set " + UnitName + "='" + bl_field.UnitName + "' where " + UnitId + "='" + bl_field.UnitId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void DeleteUnit(BL_Field bl_field)
        {
            string str = "delete from " + tblUnit + " where " + UnitId + "='" + bl_field.UnitId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }

        //group
        public void InsertGroup(BL_Field bl_field)
        {
            string str = "insert into " + tblGroup + "(" + GroupName + ")values('" + bl_field.GroupName + "')";
            dl_connection.UseExcuteNonQuery(str);
        }
        public DataTable SelectGroup()
        {
            string str = "select * from " + tblGroup + "";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }

        public void UpdateGroup(BL_Field bl_field)
        {
            string str = "update " + tblGroup + " set " + GroupName + "='" + bl_field.GroupName + "' where " + GroupId + "='" + bl_field.GroupId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void DeleteGroup(BL_Field bl_field)
        {
            string str = "delete from " + tblGroup + " where " + GroupId + "='" + bl_field.GroupId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }


        //Tax

        public void InsertTax(BL_Field bl_field)
        {
            string str = "insert into " + tblTax + "(" + TaxDescription  + ","+TaxRate +")values('" + bl_field.TaxDescription + "','"+bl_field .TaxRate +"')";
            dl_connection.UseExcuteNonQuery(str);
        }
        public DataTable SelectTax()
        {
            string str = "select * from " + tblTax + "";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }

        public void UpdateTax(BL_Field bl_field)
        {
            string str = "update " + tblTax + " set " + TaxDescription + "='" + bl_field.TaxDescription + "'," + TaxRate + "='" + bl_field.TaxRate + "' where " + TaxId + "='" + bl_field.TaxId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void DeleteTax(BL_Field bl_field)
        {
            string str = "delete from " + tblTax + " where " + TaxId + "='" + bl_field.TaxId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }


        //store location
        public void Insertstore(BL_Field bl_field)
        {
            string str = "insert into " + tblStoreLocation + "(" + StoreName + ")values('" + bl_field.StoreName + "')";
            dl_connection.UseExcuteNonQuery(str);
        }
        public DataTable Selectstore()
        {
            string str = "select * from " + tblStoreLocation + "";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }

        public void Updatestore(BL_Field bl_field)
        {
            string str = "update " + tblStoreLocation + " set " + StoreName + "='" + bl_field.StoreName + "' where " + StoreId + "='" + bl_field.StoreId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void Deletestore(BL_Field bl_field)
        {
            string str = "delete from " + tblStoreLocation + " where " + StoreId + "='" + bl_field.StoreId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }


        //store recepe
        public void InsertRecipe(BL_Field bl_field)
        {
            string str = "insert into " + tblRecipe + "(" + CatagoryId + "," + SubCatId + "," + ItemId + "," + UnitId + "," + Qty + "," + Status + ")values('" + bl_field.CatId + "','" + bl_field.SubcatId + "','" + bl_field.ItemId + "','" + bl_field.UnitId + "','" + bl_field.Qty + "','" + bl_field.Status + "')";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void UpdateRecipe(BL_Field bl_field)
        {
            string str = "update " + tblRecipe + " set " + CatagoryId + "='" + bl_field.CatId + "'," + SubCatId + "='" + bl_field.SubcatId + "'," + ItemId + "='" + bl_field.ItemId + "'," + UnitId + "='" + bl_field.UnitId + "'," + Qty + "='" + bl_field.Qty + "'," + Status + "='" + bl_field.Status + "' where " + RecipeId + "='" + bl_field.RecipeId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void DeleteRecipe(BL_Field bl_field)
        {
            string str = "delete from " + tblRecipe + " where " + RecipeId + "='" + bl_field.RecipeId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }

        public DataTable SelectRecipeById(BL_Field bl_field)
        {
            string str = "select r.RecipeId,c.CatagoryName,s.SubCatName as Dish, i.ItemNames,u.UnitName,r.Qty from Itemname i inner join Recipe r on r.ItemId=i.ItemId inner join Catagory c on r.CatId=c.CatId inner join SubCatagory s on r.SubCatId=s.SubCatId inner join Unit u on r.UnitId=u.UnitId where r.SubCatId='" + bl_field.SubcatId + "'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }
        public DataTable SelectStarRecipe(BL_Field bl_field)
        {
            string str = "select * from "+tblRecipe +" where SubCatId='" + bl_field.SubcatId + "'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }
        public DataTable SelectRecipeName()
        {
            string str = "select distinct s.SubCatId,s.SubCatName from SubCatagory s inner join Recipe r on r.SubCatId=s.SubCatId order by s.SubCatName asc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }
        //stock
        public void InsertStock(BL_Field bl_field)
        {
            string str = "insert into " + tblStock + "(" + ItemId + "," + UnitId + "," + Stock + "," + MinQty + "," + Status + ")values('" + bl_field.ItemId + "','" + bl_field.UnitId + "','" + bl_field.Stock + "','" + bl_field.MinQty + "','" + bl_field.Status + "')";
            dl_connection.UseExcuteNonQuery(str);
        }



        public string CheckItemId(BL_Field bl_field)
        {
            string str = "select ItemId from Stock where ItemId='" + bl_field.ItemId + "'";
            string name = dl_connection.UseExcuteScaler(str);
            return name;

        }





        public void UpdateStock(BL_Field bl_field)
        {
            string str = "update " + tblStock + " set " + ItemId + "='" + bl_field.ItemId + "'," + UnitId + "='" + bl_field.UnitId + "'," + Stock + "='" + bl_field.Stock + "'," + MinQty + "='" + bl_field.MinQty + "'," + Status + "='" + bl_field.Status + "' where " + StockId + "='" + bl_field.StockId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void UpdateStockValue(BL_Field bl_field)
        {
            string str = "update " + tblStock + " set "+Stock+"='"+bl_field.Stock+"' where " + ItemId + "='" + bl_field.ItemId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void StockMinus(BL_Field bl_field)
        {
            string str = "update " + tblStock + " set " + Stock + "='" + bl_field.Stock + "' where " + ItemId + "='" + bl_field.ItemId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void DeleteStock(BL_Field bl_field)
        {
            string str = "delete from " + tblStock + " where " + StockId + "='" + bl_field.StockId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }

        public DataTable SelectStock()
        {
            string str = "select distinct s.StockId,i.ItemNames,u.UnitName,s.Stock,s.MinQty from ItemName i inner join Stock s on s.ItemId=i.ItemId inner join Unit u on s.UnitId=u.UnitId";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        public DataTable SelectStockById(BL_Field bl_field)
        {
            string str = "select * from "+Stock+" where "+ItemId+"='"+bl_field.ItemId+"'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        

        //conversion

        public void InsertConversion(BL_Field bl_field)
        {
            string str = "insert into " + tblConversionUnit + "(" + PrimeryId + "," + SeconderyId + "," + ConversionUnit + ")values('" + bl_field.PrimeryUnit + "','" + bl_field.SeconderyUnit + "','" + bl_field.ConversionUnit + "')";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void UpdateConversion(BL_Field bl_field)
        {
            string str = "update " + tblConversionUnit + " set " + PrimeryId + "='" + bl_field.PrimeryUnit + "'," + SeconderyId + "='" + bl_field.SeconderyUnit + "'," + ConversionUnit + "='"+bl_field .ConversionUnit +"' where " + TaxId + "='" + bl_field.TaxId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void DeleteConversion(BL_Field bl_field)
        {
            string str = "delete from " + tblConversionUnit + " where " + ConversionId + "='" + bl_field.ConversionId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public DataTable SelectConversion()
        {
            string str = "select c." + ConversionId + ",u." + UnitName + " as PrimeryUnit,uu." + UnitName + " as SeconderyName,c."+ConversionUnit +" from " + tblUnit + " u inner join " + tblConversionUnit + " c on u." + UnitId + "=c." + PrimeryId + " inner join " + tblUnit + " uu on uu." + UnitId + "=c." + SeconderyId + "";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }


        //message
        public void InsertMessage(BL_Field bl_field)
        {
            string str = "insert into Message(Message) values('" + bl_field.Message + "')";
            dl_connection.UseExcuteNonQuery(str);
        }
        public DataTable SelectMessage()
        {
            string str = "select * from Message";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        public DataTable Select1Message()
        {
            string str = "select Message from Message where Status='Yes'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }

        public void UpdateMessage(BL_Field bl_field)
        {
            string str = "update Message set Message='" + bl_field.Message + "' where MessageId='" + bl_field.MessageId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void DeleteMessage(BL_Field bl_field)
        {
            string str = "delete from Message where MessageId='" + bl_field.MessageId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }

        public void SelectMessage(BL_Field bl_field)
        {
            string str1 = "update Message set Status='NO'";
            dl_connection.UseExcuteNonQuery(str1);

            string str2 = "update Message set Status='YES' where MessageId='" + bl_field.MessageId + "'";
            dl_connection.UseExcuteNonQuery(str2);
        }


        //printer
        public void InsertPrinter(BL_Field bl_field)
        {
            string str = "insert into PrinterSetup(BillPrinter,KotPrinter) values('" + bl_field.BillPrinter + "','" + bl_field.KotPrinter + "')";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void UpdatePrinter(BL_Field bl_field)
        {
            string str = "update PrinterSetup set BillPrinter='" + bl_field.BillPrinter + "',KotPrinter='" + bl_field.KotPrinter + "'";
            dl_connection.UseExcuteNonQuery(str);
        }

        public DataTable SelectPrinter()
        {
            string str = "select * from PrinterSetup";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }

        public string CheckIngradient(BL_Field bl_field)
        {

            string str = "select ItemId from Recipe where SubCatId='" + bl_field.SubcatId + "' and ItemId='" + bl_field.ItemId + "'";
            string name = dl_connection.UseExcuteScaler(str);
            return name;

        }



    }
}

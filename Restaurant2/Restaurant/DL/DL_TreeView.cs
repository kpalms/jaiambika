﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;


namespace Restaurant.DL
{
    class DL_TreeView
    {

        DL_Connection dl_connection = new DL_Connection();

        //catagory
        public void InsertTreeViewMenu(BL_Field bl_field)
        {
            string str = "insert into TreeViewMenu(MenuName)values('" + bl_field.MenuName + "')";
            dl_connection.UseExcuteNonQuery(str);
        }
        public DataTable SelectTreeViewMenu(BL_Field bl_field)
        {
            string str = "select * from TreeViewMenu";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }


        public void UpdateTreeViewMenu(BL_Field bl_field)
        {
            string str = "update TreeViewMenu set MenuName='" + bl_field.MenuName + "' where MenuId='" + bl_field.MenuId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void DeleteTreeViewMenu(BL_Field bl_field)
        {
            string str = "delete from TreeViewMenu where MenuId='" + bl_field.MenuId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }

      
        //item

        public void InsertTreeViewItem(BL_Field bl_field)
        {
            string str = "insert into TreeViewItem(MenuId,ItemName)values('" + bl_field.MenuId + "','" + bl_field.ItemName + "')";
            dl_connection.UseExcuteNonQuery(str);
        }
        public DataTable SelectTreeViewItem(string MenuId)
        {
            string str = "select * from TreeViewItem where MenuId='" + MenuId + "' order by ItemName asc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }


        public void UpdateTreeViewItem(BL_Field bl_field)
        {
            string str = "update TreeViewItem set MenuId='" + bl_field.MenuId + "',ItemName='" + bl_field.ItemName + "' where ItemId='" + bl_field.ItemId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void DeleteTreeViewItem(BL_Field bl_field)
        {
            string str = "delete from TreeViewItem where ItemId='" + bl_field.ItemId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }

        public void TreeViewItemChange(BL_Field bl_field)
        {
            string str = "update TreeViewItem set Status='"+bl_field.Status+"' where ItemName='"+bl_field.ItemName+"'";
            dl_connection.UseExcuteNonQuery(str);
        }

        public DataTable SelectTreeViewItem()
        {
            string str = "select * from TreeViewItem";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }

    }
}

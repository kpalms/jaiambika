﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using Restaurant.PL.Reports;
using System.Collections;
using System.Data;

namespace Restaurant.DL
{
    class DL_Customer
    {
        DL_Connection dl_connection = new DL_Connection();

        private const string CustId = "CustId";
        private const string Name = "Name";
        private const string MobileNo = "MobileNo";
        private const string Address = "Address";
        private const string Status = "Status";
        private const string tblCustomer = "Customer";
        private const string tblSale = "OrderDetail";


        private const string ParsalNo = "ParsalNo";
        private const string Date = "Date";

        public void InsertCustomer(BL_Field bl_field)
        {
            string str = "insert into " + tblCustomer + "(" + Name + "," + MobileNo + "," + Address + ",Society,Email,Birthday,Anniversary,FlatName,MobileNo2,MobileNo3)values('" + bl_field.Name + "','" + bl_field.Mobile + "','" + bl_field.Address + "','" + bl_field.Society + "','" + bl_field.Email + "', '" + bl_field.Birthday + "','" + bl_field.Anniversary + "','" + bl_field.FlatName + "','" + bl_field.Mobile2 + "','" + bl_field.Mobile3 + "')";
            dl_connection.UseExcuteNonQuery(str);
        }

        public DataTable SelectCustomer()
        {
            string str = "select CustId,Society,FlatName,Address,MobileNo,MobileNo2,MobileNo3,Name,Email,Birthday,Anniversary,Status from " + tblCustomer + " order by CustId desc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        public DataTable SelectSociety()
        {

            string str = "select distinct Society from Customer where Society !='' order by Society asc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        public DataTable SelectFlat()
        {

            string str = "select distinct FlatName from Customer where FlatName !='' order by FlatName asc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        public DataTable SelectCustomerBySearch(BL_Field bl_field)
        {
            string str = "select CustId,Society,FlatName,Address,MobileNo,MobileNo2,MobileNo3,Name,Email,Birthday,Anniversary,Status from " + tblCustomer + " where Name like '" + bl_field.TextSearch + "%'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }

        public void UpdateCustomer(BL_Field bl_field)
        {
            string str = "update " + tblCustomer + " set " + Name + "='" + bl_field.Name + "'," + MobileNo + "='" + bl_field.Mobile + "'," + Address + "='" + bl_field.Address + "',Society='" + bl_field.Society + "',Email='" + bl_field.Email + "', Birthday='" + bl_field.Birthday + "', Anniversary='" + bl_field.Anniversary + "',FlatName='" + bl_field.FlatName + "',Mobile2='" + bl_field.Mobile2 + "',Mobile3='" + bl_field.Mobile3 + "' where " + CustId + "='" + bl_field.CustId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void DeleteCustomer(BL_Field bl_field)
        {
            string str = "delete from " + tblCustomer + " where " + CustId + "='" + bl_field.CustId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }


        public DataTable SelectCustomerName(BL_Field bl_field)
        {
            string str = "select CustId,Society,FlatName,Address,MobileNo,MobileNo2,MobileNo3,Name,Email,Birthday,Anniversary,Status from " + tblCustomer + " where " + Name + " like '" + bl_field.TextSearch + "%'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        public DataTable SelectCustomerMobileNo(BL_Field bl_field)
        {
            string str = "select CustId,Society,FlatName,Address,MobileNo,MobileNo2,MobileNo3,Name,Email,Birthday,Anniversary,Status from " + tblCustomer + " where MobileNo like '" + bl_field.TextSearch + "%' or MobileNo2 like '" + bl_field.TextSearch + "%' or MobileNo3 like '" + bl_field.TextSearch + "%'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        
        public DataTable SelectCustomerById(BL_Field bl_field)
        {
            string str = "select distinct c.* from Customer c inner join OrderDetail od on od.CustId=c.CustId where od.TableNo='" + bl_field.TableNoS + "' and od.Seat='" + bl_field.Seat + "' and od.Status='Open' ";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        public DataTable SelectCustById(BL_Field bl_field)
        {
            string str = "select CustId,Society,FlatName,Address,MobileNo,MobileNo2,MobileNo3,Name,Email,Birthday,Anniversary,Status from " + tblCustomer + "  where " + CustId + "='" + bl_field.CustId + "'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        public string GetIdCustId(BL_Field bl_field)
        {
            string str = "select " + CustId + " from " + tblSale + " where " + ParsalNo + " ='" + bl_field.ParsalNo + "' and "+Status +"='"+bl_field .Status +"' and "+Date +"='"+bl_field .Date +"'";
            string id = dl_connection.UseExcuteScaler(str);
            return id;
        }

        public DataTable SelectCustomerCombo()
        {
            string str = "select CustId,Name from Customer where Name !='' order by CustId desc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }


        public DataTable SelectCustomerBySociety(BL_Field bl_field)
        {
            string str = "select CustId,Society,FlatName,Address,MobileNo,MobileNo2,MobileNo3,Name,Email,Birthday,Anniversary,Status from " + tblCustomer + " where Society like '" + bl_field.TextSearch + "%' order by CustId desc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }

        public DataTable SelectCustomerByFlat(BL_Field bl_field)
        {
            string str = "select CustId,Society,FlatName,Address,MobileNo,MobileNo2,MobileNo3,Name,Email,Birthday,Anniversary,Status from " + tblCustomer + " where FlatName like '" + bl_field.TextSearch + "%' order by CustId desc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }

    }
}

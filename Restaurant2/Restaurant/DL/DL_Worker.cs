﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;


namespace Restaurant.DL
{
    class DL_Worker
    {
        DL_Connection dl_connection = new DL_Connection();
        //workers
        private const string WorkerId = "WorkerId";
        private const string Name = "Name";
        private const string MiddleName = "MiddleName";
        private const string LastName = "LastName";
        private const string PhoneNo = "PhoneNo";
        private const string Mobile = "Mobile";
        private const string Email = "Email";
        private const string Salary = "Salary";
        private const string Country = "Country";
        private const string State = "State";
        private const string City = "City";
        private const string Address = "Address";
        private const string Role = "Role";
        private const string Username = "Username";
        private const string Password = "Password";
        private const string tblWorker = "Worker";
        private const string OrderId = "Order";
        //catagory
        public void InsertWorker(BL_Field bl_field)
        {
            string str = "insert into " + tblWorker + "(" + Name + "," + MiddleName + "," + LastName + "," + PhoneNo + "," + Mobile + "," + Email + "," + Salary + "," + Country + "," + State + "," + City + "," + Address + "," + Role + "," + Username + "," + Password + ")values('" + bl_field.Name + "','" + bl_field.MiddleName + "','" + bl_field.LastName + "','" + bl_field.PhoneNo + "','" + bl_field.Mobile + "','" + bl_field.Email + "','" + bl_field.Salary + "','" + bl_field.Country + "','" + bl_field.State + "','" + bl_field.City + "','" + bl_field.Address + "','" + bl_field.Role + "','" + bl_field.Username + "','" + bl_field.Password + "')";
            dl_connection.UseExcuteNonQuery(str);
        }
        public DataTable SelectWorker()
        {
            string str = "select * from " + tblWorker + " order by Name asc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        public DataTable SelectWaiter(BL_Field bl_field)
        {
            string str = "select * from " + tblWorker + " where Role='Waiter' and Name like '"+bl_field.TextSearch+"%'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }
        public DataTable SelectWaiterName(BL_Field bl_field)
        {
            string str = "select Name from " + tblWorker + " where Role='Waiter' and Name like '" + bl_field.TextSearch + "%'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }


        public DataTable SelectWaiterNameAll(BL_Field bl_field)
        {
            string str = "select Name from " + tblWorker + " where Role='Waiter' ";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }


        public DataTable SelectCashier()
        {
            string str = "select * from " + tblWorker + "";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        public DataTable CheckUsername(BL_Field bl_field)
        {
            string str = "select "+Username +" from " + tblWorker + " where "+ Username +"='"+ bl_field .Username +"'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        
        public void UpdateWorker(BL_Field bl_field)
        {
            string str = "update " + tblWorker + " set " + Name + "='" + bl_field.Name + "'," + MiddleName + "='" + bl_field.MiddleName + "'," + LastName + "='" + bl_field.LastName + "'," + PhoneNo + "='" + bl_field.PhoneNo + "'," + Mobile + "='" + bl_field.Mobile + "'," + Email + "='" + bl_field.Email + "'," + Salary + "='" + bl_field.Salary + "'," + Country + "='" + bl_field.Country + "'," + State + "='" + bl_field.State + "'," + City + "='" + bl_field.City + "'," + Address + "='" + bl_field.Address + "'," + Role + "='" + bl_field.Role + "'," + Username + "='" + bl_field.Username + "'," + Password + "='" + bl_field.Password + "' where " + WorkerId + "='" + bl_field.WorkerId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void DeleteWorker(BL_Field bl_field)
        {
            string str = "delete from " + tblWorker + " where " + WorkerId + "='" + bl_field.WorkerId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public SqlDataReader CheckLogin(BL_Field bl_field)
        {
            string str = "select Username,Password,Role,WorkerId from Worker where Username='" + bl_field.Username + "' and Password='" + bl_field.Password + "'";
          SqlDataReader dr=  dl_connection.UseExcuteDataReader(str);
          return dr;
        }

        public SqlDataReader CheckSecurity(BL_Field bl_field)
        {
            string str = "select Role from Worker where Password='" + bl_field.Password + "'";
            SqlDataReader dr = dl_connection.UseExcuteDataReader(str);
            return dr;
        }



        public void InsertLoginDetails(BL_Field bl_field)
        {
            string str = "insert into LoginDetails(WorkerId,LoginName,Date,LoginTime)values('" + bl_field.WorkerId + "','" + bl_field.LoginUser + "','" + bl_field.Date + "','" + bl_field.Time + "')";
            dl_connection.UseExcuteNonQuery(str);
        }

        public void UpdateLogOutDetails(BL_Field bl_field)
        {
            string str = "UPDATE LoginDetails SET LogOutTime='"+bl_field.Time+"' WHERE loginId= (SELECT MAX(loginId) FROM LoginDetails)";
            dl_connection.UseExcuteNonQuery(str);
        }


         //Worker Attendanc


        public void InsertAttendanc(BL_Worker bl_worker)
        {
            string str = "insert into Attendanc(Date,WorkerId,AttendanceType,Reasion)values('" + bl_worker.Date + "','" + bl_worker.WorkerId + "','" + bl_worker.AttendanceType + "','" + bl_worker.Reasion +"')";
            dl_connection.UseExcuteNonQuery(str);
        }

        public void UpdateAttendance(BL_Worker bl_worker)
        {
            string str = "Update Attendanc SET Date='" + bl_worker.Date + "',WorkerId='" + bl_worker.WorkerId + "',AttendanceType='" + bl_worker.AttendanceType + "',Reasion='" + bl_worker.Reasion + "' where AttendanceId='"+bl_worker.AttendanceId+"'";
            dl_connection.UseExcuteNonQuery(str);
        }

        public void DeleteAttendance(BL_Worker bl_worker)
        {
            string str = "delete Attendanc  where AttendanceId='" + bl_worker.AttendanceId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        //worker SalaryCalculator


        public void InsertSalaryCalculator(BL_Worker bl_worker)
        {
            string str = "insert into SalaryCalculator(Date,WorkerId,PresentDays,AbsentDays,Halfdays,Extra,Deduction,Debit,Credit,Remark)values('" + bl_worker.Date + "','" + bl_worker.WorkerId + "','" + bl_worker.PresentDays + "','" + bl_worker.AbsentDays + "','" + bl_worker.Halfdays + "','" + bl_worker.Extra + "','" + bl_worker.Deduction + "','" + bl_worker.Debit + "','" + bl_worker.Credit + "','" + bl_worker.Remark + "')";

            //string str = "insert into SalaryCalculator(Date,WorkerId,PresentDays,AbsentDays,Halfdays,Overtime,Deduction,Paid)values('" + bl_worker.Date + "','" + bl_worker.WorkerId + "','" + bl_worker.PresentDays + "','" + bl_worker.AbsentDays + "','" + bl_worker.Halfdays + "','" + bl_worker.Overtime + "','" + bl_worker.Deduction + "','" + bl_worker.Paid + "')";
            dl_connection.UseExcuteNonQuery(str);
        }

        public void UpdateSalaryCalculator(BL_Worker bl_worker)
        {
            string str = "Update SalaryCalculator Set Date='" + bl_worker.Date + "',WorkerId='" + bl_worker.WorkerId + "',PresentDays='" + bl_worker.PresentDays + "',AbsentDays='" + bl_worker.AbsentDays + "',Halfdays='" + bl_worker.Halfdays + "',Extra='" + bl_worker.Extra + "',Deduction='" + bl_worker.Deduction + "',Debit='" + bl_worker.Debit + "',Credit='" + bl_worker.Credit + "',Remark='" + bl_worker.Remark + "' where SalaryCalsId='"+bl_worker.SalaryCalsId+"'";
            dl_connection.UseExcuteNonQuery(str);
        }

        public void DeleteSalaryCalculator(BL_Worker bl_worker)
        {
            string str = "Delete SalaryCalculator  where SalaryCalsId='" + bl_worker.SalaryCalsId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }



        //worker Payment
        public void InsertPayment(BL_Worker bl_worker)
        {
            string str = "insert into Payment(WorkerId,Date,PaidAmount,Remark)values('" + bl_worker.WorkerId + "','" + bl_worker.Date + "','" + bl_worker.PaidAmount + "','" + bl_worker.Remark + "')";
            dl_connection.UseExcuteNonQuery(str);
        }


        public DataTable Selectworkerdetails(BL_Worker bl_worker)
        {
            string str = "select w.Salary,sum(case when AttendanceType=0 then 1 else 0 end ) as AbsentDay,sum(case when AttendanceType=1 then 1 else 0 end ) as HalfDay,(30 )-sum(case when AttendanceType=0 then 1 else 0 end )- sum(case when AttendanceType=1 then 1 else 0 end ) as PresentDay from Attendanc a inner join Worker w on a.WorkerId=w.WorkerId  where w.WorkerId='" + bl_worker.WorkerId + "' group by w.Salary";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }


        public DataTable Selectworkerpaymentdetails(BL_Worker bl_worker)
        {
            string str = " select s.SalaryCalsId,s.Date, w.Name , s.PresentDays,s.AbsentDays,s.Halfdays,s.Extra,s.Deduction,s.Debit,s.Credit,s.Remark from Worker w INNER JOIN SalaryCalculator s on w.WorkerId=s.WorkerId  where s.WorkerId='" + bl_worker.WorkerId + "' order By Date ";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }

        public DataTable Selectworkerpaymentdetailsdatewise(BL_Worker bl_worker)
        {
            string str = " select s.SalaryCalsId,s.Date, w.Name , s.PresentDays,s.AbsentDays,s.Halfdays,s.Extra,s.Deduction,s.Debit,s.Credit,s.Remark from Worker w INNER JOIN SalaryCalculator s on w.WorkerId=s.WorkerId  where s.Date='" + bl_worker.Date + "'  ";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }

        public DataTable Selectworkerhistory(BL_Worker bl_worker)
        {
            string str = "select * from SalaryCalculator where WorkerId='" + bl_worker.WorkerId + "' AND Date BETWEEN    '" + bl_worker.Date + "' AND  '" + bl_worker.Date1 + "' " ;
            //string str = "select * from SalaryCalculator where WorkerId='" + bl_worker.WorkerId + "'  AND Date='" + bl_worker.Date + "' OR  Date1='" + bl_worker.Date1 + "'  ";

            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }

       


         public DataTable Selectworkerattendance(BL_Worker bl_worker)
        {
            string str = "select sum(case when AttendanceType=0 then 1 else 0 end ) as AbsentDay,sum(case when AttendanceType=1 then 1 else 0 end ) as HalfDay,(30 )-sum(case when AttendanceType=0 then 1 else 0 end )- sum(case when AttendanceType=1 then 1 else 0 end ) as PresentDay from Attendanc   where WorkerId='" + bl_worker.WorkerId + "' AND Date BETWEEN    '" + bl_worker.Date + "' AND  '" + bl_worker.Date1 + "'  ";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }



         public DataTable SelectAttendance(BL_Worker bl_worker)
        {
            string str = "select a.AttendanceId,a.Date, w.Name , (case when AttendanceType=0 then 'Absent' else 'HalfDay' end) as AttendanceType,a.Reasion from Worker w INNER JOIN Attendanc a on w.WorkerId=a.WorkerId  where a.WorkerId='" + bl_worker.WorkerId + "' Order by Date";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }


         public DataTable SelectSalaryCalculater(BL_Worker bl_worker)
         {
             string str = "select * from SalaryCalculator where WorkerId='" + bl_worker.WorkerId + "' Order by Date";
             DataTable dt = dl_connection.UseDatatable(str);
             return dt;
         }


         public DataTable SelectAttandancedatewise(BL_Worker bl_worker)
         {
             string str = "select a.AttendanceId,a.Date, w.Name , (case when AttendanceType=0 then 'Absent' else 'HalfDay' end) as AttendanceType,a.Reasion from Worker w INNER JOIN Attendanc a on w.WorkerId=a.WorkerId  where Date='" + bl_worker.Date + "'";
             DataTable dt = dl_connection.UseDatatable(str);
             return dt;
         }

         public string CheckAttandance(BL_Worker bl_worker)
         {
             string str = "select count(*) from Attendanc where Date='"+ bl_worker.Date +"' AND WorkerId='"+ bl_worker.WorkerId +"'";
             string dr = dl_connection.UseExcuteScaler(str);
             return dr;
         }
        //supplier
         public DataTable SelectSuppliderName()
         {
             string str = "select DISTINCT s.SupplierName,sa.SupplierId   from Supplier s INNER JOIN SupplierAccount sa ON s.SupplierId=sa.SupplierId ";
             DataTable dt = dl_connection.UseDatatable(str);
             return dt;
         }


         public DataTable SelectSupplierAccount()
         {
             string str = "select sa.SupplierAId,sa.Date, s.SupplierName,sa.BillNo,sa.ReciptNo,sa.Debit,sa.Credit  from Supplier s INNER JOIN SupplierAccount sa ON s.SupplierId=sa.SupplierId ";
             DataTable dt = dl_connection.UseDatatable(str);
             return dt;
         }

         public DataTable SelectSupplierAccountNamewise(BL_Field bl_field)
         {
             string str = "select sa.SupplierAId,sa.Date, s.SupplierName,sa.BillNo,sa.ReciptNo,sa.Debit,sa.Credit  from Supplier s INNER JOIN SupplierAccount sa ON s.SupplierId=sa.SupplierId where sa.SupplierId='" + bl_field.SupplierId + "'";
             DataTable dt = dl_connection.UseDatatable(str);
             return dt;
         }

         public void SupplierAccount(BL_Field bl_field)
         {
             string str = "insert into SupplierAccount (Date,SupplierId,BillNo,ReciptNo,Debit,Credit) values('" + bl_field.Date + "','" + bl_field.SupplierId + "','" + bl_field.BillNo + "','" + bl_field.ReciptNos + "','" + bl_field.Debit + "','" + bl_field.Credit + "')";
             dl_connection.UseExcuteNonQuery(str);
         }

         public void SupplierAccountUpdate(BL_Field bl_field)
         {
             string str = "Update SupplierAccount SET Date='" + bl_field.Date + "',SupplierId='" + bl_field.SupplierId + "',BillNo='" + bl_field.BillNo + "',ReciptNo='" + bl_field.ReciptNos + "',Debit='" + bl_field.Debit + "',Credit='" + bl_field.Credit + "' Where SupplierAId='" + bl_field.SupplierAId + "' ";
             dl_connection.UseExcuteNonQuery(str);
         }

         public void SupplierAccountDelete(BL_Field bl_field)
         {
             string str = "Delete SupplierAccount  Where SupplierAId='" + bl_field.SupplierAId + "' ";
             dl_connection.UseExcuteNonQuery(str);
         }




        //RateSlap

         public void RateSlapInsert(BL_Field bl_field)
         {
             string str = "insert into RateSlap (CatId,SubCatId,RateCatId,Rate) values('" + bl_field.CatId + "','" + bl_field.ItemId + "','" + bl_field.RateCatId + "','" + bl_field.Rate + "')";
             dl_connection.UseExcuteNonQuery(str);
         }


         public void RateSlapUpdate(BL_Field bl_field)
         {
             string str = "Update  RateSlap  SET CatId='" + bl_field.CatId + "',SubCatId='" + bl_field.ItemId + "',RateCatId='" + bl_field.RateCatId + "',Rate='" + bl_field.Rate + "' Where RateSlapId='" + bl_field.RateSlapId + "' ";
             dl_connection.UseExcuteNonQuery(str);
         }

         public void RateSlapDelete(BL_Field bl_field)
         {
             string str = "Delete RateSlap  Where RateSlapId='" + bl_field.RateSlapId + "' ";
             dl_connection.UseExcuteNonQuery(str);
         }


         public DataTable SelectRateSlap()
         {
             string str = "SELECT R.RateSlapId, C.CatagoryName AS CATAGORYNAME,S.SubCatName AS SUBCATNAME, T.RateCatName AS RATECATEGARY ,R.Rate From  RateSlap R  INNER JOIN Catagory C ON C.CatId=R.CatId INNER JOIN   SubCatagory S ON S.SubCatId = R.SubCatId INNER JOIN   RateCategary T  ON T.RateCatId=R.RateCatId ORDER BY SUBCATNAME ";
             DataTable dt = dl_connection.UseDatatable(str);
             return dt;
         }
         public DataTable SelectKotDetails(string tbl)
         {

            // string str = "select ItemName,Qty,OrderDetailId from OrderDetail where TableNo='" + tbl + "' and KotStatus=2 and KotCheck=0";
             string str = "select ItemName,Qty,OrderDetailId from OrderDetail where TableNo='" + tbl + "' and KotStatus=2 and KotCheck=0 and Status='Open'";

            DataTable dt = dl_connection.UseDatatable(str);
             return dt;
         }
         public void UpdateKotDetails(string OrderDetailId)
         {
             string str = "update OrderDetail set KotCheck=1 where OrderDetailId='" + OrderDetailId + "'";
             dl_connection.UseExcuteNonQuery(str);
         }

         public string CheckRateSlap(BL_Field bl_field)
         {
             string str = "SELECT RateSlapId FROM RateSlap WHERE SubCatId='" + bl_field.ItemId + "' AND RateCatId='" + bl_field.RateCatId + "'";
             string returnvalue = dl_connection.UseExcuteScaler(str);
             return returnvalue;
         }


         //RateCategary

         public void RateCategaryInsert(BL_Worker bl_worker)
         {
             string str = "INSERT INTO RateCategary (RateCatName) VALUES('" + bl_worker.RateCatName +"')";
             dl_connection.UseExcuteNonQuery(str);
         }

         public void RateCategaryUpdate(BL_Worker bl_worker)
         {
             string str = "UPDATE  RateCategary SET RateCatName ='" + bl_worker.RateCatName + "' WHERE RateCatId='"+ bl_worker.RateCatId +"'";
             dl_connection.UseExcuteNonQuery(str);
         }

         public void RateCategaryDelete(BL_Worker bl_worker)
         {
             string str = "DELETE  RateCategary  WHERE RateCatId='" + bl_worker.RateCatId + "'";
             dl_connection.UseExcuteNonQuery(str);
         }

         public DataTable SelectRateCategary()
         {
             string str = "SELECT * FROM RateCategary ORDER BY RateCatName ";
             DataTable dt = dl_connection.UseDatatable(str);
             return dt;
         }

         public string CheckRateCategary(BL_Worker bl_worker)
         {
             string str = "SELECT RateCatName FROM RateCategary WHERE RateCatName='" + bl_worker.RateCatName + "'";
             string returnvalue = dl_connection.UseExcuteScaler(str);
             return returnvalue;
         }


        //Department


         public void DepartmentInsert(BL_Worker bl_worker)
         {
             string str = "INSERT INTO Department(Department,Sequence) VALUES('" + bl_worker.Department + "','" + bl_worker.Sequence + "')";
             dl_connection.UseExcuteNonQuery(str);
         }

         public void DepartmentUpdate(BL_Worker bl_worker)
         {
             string str = "UPDATE Department SET Department='" + bl_worker.Department + "',Sequence='" + bl_worker.Sequence + "' WHERE DpId='" + bl_worker.DpId + "' ";
             dl_connection.UseExcuteNonQuery(str);
         }


         public void DepartmentDelete(BL_Worker bl_worker)
         {
             string str = "DELETE Department WHERE DpId='"+bl_worker.DpId+"'";
             dl_connection.UseExcuteNonQuery(str);
         }



         public DataTable SelectDepartment()
         {
             string str = "SELECT * FROM Department ORDER BY Sequence asc";
             DataTable dt = dl_connection.UseDatatable(str);
             return dt;
         }


         public string CheckDepartment(BL_Worker bl_worker)
         {
             string str = "select Department from Department where Department='"+bl_worker.Department +"'";
             string returnvalue = dl_connection.UseExcuteScaler(str);
             return returnvalue;
         }
         //DepartmentTable


         public void DepartmentTableInsert(BL_Worker bl_worker)
         {
             string str = "INSERT INTO DepartmentTable(DpId,TableNo) VALUES('" + bl_worker.DpId + "','" + bl_worker.TableNo + "')";
             dl_connection.UseExcuteNonQuery(str);
         }
         public void DepartmentTableUpdate(BL_Worker bl_worker)
         {
             string str = "UPDATE DepartmentTable SET DpId='" + bl_worker.DpId + "',TableNo='" + bl_worker.TableNo + "' WHERE DptId='" + bl_worker.DptId + "'";
             dl_connection.UseExcuteNonQuery(str);
         }

         public void DepartmentTableDelete(BL_Worker bl_worker)
         {
             string str = "DELETE DepartmentTable  WHERE DptId='" + bl_worker.DptId + "'";
             dl_connection.UseExcuteNonQuery(str);
         }

         public DataTable SelectDepartmentTable()
         {
             string str = "SELECT DT.DptId,D.Department,DT.TableNo FROM DepartmentTable DT INNER JOIN Department D  ON DT.DpId=D.DpId  ORDER BY D.Department";
             DataTable dt = dl_connection.UseDatatable(str);
             return dt;
         }


         public string CheckDepartmenttable(BL_Worker bl_worker)
         {
             string str = "SELECT DptId FROM DepartmentTable WHERE DpId='" + bl_worker.DpId + "' AND TableNo='" + bl_worker.TableNo + "'";
             string returnvalue = dl_connection.UseExcuteScaler(str);
             return returnvalue;
         }
         //Rate Assign


         public void RateAssignInsert(BL_Worker bl_worker)
         {
             string str = "INSERT INTO RateAssign(DpId,RateCatId) VALUES('" + bl_worker.DpId + "','" + bl_worker.RateCatId + "')";
             dl_connection.UseExcuteNonQuery(str);
         }

         public void RateAssignUpdate(BL_Worker bl_worker)
         {
             string str = "UPDATE RateAssign SET DpId='" + bl_worker.DpId + "',RateCatId='" + bl_worker.RateCatId + "' WHERE RateAId='"+ bl_worker.RateAId +"'";
             dl_connection.UseExcuteNonQuery(str);
         }

         public void RateAssignDelete(BL_Worker bl_worker)
         {
             string str = "DELETE RateAssign  WHERE RateAId='" + bl_worker.RateAId + "'";
             dl_connection.UseExcuteNonQuery(str);
         }


         public DataTable SelectRateAssign()
         {
             string str = "SELECT RA.RateAId,D.Department,RC.RateCatName FROM  RateAssign RA INNER JOIN Department D ON D.DpId=RA.DpId INNER JOIN RateCategary RC ON RC.RateCatId=RA.RateCatId ORDER BY Department";
             DataTable dt = dl_connection.UseDatatable(str);
             return dt;
         }

         public string CheckRateAssign(BL_Worker bl_worker)
         {
             //string str = "SELECT RateAId FROM RateAssign WHERE DpId='" + bl_worker.DpId + "' AND RateCatId='" + bl_worker.RateCatId + "'";
             string str = "SELECT RateAId FROM RateAssign WHERE DpId='" + bl_worker.DpId + "' ";
             string returnvalue = dl_connection.UseExcuteScaler(str);
             return returnvalue;
         }

        //Table
         public DataTable SelectTable()
         {
             string str = "SELECT TableNo,TblId FROM TableCount order by TableNo asc";
             DataTable dt = dl_connection.UseDatatable(str);
             return dt;
         }
         public DataTable SelectTablefrom()
         {
             string str = "SELECT * FROM TableCount order by TableNo asc";
             DataTable dt = dl_connection.UseDatatable(str);
             return dt;
         }
        //Kot Order
         public DataTable SelectKotOrder()
         {
             string str = "SELECT ItemName AS ITEMNAME ,Qty AS QUANTITY FROM OrderDetail WHERE KotStatus=2 ";
             DataTable dt = dl_connection.UseDatatable(str);
             return dt;
         }


         public DataTable SelectItemQuantity()
         {
             //string str = "SELECT ItemName, SUM(Qty) As Qty FROM OrderDetail WHERE KotStatus=2 and Status='Open' and KotCheck=0 GROUP BY ItemName";
             string str = "SELECT ItemName, SUM(Qty) As Qty FROM OrderDetail WHERE KotStatus=2 and Status='Open' and KotCheck=0 GROUP BY ItemName order by Qty desc";
            
             DataTable dt = dl_connection.UseDatatable(str);
             return dt;
         }


         public DataTable SelectModifyTable()
         {

             string str = "Select distinct CONVERT(varchar(50), TableNo,103) +' '+ Seat As TableNo From OrderDetail where Status='P'";
             DataTable dt = dl_connection.UseDatatable(str);
             return dt;
         }

         public void ModifyTable(string orderid)
         {
             string str = "update OrderDetail set OrderId=null,Status='Open' where OrderId='" + orderid + "';delete from Orders where OrderId='" + orderid + "'";
             dl_connection.UseExcuteNonQuery(str);
         }



         public DataSet SelectAllAttendance(BL_Worker bl_worker)
         {
             string str = "select a.*,w.Name from Attendanc a inner join Worker w on a.WorkerId=w.WorkerId where convert(datetime, Date, 103) between convert(datetime, '" + bl_worker.FromDate + "', 103) and convert(datetime, '" + bl_worker.Todate + "', 103) Order by Date asc";
             DataSet ds = dl_connection.UseDataSet(str);
             return ds;
         }

         public DataTable SelectRateSlapById(BL_Field bl_field)
         {

             string str = "SELECT R.RateSlapId, C.CatagoryName AS CATAGORYNAME,S.SubCatName AS SUBCATNAME, T.RateCatName AS RATECATEGARY ,R.Rate From  RateSlap R  INNER JOIN Catagory C ON C.CatId=R.CatId INNER JOIN   SubCatagory S ON S.SubCatId = R.SubCatId INNER JOIN   RateCategary T  ON T.RateCatId=R.RateCatId where s.SubCatId='" + bl_field.ItemId + "' ORDER BY s.SUBCATNAME asc ";
             DataTable dt = dl_connection.UseDatatable(str);
             return dt;
         }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Windows.Forms;
using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using Restaurant.PL.Reports;
using System.Collections;

using System.Drawing.Printing;
using System.Data;

namespace Restaurant.DL
{
    class Common
    {

        DL_Connection dl_connection = new DL_Connection();
        BL_Field bl_field = new BL_Field();
        BL_Worker bl_worker = new BL_Worker();
        BL_NewItem bl_newitem = new BL_NewItem();
        BL_Catagory bl_catagory = new BL_Catagory();
        BL_Customer bl_customer = new BL_Customer();
        public int ReceiptNo()
        {
            int receiptno = 0;
            //string str = "select max (ReceiptNo) from Sale where Date='"+DateTime .Now .ToString ("dd/MM/yyyy")+"'";
            string str = "select max (BillNo) from Orders";
            string id = dl_connection.UseExcuteScaler(str);
            if (id == "")
            {
                receiptno = Convert.ToInt32("1");

            }
            else
            {
                receiptno = Convert.ToInt32(id) + 1;
            }
            return receiptno;

        }

        public int PasalNo()
        {
            int pasalno = 0;
            string str = "select max (ParsalNo) from OrderDetail where Date='" + DateTime.Now.ToString("dd/MM/yyyy") + "'";
            string id = dl_connection.UseExcuteScaler(str);
            if (id == "")
            {
                pasalno = Convert.ToInt32("1");

            }
            else
            {
                pasalno = Convert.ToInt32(id) + 1;
            }
            return pasalno;

        }


        public int KotNo()
        {
            int kotno = 0;
            string str = "select max (KotOrderNo) from OrderDetail";
            string id = dl_connection.UseExcuteScaler(str);
            if (id == "")
            {
                kotno = Convert.ToInt32("1");

            }
            else
            {
                kotno = Convert.ToInt32(id) + 1;
            }
            return kotno;

        }
        public int BillNo(string query)
        {
            int kotno = 0;
            string id = dl_connection.UseExcuteScaler(query);
            if (id == "")
            {
                kotno = Convert.ToInt32("1");

            }
            else
            {
                kotno = Convert.ToInt32(id) + 1;
            }
            return kotno;

        }

        public string ReturnOneValue(string query)
        {
            string OneValue = "";
            string id = dl_connection.UseExcuteScaler(query);
            if (id == "")
            {
                OneValue = "0";

            }
            else
            {
                OneValue = id;
            }
            return OneValue;

        }

        public void UseExecuteNonQuery(string query)
        {
           dl_connection.UseExcuteNonQuery(query);
        }


        public void NumberOnly(KeyPressEventArgs e)
        {
            if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            {
                e.Handled = true;
                
            }

        }


        //convert word

        public String changeToWords(String numb, bool isCurrency)
        {
            String val = "", wholeNo = numb, points = "", andStr = "", pointStr = "";
            String endStr = (isCurrency) ? ("Only") : ("");
            try
            {
                int decimalPlace = numb.IndexOf(".");
                if (decimalPlace > 0)
                {
                    wholeNo = numb.Substring(0, decimalPlace);
                    points = numb.Substring(decimalPlace + 1);
                    if (Convert.ToInt32(points) > 0)
                    {
                        andStr = (isCurrency) ? ("and") : ("point");// just to separate whole numbers from points/cents
                        endStr = (isCurrency) ? ("Cents " + endStr) : ("");
                        pointStr = translateCents(points);
                    }
                }
                val = String.Format("{0} {1}{2} {3}", translateWholeNumber(wholeNo).Trim(), andStr, pointStr, endStr);
            }
            catch { ;}
            return val;
        }
        private String translateWholeNumber(String number)
        {
            string word = "";
            try
            {
                bool beginsZero = false;//tests for 0XX
                bool isDone = false;//test if already translated
                double dblAmt = (Convert.ToDouble(number));
                //if ((dblAmt > 0) && number.StartsWith("0"))
                if (dblAmt > 0)
                {//test for zero or digit zero in a nuemric
                    beginsZero = number.StartsWith("0");
                    int numDigits = number.Length;
                    int pos = 0;//store digit grouping
                    String place = "";//digit grouping name:hundres,thousand,etc...
                    switch (numDigits)
                    {
                        case 1://ones' range
                            word = ones(number);
                            isDone = true;
                            break;
                        case 2://tens' range
                            word = tens(number);
                            isDone = true;
                            break;
                        case 3://hundreds' range
                            pos = (numDigits % 3) + 1;
                            place = " Hundred ";
                            break;
                        case 4://thousands' range
                        case 5:
                        case 6:
                            pos = (numDigits % 4) + 1;
                            place = " Thousand ";
                            break;
                        case 7://millions' range
                        case 8:
                        case 9:
                            pos = (numDigits % 7) + 1;
                            place = " Million ";
                            break;
                        case 10://Billions's range
                            pos = (numDigits % 10) + 1;
                            place = " Billion ";
                            break;
                        //add extra case options for anything above Billion...
                        default:
                            isDone = true;
                            break;
                    }
                    if (!isDone)
                    {//if transalation is not done, continue...(Recursion comes in now!!)
                        word = translateWholeNumber(number.Substring(0, pos)) + place + translateWholeNumber(number.Substring(pos));
                        //check for trailing zeros
                        if (beginsZero) word = " and " + word.Trim();
                    }
                    //ignore digit grouping names
                    if (word.Trim().Equals(place.Trim())) word = "";
                }
            }
            catch { ;}
            return word.Trim();
        }
        private String tens(String digit)
        {
            int digt = Convert.ToInt32(digit);
            String name = null;
            switch (digt)
            {
                case 10:
                    name = "Ten";
                    break;
                case 11:
                    name = "Eleven";
                    break;
                case 12:
                    name = "Twelve";
                    break;
                case 13:
                    name = "Thirteen";
                    break;
                case 14:
                    name = "Fourteen";
                    break;
                case 15:
                    name = "Fifteen";
                    break;
                case 16:
                    name = "Sixteen";
                    break;
                case 17:
                    name = "Seventeen";
                    break;
                case 18:
                    name = "Eighteen";
                    break;
                case 19:
                    name = "Nineteen";
                    break;
                case 20:
                    name = "Twenty";
                    break;
                case 30:
                    name = "Thirty";
                    break;
                case 40:
                    name = "Fourty";
                    break;
                case 50:
                    name = "Fifty";
                    break;
                case 60:
                    name = "Sixty";
                    break;
                case 70:
                    name = "Seventy";
                    break;
                case 80:
                    name = "Eighty";
                    break;
                case 90:
                    name = "Ninety";
                    break;
                default:
                    if (digt > 0)
                    {
                        name = tens(digit.Substring(0, 1) + "0") + " " + ones(digit.Substring(1));
                    }
                    break;
            }
            return name;
        }
        private String ones(String digit)
        {
            int digt = Convert.ToInt32(digit);
            String name = "";
            switch (digt)
            {
                case 1:
                    name = "One";
                    break;
                case 2:
                    name = "Two";
                    break;
                case 3:
                    name = "Three";
                    break;
                case 4:
                    name = "Four";
                    break;
                case 5:
                    name = "Five";
                    break;
                case 6:
                    name = "Six";
                    break;
                case 7:
                    name = "Seven";
                    break;
                case 8:
                    name = "Eight";
                    break;
                case 9:
                    name = "Nine";
                    break;
            }
            return name;
        }
        private String translateCents(String cents)
        {
            String cts = "", digit = "", engOne = "";
            for (int i = 0; i < cents.Length; i++)
            {
                digit = cents[i].ToString();
                if (digit.Equals("0"))
                {
                    engOne = "Zero";
                }
                else
                {
                    engOne = ones(digit);
                }
                cts += " " + engOne;
            }
            return cts;
        }
        //conver word




       public string GetDefaultPrinter()
        {
            PrinterSettings settings = new PrinterSettings();
            foreach (string printer in PrinterSettings.InstalledPrinters)
            {
                settings.PrinterName = printer;
                if (settings.IsDefaultPrinter)
                    return printer;
            }
            return string.Empty;
        }


       public int ChangeSize()
       {
           System.Drawing.Printing.PrintDocument doctoprint = new System.Drawing.Printing.PrintDocument();
           string pp = GetDefaultPrinter();
    
           doctoprint.PrinterSettings.PrinterName = pp;
           int rawKind = 0;
           for (int i = 0; i < doctoprint.PrinterSettings.PaperSizes.Count - 1; i++)
           {

               if (doctoprint.PrinterSettings.PaperSizes[i].PaperName == "KPALMS")
               {
                   rawKind = Convert.ToInt32(doctoprint.PrinterSettings.PaperSizes[i].GetType().GetField("kind", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).GetValue(doctoprint.PrinterSettings.PaperSizes[i]));
                   break;
               }
           }


           return rawKind;
       }



       public void BindUnitName(ComboBox ddlname)
       {
           DataTable dt = bl_newitem.SelectUnit();
           DataRow row = dt.NewRow();
           row["UnitName"] = "Select";
           dt.Rows.InsertAt(row, 0);
           ddlname.DataSource = dt;
           ddlname.ValueMember = "UnitId";
           ddlname.DisplayMember = "UnitName";
       }

       public void BindCatagory(ComboBox ddlname)
       {
           DataTable dt = bl_catagory.SelectCatagory(bl_field);
           DataRow row = dt.NewRow();
           row["CatagoryName"] = "Select";
           dt.Rows.InsertAt(row, 0);
           ddlname.DataSource = dt;
           ddlname.ValueMember = "CatId";
           ddlname.DisplayMember = "CatagoryName";
       }

       public void BindUnitName(ComboBox ddlname, string primeryid, string seconderyid)
       {
           DataTable dt = bl_newitem.SelectUnitById(primeryid, seconderyid);
           ddlname.DataSource = dt;
           ddlname.ValueMember = "UnitId";
           ddlname.DisplayMember = "UnitName";
       }

       public void BindNewItem(ComboBox ddlname)
       {
           DataTable dt = bl_newitem.SelectNewItem();
           ddlname.DataSource = dt;
           ddlname.ValueMember = "ItemId";
           ddlname.DisplayMember = "ItemNames";
       }


       public void ComboBoxCheckValue(object sender, KeyPressEventArgs e)
       {
           ComboBox cb = (ComboBox)sender;
           cb.DroppedDown = true;
           string strFindStr = "";
           if (e.KeyChar == (char)8)
           {
               if (cb.SelectionStart <= 1)
               {
                   cb.Text = "";
                   return;
               }

               if (cb.SelectionLength == 0)
                   strFindStr = cb.Text.Substring(0, cb.Text.Length - 1);
               else
                   strFindStr = cb.Text.Substring(0, cb.SelectionStart - 1);
           }
           else
           {
               if (cb.SelectionLength == 0)
                   strFindStr = cb.Text + e.KeyChar;
               else
                   strFindStr = cb.Text.Substring(0, cb.SelectionStart) + e.KeyChar;
           }
           int intIdx = -1;
           intIdx = cb.FindString(strFindStr);
           if (intIdx != -1)
           {
               cb.SelectedText = "";
               cb.SelectedIndex = intIdx;
               cb.SelectionStart = strFindStr.Length;
               cb.SelectionLength = cb.Text.Length;
               e.Handled = true;
           }
           else
               e.Handled = true;
       }


       public string GetDate(DateTime dates)
       {
           string day = Get2Digit(dates.Day);
           string month = Get2Digit(dates.Month);

           int year = dates.Year;
           string strDate = day.ToString() + "/" + month.ToString() + "/" + year.ToString();
           string strDates = String.Format("{0}/{1}/{2}", day.ToString(), month.ToString(), year.ToString());
           return strDates;
       }

       public string Get2Digit(int dayMonth)
       {
           string ret = dayMonth.ToString();
           if (dayMonth.ToString().Length == 1)
           {
               ret = "0" + dayMonth.ToString();
           }
           return ret;
       }


       public void BindCustomerCombo(ComboBox ddlname)
       {

           DataTable dt = bl_customer.SelectCustomerCombo();
           DataRow row = dt.NewRow();
           row["name"] = "Select";
           dt.Rows.InsertAt(row, 0);
           ddlname.DataSource = dt;
           ddlname.ValueMember = "CustId";
           ddlname.DisplayMember = "Name";
       }


       public void OneDecimalPoint(object sender, KeyPressEventArgs e)
       {
           if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.' && (e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
           {
               e.Handled = true;
           }

           if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
           {
               e.Handled = true;
           }
       }


    }
}

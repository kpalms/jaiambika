﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;


namespace Restaurant.DL
{
    class DL_Table
    {
        private const string TblId = "TblId";
        private const string TableNo = "TableNo";
        private const string tblTableCount = "TableCount";
        private const string tblSale = "OrderDetail";
        private const string Status = "Status";

        DL_Connection dl_connection = new DL_Connection();
        public DataTable SelectTable()
        {
            string str = "select * from " + tblTableCount + "";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        public DataTable SearchTable(BL_Field bl_field)
        {
            string str = "select * from " + tblTableCount + " where TableNo='" + bl_field.TableNo + "'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }

        public DataTable SearchTableAll(BL_Field bl_field)
        {
            string str = "select TblId,TableNo as TableNo,Location from " + tblTableCount + " order by TableNo asc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        public DataTable CheckTable()
        {
            string str = "select distinct t." + TblId + ", t." + TableNo + " from " + tblTableCount + " t left join " + tblSale + " s on t." + TableNo + "=s." + TableNo + " and s.Status in ('Open','P') order by TableNo asc";
            //string str = "select distinct TableNo from OrderDetail where Status='Open' or Status='P'";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        
        public string SelectMaxTable()
        {
            string str = "select max(TableNo) from " + tblTableCount + "";
            string tbl = dl_connection.UseExcuteScaler (str);
            return tbl;

        }
        
        public void InsertTable(BL_Field bl_field)
        {
            string str = "insert into " + tblTableCount + "(" + TableNo + ",Location)values('" + bl_field.TableNo + "','"+bl_field.Location+"')";
            dl_connection.UseExcuteNonQuery(str);
        }

        public void RemoveTable(BL_Field bl_field)
        {
            string str = "delete from " + tblTableCount + " where " + TblId + "='" + bl_field.TblId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }

        public void UpdateTable(BL_Field bl_field)
        {
            string str = "update TableCount set TableNo='" + bl_field.TableNo + "',Location='" + bl_field.Location + "' where TblId='" + bl_field.TblId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }

        public DataTable CheckTableKot()
        {
           
           string str = "select distinct TableNo from OrderDetail where Status='Open' and KotStatus=2";
           // string str = "select distinct KotOrderNo, TableNo from OrderDetail where Status='Open' and KotStatus=2";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }

        public DataTable CheckOrderKot()
        {
            
            string str = "select distinct OrderId from OrderDetail where Status='Open' and KotStatus=2";

            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }

        public DataTable CheckTableSeat(string tbl)
        {
           //string str = "select distinct TableNo,Seat,Status from OrderDetail where TableNo='" + tbl + "' and Status in ('Open','P')";
            string str = "select distinct TableNo,Seat,Status  from OrderDetail where TableNo='" + tbl + "'  and Status in ('Open','P')";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }

        public DataTable CheckParcel()
        {
            string str = "select distinct convert(varchar(10),TableNo,103)+' '+Seat as TableNo,Status from OrderDetail where Status='Open' or Status='P' and TableNo=0";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
    }
}

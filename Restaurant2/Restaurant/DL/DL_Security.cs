﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using Restaurant.PL.Reports;
using System.Data;

namespace Restaurant.DL
{
    class DL_Security
    {
        DL_Connection dl_connection = new DL_Connection();

        public void TruncateData()
        {
            string str = "delete from Purchase DBCC CHECKIDENT ('Purchase', RESEED, 0)";
            dl_connection.UseExcuteNonQuery(str);

        }

        public void TruncateDataSale()
        {
            string str = "delete from Orders DBCC CHECKIDENT ('Orders', RESEED, 0)";
            dl_connection.UseExcuteNonQuery(str);

            //string str2 = "truncate table Bill";
            //dl_connection.UseExcuteNonQuery(str2);

        }


        public void ExcuteQuery(string qry)
        {
           
            dl_connection.UseExcuteNonQuery(qry);

        }

        public DataTable MenuMaster()
        {
            string str = "select * from MenuMaster";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }

    }
}

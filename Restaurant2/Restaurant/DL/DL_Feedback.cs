﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using Restaurant.PL.Reports;
using System.Collections;
using System.Data;


namespace Restaurant.DL
{
    class DL_Feedback
    {
        DL_Connection dl_connection = new DL_Connection();
        public DataTable SelectCustomer()
        {
            string str = "select * from Customer where Name!='' order by CustId desc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }
        public DataTable SelectFeedback()
        {
            string str = "select f.Date, f.FeedbackId, f.CustId,w.WorkerId, case when c.Name=c.Name then c.Name end as Customer,case when  w.Name=w.Name then w.Name end as ServersName, case when f.QualityOfFood=1 then 'Excellent' when f.QualityOfFood=2 then 'Good' when f.QualityOfFood=3 then 'Average' when f.QualityOfFood=4 then 'Poor' end as 'QualityOfFood', case when f.Service=1 then 'Excellent' when f.Service=2 then 'Good' when f.Service=3 then 'Average' when f.Service=4 then 'Poor' end as 'Service', case when f.Ambience=1 then 'Excellent' when f.Ambience=2 then 'Good' when f.Ambience=3 then 'Average' when f.Ambience=4 then 'Poor' end as 'Ambience', f.Comments from Feedback f inner join Customer c on c.CustId = f.CustId inner join Worker w on w.WorkerId = f.ServersName order by FeedbackId desc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;
        }

        public DataTable SelectServer()
        {
            string str = "select WorkerId,Name from Worker where Role= 'Waiter' order by WorkerId desc";
            DataTable dt = dl_connection.UseDatatable(str);
            return dt;

        }
        public void InsertFeedback(BL_Feedback bl_Feedback)
        {
            string str = "insert into Feedback(Date,CustId,ServersName,QualityOfFood,Service,Ambience,Comments)values('" + bl_Feedback.Date + "','" + bl_Feedback.CustId + "','" + bl_Feedback.ServersName + "','" + bl_Feedback.QualityOfFood + "','" + bl_Feedback.Service + "','" + bl_Feedback.Ambience + "','" + bl_Feedback.Comments + "')";
            dl_connection.UseExcuteNonQuery(str);
        }

        public void UpdateFeedback(BL_Feedback bl_Feedback)
        {
            string str = "update Feedback set Date='" + bl_Feedback.Date + "' ,CustId='" + bl_Feedback.CustId + "',ServersName='" + bl_Feedback.ServersName + "',QualityOfFood='" + bl_Feedback.QualityOfFood + "',Service='" + bl_Feedback.Service + "',Ambience='" + bl_Feedback.Ambience + "',Comments='" + bl_Feedback.Comments + "' where FeedbackId='" + bl_Feedback.FeedbacpkId+ "'";
            dl_connection.UseExcuteNonQuery(str);
        }
        public void DeleteFeedback(BL_Feedback bl_Feedback)
        {
            string str = "delete from Feedback where FeedbackId='" + bl_Feedback.FeedbacpkId + "'";
            dl_connection.UseExcuteNonQuery(str);
        }
    }
}

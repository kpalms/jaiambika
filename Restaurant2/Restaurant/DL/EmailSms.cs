﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Net.NetworkInformation;
using System.IO;
using System.Net;
using System.Collections;

using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using Restaurant.PL.Reports;
using System.Data;
using System.Net.Mail;
using System.Net.Mime;



namespace Restaurant.DL
{
    class EmailSms
    {
        BL_SmsEmail bl_smsEmailsetting = new BL_SmsEmail();

        public void SendMail(string sub, string body1, ArrayList to)
        {
            ArrayList BbcEmail = new ArrayList();
            string from = "";
            string fromPassword = "";
            string host = "";
            string port = "";

            DataTable dt = bl_smsEmailsetting.Selectemail();

            if (dt.Rows.Count > 0)
            {
                from = dt.Rows[0][1].ToString();
                fromPassword = dt.Rows[0][2].ToString();
                host = dt.Rows[0][3].ToString();
                port = dt.Rows[0][4].ToString();
            }



            string body = body1;
            string AllMailsRecpsbcc = string.Join(",", (string[])to.ToArray(Type.GetType("System.String")));
 


            MailMessage mailMessage = new MailMessage(from, "prasad_saraf@logesolutions.net");
            mailMessage.Subject = sub;
            mailMessage.Body = body1;
            mailMessage.From = new MailAddress(from);
            mailMessage.IsBodyHtml = true;
            mailMessage.To.Add(new MailAddress("prasad_saraf@logesolutions.net"));
            var smtp = new System.Net.Mail.SmtpClient();
            {

                smtp.Host = host;

                smtp.Port = int.Parse(port); ;

                smtp.EnableSsl = true;

                smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;

                smtp.Credentials = new System.Net.NetworkCredential(from, fromPassword);

                smtp.EnableSsl = true;
                smtp.Timeout = 999999999;
            }


            mailMessage.Bcc.Add(AllMailsRecpsbcc);
            smtp.Send(mailMessage);



        }

        public static string SendSms(string sNumber, string sMessage)
        {
            string URL = "";
            string sUserID = "";
            string sPwd = "";
            string sSID = "";
            string sResponse = "";

            BL_SmsEmail sm = new BL_SmsEmail();
            DataTable dtsms = sm.Selectsms();

            if (dtsms.Rows.Count > 0)
            {
                URL = dtsms.Rows[0]["Url"].ToString();
                sUserID = dtsms.Rows[0]["UserId"].ToString();
                sPwd = dtsms.Rows[0]["Password"].ToString();
                sSID = "TESTSM";

                string sURL = URL + "" + sUserID + "&password=" + sPwd + "&mobiles=" + sNumber + "&sms=" + sMessage + "&senderid=" + sSID;
                sResponse = GetResponse(sURL);
                return (sResponse);

            }
            else
            {
                return sResponse = "";
               
            }



        }

        public static string GetResponse(string sURL)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(sURL);
            request.MaximumAutomaticRedirections = 4;
            request.Credentials = CredentialCache.DefaultCredentials;

            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                string sResponse = readStream.ReadToEnd();
                response.Close();
                readStream.Close();
                return sResponse;
            }
            catch
            {
                return "";
            }

        }


        public string GetPublicIP()
        {
            String direction = "";
            WebRequest request = WebRequest.Create("http://checkip.dyndns.org/");
            using (WebResponse response = request.GetResponse())
            using (StreamReader stream = new StreamReader(response.GetResponseStream()))
            {
                direction = stream.ReadToEnd();
            }

            //Search for the ip in the html
            int first = direction.IndexOf("Address: ") + 9;
            int last = direction.LastIndexOf("</body>");
            direction = direction.Substring(first, last - first);

            return direction;
        }





    }
}

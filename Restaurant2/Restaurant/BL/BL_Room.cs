﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;

namespace Restaurant.BL
{
    class BL_Room
    {
        DL_Room dl_roomno = new DL_Room();

        public void InsertRoom(BL_Field bl_field)
        {
            dl_roomno.InsertRoom(bl_field);
        }

        public void UpdateRoom(BL_Field bl_field)
        {
            dl_roomno.UpdateRoom(bl_field);
        }

        public void DeleteRoom(BL_Field bl_field)
        {
            dl_roomno.DeleteRoom(bl_field);
        }

        public DataTable SelectRoom()
        {
            DataTable dt = dl_roomno.SelectRoom();
            return dt;
        }

        public string CheckRoom(BL_Field bl_field)
        {
            string roomno = dl_roomno.CheckRoom(bl_field);
            return roomno;
        }

    }
}

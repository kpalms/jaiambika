﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using Restaurant.PL.Reports;
using System.Data;


namespace Restaurant.BL
{
    class BL_Security
    {
        DL_Security dl_security = new DL_Security();

        public void TruncateData()
        {
            dl_security.TruncateData();
        }

        public void TruncateDataSale()
        {
            dl_security.TruncateDataSale();
        }

        public void ExcuteQuery(string qry)
        {
            dl_security.ExcuteQuery(qry);
        }
        public DataTable MenuMaster()
        {
            DataTable dt = dl_security.MenuMaster();
            return dt;
        }

    }
}

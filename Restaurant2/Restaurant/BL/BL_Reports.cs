﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;


namespace Restaurant.BL
{
    class BL_Reports
    {
        DL_Reports dl_reports = new DL_Reports();


        public DataSet SearchTodayClose(BL_Field bl_field)
        {
            DataSet dt = dl_reports.SearchTodayClose(bl_field);
            return dt;
        }

        public DataSet SearchSaleByItem(BL_Field bl_field)
        {
            DataSet dt = dl_reports.SearchSaleByItem(bl_field);
            return dt;
        }
        public DataSet SearchSaleByItemBar(BL_Field bl_field)
        {
            DataSet dt = dl_reports.SearchSaleByItemBar(bl_field);
            return dt;
        }
        public DataSet Yesturday(BL_Field bl_field)
        {
            DataSet dt = dl_reports.Yesturday(bl_field);
            return dt;
        }


        public DataSet SearchSaleByTable(BL_Field bl_field)
        {
            DataSet dt = dl_reports.SearchSaleByTable(bl_field);
            return dt;
        }

        public DataSet SearchSaleByBillNo(BL_Field bl_field)
        {
            DataSet dt = dl_reports.SearchSaleByBillNo(bl_field);
            return dt;
        }
        public DataSet SearchSaleByBillNoBar(BL_Field bl_field)
        {
            DataSet dt = dl_reports.SearchSaleByBillNoBar(bl_field);
            return dt;
        }
        public DataSet SearchCashierWise(BL_Field bl_field)
        {
            DataSet dt = dl_reports.SearchCashierWise(bl_field);
            return dt;
        }
        public DataSet SearchBillNo(BL_Field bl_field)
        {
            DataSet dt = dl_reports.SearchBillNo(bl_field);
            return dt;
        }
        public DataSet SearchSaleByBillViewItem(BL_Field bl_field)
        {
            DataSet dt = dl_reports.SearchSaleByBillViewItem(bl_field);
            return dt;
        }


        public DataSet SearchByItemList(BL_Field bl_field)
        {
            DataSet dt = dl_reports.SearchByItemList(bl_field);
            return dt;
        }
        public DataSet SearchByItemListBar(BL_Field bl_field)
        {
            DataSet dt = dl_reports.SearchByItemListBar(bl_field);
            return dt;
        }
        public DataSet SelectPurchaseExp(BL_Field bl_field)
        {
            DataSet dt = dl_reports.SelectPurchaseExp(bl_field);
            return dt;
        }

        public DataSet CancelBillAllDays(BL_Field bl_field)
        {
            DataSet dt = dl_reports.CancelBillAllDays(bl_field);
            return dt;
        }

        public DataSet ParsalReports(BL_Field bl_field)
        {
            DataSet dt = dl_reports.ParsalReports(bl_field);
            return dt;
        }
        public DataSet GetParsal(BL_Field bl_field)
        {
            DataSet dt = dl_reports.GetParsal(bl_field);
            return dt;
        }
        public DataSet GetParsalDetails(BL_Field bl_field)
        {
            DataSet dt = dl_reports.GetParsalDetails(bl_field);
            return dt;
        }
        public DataSet GetBillDetails(BL_Field bl_field)
        {
            DataSet dt = dl_reports.GetBillDetails(bl_field);
            return dt;
        }
        public DataSet CustomerList(BL_Field bl_field)
        {
            DataSet dt = dl_reports.CustomerList(bl_field);
            return dt;
        }
        public DataSet DisCountReports(BL_Field bl_field)
        {
            DataSet dt = dl_reports.DisCountReports(bl_field);
            return dt;
        }
        public DataSet CashBookReports(BL_Field bl_field)
        {
            DataSet dt = dl_reports.CashBookReports(bl_field);
            return dt;
        }

        public DataSet SelectCheckIn(BL_Field bl_field)
        {
            DataSet dt = dl_reports.SelectCheckIn(bl_field);
            return dt;
        }
        public DataSet CheckInReports(BL_Field bl_field)
        {
            DataSet dt = dl_reports.CheckInReports(bl_field);
            return dt;
        }
        public DataSet CheckOutReports(BL_Field bl_field)
        {
            DataSet dt = dl_reports.CheckOutReports(bl_field);
            return dt;
        }
        public DataSet BillWiseCheckOut(BL_Field bl_field)
        {
            DataSet dt = dl_reports.BillWiseCheckOut(bl_field);
            return dt;
        }
        public DataSet CheckOutCashierWise(BL_Field bl_field)
        {
            DataSet dt = dl_reports.CheckOutCashierWise(bl_field);
            return dt;
        }
        public DataSet RoomWise(BL_Field bl_field)
        {
            DataSet dt = dl_reports.RoomWise(bl_field);
            return dt;
        }
        public DataSet SelectCheckOut(BL_Field bl_field)
        {
            DataSet dt = dl_reports.SelectCheckOut(bl_field);
            return dt;
        }

        public DataSet KotWise(BL_Field bl_field)
        {
            DataSet dt = dl_reports.KotWise(bl_field);
            return dt;
        }
        public DataSet KotWiseItem(BL_Field bl_field)
        {
            DataSet dt = dl_reports.KotWiseItem(bl_field);
            return dt;
        }
        public DataSet BillSummary(BL_Field bl_field)
        {
            DataSet dt = dl_reports.BillSummary(bl_field);
            return dt;
        }


        public DataSet PurchasingReport(BL_Field bl_field)
        {
            DataSet dt = dl_reports.PurchasingReport(bl_field);
            return dt;
        }
        public DataSet WastageReport(BL_Field bl_field)
        {
            DataSet dt = dl_reports.WastageReport(bl_field);
            return dt;
        }

        public DataSet BillDetailsReport(BL_Field bl_field)
        {
            DataSet dt = dl_reports.BillDetailsReport(bl_field);
            return dt;
        }
        public DataSet CounterReport(BL_Field bl_field)
        {
            DataSet dt = dl_reports.CounterReport(bl_field);
            return dt;
        }
        public DataSet StockReport(BL_Field bl_field)
        {
            DataSet dt = dl_reports.StockReport(bl_field);
            return dt;
        }
        public DataSet RecipieReport(BL_Field bl_field)
        {
            DataSet dt = dl_reports.RecipieReport(bl_field);
            return dt;
        }
        public DataSet CancelBill(BL_Field bl_field)
        {
            DataSet dt = dl_reports.CancelBill(bl_field);
            return dt;
        }
        public DataSet CustomerHistory(BL_Field bl_field)
        {
            DataSet dt = dl_reports.CustomerHistory(bl_field);
            return dt;
        }

    }
}

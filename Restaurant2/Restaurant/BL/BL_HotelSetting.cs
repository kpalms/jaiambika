﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;

namespace Restaurant.BL
{
    class BL_HotelSetting
    {

        DL_HotelSetting dl_hotelSetting = new DL_HotelSetting();

        public void InsertHotelSetting(BL_Field bl_field)
        {
            dl_hotelSetting.InsertHotelSetting(bl_field);
        }

        public DataTable SelectHotelSetting()
        {
            DataTable dt = dl_hotelSetting.SelectHotelSetting();
            return dt;
        }

        public void UpdateHotelSetting(BL_Field bl_field)
        {
            dl_hotelSetting.UpdateHotelSetting(bl_field);
        }
        public void DeleteHotelSetting(BL_Field bl_field)
        {
            dl_hotelSetting.DeleteHotelSetting(bl_field);
        }

        public void InsertDate(BL_Field bl_field)
        {
            dl_hotelSetting.InsertDate(bl_field);
        }

        public void CloseDate(BL_Field bl_field)
        {
            dl_hotelSetting.CloseDate(bl_field);
        }


    }
}

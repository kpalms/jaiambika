﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;


namespace Restaurant.BL
{
    class BL_CheckIn
    {
        DL_CheckIn dl_checkin = new DL_CheckIn();

        public string InsertCheckIn(BL_Field bl_field)
        {
          string id= dl_checkin.InsertCheckIn(bl_field);
          return id;
        }

        public void UpdateCheckIn(BL_Field bl_field)
        {
            dl_checkin.UpdateCheckIn(bl_field);
        }

        public void DeleteCheckIn(BL_Field bl_field)
        {
            dl_checkin.DeleteCheckIn(bl_field);
        }

        public DataTable SelectCheckIn(BL_Field bl_field)
        {
            DataTable dt = dl_checkin.SelectCheckIn(bl_field);
            return dt;
        }
        public DataTable SelectRoomTotal(BL_Field bl_field)
        {
            DataTable dt = dl_checkin.SelectRoomTotal(bl_field);
            return dt;
        }


        public DataTable SelectCheckInById(BL_Field bl_field)
        {

            DataTable dt = dl_checkin.SelectCheckInById(bl_field);
            return dt;
        }

        public void InsertCheckOut(BL_Field bl_field)
        {
            dl_checkin.InsertCheckOut(bl_field);
           
        }

        public void DeleteCheckOut(BL_Field bl_field)
        {
            dl_checkin.DeleteCheckOut(bl_field);
        }

        public DataTable SelectCheckInOut(BL_Field bl_field)
        {
            DataTable dt = dl_checkin.SelectCheckInOut(bl_field);
            return dt;
        }

    }
}

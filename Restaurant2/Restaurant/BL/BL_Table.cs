﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;



namespace Restaurant.BL
{
    class BL_Table
    {
        DL_Table dl_table = new DL_Table();

        public DataTable SelectTable()
        {
            DataTable dt = dl_table.SelectTable();
            return dt;
        }
        public DataTable SearchTable(BL_Field bl_field)
        {
            DataTable dt = dl_table.SearchTable(bl_field);
            return dt;
        }

        public DataTable SearchTableAll(BL_Field bl_field)
        {
            DataTable dt = dl_table.SearchTableAll(bl_field);
            return dt;
        }

        public DataTable CheckTable()
        {
            DataTable dt = dl_table.CheckTable();
            return dt;
        }

        public string SelectMaxTable()
        {
            string tbl = dl_table.SelectMaxTable();
            return tbl;
        }

        public void InsertTable(BL_Field bl_field)
        {
            dl_table .InsertTable(bl_field);
        }

        public void RemoveTable(BL_Field bl_field)
        {
            dl_table.RemoveTable(bl_field);
        }

        public void UpdateTable(BL_Field bl_field)
        {
            dl_table.UpdateTable(bl_field);
        }




        public DataTable CheckTableKot()
        {
            DataTable dt = dl_table.CheckTableKot();
            return dt;
        }
        public DataTable CheckOrderKot()
        {
            DataTable dt = dl_table.CheckOrderKot();
            return dt;
        }

        public DataTable CheckTableSeat(string tbl)
        {
            DataTable dt = dl_table.CheckTableSeat(tbl);
            return dt;
        }

        public DataTable CheckParcel()
        {
            DataTable dt = dl_table.CheckParcel();
            return dt;
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using Restaurant.PL.Reports;
using System.Collections;
using System.Data;

namespace Restaurant.BL
{
    class BL_Customer
    {

        static private int customeridstatic;



        public int CustomerIdStatic
        {
            get { return customeridstatic; }
            set { customeridstatic = value; }
        }



        DL_Customer dl_customer = new DL_Customer();

        public void InsertCustomer(BL_Field bl_field)
        {
            dl_customer.InsertCustomer(bl_field);
        }

        public DataTable SelectCustomer()
        {
            DataTable dt = dl_customer.SelectCustomer();
            return dt;
        }
        public DataTable SelectSociety()
        {
            DataTable dt = dl_customer.SelectSociety();
            return dt;
        }
        public DataTable SelectFlat()
        {
            DataTable dt = dl_customer.SelectFlat();
            return dt;
        }
        public DataTable SelectCustomerBySearch(BL_Field bl_field)
        {
            DataTable dt = dl_customer.SelectCustomerBySearch(bl_field);
            return dt;
        }

        public void UpdateCustomer(BL_Field bl_field)
        {
            dl_customer.UpdateCustomer(bl_field);
        }
        public void DeleteCustomer(BL_Field bl_field)
        {
            dl_customer.DeleteCustomer(bl_field);
        }

        public DataTable SelectCustomerName(BL_Field bl_field)
        {
            DataTable dt = dl_customer.SelectCustomerName(bl_field);
            return dt;
        }

        public DataTable SelectCustomerMobileNo(BL_Field bl_field)
        {
            DataTable dt = dl_customer.SelectCustomerMobileNo(bl_field);
            return dt;
        }

        public DataTable SelectCustomerById(BL_Field bl_field)
        {
            DataTable dt = dl_customer.SelectCustomerById(bl_field);
            return dt;
        }
        public DataTable SelectCustById(BL_Field bl_field)
        {
            DataTable dt = dl_customer.SelectCustById(bl_field);
            return dt;
        }
        public string GetIdCustId(BL_Field bl_field)
        {
            string id = dl_customer.GetIdCustId(bl_field );
            return id;
        }

        public DataTable SelectCustomerCombo()
        {
            DataTable dt = dl_customer.SelectCustomerCombo();
            return dt;
        }

        public DataTable SelectCustomerBySociety(BL_Field bl_field)
        {
            DataTable dt = dl_customer.SelectCustomerBySociety(bl_field);
            return dt;
        }
        public DataTable SelectCustomerByFlat(BL_Field bl_field)
        {
            DataTable dt = dl_customer.SelectCustomerByFlat(bl_field);
            return dt;
        }

    }
}

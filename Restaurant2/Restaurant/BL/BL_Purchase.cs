﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;


namespace Restaurant.BL
{
    class BL_Purchase
    {

       
        DL_Purchase dl_purchase = new DL_Purchase();


        //==============PurchaseDetail====================================================================================

        public void InsertPurchaseDetail(BL_Field bl_field)
        {
            dl_purchase.InsertPurchaseDetail(bl_field);
        }

        public DataTable SelectPurchaseDetail(BL_Field bl_field)
        {
            DataTable dt = dl_purchase.SelectPurchaseDetail(bl_field);
            return dt;
        }
        public void UpdatePurchaseDetail(BL_Field bl_field)
        {
            dl_purchase.UpdatePurchaseDetail(bl_field);
        }
        public void DeletePurchaseDetail(BL_Field bl_field)
        {
            dl_purchase.DeletePurchaseDetail(bl_field);
        }
        public void UpdatePurchaseId(BL_Field bl_field)
        {
            dl_purchase.UpdatePurchaseId(bl_field);
        }


        //===================Purchase=================================================================================



        public string InsertPurchase(BL_Field bl_field)
        {
            string id = dl_purchase.InsertPurchase(bl_field);
           return id;
        }


        //===================Stock=================================================================================

        public string SelectItemId(BL_Field bl_field)
        {
            string id = dl_purchase.SelectItemId(bl_field);
            return id;
        }


        public string SelectStockByID(BL_Field bl_field)
        {
            string qty = dl_purchase.SelectStockByID(bl_field);
            return qty;
        }


        public void AddStock(BL_Field bl_field)
        {
            dl_purchase.AddStock(bl_field);
        }


        //===================Purchase Security=================================================================================



        public DataTable SelectPurchaseByDate(BL_Field bl_field)
        {
            DataTable dt = dl_purchase.SelectPurchaeByDate(bl_field);
            return dt;
        }


        public DataTable SelectInvoiceNo(BL_Field bl_field)
        {
            DataTable dt = dl_purchase.SelectInvoiceNo(bl_field);
            return dt;
        }

        public DataTable SelectDataByInvoice(BL_Field bl_field)
        {
            DataTable dt = dl_purchase.SelectDataByInvoice(bl_field);
            return dt;
        }


        public void UpdatePurchase(BL_Field bl_field)
        {
            dl_purchase.UpdatePurchase(bl_field);
        }

        




        //====================================================================================================

        public DataTable SelectExpensesByDate(BL_Field bl_field)
        {
            DataTable dt = dl_purchase.SelectExpensesByDate(bl_field);
            return dt;
        }
        public DataSet SelectExpensesByStatus(BL_Field bl_field)
        {
            DataSet dt = dl_purchase.SelectExpensesByStatus(bl_field);
            return dt;
        }

        public void CloseExpenses()
        {
            dl_purchase.CloseExpenses();
        }


        public void InsertExpenses(BL_Field bl_field)
        {
            dl_purchase.InsertExpenses(bl_field);
        }

        public DataTable SelectExpenses(BL_Field bl_field)
        {
            DataTable dt = dl_purchase.SelectExpenses(bl_field);
            return dt;
        }
        public void UpdateExpenses(BL_Field bl_field)
        {
            dl_purchase.UpdateExpenses(bl_field);
        }
        public void DeleteExpenses(BL_Field bl_field)
        {
            dl_purchase.DeleteExpenses(bl_field);
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;


namespace Restaurant.BL
{
    class BL_Vendor
    {
        DL_Vendor dl_vendor = new DL_Vendor();
        public void InsertVendor(BL_Field bl_field)
        {
            dl_vendor.InsertVendor(bl_field);
        }

        public void UpdateVendor(BL_Field bl_field)
        {
            dl_vendor.UpdateVendor(bl_field);
        }
        public void DeleteVendor(BL_Field bl_field)
        {
            dl_vendor.DeleteVendor(bl_field);
        }
        public DataTable SelectVendor()
        {
            DataTable dt = dl_vendor.SelectVendor();
            return dt;
        }

        public DataTable SearchVendor(BL_Field bl_field)
        {
            DataTable dt = dl_vendor.SearchVendor(bl_field);
            return dt;
        }

        public DataTable SearchVendorAll(BL_Field bl_field)
        {
            DataTable dt = dl_vendor.SearchVendorAll(bl_field);
            return dt;
        }
    }
}

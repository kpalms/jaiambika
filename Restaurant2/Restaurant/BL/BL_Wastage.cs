﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;


namespace Restaurant.BL
{
    class BL_Wastage
    {


        DL_Wastage dl_Wastage = new DL_Wastage();

        public void InsertWastage(BL_Field bl_field)
        {
            dl_Wastage.InsertWastage(bl_field);
        }

        public DataTable SelectWastage(BL_Field bl_field)
        {
            DataTable dt = dl_Wastage.SelectWastage(bl_field);
            return dt;
        }
        public void UpdateWastage(BL_Field bl_field)
        {
            dl_Wastage.UpdateWastage(bl_field);
        }
        public void DeleteWastage(BL_Field bl_field)
        {
            dl_Wastage.DeleteWastage(bl_field);
        }



    }
}

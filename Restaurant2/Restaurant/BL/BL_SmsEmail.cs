﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;


namespace Restaurant.BL
{
    class BL_SmsEmail
    {


        //group
        DL_SmsEmail dl_smsemail = new DL_SmsEmail();

        public void InsertSms(BL_Field bl_field)
        {
            dl_smsemail.InsertSms(bl_field);
        }

        public void Updatesms(BL_Field bl_field)
        {
            dl_smsemail.Updatesms(bl_field);
        }
        public void Deletesms(BL_Field bl_field)
        {
            dl_smsemail.Deletesms(bl_field);
        }
        public DataTable Selectsms()
        {
            DataTable dt = dl_smsemail.Selectsms();
            return dt;
        }


        //emails
        public void Insertemail(BL_Field bl_field)
        {
            dl_smsemail.Insertemail(bl_field);
        }

        public void Updateemail(BL_Field bl_field)
        {
            dl_smsemail.Updateemail(bl_field);
        }
        public void Deleteemail(BL_Field bl_field)
        {
            dl_smsemail.Deleteemail(bl_field);
        }
        public DataTable Selectemail()
        {
            DataTable dt = dl_smsemail.Selectemail();
            return dt;
        }

    }
}

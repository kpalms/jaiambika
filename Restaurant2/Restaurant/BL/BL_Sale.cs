﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;

namespace Restaurant.BL
{
    class BL_Sale
    {
        DL_Sale dl_sale = new DL_Sale();

        public void InsertSale(BL_Field bl_field)
        {
         dl_sale.InsertSale(bl_field );
        
        }

        private string textsearch;
        public string TextSearch
        {
            get { return textsearch; }
            set { textsearch = value; }
        }


        public DataTable SelectItemName(BL_Field bl_field)
        {
            DataTable dt = dl_sale.SelectItemName(bl_field );
            return dt;
        }
        public DataTable SelectParsal(BL_Field bl_field)
        {
            DataTable dt = dl_sale.SelectParsal(bl_field);
            return dt;
        }
        //public DataSet  SelectItemNamePrint(BL_Field bl_field)
        //{
        //    DataSet dt = dl_sale.SelectItemNamePrint(bl_field);
        //    return dt;
        //}
        public DataSet SelectRestoPrint(BL_Field bl_field)
        {
            DataSet dt = dl_sale.SelectRestoPrint(bl_field);
            return dt;
        }

        public DataSet SelectParsalPrint(BL_Field bl_field)
        {
            DataSet dt = dl_sale.SelectParsalPrint(bl_field);
            return dt;
        }
        public DataSet PrintQt(BL_Field bl_field)
        {
            DataSet dt = dl_sale.PrintQt(bl_field);
            return dt;
        }
        //public DataSet PrintKOT(BL_Field bl_field)
        //{
        //    DataSet dt = dl_sale.PrintKOT(bl_field);
        //    return dt;
        //}
        public DataSet RePrintQt(BL_Field bl_field)
        {
            DataSet dt = dl_sale.RePrintQt(bl_field);
            return dt;
        }
        public DataSet ParsalPrintQt(BL_Field bl_field)
        {
            DataSet dt = dl_sale.ParsalPrintQt(bl_field);
            return dt;
        }
        public DataSet ParsalRePrintQt(BL_Field bl_field)
        {
            DataSet dt = dl_sale.ParsalRePrintQt(bl_field);
            return dt;
        }
        public void UpdateQt(BL_Field bl_field)
        {
            dl_sale.UpdateQt(bl_field);
        }
        public void UpdateOrderNo(BL_Field bl_field)
        {
            dl_sale.UpdateOrderNo(bl_field);
        }
        public void UpdateOrderNoParsal(BL_Field bl_field)
        {
            dl_sale.UpdateOrderNoParsal(bl_field);
        }
        //public void UpdateKot(BL_Field bl_field)
        //{
        //    dl_sale.UpdateKot(bl_field);
        //}

        public void ParsalUpdateQt(BL_Field bl_field)
        {
            dl_sale.ParsalUpdateQt(bl_field);
        }
        public void UpdateQty(BL_Field bl_field)
        {
            dl_sale.UpdateQty(bl_field);
        }
        public void UpdatePerson(BL_Field bl_field)
        {
            dl_sale.UpdatePerson(bl_field);
        }
        public void UpdateItemName(BL_Field bl_field)
        {
            dl_sale.UpdateItemName(bl_field);
        }
        public void ChangePrice(BL_Field bl_field)
        {
            dl_sale.ChangePrice(bl_field);
        }
        public void ChangeDis(BL_Field bl_field)
        {
            dl_sale.ChangeDis(bl_field);
        }


        public void KotRemark(BL_Field bl_field)
        {
            dl_sale.KotRemark(bl_field);
        }



        public void DeleteSaleItem(BL_Field bl_field)
        {
            dl_sale.DeleteSaleItem(bl_field);
        }

        public void CancelSaleItem(BL_Field bl_field)
        {
            dl_sale.CancelSaleItem(bl_field);
        }

        public void CloseOrderResto(BL_Field bl_field)
        {
            dl_sale.CloseOrderResto(bl_field);
        }
        public void CloseDivideBill(BL_Field bl_field)
        {
            dl_sale.CloseDivideBill(bl_field);
        }

        public void CloseParsal(BL_Field bl_field)
        {
            dl_sale.CloseParsal(bl_field);
        }

//search sale

        public DataTable  SearchAllDaysLoad(BL_Field bl_field)
        {
            DataTable dt = dl_sale.SearchAllDaysLoad(bl_field);
            return dt;
        }
        public DataSet SearchAllDays(BL_Field bl_field)
        {
            DataSet dt = dl_sale.SearchAllDays(bl_field);
            return dt;
        }

        public DataSet SearchToday(BL_Field bl_field)
        {
            DataSet dt = dl_sale.SearchToday(bl_field);
            return dt;
        }

        public DataSet SearchTodayStatus(BL_Field bl_field)
        {
            DataSet dt = dl_sale.SearchTodayStatus(bl_field);
            return dt;
        }
        public DataSet SelectBillByNo(BL_Field bl_field)
        {
            DataSet dt = dl_sale.SelectBillByNo(bl_field);
            return dt;
        }
        public DataSet SelectBill(BL_Field bl_field)
        {
            DataSet dt = dl_sale.SelectBill(bl_field);
            return dt;
        }
        public DataSet SearchTodayOpen(BL_Field bl_field)
        {
            DataSet dt = dl_sale.SearchTodayOpen(bl_field);
            return dt;
        }
        public DataSet SearchYesturday(BL_Field bl_field)
        {
            DataSet dt = dl_sale.SearchYesturday(bl_field);
            return dt;
        }
        //public DataSet Yesturday(BL_Field bl_field)
        //{
        //    DataSet dt = dl_sale.Yesturday(bl_field);
        //    return dt;
        //}
        public DataSet SearchFromTo(BL_Field bl_field)
        {
            DataSet dt = dl_sale.SearchFromTo(bl_field);
            return dt;
        }

        public DataSet GetAllSale(BL_Field bl_field)
        {
            DataSet dt = dl_sale.GetAllSale(bl_field);
            return dt;
        }
  
        public void CancelBill(BL_Field bl_field)
        {
            dl_sale.CancelBill(bl_field);
        }

        public string CheckQt(BL_Field bl_field)
        {
            string chkqt = dl_sale.CheckQt(bl_field);
            return chkqt;
        }


        public DataTable SelectBillNo(BL_Field bl_field)
        {
            DataTable dt = dl_sale.SelectBillNo(bl_field);
            return dt;
        }

        public DataTable SelectDataByDate(BL_Field bl_field)
        {
            DataTable dt = dl_sale.SelectDataByDate(bl_field);
            return dt;
        }





        public DataTable SelectParsalNo(BL_Field bl_field)
        {
            DataTable dt = dl_sale.SelectParsalNo(bl_field);
            return dt;
        }
        public DataSet SearchByBillNo(BL_Field bl_field)
        {
            DataSet dt = dl_sale.SearchByBillNo(bl_field);
            return dt;
        }
        public DataSet RePrint(BL_Field bl_field)
        {
            DataSet dt = dl_sale.RePrint(bl_field);
            return dt;
        }
        public DataSet SearchByParsalNo(BL_Field bl_field)
        {
            DataSet dt = dl_sale.SearchByParsalNo(bl_field);
            return dt;
        }
        public void CloseCancelBill(BL_Field bl_field)
        {
            dl_sale.CloseCancelBill(bl_field);
        }

        public void CloseCancelBillByItem(BL_Field bl_field)
        {
            dl_sale.CloseCancelBillByItem(bl_field);
        }


        public void DeleteSaleLast(BL_Field bl_field)
        {
            dl_sale.DeleteSaleLast(bl_field);
        }

        public string GetQty(BL_Field bl_field)
        {
          string qty=  dl_sale.GetQty(bl_field );
          return qty;
        }


        public void ParsalToCustomer(BL_Field bl_field)
        {
            dl_sale.ParsalToCustomer(bl_field);
        }

        //public DataSet ParsalReports(BL_Field bl_field)
        //{
        //    DataSet dt = dl_sale.ParsalReports(bl_field);
        //    return dt;
        //}

        public void InsertGrandTotal(BL_Field bl_field)
        {
            dl_sale.InsertGrandTotal(bl_field);
        }

        //public DataSet CustomerList(BL_Field bl_field)
        //{
        //    DataSet dt = dl_sale.CustomerList(bl_field);
        //    return dt;
        //}

        public string InsertBill(BL_Field bl_field)
        {
          string id=  dl_sale.InsertBill(bl_field);
          return id;
        }

        public void InsertCancelBill(BL_Field bl_field)
        {
            dl_sale.InsertCancelBill(bl_field);
        }

        public DataSet GetTableDetails(BL_Field bl_field)
        {
          DataSet ds=  dl_sale.GetTableDetails(bl_field);
          return ds;
        }
        public DataSet GetKotDetails(BL_Field bl_field)
        {
            DataSet ds = dl_sale.GetKotDetails(bl_field);
            return ds;
        }
        public DataSet GetPendingBill(BL_Field bl_field)
        {
            DataSet ds = dl_sale.GetPendingBill(bl_field);
            return ds;
        }
        public DataSet GetSettleBills(BL_Field bl_field)
        {
            DataSet ds = dl_sale.GetSettleBills(bl_field);
            return ds;
        }
        public DataTable GetPendingBills(BL_Field bl_field)
        {
            DataTable ds = dl_sale.GetPendingBills(bl_field);
            return ds;
        }


        public DataSet GetBill(BL_Field bl_field)
        {
            DataSet ds = dl_sale.GetBill(bl_field);
            return ds;
        }
        public DataSet GetBillCheckOut(BL_Field bl_field)
        {
            DataSet ds = dl_sale.GetBillCheckOut(bl_field);
            return ds;
        }
        public void UpdateBill(BL_Field bl_field)
        {
            dl_sale.UpdateBill(bl_field);
        }
        public void InsertDebitAmt(BL_Field bl_field)
        {
            dl_sale.InsertDebitAmt(bl_field);
        }
        public void InsertKot(BL_Field bl_field)
        {
            dl_sale.InsertKot(bl_field);
        }


        public DataSet ShowBill(BL_Field bl_field)
        {
            DataSet dt = dl_sale.ShowBill(bl_field);
            return dt;
        }

        public void InsertRemoveItem(BL_Field bl_field)
        {
            dl_sale.InsertRemoveItem(bl_field);
        }



        public DataTable BindorderNo(BL_Field bl_field)
        {
            DataTable dt = dl_sale.BindorderNo(bl_field);
            return dt;
        }

        public DataTable BindKotReprint(BL_Field bl_field)
        {
            DataTable dt = dl_sale.BindKotReprint(bl_field);
            return dt;
        }


        public DataSet BindKotReprintSearch(BL_Field bl_field)
        {
            DataSet dt = dl_sale.BindKotReprintSearch(bl_field);
            return dt;
        }


        public DataTable CheckPendingStatus(BL_Field bl_field)
        {
            DataTable dt = dl_sale.CheckPendingStatus(bl_field);
            return dt;
        }

        public DataTable SelectCustomer(BL_Sale bl_sale)
        {
            DataTable dt = dl_sale.SelectCustomer(bl_sale);
            return dt;
        }

        public DataTable SelectCustomerAll()
        {
            DataTable dt = dl_sale.SelectCustomerAll();
            return dt;
        }



        public DataTable SaleByPaymentMode(BL_Field bl_field)
        {
            DataTable dt = dl_sale.SaleByPaymentMode(bl_field);
            return dt;
        }

        public DataTable SaleSummery(BL_Field bl_field)
        {
            DataTable dt = dl_sale.SaleSummery(bl_field);
            return dt;
        }

        public void CloseCounter(BL_Field bl_field)
        {
            dl_sale.CloseCounter(bl_field);
        }

        public void DeleveryCharge(BL_Field bl_field)
        {
            dl_sale.DeleveryCharge(bl_field);
        }

    }
}

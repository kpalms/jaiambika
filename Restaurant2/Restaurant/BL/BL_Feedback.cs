﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;



using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using Restaurant.PL.Reports;
using System.Collections;



namespace Restaurant.BL
{
    class BL_Feedback
    {
        DL_Feedback dl_feedback = new DL_Feedback();

        private int feedbackId;
        private string date;
        private int custId;
        private int qualityoffood;
        private int service;
        private int ambience;
        private string comments;
        private int serversName;



        public int FeedbacpkId
        {
            get { return feedbackId; }
            set { feedbackId = value; }
        }
        public int ServersName
        {
            get { return serversName; }
            set { serversName = value; }
        }
        public string Date
        {
            get { return date; }
            set { date = value; }
        }
        public int CustId
        {
            get { return custId; }
            set { custId = value; }
        }
       
        public int QualityOfFood
        {
            get { return qualityoffood; }
            set { qualityoffood = value; }
        }

        public int Service
        {
            get { return service; }
            set { service = value; }
        }
        public int Ambience
        {
            get { return ambience; }
            set { ambience = value; }
        }
        public string Comments
        {
            get { return comments; }
            set { comments = value; }
        }







        public DataTable SelectCustomer()
        {
            DataTable dt = dl_feedback.SelectCustomer();
            return dt;
        }
        public DataTable SelectFeedback()
        {
            DataTable dt = dl_feedback.SelectFeedback();
            return dt;
        }
        public DataTable SelectServer()
        {
            DataTable dt = dl_feedback.SelectServer();
            return dt;
        }
        public void InsertFeedback(BL_Feedback bl_Feedback)
        {
            dl_feedback.InsertFeedback(bl_Feedback);
        }

        public void UpdateFeedback(BL_Feedback bl_Feedback)
        {
            dl_feedback.UpdateFeedback(bl_Feedback);
        }
        public void DeleteFeedback(BL_Feedback bl_Feedback)
        {
            dl_feedback.DeleteFeedback(bl_Feedback);
        }
    }

       
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;

namespace Restaurant.BL
{
    class BL_Merge
    {
        DL_Merge dl_merge = new DL_Merge();

        private  int tableno;
        private string seat;
        private int tableno1 ;
        private string seat1 ;
        private string seatfrom;
        private int tableNonew;

        public int TableNo
        {
            get { return tableno; }
            set { tableno = value; }
        }
        public int TableNoNew
        {
            get { return tableNonew; }
            set { tableNonew = value; }
        }

        public int TableNo1
        {
            get { return tableno1; }
            set { tableno1 = value; }
        }

        public string Seat
        {
            get { return seat; }
            set { seat = value; }
        }
        public string SeatFrom
        {
            get { return seatfrom; }
            set { seatfrom = value; }
        }
        public string Seat1
        {
            get { return seat1; }
            set { seat1 = value; }
        }

        public DataTable SelectTableName()
        {
            DataTable dt = dl_merge.SelectTableName();
            return dt;
        }


        public void UpdateTableName(BL_Merge bl_merge)
        {
            dl_merge.UpdateTableName(bl_merge);
        }

    }
}

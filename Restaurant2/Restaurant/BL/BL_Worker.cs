﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;



namespace Restaurant.BL
{
    class BL_Worker
    {
        DL_Worker dl_worker = new DL_Worker();

        //Worker Attendanc
        private string reasion;
        private int attendanceid;
        private string date;
        private string date1;
        private int workerid;
        private int attendancetype;
        private static string fromdate;
        private static string todate;

        public string FromDate
        {
            get { return fromdate; }
            set { fromdate = value; }
        }
        public string Todate
        {
            get { return todate; }
            set { todate = value; }
        }


        public int AttendanceId
        {
            get { return attendanceid; }
            set { attendanceid = value; }
        }

        public int WorkerId
        {
            get { return workerid; }
            set { workerid = value; }
        }
        public int AttendanceType
        {
            get { return attendancetype; }
            set { attendancetype = value; }
        }

        public string Date
        {
            get { return date; }
            set { date = value; }
        }
        public string Date1
        {
            get { return date1; }
            set { date1 = value; }
        }
        public string Reasion
        {
            get { return reasion; }
            set { reasion = value; }
        }

        //Rate Categary

        private int ratecatid;
        private string ratecatname;

        public int RateCatId
        {
            get { return ratecatid; }
            set { ratecatid = value; }
        }

        public string RateCatName
        {
            get { return ratecatname; }
            set { ratecatname = value; }
        }

        //Department

        private int dpid;
        private string department;
        private int sequence;

        public int DpId
        {
            get { return dpid; }
            set { dpid = value; }
        }

        public string Department
        {
            get { return department; }
            set { department = value; }
        }
        public int Sequence
        {
            get { return sequence; }
            set { sequence = value; }
        }
        //DepartmentTable

        private int dptid;
        //private int dpid;
        private int tableno;

        //public int DpId
        //{
        //    get { return dpid; }
        //    set { dpid = value; }
        //}

        public int DptId
        {
            get { return dptid; }
            set { dptid = value; }
        }

        public int TableNo
        {
            get { return tableno; }
            set { tableno = value; }
        }


        //Rate Assign

        private int rateaid;


        public int RateAId
        {
            get { return rateaid; }
            set { rateaid = value; }
        }

       




        //worker SalaryCalculator

        private int salarycalsid;
        //private string date;
        //private int workerid;
        private int presentdays;
        private int absentdays;
        private int halfdays;
        private decimal extra;
        private decimal deduction;
        private decimal debit;
        private decimal credit;
        private string remark;


        public int SalaryCalsId
        {
            get { return salarycalsid; }
            set { salarycalsid = value; }
        }

        public int PresentDays
        {
            get { return presentdays; }
            set { presentdays = value; }
        }


        public int AbsentDays
        {
            get { return absentdays; }
            set { absentdays = value; }
        }


        public int Halfdays
        {
            get { return halfdays; }
            set { halfdays = value; }
        }


        public decimal Extra
        {
            get { return extra; }
            set { extra = value; }
        }

        public decimal Credit
        {
            get { return credit; }
            set { credit = value; }
        }


        public decimal Deduction
        {
            get { return deduction; }
            set { deduction = value; }
        }

        public decimal Debit
        {
            get { return debit; }
            set { debit = value; }
        }

        public string Remark
        {
            get { return remark; }
            set { remark = value; }
        }




        //worker Payment


        private int paymentid;
        //private int workerid;
        //private string date;
        private decimal paidamount;
        //private string remark;

        public int PaymentId
        {
            get { return paymentid; }
            set { paymentid = value; }
        }

        public decimal PaidAmount
        {
            get { return paidamount; }
            set { paidamount = value; }
        }

   


        public void InsertWorker(BL_Field bl_field)
        {
            dl_worker.InsertWorker(bl_field);
        }

        public DataTable SelectWorker()
        {
            DataTable dt = dl_worker.SelectWorker();
            return dt;
        }
        public DataTable SelectWaiter(BL_Field bl_field)
        {
            DataTable dt = dl_worker.SelectWaiter(bl_field);
            return dt;
        }
        public DataTable SelectWaiterName(BL_Field bl_field)
        {
            DataTable dt = dl_worker.SelectWaiterName(bl_field);
            return dt;
        }


        public DataTable SelectWaiterNameAll(BL_Field bl_field)
        {
            DataTable dt = dl_worker.SelectWaiterNameAll(bl_field);
            return dt;
        }


        public DataTable SelectCashier()
        {
            DataTable dt = dl_worker.SelectCashier();
            return dt;
        }
        public DataTable CheckUsername(BL_Field bl_field)
        {
            DataTable dt = dl_worker.CheckUsername(bl_field);
            return dt;
        }
        public void UpdateWorker(BL_Field bl_field)
        {
            dl_worker.UpdateWorker(bl_field);
        }
        public void DeleteWorker(BL_Field bl_field)
        {
            dl_worker.DeleteWorker(bl_field);
        }

        public SqlDataReader CheckLogin(BL_Field bl_field)
        {
          SqlDataReader dr=  dl_worker.CheckLogin(bl_field);
          return dr;
        }


        public SqlDataReader CheckSecurity(BL_Field bl_field)
        {
            SqlDataReader dr = dl_worker.CheckSecurity(bl_field);
            return dr;
        }

        public void InsertLoginDetails(BL_Field bl_field)
        {
            dl_worker.InsertLoginDetails(bl_field);
        }
        public void UpdateLogOutDetails(BL_Field bl_field)
        {
            dl_worker.UpdateLogOutDetails(bl_field);
        }


        public void InsertAttendanc(BL_Worker bl_worker)
        {
            dl_worker.InsertAttendanc(bl_worker);
        }

        public void InsertSalaryCalculator(BL_Worker bl_worker)
        {
            dl_worker.InsertSalaryCalculator(bl_worker);
        }
        public void InsertPayment(BL_Worker bl_worker)
        {
            dl_worker.InsertPayment(bl_worker);
        }

        //public SqlDataReader SelectSalary(BL_Worker bl_worker)
        //{
        //    SqlDataReader dr = dl_worker.SelectSalary(bl_worker);
        //    return dr;
        //}

        public DataTable Selectworkerdetails(BL_Worker bl_worker)
        {
            DataTable dt = dl_worker.Selectworkerdetails(bl_worker);
            return dt;
        }



        public DataTable Selectworkerpaymentdetails(BL_Worker bl_worker)
        {
            DataTable dt = dl_worker.Selectworkerpaymentdetails(bl_worker);
            return dt;
        }

        public DataTable Selectworkerpaymentdetailsdatewise(BL_Worker bl_worker)
        {
            DataTable dt = dl_worker.Selectworkerpaymentdetailsdatewise(bl_worker);
            return dt;
        }

        public DataTable Selectworkerhistory(BL_Worker bl_worker)
        {
            DataTable dt = dl_worker.Selectworkerhistory(bl_worker);
            return dt;
        }

        public DataTable Selectworkerattendance(BL_Worker bl_worker)
        {
            DataTable dt = dl_worker.Selectworkerattendance(bl_worker);
            return dt;
        }
          public DataTable  SelectAttendance(BL_Worker bl_worker)
        {
            DataTable dt = dl_worker.SelectAttendance(bl_worker);
            return dt;
        }




          public void UpdateAttendance(BL_Worker bl_worker)
        {
            dl_worker.UpdateAttendance(bl_worker);
        }

          public void DeleteAttendance(BL_Worker bl_worker)
        {
            dl_worker.DeleteAttendance(bl_worker);
        }

          public DataTable SelectSalaryCalculater(BL_Worker bl_worker)
        {
            DataTable dt = dl_worker.SelectSalaryCalculater(bl_worker);
            return dt;
        }

          public void UpdateSalaryCalculator(BL_Worker bl_worker)
        {
            dl_worker.UpdateSalaryCalculator(bl_worker);
        }



          public void DeleteSalaryCalculator(BL_Worker bl_worker)
        {
            dl_worker.DeleteSalaryCalculator(bl_worker);
        }



          public DataTable SelectAttandancedatewise(BL_Worker bl_worker)
        {
            DataTable dt = dl_worker.SelectAttandancedatewise(bl_worker);
            return dt;
        }


          public DataTable SelectSuppliderName()
        {
            DataTable dt = dl_worker.SelectSuppliderName();
            return dt;
        }

          public DataTable SelectSupplierAccount()
          {
              DataTable dt = dl_worker.SelectSupplierAccount();
              return dt;
          }


          public DataTable SelectSupplierAccountNamewise(BL_Field bl_field)
          {
              DataTable dt = dl_worker.SelectSupplierAccountNamewise(bl_field);
              return dt;
          }
          public string CheckAttandance(BL_Worker bl_worker)
          {
              string dr = dl_worker.CheckAttandance(bl_worker);
              return dr;
          }

          public void SupplierAccount(BL_Field bl_field )
        {
            dl_worker.SupplierAccount(bl_field);
        }

          public void SupplierAccountUpdate(BL_Field bl_field)
          {
              dl_worker.SupplierAccountUpdate(bl_field);
          }



          public void SupplierAccountDelete(BL_Field bl_field)
          {
              dl_worker.SupplierAccountDelete(bl_field);
          }




          //RateSlap

          public void RateSlapInsert(BL_Field bl_field)
          {
              dl_worker.RateSlapInsert(bl_field);
          }

          public void RateSlapUpdate(BL_Field bl_field)
          {
              dl_worker.RateSlapUpdate(bl_field);
          }

          public void RateSlapDelete(BL_Field bl_field)
          {
              dl_worker.RateSlapDelete(bl_field);
          }

          public DataTable SelectRateSlap()
          {
              DataTable dt = dl_worker.SelectRateSlap();
              return dt;
          }
          public string CheckRateSlap(BL_Field bl_field)
          {
             string returnvalue= dl_worker.CheckRateSlap(bl_field);
              return returnvalue;
          }

          public DataTable SelectKotDetails(string tbl)
          {
              DataTable dt = dl_worker.SelectKotDetails(tbl);
              return dt;
          }
          public void UpdateKotDetails(string OrderDetailId)
          {
              dl_worker.UpdateKotDetails(OrderDetailId);
          }

        //RateCategary

          public void RateCategaryInsert(BL_Worker bl_worker)
          {
              dl_worker.RateCategaryInsert(bl_worker);
          }

          public void RateCategaryUpdate(BL_Worker bl_worker)
          {
              dl_worker.RateCategaryUpdate(bl_worker);
          }

          public void RateCategaryDelete(BL_Worker bl_worker)
          {
              dl_worker.RateCategaryDelete(bl_worker);
          }

          public DataTable SelectRateCategary()
          {
              DataTable dt = dl_worker.SelectRateCategary();
              return dt;
          }

          public string CheckRateCategary(BL_Worker bl_worker)
          {
              string returnvalue = dl_worker.CheckRateCategary(bl_worker);
              return returnvalue;
          }

        //Department

          public void DepartmentInsert(BL_Worker bl_worker)
          {
              dl_worker.DepartmentInsert(bl_worker);
          }

          public void DepartmentUpdate(BL_Worker bl_worker)
          {
              dl_worker.DepartmentUpdate(bl_worker);
          }

          public void DepartmentDelete(BL_Worker bl_worker)
          {
              dl_worker.DepartmentDelete(bl_worker);
          }

          public DataTable SelectDepartment()
          {
              DataTable dt = dl_worker.SelectDepartment();
              return dt;
          }
          public string CheckDepartment(BL_Worker bl_worker)
          {
              string returnvalue = dl_worker.CheckDepartment(bl_worker);
              return returnvalue;
          }

          //DepartmentTable

          public void DepartmentTableInsert(BL_Worker bl_worker)
          {
              dl_worker.DepartmentTableInsert(bl_worker);
          }

          public void DepartmentTableUpdate(BL_Worker bl_worker)
          {
              dl_worker.DepartmentTableUpdate(bl_worker);
          }

          public void DepartmentTableDelete(BL_Worker bl_worker)
          {
              dl_worker.DepartmentTableDelete(bl_worker);
          }

          public DataTable SelectDepartmentTable()
          {
              DataTable dt = dl_worker.SelectDepartmentTable();
              return dt;
          }



          public string CheckDepartmenttable(BL_Worker bl_worker)
          {
              string returnvalue = dl_worker.CheckDepartmenttable(bl_worker);
              return returnvalue;
          }
          //Rate Assign


          public void RateAssignInsert(BL_Worker bl_worker)
          {
              dl_worker.RateAssignInsert(bl_worker);
          }
          public void RateAssignUpdate(BL_Worker bl_worker)
          {
              dl_worker.RateAssignUpdate(bl_worker);
          }

          public void RateAssignDelete(BL_Worker bl_worker)
          {
              dl_worker.RateAssignDelete(bl_worker);
          }


          public DataTable SelectRateAssign()
          {
              DataTable dt = dl_worker.SelectRateAssign();
              return dt;
          }


          public string CheckRateAssign(BL_Worker bl_worker)
          {
              string returnvalue = dl_worker.CheckRateAssign(bl_worker);
              return returnvalue;
          }

        //TABLE
          public DataTable SelectTable()
          {
              DataTable dt = dl_worker.SelectTable();
              return dt;
          }
          public DataTable SelectTablefrom()
          {
              DataTable dt = dl_worker.SelectTablefrom();
              return dt;
          }
          //Kot Order
          public DataTable SelectKotOrder()
          {

              DataTable dt = dl_worker.SelectKotOrder();
              return dt;
          }

        //itemquantity sum

          public DataTable SelectItemQuantity()
          {

              DataTable dt = dl_worker.SelectItemQuantity();
              return dt;
          }

          public DataTable SelectModifyTable()
          {
              DataTable dt = dl_worker.SelectModifyTable();
              return dt;
          }


          public void ModifyTable(string orderid)
          {
              dl_worker.ModifyTable(orderid);
          }

          public DataSet SelectAllAttendance(BL_Worker bl_worker)
          {
              DataSet ds = dl_worker.SelectAllAttendance(bl_worker);
              return ds;
          }

          public DataTable SelectRateSlapById(BL_Field bl_field)
          {
              DataTable dt = dl_worker.SelectRateSlapById(bl_field);
              return dt;
          }

    }
}

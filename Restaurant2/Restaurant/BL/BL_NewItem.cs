﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;


namespace Restaurant.BL
{
    class BL_NewItem
    {
        //catagory
        DL_NewItem dl_newitem = new DL_NewItem();
        public void InsertNewItem(BL_Field bl_field)
        {
            dl_newitem.InsertNewItem(bl_field);
        }


        public string CheckItemName(BL_Field bl_field)
        {
            string name = dl_newitem.CheckItemName(bl_field);
            return name;
        }

        public void UpdateNewItem(BL_Field bl_field)
        {
            dl_newitem.UpdateNewItem(bl_field);
        }
        public void DeleteNewItem(BL_Field bl_field)
        {
            dl_newitem.DeleteNewItem(bl_field);
        }
        public DataTable SelectNewItem()
        {
            DataTable dt = dl_newitem.SelectNewItem();
            return dt;
        }
        public DataTable SelectIdNewItem(BL_Field bl_field)
        {
            DataTable dt = dl_newitem.SelectIdNewItem(bl_field);
            return dt;
        }
        public DataTable SelectNewItemById(BL_Field bl_field)
        {
            DataTable dt = dl_newitem.SelectNewItemById(bl_field);
            return dt;
        }
        public DataTable SearchNewItemBySearch(BL_Field bl_field)
        {
            DataTable dt = dl_newitem.SearchNewItemBySearch(bl_field);
            return dt;
        }

        public DataTable SearchNewItemAll()
        {
            DataTable dt = dl_newitem.SearchNewItemAll();
            return dt;
        }


        //unit

        public void InsertUnit(BL_Field bl_field)
        {
            dl_newitem.InsertUnit(bl_field);
        }

        public void UpdateUnit(BL_Field bl_field)
        {
            dl_newitem.UpdateUnit(bl_field);
        }
        public void DeleteUnit(BL_Field bl_field)
        {
            dl_newitem.DeleteUnit(bl_field);
        }
        public DataTable SelectUnit()
        {
            DataTable dt = dl_newitem.SelectUnit();
            return dt;
        }
        public DataTable SelectUnitById(string unitid,string secondery)
        {
            DataTable dt = dl_newitem.SelectUnitById(unitid, secondery);
            return dt;
        }

        //group


        public void InsertGroup(BL_Field bl_field)
        {
            dl_newitem.InsertGroup(bl_field);
        }

        public void UpdateGroup(BL_Field bl_field)
        {
            dl_newitem.UpdateGroup(bl_field);
        }
        public void DeleteGroup(BL_Field bl_field)
        {
            dl_newitem.DeleteGroup(bl_field);
        }
        public DataTable SelectGroup()
        {
            DataTable dt = dl_newitem.SelectGroup();
            return dt;
        }

        //tax

        public void InsertTax(BL_Field bl_field)
        {
            dl_newitem.InsertTax(bl_field);
        }

        public void UpdateTax(BL_Field bl_field)
        {
            dl_newitem.UpdateTax(bl_field);
        }
        public void DeleteTax(BL_Field bl_field)
        {
            dl_newitem.DeleteTax(bl_field);
        }
        public DataTable SelectTax()
        {
            DataTable dt = dl_newitem.SelectTax();
            return dt;
        }

        //store location
        public void Insertstore(BL_Field bl_field)
        {
            dl_newitem.Insertstore(bl_field);
        }

        public void Updatestore(BL_Field bl_field)
        {
            dl_newitem.Updatestore(bl_field);
        }
        public void Deletestore(BL_Field bl_field)
        {
            dl_newitem.Deletestore(bl_field);
        }
        public DataTable Selectstore()
        {
            DataTable dt = dl_newitem.Selectstore();
            return dt;
        }

        //store Receipe
        public void InsertRecipe(BL_Field bl_field)
        {
            dl_newitem.InsertRecipe(bl_field);
        }

        public void UpdateRecipe(BL_Field bl_field)
        {
            dl_newitem.UpdateRecipe(bl_field);
        }
        public void DeleteRecipe(BL_Field bl_field)
        {
            dl_newitem.DeleteRecipe(bl_field);
        }
        public DataTable SelectRecipeById(BL_Field bl_field)
        {
            DataTable dt = dl_newitem.SelectRecipeById(bl_field);
            return dt;
        }
        public DataTable SelectStarRecipe(BL_Field bl_field)
        {
            DataTable dt = dl_newitem.SelectStarRecipe(bl_field);
            return dt;
        }
        public DataTable SelectRecipeName()
        {
            DataTable dt = dl_newitem.SelectRecipeName();
            return dt;
        }
        //store stock
        public void InsertStock(BL_Field bl_field)
        {
            dl_newitem.InsertStock(bl_field);
        }



        public string CheckItemId(BL_Field bl_field)
        {
            string name = dl_newitem.CheckItemId(bl_field);
            return name;
        }


        public void UpdateStock(BL_Field bl_field)
        {
            dl_newitem.UpdateStock(bl_field);
        }
        public void UpdateStockValue(BL_Field bl_field)
        {
            dl_newitem.UpdateStockValue(bl_field);
        }
        public void StockMinus(BL_Field bl_field)
        {
            dl_newitem.StockMinus(bl_field);
        }
        public void DeleteStock(BL_Field bl_field)
        {
            dl_newitem.DeleteStock(bl_field);
        }
        public DataTable SelectStock()
        {
            DataTable dt = dl_newitem.SelectStock();
            return dt;
        }
        public DataTable SelectStockById(BL_Field bl_field)
        {
            DataTable dt = dl_newitem.SelectStockById(bl_field);
            return dt;
        }

        //conversion

        public void InsertConversion(BL_Field bl_field)
        {
            dl_newitem.InsertConversion(bl_field);
        }

        public void UpdateConversion(BL_Field bl_field)
        {
            dl_newitem.UpdateConversion(bl_field);
        }
        public void DeleteConversion(BL_Field bl_field)
        {
            dl_newitem.DeleteConversion(bl_field);
        }
        public DataTable SelectConversion()
        {
            DataTable dt = dl_newitem.SelectConversion();
            return dt;
        }

        //message
        public void InsertMessage(BL_Field bl_field)
        {
            dl_newitem.InsertMessage(bl_field);
        }

        public void UpdateMessage(BL_Field bl_field)
        {
            dl_newitem.UpdateMessage(bl_field);
        }
        public void DeleteMessage(BL_Field bl_field)
        {
            dl_newitem.DeleteMessage(bl_field);
        }
        public DataTable SelectMessage()
        {
            DataTable dt = dl_newitem.SelectMessage();
            return dt;
        }
        public DataTable Select1Message()
        {
            DataTable dt = dl_newitem.Select1Message();
            return dt;
        }

        public void SelectMessage(BL_Field bl_field)
        {
            dl_newitem.SelectMessage(bl_field);
        }

        //Printer 

        public void InsertPrinter(BL_Field bl_field)
        {
            dl_newitem.InsertPrinter(bl_field);
        }

        public void UpdatePrinter(BL_Field bl_field)
        {
            dl_newitem.UpdatePrinter(bl_field);
        }

        public DataTable SelectPrinter()
        {
            DataTable dt = dl_newitem.SelectPrinter();
            return dt;
        }


        public string CheckIngradient(BL_Field bl_field)
        {
            string name = dl_newitem.CheckIngradient(bl_field);
            return name;
        }



    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data ;

namespace Restaurant.BL
{
    class BL_Catagory
    {
        
        //catagory
        DL_Catagory dl_catagory = new DL_Catagory();


        public void InsertCatagory(BL_Field bl_field)
        {
            dl_catagory.InsertCatagory(bl_field );
        }

        public DataTable SelectCatagory(BL_Field bl_field)
        {
            DataTable dt = dl_catagory.SelectCatagory(bl_field);
           return dt;
        }

        public DataTable Catagory(BL_Field bl_field)
        {
            DataTable dt = dl_catagory.Catagory(bl_field);
            return dt;
        }

        public DataTable SelectBottal(BL_Field bl_field)
        {
            DataTable dt = dl_catagory.SelectBottal(bl_field);
            return dt;
        }

        public DataTable SelectItemBySearch(BL_Field bl_field)
        {
            DataTable dt = dl_catagory.SelectItemBySearch(bl_field );
            return dt;
        }
        public DataTable SelectItemBySearchNo(BL_Field bl_field)
        {
            DataTable dt = dl_catagory.SelectItemBySearchNo(bl_field);
            return dt;
        }
        public void UpdateCatagory(BL_Field bl_field)
        {
            dl_catagory.UpdateCatagory(bl_field);
        }
        public void DeleteCatagory(BL_Field bl_field)
        {
            dl_catagory.DeleteCatagory(bl_field);
        }

        //subcatagory

         public void InsertSubCatagory(BL_Field bl_field)
        {
            dl_catagory.InsertSubCatagory(bl_field);
        }

         public DataTable SelectSubCatagory(string cataid)
        {
            DataTable dt = dl_catagory.SelectSubCatagory(cataid);
           return dt;
        }


         public DataTable SelectSubCatagorydp(BL_Field bl_field)
        {
            DataTable dt = dl_catagory.SelectSubCatagorydp(bl_field);
           return dt;
        }
         public DataTable GetSubCatagory(string cataid)
         {
             DataTable dt = dl_catagory.GetSubCatagory(cataid);
             return dt;
         }
        public void UpdateSubCatagory(BL_Field bl_field)
        {
            dl_catagory.UpdateSubCatagory(bl_field);
        }
        public void DeleteSubCatagory(BL_Field bl_field)
        {
            dl_catagory.DeleteSubCatagory(bl_field);
        }

        public string  GetIdCatagory(BL_Field bl_field)
        {
          string id=  dl_catagory.GetIdCatagory(bl_field);
          return id;
        }


        public string GetNo(BL_Field bl_field)
        {
            string id = dl_catagory.GetNo(bl_field);
            return id;
        }


        public DataTable SearchShortName(BL_Field bl_field)
        {
            DataTable dt = dl_catagory.SearchShortName(bl_field);
            return dt;
        }


        //group


        public void InsertType(BL_Field bl_field)
        {
            dl_catagory.InsertType(bl_field);
        }

        public void UpdateType(BL_Field bl_field)
        {
            dl_catagory.UpdateType(bl_field);
        }
        public void DeleteType(BL_Field bl_field)
        {
            dl_catagory.DeleteType(bl_field);
        }
        public DataTable SelectType()
        {
            DataTable dt = dl_catagory.SelectType();
            return dt;
        }

        //KotRemark
        public void InsertKotRemark(BL_Field bl_field)
        {
            dl_catagory.InsertKotRemark(bl_field);
        }

        public void UpdateKotRemark(BL_Field bl_field)
        {
            dl_catagory.UpdateKotRemark(bl_field);
        }
        public void DeleteKotRemark(BL_Field bl_field)
        {
            dl_catagory.DeleteKotRemark(bl_field);
        }
        public DataTable SelectKotRemark()
        {
            DataTable dt = dl_catagory.SelectKotRemark();
            return dt;
        }


        public DataTable SearchKotRemark(BL_Field bl_field)
        {
            DataTable dt = dl_catagory.SearchKotRemark(bl_field);
            return dt;
        }



    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;

namespace Restaurant.BL
{
    
    class BL_Shifting
    {
        DL_Shifting dl_shifting = new DL_Shifting();

        public void UpdateShifting(BL_Merge bl_merge)
        {
            dl_shifting.UpdateShifting(bl_merge);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Restaurant.DL;
using System.Data;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;

namespace Restaurant.BL
{
    class BL_ReprintKOT
    {
        DL_ReprintKOT dl_reprintkot = new DL_ReprintKOT();

        public DataTable SelectKotTable(BL_ReprintKOT bl_reprintkot)
        {
            DataTable dt = dl_reprintkot.SelectKotTable(bl_reprintkot);
            return dt;
        }
        public DataTable SelectKotDetails(String KotOrderNo)
        {
            DataTable dt = dl_reprintkot.SelectKotDetails(KotOrderNo);
            return dt;
        }

        public DataSet ReprintKot(string kotorderno)
        {
            DataSet ds = dl_reprintkot.ReprintKot(kotorderno);
            return ds;
        }

    }
}

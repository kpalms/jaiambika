﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;

namespace Restaurant.BL
{
    class BL_Serial
    {
        DL_Serial dl_serial = new DL_Serial();

        public void CreateTable()
        {
            dl_serial.CreateTable();    
        }

        public void InsertKey(BL_Field bl_field)
        {
            dl_serial.InsertKey(bl_field);
        }
        public void EnterdKey(BL_Field bl_field)
        {
            dl_serial.EnterdKey(bl_field);
        }
        public SqlDataReader SelectKey(BL_Field bl_field)
        {
           SqlDataReader dr= dl_serial.SelectKey(bl_field);
           return dr;
        }

        public void Try(BL_Field bl_field)
        {
            dl_serial.Try(bl_field);
        }

        public string SelectMaxNo()
        {
            string tbl = dl_serial.SelectMaxNo();
            return tbl;
        }

    }
}

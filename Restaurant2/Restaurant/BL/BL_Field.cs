﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.SqlClient;

namespace Restaurant.BL
{
    class BL_Field
    {

        private static string loginid;
        private string catid;
        private static string Staticcatid;
        private static string loginuser;
        
        private string catagoryname;
        private string subcatid;
        private string subcatagoryname;
        private string price;
        private string textsearch;
        //table
        private string tblid;
        private string tableno;
        private string qty;

        //sale
        private string saleId;
        private static string saleIdstatic;
        private string tableNoS;
        private static string tableNoprint;
        private string person;
        private string itemName;
        private string prices;
        private static string pricestatic;
        private int qtys;
        private static int qtystatic;
        private string dis;
        private decimal total;
        private static decimal totalstatic;
        private decimal subtotal;
        private string date;
        private string entrydate;

        private string deliverycharge;



        //RateSlap
          private int rateslapid;
          private int ratecatid;

          public int RateCatId
          {
              get { return ratecatid; }
              set { ratecatid = value; }
          }
          public int RateSlapId
        {
            get { return rateslapid; }
            set { rateslapid = value; }
        }



        private string status;
        private string no;
        private static string fromdate;
        private static string todate;
        private static int receiptno;
        //worker
        private string workerid;
        private string salary;
        private string name;
        private string flatname;
        private string middlename;
        private string lastname;
        private string phoneno;
        private string mobileno;
        private string mobileno2;
        private string mobileno3;
        private string email;
        private string  country;
        private string state;
        private string city;
        private string address;
        private string society;
        private string role;
        private static string rolelogin;
        private string username;
        private string password;
        private string shortname;
        //purchase
        private int expenseid;
        private string cost;
        private string notes;
        private int parsalno;
        private static int parsalnostatic;
        //key
        private string serialkey;
        private static string firstuse;
        private static string printtotal;
        private static string discountstatic;
        private string custid;
        private string colname;
        private string billno;
        private static string custidstatic;

        private string hotelId;
        private string kotremark;
        private decimal grandtotal;
        private decimal disamount;


        //cash book
        private string cashbookId;
        private string openingbal;
        private string sale;
        private string expencess;
        private string balence;
        private string given;
        private string totalc;
        private string closing;
        private string cashiername;
        private static string cashiernamestatic;
        
        private string kotno;
        private string kotprint;
        private string botno;
        private string resion;

        private int paidby;
        private int type;
        private string sequence;
        private static string typestatic;
        private static string headername;
        private string time;
        private string kotorderno;
        private string tablenonew;
        

        //roomno
        private string roomno;
        private string floor;
        private string roomid;
        //checkin customer

        private string checkInid;
        private string customername;
        private string rate;
        private string persons;
        private string extrapersons;
        private string extrarate;
        private string advance;
        private string dateofarr;
        private string arrtime;
        private string dateofdep;
        private string deptime;
        private string cashier;
        private string noOfdays;
        private string charges;
        private string discount;
        private string telephone;
        private string misc;
        private string ltax;
        private string ltaxrate;
        private string birthday;
        private string anniversary;
        private static string advanceIdstatic;

        private static string clientclick;

        private string typeid;

        //unit
        private string unitId;
        private string unitname;
        private string unitnameml;
        private static string unitnamestatic;
        private string balenceUnit;
        private string purchasingunit;
        private string salebyml;
        private string reorderlevel;

        private int stockid;
        private string stock;
        private int newstock;
        private string newbalanceunit;
        private string packid;

        private static string parsalchecktrue;
        private static string customerpanal;


        //purchase

        private string girno;
        private string girdate;
        private string billdate;
        private string tpno;
        private string tpdate;
        private string totalamt;
        private string tradediscount;
        private string discountper;
        private string saletaxper;
        private string saletaxamt;
        private string tcsper;
        private string tcsamt;
        private string surchageper;
        private string surchageamt;
        private string educationcessper;
        private string educationCessamt;
        private string billamt;
        private string remarks;
        //supplier
        private string supplierid;
        private string suppliername;
        private string contactperson;
        private string note;
        private int supplieraid;
        private string reciptnos;
        private string messageid;
        private string message;

        private string billprinter;
        private string kotprinter;
        private string location;

        private string orderid;



        //Vendor
        private string vendorid;
        private string vendorname;

        //group
        private string groupid;
        private string groupname;

        //tax
        private string taxid;
        private string taxdes;
        private string taxrate;

        //store location
        private string storeid;
        private string storename;
        private int recipeid;


        //stock

        private string minqty;

        //conversion
        private string primeryunit;
        private string seconderyunit;
        private string conversionunit;
        private string conversionid;

        private string starttime;
        private string itemid;
        private static string itemidstatic;
        private static string itemnamesstatic;
        private string amount;

        private int wastageid;
        private int inventory;

        private int menuid;
        private string menuname;

        private string url;
        private string userid;
        private int smsid;

        private string emailid;
        private string host;
        private string port;
        private string seat;
        private int purchaseid;
        private int purchasedetailid;
        private string dpid;
        private string purchasingrate;



        private int addkotid;
        private string addkotremark;
        private static string globaldate;


        private string hotelname = "Jai Ambika";
        private string hoteladdress = "Hadapsar Pune 411028";


        public int PurchaseId
        {
            get { return purchaseid; }
            set { purchaseid = value; }
        }

        public int PurchaseDetailsId
        {
            get { return purchasedetailid; }
            set { purchasedetailid = value; }
        } 

        public string PurchasingRate
        {
            get { return purchasingrate; }
            set { purchasingrate = value; }
        }

        public string CatId
        {
            get {return catid; }
            set { catid = value; }
        }
        public string SaticCatId
        {
            get { return Staticcatid ; }
            set { Staticcatid = value; }
        }

        public string DpId
        {
            get { return dpid; }
            set { dpid = value; }
        }
        public string LoginIdStatic
        {
            get { return loginid; }
            set { loginid = value; }
        }
        public string CatagoryName
        {
            get { return catagoryname; }
            set { catagoryname = value; }
        }

        public string SubcatId
        {
            get { return subcatid; }
            set { subcatid = value; }
        }

        public string SubcatagoryName
        {
            get { return subcatagoryname; }
            set { subcatagoryname = value; }
        }
        public string Price
        {
            get { return price; }
            set { price = value; }
        }
        public string TextSearch
        {
            get { return textsearch ; }
            set { textsearch = value; }
        }
        //table
        public string TblId
        {
            get { return tblid; }
            set { tblid = value; }
        }

        public string TableNo
        {
            get { return tableno; }
            set { tableno = value; }
        }
        public string TableNoNew
        {
            get { return tablenonew; }
            set { tablenonew = value; }
        }
        public string KotNo
        {
            get { return kotno; }
            set { kotno = value; }
        }
        public string BotNo
        {
            get { return botno; }
            set { botno = value; }
        }

        public string Qty
        {
            get { return qty ; }
            set { qty = value; }
        }

//sale

        public string Seat
        {
            get { return seat; }
            set { seat = value; }
        }

        public string SaleId
        {
            get { return saleId; }
            set { saleId = value; }
        }
        public string SaleIdStatic
        {
            get { return saleIdstatic; }
            set { saleIdstatic = value; }
        }
        public string BillNo
        {
            get { return billno; }
            set { billno = value; }
        }
        public string TableNoS
        {
            get { return tableNoS  ; }
            set { tableNoS = value; }
        }
        public string TableNoPrintBill
        {
            get { return tableNoprint; }
            set { tableNoprint = value; }
        }
        public string Person
        {
            get { return person ; }
            set { person = value; }
        }
        public string ItemName
        {
            get { return itemName ; }
            set { itemName = value; }
        }
        public string PriceS
        {
            get { return prices ; }
            set { prices = value; }
        }
        public string PriceStatic
        {
            get { return pricestatic; }
            set { pricestatic = value; }
        }
        public int QtyS
        {
            get { return qtys ; }
            set { qtys = value; }
        }

        /// <summary>
        /// ////////////////////
        /// </summary>
        public string KotRemark
        {
            get { return kotremark; }
            set { kotremark = value; }
        }



        public int QtyStatic
        {
            get { return qtystatic; }
            set { qtystatic = value; }
        }
        public string Dis
        {
            get { return dis ; }
            set { dis = value; }
        }
        public decimal Total
        {
            get { return total ; }
            set { total = value; }
        }
        public decimal TotalStatic
        {
            get { return totalstatic; }
            set { totalstatic = value; }
        }
        public decimal SubTotal
        {
            get { return subtotal; }
            set { subtotal = value; }
        }
        public string Date 
        {
            get { return date; }
            set { date  = value; }
        }
        public string DeliveryCharge
        {
            get { return deliverycharge; }
            set { deliverycharge = value; }
        }
        public string EntryDate
        {
            get { return entrydate; }
            set { entrydate = value; }
        }
        public string Time
        {
            get { return time; }
            set { time = value; }
        }
        public int ReceiptNo
        {
            get { return receiptno; }
            set { receiptno = value; }
        }
        public int ParsalNo
        {
            get { return parsalno; }
            set { parsalno = value; }
        }
        public int ParsalNoStatic
        {
            get { return parsalnostatic ; }
            set { parsalnostatic = value; }
        }
        public string Status
        {
            get { return status ; }
            set { status = value; }
        }
        public string FromDate
        {
            get { return fromdate ; }
            set { fromdate  = value; }
        }
        public string Todate
        {
            get { return todate ; }
            set { todate = value; }
        }

//worker
        public string WorkerId
        {
            get { return workerid ; }
            set { workerid  = value; }
        }
        public string Salary
        {
            get { return salary; }
            set { salary = value; }
        }
        public string Name
        {
            get { return name ; }
            set { name = value; }
        }
        public string FlatName
        {
            get { return flatname; }
            set { flatname = value; }
        }
        public string CashierName
        {
            get { return cashiername; }
            set { cashiername = value; }
        }
        public string CashierNameStatic
        {
            get { return cashiernamestatic; }
            set { cashiernamestatic = value; }
        }
        public int Type
        {
            get { return type; }
            set { type = value; }
        }
        public string Sequence
        {
            get { return sequence; }
            set { sequence = value; }
        }
        public string AddKotRemark
        {
            get { return addkotremark; }
            set { addkotremark = value; }
        }
        public int AddKotId
        {
            get { return addkotid; }
            set { addkotid = value; }
        }

        public string TypeStatic
        {
            get { return typestatic; }
            set { typestatic = value; }
        }
        public string HeaderName
        {
            get { return headername; }
            set { headername = value; }
        }
        public string TypeId
        {
            get { return typeid; }
            set { typeid = value; }
        }
        public string MiddleName
        {
            get { return middlename ; }
            set { middlename = value; }
        }

        public string LastName
        {
            get { return lastname ; }
            set { lastname = value; }
        }
        public string  PhoneNo
        {
            get { return phoneno ; }
            set { phoneno  = value; }
        }


        public string Mobile
        {
            get { return mobileno; }
            set { mobileno = value; }
        }
        public string Mobile2
        {
            get { return mobileno2; }
            set { mobileno2 = value; }
        }
        public string Mobile3
        {
            get { return mobileno3; }
            set { mobileno3 = value; }
        }
        public string Email
        {
            get { return email ; }
            set { email = value; }
        }


        public string Country
        {
            get { return country ; }
            set { country = value; }
        }
        public string State
        {
            get { return state ; }
            set { state = value; }
        }
        public string City
        {
            get { return city ; }
            set { city = value; }
        }
        public string Address
        {
            get { return address ; }
            set { address = value; }
        }
        public string Society
        {
            get { return society; }
            set { society = value; }
        }
        public string Role
        {
            get { return role ; }
            set { role = value; }
        }

        public string RoleLogin
        {
            get { return rolelogin; }
            set { rolelogin = value; }
        }

        public string Username
        {
            get { return username; }
            set { username = value; }
        }
        public string Password
        {
            get { return password; }
            set { password = value; }
        }
        public string LoginUser
        {
            get { return loginuser; }
            set { loginuser = value; }
        }
//purchase



        public string Cost
        {
            get { return cost ; }
            set { cost = value; }
        }
        public string Notes
        {
            get { return notes ; }
            set { notes = value; }
        }

        public string SerialKey
        {
            get { return serialkey ; }
            set { serialkey = value; }
        }

        public string FirstUse
        {
            get { return firstuse ; }
            set { firstuse = value; }
        }


        public string PrintTotal
        {
            get { return printtotal ; }
            set { printtotal = value; }
        }

        public string No
        {
            get { return no ; }
            set { no = value; }
        }

        public string KotOrderNo
        {
            get { return kotorderno; }
            set { kotorderno = value; }
        }
        public string ShortName
        {
            get { return shortname; }
            set { shortname = value; }
        }
        public string CustId
        {
            get { return custid ; }
            set { custid = value; }
        }

        public string CustIdStatic
        {
            get { return custidstatic; }
            set { custidstatic = value; }
        }
        public string ColName
        {
            get { return colname ; }
            set { colname = value; }
        }


        public string HotelId
        {
            get { return hotelId; }
            set { hotelId = value; }
        }

        

        public decimal GrandTotal
        {
            get { return grandtotal; }
            set { grandtotal = value; }
        }
        public string OrderId
        {
            get { return orderid; }
            set { orderid = value; }
        }
        public decimal DisAmount
        {
            get { return disamount; }
            set { disamount = value; }
        }

        public string OpeningBal
        {
            get { return openingbal; }
            set { openingbal = value; }
        }
        public string Sale
        {
            get { return sale; }
            set { sale = value; }
        }
        public string TotalC
        {
            get { return totalc; }
            set { totalc = value; }
        }
        public string Expencess
        {
            get { return expencess; }
            set { expencess = value; }
        }
        public string Balence
        {
            get { return balence; }
            set { balence = value; }
        }
        public string BalenceUnit
        {
            get { return balenceUnit; }
            set { balenceUnit = value; }
        }
        public string Given
        {
            get { return given; }
            set { given = value; }
        }
        public string Closing
        {
            get { return closing; }
            set { closing = value; }
        }

        public string CashBookId
        {
            get { return cashbookId; }
            set { cashbookId= value; }
        }

        public string Resion
        {
            get { return resion; }
            set { resion = value; }
        }

        public int PayModeId
        {
            get { return paidby; }
            set { paidby = value; }
        }
        private int memo;

        public int Memo
        {
            get { return memo; }
            set { memo = value; }
        }
        //Room No

        public string RoomId
        {
            get { return roomid; }
            set { roomid = value; }
        }
        public string RoomNo
        {
            get { return roomno; }
            set { roomno = value; }
        }
        public string Floor
        {
            get { return floor; }
            set { floor = value; }
        }


        //checkin customer
        public string CheckInId
        {
            get { return checkInid; }
            set { checkInid = value; }
        }
        public string AdvanceIdStatic
        {
            get { return advanceIdstatic; }
            set { advanceIdstatic = value; }
        }
        public string CustomerName
        {
            get { return customername; }
            set { customername = value; }
        }

        public string Rate
        {
            get { return rate; }
            set { rate = value; }
        }
        public string Persons
        {
            get { return persons; }
            set { persons = value; }
        }
        public string ExtraPersons
        {
            get { return extrapersons; }
            set { extrapersons = value; }
        }
        public string ExtraRate
        {
            get { return extrarate; }
            set { extrarate = value; }

        }
        public string Advance
        {
            get { return advance; }
            set { advance = value; }
        }
        public string Dateofarr
        {
            get { return dateofarr; }
            set { dateofarr = value; }
        }
        public string ArrTime
        {
            get { return arrtime; }
            set { arrtime = value; }
        }
        public string DateofDep
        {
            get { return dateofdep; }
            set { dateofdep = value; }
        }
        public string DepTime
        {
            get { return deptime; }
            set { deptime = value; }
        }
        public string Cashier
        {
            get { return cashier; }
            set { cashier = value; }
        }
        public string NoOfDays
        {
            get { return noOfdays; }
            set { noOfdays = value; }
        }
        public string Charges
        {
            get { return charges; }
            set { charges = value; }
        }
        public string LTax
        {
            get { return ltax; }
            set { ltax = value; }
        }
        public string LTaxRate
        {
            get { return ltaxrate; }
            set { ltaxrate = value; }
        }
        public string Discount
        {
            get { return discount; }
            set { discount = value; }
        }
        public string Telephone
        {
            get { return telephone; }
            set { telephone = value; }
        }
        public string Misc
        {
            get { return misc; }
            set { misc = value; }
        }

        public string ClientClick
        {
            get { return clientclick; }
            set { clientclick = value; }
        }


        public string UnitId
        {
            get { return unitId; }
            set { unitId = value; }
        }
        public string UnitName
        {
            get { return unitname; }
            set { unitname = value; }
        }
        public string UnitNameML
        {
            get { return unitnameml; }
            set { unitnameml = value; }
        }
        public string UnitNameStatic
        {
            get { return unitnamestatic; }
            set { unitnamestatic = value; }
        }
        public string PurchasingUnit
        {
            get { return purchasingunit; }
            set { purchasingunit = value; }
        }
        public string SaleByML
        {
            get { return salebyml; }
            set { salebyml = value; }
        }
        public string ReorderLevel
        {
            get { return reorderlevel; }
            set { reorderlevel = value; }
        }

        public int StockId
        {
            get { return stockid; }
            set { stockid = value; }
        }
        public string Stock
        {
            get { return stock; }
            set { stock = value; }
        }
        public int NewStock
        {
            get { return newstock; }
            set { newstock = value; }
        }

        public string NewBalanceUnit
        {
            get { return newbalanceunit; }
            set { newbalanceunit = value; }
        }

        public string PackId
        {
            get { return packid; }
            set { packid = value; }
        }
        public string ParsalCheckTrue
        {
            get { return parsalchecktrue; }
            set { parsalchecktrue = value; }
        }
        public string CustomerPanal
        {
            get { return customerpanal; }
            set { customerpanal = value; }
        }
        public string Anniversary
        {
            get { return anniversary; }
            set { anniversary = value; }
        }

        public string Birthday
        {
            get { return birthday; }
            set { birthday = value; }
        }


        //purchase

        public int ExpensesId
        {
            get { return expenseid; }
            set { expenseid = value; }
        }
        public string GIRNO
        {
            get { return girno; }
            set { girno = value; }
        }

        public string GIRDate
        {
            get { return girdate; }
            set { girdate = value; }
        }
        public string BillDate
        {
            get { return billdate; }
            set { billdate = value; }
        }
        public string TPNO
        {
            get { return tpno; }
            set { tpno = value; }
        }
        public string TPDate
        {
            get { return tpdate; }
            set { tpdate = value; }
        }
        public string TotalAmt
        {
            get { return totalamt; }
            set { totalamt = value; }
        }
        public string TradeDiscount
        {
            get { return tradediscount; }
            set { tradediscount = value; }
        }
        public string DiscountPer
        {
            get { return discountper; }
            set { discountper = value; }
        }
        public string DiscountStatic
        {
            get { return discountstatic; }
            set { discountstatic = value; }
        }
        public string SaleTaxPer
        {
            get { return saletaxper; }
            set { saletaxper = value; }
        }
        public string SaleTaxAmt
        {
            get { return saletaxamt; }
            set { saletaxamt = value; }
        }
        public string TCSPer
        {
            get { return tcsper; }
            set { tcsper = value; }
        }
        public string TCSAmt
        {
            get { return tcsamt; }
            set { tcsamt = value; }
        }
        public string SurchagePer
        {
            get { return surchageper; }
            set { surchageper = value; }
        }
        public string SurchageAmt
        {
            get { return surchageamt; }
            set { surchageamt = value; }
        }
        public string EducationCessPer
        {
            get { return educationcessper; }
            set { educationcessper = value; }
        }
        public string EducationCessAmt
        {
            get { return educationCessamt; }
            set { educationCessamt = value; }
        }
        public string BillAmt
        {
            get { return billamt; }
            set { billamt = value; }
        }
        public string Remarks
        {
            get { return remarks; }
            set { remarks = value; }
        }

        //supplier
        public string SupplierId
        {
            get { return supplierid; }
            set { supplierid = value; }
        }
 
         public string SupplierName
        {
            get { return suppliername; }
            set { suppliername = value; }
        }
        public string ContactPerson
        {
            get { return contactperson; }
            set { contactperson = value; }
        }



        public string ReciptNos
        {
            get { return reciptnos; }
            set { reciptnos = value; }
        }

        public int SupplierAId
        {
            get { return supplieraid; }
            set { supplieraid = value; }
        }
        

        public string Note
        {
            get { return note; }
            set { note = value; }
        }

        public string MessageId
        {
            get { return messageid; }
            set { messageid = value; }
        }
        public string Message
        {
            get { return message; }
            set { message = value; }
        }
        public string KOTPRINT
        {
            get { return kotprint; }
            set { kotprint = value; }
        }

        public string BillPrinter
        {
            get { return billprinter; }
            set { billprinter = value; }
        }
        public string KotPrinter
        {
            get { return kotprinter; }
            set { kotprinter = value; }
        }

        public string Location
        {
            get { return location; }
            set { location = value; }
        }


        public string HotelName
        {
            get { return hotelname; }
            set { hotelname = value; }
        }
        public string HotelAddress
        {
            get { return hoteladdress; }
            set { hoteladdress = value; }
        }



        //vendor
        public string VendorId
        {
            get { return vendorid; }
            set { vendorid = value; }
        }
        public string VendorName
        {
            get { return vendorname; }
            set { vendorname = value; }
        }

 

        public string GroupId
        {
            get { return groupid; }
            set { groupid = value; }
        }
        public string GroupName
        {
            get { return groupname; }
            set { groupname = value; }
        }


        public string TaxId
        {
            get { return taxid; }
            set { taxid = value; }
        }
        public string TaxDescription
        {
            get { return taxdes; }
            set { taxdes = value; }
        }
        public string TaxRate
        {
            get { return taxrate; }
            set { taxrate = value; }
        }

        //store location
        public string StoreId
        {
            get { return storeid; }
            set { storeid = value; }
        }
        public string StoreName
        {
            get { return storename; }
            set { storename = value; }
        }


        public int RecipeId
        {
            get { return recipeid; }
            set { recipeid = value; }
        }
        //stock

        public string MinQty
        {
            get { return minqty; }
            set { minqty = value; }
        }


        public string PrimeryUnit
        {
            get { return primeryunit; }
            set { primeryunit = value; }
        }
        public string SeconderyUnit
        {
            get { return seconderyunit; }
            set { seconderyunit = value; }
        }
        public string ConversionUnit
        {
            get { return conversionunit; }
            set { conversionunit = value; }
        }
        public string ConversionId
        {
            get { return conversionid; }
            set { conversionid = value; }
        }



        public string StartTime
        {
            get { return starttime; }
            set { starttime = value; }
        }

        public string ItemId
        {
            get { return itemid; }
            set { itemid = value; }
        }


        public string ItemIdStatic
        {
            get { return itemidstatic; }
            set { itemidstatic = value; }
        }


        public string ItemNamesStatic
        {
            get { return itemnamesstatic; }
            set { itemnamesstatic = value; }
        }


        public string Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        public int WastageId
        {
            get { return wastageid; }
            set { wastageid = value; }
        }
        public int Inventory
        {
            get { return inventory; }
            set { inventory = value; }
        }
//treeview

        public int MenuId
        {
            get { return menuid; }
            set { menuid = value; }
        }

        public string MenuName
        {
            get { return menuname; }
            set { menuname = value; }
        }

        public string Url
        {
            get { return url; }
            set { url = value; }
        }

        public string UserId
        {
            get { return userid; }
            set { userid = value; }
        }
        public int SmsId
        {
            get { return smsid; }
            set { smsid = value; }
        }

        //email

        public string EmailId
        {
            get { return emailid; }
            set { emailid = value; }
        }

        public string Host
        {
            get { return host; }
            set { host = value; }
        }
        public string Port
        {
            get { return port; }
            set { port = value; }
        }

        public int receiptNo
        {
            get { return receiptNo; }
            set { receiptNo = value; }
        }

        private decimal debit;
        private decimal credit;
        private int customerId;

        public decimal Debit
        {
            get { return debit; }
            set { debit = value; }
        }
        public decimal Credit
        {
            get { return credit; }
            set { credit = value; }
        }

        public int CustomerId
        {
            get { return customerId; }
            set { customerId = value; }
        }

        public string GlobalDate
        {
            get { return globaldate; }
            set { globaldate = value; }
        }

        private static string billnostatic;
        public string BillNoStatic
        {
            get { return billnostatic; }
            set { billnostatic = value; }
        }


    }//end

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;


namespace Restaurant.BL
{
    class BL_CashBook
    {


        DL_CashBook dl_cashbook = new DL_CashBook();

        public void InsertCashBook(BL_Field bl_field)
        {
            dl_cashbook.InsertCashBook(bl_field);
        }

        public DataTable SelectCashBook()
        {
            DataTable dt = dl_cashbook.SelectCashBook();
            return dt;
        }

        public void UpdateCashBook(BL_Field bl_field)
        {
            dl_cashbook.UpdateCashBook(bl_field);
        }
        //public void DeleteHotelSetting(BL_Field bl_field)
        //{
        //    dl_hotelSetting.DeleteHotelSetting(bl_field);
        //}
        public DataTable GetMaxDate(BL_Field bl_field)
        {
            DataTable dt = dl_cashbook.GetMaxDate(bl_field);
            return dt;
        }
        public string TodaySale(BL_Field bl_field)
        {
            string dt = dl_cashbook.TodaySale(bl_field);
            return dt;
        }
        public DataTable SaleByDate(BL_Field bl_field)
        {
            DataTable dt = dl_cashbook.SaleByDate(bl_field);
            return dt;
        }
        public DataTable PurchaseByDate(BL_Field bl_field)
        {
            DataTable dt = dl_cashbook.PurchaseByDate(bl_field);
            return dt;
        }
        public DataTable TodayParchase(BL_Field bl_field)
        {
            DataTable dt = dl_cashbook.TodayParchase(bl_field);
            return dt;
        }

        public string GetOpening(BL_Field bl_field)
        {
            string dt = dl_cashbook.GetOpening(bl_field);
            return dt;
        }
        public string CheckDate(BL_Field bl_field)
        {
            string dt = dl_cashbook.CheckDate(bl_field);
            return dt;
        }



   










    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;


using System.Data;
namespace Restaurant.BL
{
    class BL_Installment
    {


        DL_Installment dlInstallment = new DL_Installment();

        private string customerid;
        private string customeridstatic;
        private string invoiceno;
        private decimal amount;
        private decimal credit;
        private string paydate;
        private int receiptno;
        private string inststatus;
        private string status;
        private int instid;
        private string nextinstdate;


        public string CustomerId
        {
            get { return customerid; }
            set { customerid = value; }

        }

        public string CustomerIdStatic
        {
            get { return customeridstatic; }
            set { customeridstatic = value; }

        }

        public string InvoiceNo
        {
            get { return invoiceno; }
            set { invoiceno = value; }

        }

        public decimal Amount
        {
            get { return amount; }
            set { amount = value; }

        }
        public decimal Credit
        {
            get { return credit; }
            set { credit = value; }

        }
        public string PayDate
        {
            get { return paydate; }
            set { paydate = value; }

        }
        public int ReceiptNo
        {
            get { return receiptno; }
            set { receiptno = value; }

        }


        public string InstStatus
        {
            get { return inststatus; }
            set { inststatus = value; }

        }

        public string Status
        {
            get { return status; }
            set { status = value; }

        }

        public int InstId
        {
            get { return instid; }
            set { instid = value; }
        }


        public String NextInstDate
        {
            get { return nextinstdate; }
            set { nextinstdate = value; }
        }

        public DataTable SelectBill(BL_Customer bl_Customer)
        {
            DataTable dt = dlInstallment.SelectBill(bl_Customer);
            return dt;
        }

        public DataTable SelectOrderDetailsByBillno(BL_Installment blInstallment)
        {
            DataTable dt = dlInstallment.SelectOrderDetailsByBillno(blInstallment);
            return dt;
        }


        public void InsertInstallment(BL_Installment blInstallment)
        {
            dlInstallment.InsertInstallment(blInstallment);
        }



        public void InstallmentUpdate(BL_Installment blInstallment)
        {
            dlInstallment.InstallmentUpdate(blInstallment);
        }

        public void InstallmentDelete(BL_Installment blInstallment)
        {
            dlInstallment.InstallmentDelete(blInstallment);
        }


        public DataTable GetCustomerBalence(BL_Installment blInstallment)
        {
            DataTable dt = dlInstallment.GetCustomerBalence(blInstallment);
            return dt;
        }



        public DataTable GetSaleDetails(BL_Customer bl_Customer)
        {
            DataTable dt = dlInstallment.GetSaleDetails(bl_Customer);
            return dt;
        }

        public DataTable SelectInstallment(BL_Customer bl_Customer)
        {
            DataTable dt = dlInstallment.SelectInstallment(bl_Customer);
            return dt;
        }


        public DataTable SelectBalence()
        {
            DataTable dt = dlInstallment.SelectBalence();
            return dt;
        }


    }
}

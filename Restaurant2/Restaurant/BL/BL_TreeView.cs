﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;

namespace Restaurant.BL
{
    class BL_TreeView
    {

        //catagory
        DL_TreeView dl_treeview= new DL_TreeView();


        public void InsertTreeViewMenu(BL_Field bl_field)
        {
            dl_treeview.InsertTreeViewMenu(bl_field);
        }

        public DataTable SelectTreeViewMenu(BL_Field bl_field)
        {
            DataTable dt = dl_treeview.SelectTreeViewMenu(bl_field);
            return dt;
        }

        public void UpdateTreeViewMenu(BL_Field bl_field)
        {
            dl_treeview.UpdateTreeViewMenu(bl_field);
        }
        public void DeleteTreeViewMenu(BL_Field bl_field)
        {
            dl_treeview.DeleteTreeViewMenu(bl_field);
        }

        //subcatagory

        public void InsertTreeViewItem(BL_Field bl_field)
        {
            dl_treeview.InsertTreeViewItem(bl_field);
        }

        public DataTable SelectTreeViewItem(string cataid)
        {
            DataTable dt = dl_treeview.SelectTreeViewItem(cataid);
            return dt;
        }

        public void UpdateTreeViewItem(BL_Field bl_field)
        {
            dl_treeview.UpdateTreeViewItem(bl_field);
        }

        public void DeleteTreeViewItem(BL_Field bl_field)
        {
            dl_treeview.DeleteTreeViewItem(bl_field);
        }

        public void TreeViewItemChange(BL_Field bl_field)
        {
            dl_treeview.TreeViewItemChange(bl_field);
        }


        public DataTable SelectTreeViewItem()
        {
            DataTable dt = dl_treeview.SelectTreeViewItem();
            return dt;
        }

    }
}

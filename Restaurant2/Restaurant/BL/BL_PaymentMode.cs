﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.SqlClient;
using Restaurant.DL;
using Restaurant.BL;
using Restaurant.PL;
using System.Data;


namespace Restaurant.BL
{
    class BL_PaymentMode
    {
        private int paymodeid;
        private string paymentmode;

        
        public int PayModeId
        {
            get { return paymodeid; }
            set { paymodeid = value; }
        }

        public string PaymentMode
        {
            get { return paymentmode; }
            set { paymentmode = value; }
        }


        DL_PaymentMode dl_paymode = new DL_PaymentMode();


        public void InsertPaymentMode(BL_PaymentMode bl_paymentmode)
        {
            dl_paymode.InsertPaymentMode(bl_paymentmode);
        }

        public DataTable SelectPaymentMode()
        {
            DataTable dt = dl_paymode.SelectPaymentMode();
            return dt;
        }

        public void DeletePaymentMode(BL_PaymentMode bl_paymentmode)
        {
            dl_paymode.DeletePaymentMode(bl_paymentmode);
        }

        public void UpdatePaymentMode(BL_PaymentMode bl_paymentmode)
        {
            dl_paymode.UpdatePaymentMode(bl_paymentmode);
        }

    }
}
